FROM microsoft/aspnetcore-build:2.0

# Copy Source
COPY server app/server
COPY shared app/shared
COPY website/source/ app/website/source

# Build Website
WORKDIR app/
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get update && apt-get install -y nodejs

RUN npm update -g npm@3.x
RUN npm install -g gulp-cli@1.x

WORKDIR website/source/
RUN npm install
RUN gulp production-build

# Build Server
WORKDIR ../../server/

WORKDIR PlayGen.InterviewSkills.WebAPI/
RUN dotnet publish --framework netcoreapp2.0 --configuration Release --output out

ENTRYPOINT ["dotnet", "out/PlayGen.InterviewSkills.WebAPI.dll"]