: Copy configs
XCOPY .\configs\opentok-headless-subscriber\config.json .\opentok-headless-subscriber /Y

docker-compose -f docker-compose_2.0.yml build
docker-compose -f docker-compose_2.0.yml run --rm wait-for-db
docker-compose -f docker-compose_2.0.yml up -d interview-skills