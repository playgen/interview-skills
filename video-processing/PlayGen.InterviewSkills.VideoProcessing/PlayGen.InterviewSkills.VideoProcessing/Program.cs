﻿using OpenTok.Server;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using PlayGen.OpenTok.Server.Archive;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.VideoProcessing
{
	class Program
	{
		private static readonly OpenTok.Server.Archive.Configuration ArchiveAnalysisConfiguration =
			OpenTok.Server.Archive.Configuration.Load();

		static void Main(string[] args)
		{
			Console.WriteLine("Starting...");

			var matchId = args[0];
			var outDir = args.Length > 1 ? args[1] : "/Archives";
			var serverUrl = args.Length > 2 ? args[2] : "https://interviewskills.playgen.com";

			Console.WriteLine($"Match: {matchId}");
			Console.WriteLine($"Output Directory: {outDir}");
			Console.WriteLine($"Server: {serverUrl}");

			var videos = GetVideos(serverUrl, outDir, matchId).Result;

			Console.WriteLine($"Saved {videos.Count} videos o: {outDir}");

			var orderedUserVideos = OrderVideos(videos);

			Console.WriteLine($"Merging videos...");

			MergeVideos(orderedUserVideos);

			Console.WriteLine("Press any key to exit.");
			Console.ReadKey();
		}

		private static void MergeVideos(Dictionary<string, List<string>> orderedUserVideos)
		{
			foreach (var userAndVideos in orderedUserVideos)
			{
				var anyVideo = userAndVideos.Value.First();
				var videoDir = Path.GetDirectoryName(anyVideo);

				var lines = new List<string>();
				userAndVideos.Value.ForEach(video =>
				{
					lines.Add($"file {Path.GetFileName(video)}");
				});

				var mergeList = Path.Combine(videoDir, $"{userAndVideos.Key}_merge.txt");
				File.WriteAllLines(mergeList, lines);

				var mergedVideo = Path.Combine(videoDir, $"{userAndVideos.Key}_merged{Path.GetExtension(anyVideo)}");

				var startInfo = new ProcessStartInfo
				{
					WorkingDirectory = Path.GetFullPath(videoDir),
					FileName = "ffmpeg",
					Arguments = $"-f concat -i \"{mergeList}\" -c copy \"{mergedVideo}\""
				};

				Console.WriteLine($"Executing: {startInfo.FileName} {startInfo.Arguments}");
				Console.WriteLine($"In: {startInfo.WorkingDirectory}");

				var process = Process.Start(startInfo);

				process.WaitForExit();
				if (process.ExitCode > 0)
				{
					Console.WriteLine($"Errors merging video for: {userAndVideos.Key}");
					var errors = process.StandardError.ReadToEnd();
					Console.WriteLine(errors);
				}
				else
				{
					Console.WriteLine($"Created: {Path.Combine(videoDir, mergedVideo)}");
				}

				File.Delete(mergeList);
			}
		}

		private static Dictionary<string, List<string>> OrderVideos(List<string> videos)
		{
			var videosByUser = new Dictionary<string, List<string>>();

			videos.ForEach(video =>
			{
				var user = Path.GetFileName(video).Split("_")[0];

				if (!videosByUser.TryGetValue(user, out var userVideos))
				{
					userVideos = new List<string>();
					videosByUser.Add(user, userVideos);
				}

				userVideos.Add(video);
			});


			foreach (var userVideos in videosByUser.Values)
			{
				userVideos.Sort();
			}

			return videosByUser;
		}

		private static async Task<List<string>> GetVideos(string serverUrl, string outDir, string matchId)
		{
			var client = new System.Net.Http.HttpClient();


			var archiveMetadatas = await client.GetAsync<List<Archive>>($"{serverUrl}/api/sentimentAnalysisArchive" +
																		$"/archiveMetadatas" +
																		$"/{matchId}");
			
			var outFiles = new List<string>(archiveMetadatas?.Count ?? 0);
			var tasks = new List<Task>(archiveMetadatas?.Count ?? 0);

			archiveMetadatas?.ForEach(archiveMetadata =>
			{
				var fileRetriever = new ArchiveVideosRetriever(
					ArchiveAnalysisConfiguration.VideoExtensions,
					ArchiveAnalysisConfiguration.MetadataExtension,
					archiveMetadata.Url,
					outDir);

				Console.WriteLine($"Downloading Archive: {archiveMetadata.Id}");

				var task = fileRetriever.RetrieveFiles()
					.ContinueWith(retrieveTask =>
					{
						Console.WriteLine($"Downloaded Archive: {archiveMetadata.Id}");

						outFiles.AddRange(retrieveTask.Result);

						retrieveTask.Result.ForEach(r => Console.WriteLine($"Downloaded File: {r}"));
					});

				tasks.Add(task);
			});

			await Task.WhenAll(tasks);

			return outFiles;
		}
	}
}
