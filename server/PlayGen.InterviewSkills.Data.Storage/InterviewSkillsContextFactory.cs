﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace PlayGen.InterviewSkills.Data.Storage
{
    public class InterviewSkillsContextFactory
    {
		private readonly DbContextOptions _options;
		private readonly string _connectionString;
		private readonly ILoggerFactory _loggerFactory;

		public InterviewSkillsContextFactory(string connectionString, ILoggerFactory loggerFactory)
		{
			_connectionString = connectionString;
			_loggerFactory = loggerFactory;
			_options = ApplyOptions(new DbContextOptionsBuilder()).Options;
		}

		public InterviewSkillsContext Create()
		{
			return new InterviewSkillsContext(_options, _loggerFactory.CreateLogger<InterviewSkillsContext>());
		}

		public DbContextOptionsBuilder ApplyOptions(DbContextOptionsBuilder options)
		{
			options.UseMySql(_connectionString);
			return options;
		}
	}
}
