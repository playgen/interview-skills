﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Authentications;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class AuthenticationExtensions
    {
		public static Authentication TryAdd(this DbSet<Authentication> authentications, InterviewSkillsContext context, Authentication tryAddAuthentication)
		{
			var existingAuthentication = authentications.SingleOrDefault(a => a.AuthenticationUserId == tryAddAuthentication.AuthenticationUserId && a.AuthenticationType == tryAddAuthentication.AuthenticationType);

			if (existingAuthentication == null)
			{
				if (tryAddAuthentication.User == null && tryAddAuthentication.AuthenticationType == AuthenticationType.Guest)
				{
					var guestUser = new User();
					context.Users.Add(guestUser);

					tryAddAuthentication.User = guestUser;
					context.Authentications.Add(tryAddAuthentication);
				}

				context.Authentications.Add(tryAddAuthentication);

				return tryAddAuthentication;
			}
			else
			{
				return existingAuthentication;
			}
		}
    }
}
