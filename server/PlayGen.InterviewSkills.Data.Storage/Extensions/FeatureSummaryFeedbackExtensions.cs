﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Storage.Exceptions;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class FeatureSummaryFeedbackExtensions
    {
	    public static IQueryable<FeatureSummaryFeedback> Include(this IQueryable<FeatureSummaryFeedback> feedbacks, string[] includes)
	    {
		    foreach (var include in includes)
		    {
			    switch (include)
			    {
				    case nameof(FeatureSummaryFeedback.Feature):
					    feedbacks = feedbacks.Include(fsb => fsb.Feature);
					    break;

				    default:
					    throw new UnhandledInclude($"No case for: \"{include}\".");
			    }
		    }

		    return feedbacks;
	    }

	    public static Feature TryAddOrUpdateSummaryFeedbacks(this Feature feature, InterviewSkillsContext context,
		    params FeatureSummaryFeedback[] newOrUpdatedFeedbacks)
	    {
		    foreach (var newOrUpdatedFeedback in newOrUpdatedFeedbacks)
		    {
			    var existingFeedback = context.FeatureSummaryFeedbacks
				    .FirstOrDefault(fsb => fsb.FeatureId == feature.Id && fsb.Feedback == newOrUpdatedFeedback.Feedback);

			    if (existingFeedback != null)
			    {
				    newOrUpdatedFeedback.Id = existingFeedback.Id;
				    newOrUpdatedFeedback.FeatureId = feature.Id;
				    context.Entry(existingFeedback).CurrentValues.SetValues(newOrUpdatedFeedback);
			    }
			    else
			    {
				    newOrUpdatedFeedback.FeatureId = feature.Id;
				    context.FeatureSummaryFeedbacks.Add(newOrUpdatedFeedback);
			    }
		    }

		    return feature;
	    }
	}
}
