﻿using System;
using PlayGen.InterviewSkills.Data.Model.MatchSession;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class MatchSessionExtensions
    {
	    public static MatchSession AddUserSessionMappings(this MatchSession matchSession, InterviewSkillsContext context, params Guid[] userSessionIds)
	    {
		    foreach (var userSessionId in userSessionIds)
		    {
				context.MatchSessionUserMappings.Add(new MatchSessionUserSessionMappings
				{
					UserSessionId = userSessionId,
					MatchSession = matchSession
				});
		    }

		    return matchSession;
	    }
	}
}
