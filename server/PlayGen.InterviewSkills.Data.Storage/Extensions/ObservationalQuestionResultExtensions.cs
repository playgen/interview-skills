﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.QuestionResults;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class ObservationalQuestionResultExtensions
	{
		public static ObservationalQuestionResult AddOrUpdate(
			this DbSet<ObservationalQuestionResult> results,
			InterviewSkillsContext context,
			ObservationalQuestionResult addOrUpdateResult)
		{
			var existingResult = results.FirstOrDefault(
				oqr => oqr.QuestionId == addOrUpdateResult.QuestionId
				&& oqr.MatchSessionId == addOrUpdateResult.MatchSessionId
				&& oqr.UserSessionId == addOrUpdateResult.UserSessionId
				&& oqr.Received.EqualsWithinThreshold(addOrUpdateResult.Received));

			if (existingResult != null)
			{
				context.Entry(existingResult).CurrentValues.OverwriteWithNonNull(addOrUpdateResult);
				return existingResult;
			}
			else
			{
				results.Add(addOrUpdateResult);
				return addOrUpdateResult;
			}
		}
	}
}
