﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Storage.Exceptions;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class FeatureActionableFeedbackExtensions
    {
	    public static IQueryable<FeatureActionableFeedback> Include(this IQueryable<FeatureActionableFeedback> feedbacks, string[] includes)
	    {
		    foreach (var include in includes)
		    {
			    switch (include)
			    {
				    case nameof(FeatureActionableFeedback.Feature):
					    feedbacks = feedbacks.Include(fab => fab.Feature);
					    break;

				    default:
					    throw new UnhandledInclude($"No case for: \"{include}\".");
			    }
		    }

		    return feedbacks;
	    }

	    public static Feature TryAddOrUpdateActionableFeedbacks(this Feature feature, InterviewSkillsContext context,
		    params FeatureActionableFeedback[] newOrUpdatedFeedbacks)
	    {
		    foreach (var newOrUpdatedFeedback in newOrUpdatedFeedbacks)
		    {
			    var existingFeedback = context.FeatureActionableFeedbacks
				    .FirstOrDefault(fab => fab.FeatureId == feature.Id && fab.Feedback == newOrUpdatedFeedback.Feedback);

			    if (existingFeedback != null)
			    {
				    newOrUpdatedFeedback.Id = existingFeedback.Id;
				    newOrUpdatedFeedback.FeatureId = feature.Id;
				    context.Entry(existingFeedback).CurrentValues.SetValues(newOrUpdatedFeedback);
			    }
			    else
			    {
				    newOrUpdatedFeedback.FeatureId = feature.Id;
				    context.FeatureActionableFeedbacks.Add(newOrUpdatedFeedback);
			    }
		    }

		    return feature;
	    }
	}
}
