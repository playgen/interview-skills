﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Model.Questions.Observational;
using PlayGen.InterviewSkills.Data.Storage.Exceptions;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class ObservationalQuestionFeedbackExtensions
	{
		public static IQueryable<ObservationalQuestionFeedback> Include(this IQueryable<ObservationalQuestionFeedback> feedbacks, string[] includes)
		{
			foreach (var include in includes)
			{
				switch (include)
				{
					case nameof(ObservationalQuestionFeedback.Question):
						feedbacks = feedbacks.Include(oqf => oqf.Question)
							.ThenInclude(oq => oq.FeatureMappings)
							.ThenInclude(fm => fm.Feature);
						break;

					case nameof(ObservationalQuestion.FeatureMappings):
						feedbacks = feedbacks.Include(oqf => oqf.Question)
							.ThenInclude(oq => oq.FeatureMappings)
							.ThenInclude(fm => fm.Feature);
						break;

					default:
						throw new UnhandledInclude($"No case for: \"{include}\".");
				}
			}

			return feedbacks;
		}
	}
}
