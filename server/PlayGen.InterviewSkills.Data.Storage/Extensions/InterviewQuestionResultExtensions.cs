﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.QuestionResults;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class InterviewQuestionResultExtensions
	{
		public static InterviewQuestionResult AddOrUpdate(
			this DbSet<InterviewQuestionResult> results,
			InterviewSkillsContext context,
			InterviewQuestionResult addOrUpdateResult)
		{
			var existingResult = results.FirstOrDefault(
				iqr => iqr.QuestionId == addOrUpdateResult.QuestionId
				&& iqr.MatchSessionId == addOrUpdateResult.MatchSessionId);

			if (existingResult != null)
			{
				context.Entry(existingResult).CurrentValues.OverwriteWithNonNull(addOrUpdateResult);
				return existingResult;
			}
			else
			{
				results.Add(addOrUpdateResult);
				return addOrUpdateResult;
			}
		}
	}
}
