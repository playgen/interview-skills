﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Job;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class JobThemeExtensions
	{
		public static JobTheme GetOrAdd(this DbSet<JobTheme> jobs, JobTheme jobTheme)
		{
			return jobs.FirstOrDefault(jc => jc.Id == jobTheme.Id) ?? jobs.Add(jobTheme).Entity;
		}
	}
}
