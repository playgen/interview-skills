﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Role;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class RoleExtensions
    {
	    public static Role AddOrUpdate(this DbSet<Role> roles, InterviewSkillsContext context, Role addOrUpdateRole)
	    {
			var existingRole = roles.FirstOrDefault(r => r.Id == addOrUpdateRole.Id);

		    if (existingRole != null)
		    {
			    addOrUpdateRole.Id = existingRole.Id;
			    context.Entry(existingRole).CurrentValues.SetValues(addOrUpdateRole);
			    return existingRole;
		    }
		    else
		    {
			    roles.Add(addOrUpdateRole);
			    return addOrUpdateRole;
		    }
	    }
	}
}
