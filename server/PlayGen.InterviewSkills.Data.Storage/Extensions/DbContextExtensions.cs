﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Serilog;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class DbContextExtensions
	{
		public static bool AllMigrationsApplied(this Microsoft.EntityFrameworkCore.DbContext context)
		{
			var applied = context.GetService<IHistoryRepository>()
				.GetAppliedMigrations()
				.Select(m => m.MigrationId);

			var total = context.GetService<IMigrationsAssembly>()
				.Migrations
				.Select(m => m.Key);

			return !total.Except(applied).Any();
		}

		public static void OverwriteWithNonNull(this PropertyValues values, object overwriteWith)
		{
			var copyProperties = new Dictionary<string, object>();

			foreach (var property in overwriteWith.GetType().GetProperties())
			{
				var value = property.GetValue(overwriteWith);
				if (value == null) continue;

				if (!property.PropertyType.GetTypeInfo().IsValueType
					|| (property.PropertyType.GetTypeInfo().IsGenericType
						&& property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
				{
					copyProperties.Add(property.Name, value);
				}
			}

			values.SetValues(copyProperties);
		}
	}
}
