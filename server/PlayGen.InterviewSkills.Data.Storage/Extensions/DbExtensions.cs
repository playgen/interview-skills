﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
    public static class DbExtensions
	{
		private const int MillisecondPrecisionThreshold = 1000;

		public static bool EqualsWithinThreshold(this DateTime a, DateTime b)
		{
			return Math.Abs(a.Subtract(b).TotalMilliseconds) < MillisecondPrecisionThreshold;
		}
	}
}
