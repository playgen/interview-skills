﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.FeedbackResults;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class FeatureActionableFeedbackSystemResultExtensions
	{
		public static FeatureActionableFeedbackSystemResult AddOrUpdate(
			this DbSet<FeatureActionableFeedbackSystemResult> results,
			InterviewSkillsContext context,
			FeatureActionableFeedbackSystemResult addOrUpdateResult)
		{
			var existingResult = results.FirstOrDefault(
				fafsr => fafsr.FeedbackId == addOrUpdateResult.FeedbackId
				&& fafsr.ArchiveAnalysisResultId == addOrUpdateResult.ArchiveAnalysisResultId);

			if (existingResult != null)
			{
				context.Entry(existingResult).CurrentValues.OverwriteWithNonNull(addOrUpdateResult);
				return existingResult;
			}
			else
			{
				results.Add(addOrUpdateResult);
				return addOrUpdateResult;
			}
		}
	}
}
