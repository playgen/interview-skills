﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class TagExtensions
    {
	    public static Tag GetOrAdd(this DbSet<Tag> tags, Tag tag)
	    {
		    return tags.FirstOrDefault(f => f.Id == tag.Id) ?? tags.Add(tag).Entity;
	    }
    }
}
