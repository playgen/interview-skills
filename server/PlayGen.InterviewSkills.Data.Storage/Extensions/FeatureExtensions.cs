﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class FeatureExtensions
	{
		public static Feature AddOrUpdate(this DbSet<Feature> features, InterviewSkillsContext context, Feature addOrUpdateFeature)
		{
			var existingFeature = features.FirstOrDefault(f => f.Id == addOrUpdateFeature.Id);

			if (existingFeature != null)
			{
				addOrUpdateFeature.Id = existingFeature.Id;
				context.Entry(existingFeature).CurrentValues.SetValues(addOrUpdateFeature);
				return existingFeature;
			}
			else
			{
				features.Add(addOrUpdateFeature);
				return addOrUpdateFeature;
			}
		}
	}
}
