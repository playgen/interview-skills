﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Model.Job;
using PlayGen.InterviewSkills.Data.Storage.Exceptions;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class JobExtensions
    {
	    public static Job AddOrUpdate(this DbSet<Job> jobs, InterviewSkillsContext context, Job addOrUpdateJob)
	    {
			var existingJob = jobs.FirstOrDefault(j => j.Id == addOrUpdateJob.Id);

		    if (existingJob != null)
		    {
			    addOrUpdateJob.Id = existingJob.Id;
			    addOrUpdateJob.ThemeId = addOrUpdateJob.Theme?.Id ?? existingJob.ThemeId;
			    context.Entry(existingJob).CurrentValues.SetValues(addOrUpdateJob);
			    return existingJob;
		    }
		    else
		    {
			    jobs.Add(addOrUpdateJob);
			    return addOrUpdateJob;
		    }
	    }

	    public static Job TryAddTagMappings(this Job job, InterviewSkillsContext context, params Tag[] tags)
	    {
		    foreach (var tag in tags)
		    {
			    if (!context.JobTagMappings.Any(jtm => jtm.JobId == job.Id && jtm.TagId == tag.Id))
			    {
				    context.JobTagMappings.Add(new JobTagMapping
				    {
					    JobId = job.Id,
					    TagId = tag.Id
				    });
			    }
		    }

		    return job;
	    }

	    public static IQueryable<Job> Include(this IQueryable<Job> jobs, string[] includes)
	    {
			if (includes?.Length > 0)
			{
				foreach (var include in includes)
				{
					switch (include)
					{
						case nameof(Job.Theme):
							jobs = jobs.Include(j => j.Theme);
							break;

						case nameof(Job.TagMappings):
							jobs = jobs.Include(j => j.TagMappings)
								.ThenInclude(mapping => mapping.Tag);
							;
							break;

						default:
							throw new UnhandledInclude($"No case for: \"{include}\".");
					}
				}
			}

			return jobs;
	    }
	}
}
