﻿using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Authentications;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class UserExtensions
    {
	    public static User AddOrUpdate(this DbSet<User> users, InterviewSkillsContext context, User addOrUpdateUser)
	    {
		    var existingUser = users.Find(addOrUpdateUser.Id);

		    if (existingUser != null)
		    {
			    addOrUpdateUser.Id = existingUser.Id;
			    context.Entry(existingUser).CurrentValues.SetValues(addOrUpdateUser);
			    return existingUser;
		    }
		    else
		    {
			    users.Add(addOrUpdateUser);
			    return addOrUpdateUser;
		    }
	    }

	    public static User TryAddOrUpdateAuthentications(this User user, InterviewSkillsContext context, params Authentication[] addOrUpdateAuthentications)
	    {
		    foreach (var addOrUpdateAuthentication in addOrUpdateAuthentications)
		    {
			    var existingAuthentication =
				    context.Authentications.Find(addOrUpdateAuthentication.SerializedAuthenticationType,
					    addOrUpdateAuthentication.AuthenticationUserId);

			    if (existingAuthentication != null)
			    {
				    addOrUpdateAuthentication.AuthenticationType = existingAuthentication.AuthenticationType;
				    addOrUpdateAuthentication.AuthenticationUserId = addOrUpdateAuthentication.AuthenticationUserId;
				    addOrUpdateAuthentication.User = user;
				    context.Entry(existingAuthentication).CurrentValues.SetValues(addOrUpdateAuthentication);
			    }
			    else
			    {
				    addOrUpdateAuthentication.User = user;
				    context.Authentications.Add(addOrUpdateAuthentication);
			    }
		    }

		    return user;
	    }
	}
}
