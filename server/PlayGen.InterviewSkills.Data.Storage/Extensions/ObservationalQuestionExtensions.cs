﻿using PlayGen.InterviewSkills.Data.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Model.Questions.Observational;
using PlayGen.InterviewSkills.Data.Storage.Exceptions;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class ObservationalQuestionExtensions
    {
	    public static ObservationalQuestion AddOrUpdate(this DbSet<ObservationalQuestion> questions, InterviewSkillsContext context, ObservationalQuestion addOrUpdateQuestion)
	    {
		    var existingQuestion = questions.SingleOrDefault(oq => oq.Question == addOrUpdateQuestion.Question);

		    if (existingQuestion != null)
		    {
			    addOrUpdateQuestion.Id = existingQuestion.Id;
			    context.Entry(existingQuestion).CurrentValues.SetValues(addOrUpdateQuestion);
		    }
		    else
		    {
			    questions.Add(addOrUpdateQuestion);
		    }

		    return addOrUpdateQuestion;
	    }

		public static ObservationalQuestion TryAddFeatureMappings(this ObservationalQuestion question, InterviewSkillsContext context, params Feature[] features)
	    {
		    foreach (var feature in features)
		    {
			    if (!context.ObservationalQuestionFeatureMappings.Any(oqfm => oqfm.QuestionId == question.Id && oqfm.FeatureId == feature.Id))
			    {
				    context.ObservationalQuestionFeatureMappings.Add(new ObservationalQuestionFeatureMapping
				    {
					    QuestionId = question.Id,
					    FeatureId = feature.Id
				    });
			    }
		    }

		    return question;
	    }

	    public static ObservationalQuestion TryAddTagMappings(this ObservationalQuestion question, InterviewSkillsContext context, params Tag[] tags)
	    {
		    foreach (var tag in tags)
		    {
			    if (!context.ObservationalQuestionTagMappings.Any(oqtm => oqtm.QuestionId == question.Id && oqtm.TagId == tag.Id))
			    {
				    context.ObservationalQuestionTagMappings.Add(new ObservationalQuestionTagMapping
				    {
					    QuestionId = question.Id,
					    TagId = tag.Id
				    });
			    }
		    }

		    return question;
	    }

		public static ObservationalQuestion TryAddOrUpdateFeedbacks(this ObservationalQuestion question, InterviewSkillsContext context, params ObservationalQuestionFeedback[] addOrUpdateFeedbacks)
	    {
			foreach (var addOrUpdateFeedback in addOrUpdateFeedbacks)
			{
				var existingFeedback =
					context.ObservationalQuestionFeedbacks.FirstOrDefault(
						oqf => oqf.Feedback == addOrUpdateFeedback.Feedback && oqf.QuestionId == question.Id);

				if (existingFeedback != null)
				{
					addOrUpdateFeedback.Id = existingFeedback.Id;
					addOrUpdateFeedback.QuestionId = question.Id;
					context.Entry(existingFeedback).CurrentValues.SetValues(addOrUpdateFeedback);
				}
				else
				{
					addOrUpdateFeedback.QuestionId = question.Id;
					context.ObservationalQuestionFeedbacks.Add(addOrUpdateFeedback);
				}
		    }

		    return question;
	    }

	    public static IQueryable<ObservationalQuestion> Include(this IQueryable<ObservationalQuestion> questions, string[] includes)
	    {
		    foreach (var include in includes)
		    {
			    switch (include)
			    {
				    case nameof(ObservationalQuestion.TagMappings):
					    questions = questions.Include(oq => oq.TagMappings)
						    .ThenInclude(mapping => mapping.Tag);
					    break;

				    case nameof(ObservationalQuestion.FeatureMappings):
					    questions = questions.Include(oq => oq.FeatureMappings)
						    .ThenInclude(mapping => mapping.Feature);
					    break;

				    case nameof(ObservationalQuestion.JobMappings):
					    questions = questions.Include(oq => oq.JobMappings)
						    .ThenInclude(mapping => mapping.Job);
					    break;

				    default:
					    throw new UnhandledInclude($"No case for: \"{include}\".");
			    }
		    }

		    return questions;
	    }
	}
}
