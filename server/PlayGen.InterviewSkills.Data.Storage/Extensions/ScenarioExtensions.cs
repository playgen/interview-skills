﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class ScenarioExtensions
	{
		private static JsonSerializerSettings JsonSerializerSettings
		{
			get
			{
				var jsonSerializerSettings = new JsonSerializerSettings
				{
					TypeNameHandling = TypeNameHandling.Objects
				};
				jsonSerializerSettings.Converters.Add(new StringEnumConverter { CamelCaseText = true });

				return jsonSerializerSettings;
			}
		}

		public static Model.Scenario ToModel(this Scenario.Model.Scenario scenario)
		{
			return new Model.Scenario
			{
				Category = scenario.Category,
				Name = scenario.Name,
				IsSynchronised = scenario.IsSynchronised,
				Steps = JsonConvert.SerializeObject(scenario.Steps, JsonSerializerSettings)
			};
		}

		public static Model.Scenario AddOrUpdate(this DbSet<Model.Scenario> scenarios, InterviewSkillsContext context, Model.Scenario addOrUpdateScenario)
		{
			var existingScenario = scenarios.FirstOrDefault(s =>
				s.Category == addOrUpdateScenario.Category
				&& s.Name == addOrUpdateScenario.Name
				&& s.IsSynchronised == addOrUpdateScenario.IsSynchronised);

			if (existingScenario != null)
			{
				addOrUpdateScenario.Id = existingScenario.Id;
				context.Entry(existingScenario).CurrentValues.SetValues(addOrUpdateScenario);
				return existingScenario;
			}
			else
			{
				scenarios.Add(addOrUpdateScenario);
				return addOrUpdateScenario;
			}
		}
	}
}
