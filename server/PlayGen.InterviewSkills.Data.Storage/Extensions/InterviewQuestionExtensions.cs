﻿using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Model.Questions.Interview;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Storage.Exceptions;

namespace PlayGen.InterviewSkills.Data.Storage.Extensions
{
	public static class InterviewQuestionExtensions
    {
	    public static InterviewQuestion AddOrUpdate(this DbSet<InterviewQuestion> questions, InterviewSkillsContext context, InterviewQuestion addOrUpdateQuestion)
	    {
		    var existingQuestion = questions.SingleOrDefault(iq => iq.Question == addOrUpdateQuestion.Question);

		    if (existingQuestion != null)
		    {
			    addOrUpdateQuestion.Id = existingQuestion.Id;
			    context.Entry(existingQuestion).CurrentValues.SetValues(addOrUpdateQuestion);
			    return existingQuestion;
			}
		    else
		    {
			    questions.Add(addOrUpdateQuestion);
			    return addOrUpdateQuestion;
			}
	    }

	    public static InterviewQuestion TryAddFeatureMappings(this InterviewQuestion question, InterviewSkillsContext context, params Feature[] features)
		{
			foreach (var feature in features)
			{
				if (!context.InterviewQuestionFeatureMappings.Any(iqfm => iqfm.QuestionId == question.Id && iqfm.FeatureId == feature.Id))
				{
					context.InterviewQuestionFeatureMappings.Add(new InterviewQuestionFeatureMapping
					{
						QuestionId = question.Id,
						FeatureId = feature.Id
					});
				}
			}

			return question;
		}

	    public static InterviewQuestion TryAddTagMappings(this InterviewQuestion question, InterviewSkillsContext context, params Tag[] tags)
		{
			foreach (var tag in tags)
			{
				if (!context.InterviewQuestionTagMappings.Any(iqtm => iqtm.QuestionId == question.Id && iqtm.TagId == tag.Id))
				{
					context.InterviewQuestionTagMappings.Add(new InterviewQuestionTagMapping
					{
						QuestionId = question.Id,
						TagId = tag.Id
					});
				}
			}

			return question;
		}

	    public static IQueryable<InterviewQuestion> Include(this IQueryable<InterviewQuestion> questions, string[] includes)
	    {
		    foreach (var include in includes)
		    {
			    switch (include)
			    {
				    case nameof(InterviewQuestion.TagMappings):
					    questions = questions.Include(iq => iq.TagMappings)
						    .ThenInclude(mapping => mapping.Tag); ;
					    break;

				    case nameof(InterviewQuestion.FeatureMappings):
					    questions = questions.Include(iq => iq.FeatureMappings)
							.ThenInclude(mapping => mapping.Feature);
					    break;

				    case nameof(InterviewQuestion.JobMappings):
					    questions = questions.Include(iq => iq.JobMappings)
						    .ThenInclude(mapping => mapping.Job);
					    break;

					default:
					    throw new UnhandledInclude($"No case for: \"{include}\".");
			    }
		    }

		    return questions;
	    }
	}
}
