﻿namespace PlayGen.InterviewSkills.Data.Storage.Exceptions
{
	public class UnhandledInclude : StorageException
	{
		public UnhandledInclude(string message) : base(message)
		{ 
		}
	}
}
