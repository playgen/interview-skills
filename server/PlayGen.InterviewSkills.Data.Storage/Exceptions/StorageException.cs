﻿using System;

namespace PlayGen.InterviewSkills.Data.Storage.Exceptions
{
	public abstract class StorageException : Exception
    {
        protected StorageException()
        {
        }

        protected StorageException(string message) : base(message)
        {
        }

        protected StorageException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
