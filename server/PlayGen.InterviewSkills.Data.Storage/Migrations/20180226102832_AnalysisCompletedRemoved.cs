﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PlayGen.InterviewSkills.Data.Storage.Migrations
{
    public partial class AnalysisCompletedRemoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnalysisCompleted",
                table: "AnlysMtadtas");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AnalysisCompleted",
                table: "AnlysMtadtas",
                nullable: true);
        }
    }
}
