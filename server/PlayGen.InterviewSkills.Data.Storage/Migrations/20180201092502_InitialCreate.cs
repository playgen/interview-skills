﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PlayGen.InterviewSkills.Data.Storage.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ftrs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "VARCHAR(100)", nullable: false),
                    IdealWeightMax = table.Column<float>(nullable: false),
                    IdealWeightMin = table.Column<float>(nullable: false),
                    Inverse = table.Column<string>(type: "VARCHAR(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ftrs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IntrvQstns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Duration = table.Column<int>(nullable: false),
                    Question = table.Column<string>(type: "VARCHAR(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntrvQstns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobThms",
                columns: table => new
                {
                    Id = table.Column<string>(type: "VARCHAR(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobThms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MtchSsn",
                columns: table => new
                {
                    Id = table.Column<string>(type: "VARCHAR(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MtchSsn", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ObsrvQstns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EndLabel = table.Column<string>(type: "VARCHAR(36)", nullable: false),
                    IsBinary = table.Column<bool>(nullable: false),
                    IsInverse = table.Column<bool>(nullable: false),
                    Question = table.Column<string>(type: "VARCHAR(100)", nullable: false),
                    StartLabel = table.Column<string>(type: "VARCHAR(36)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObsrvQstns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "VARCHAR(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Scenarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Category = table.Column<string>(type: "VARCHAR(36)", nullable: false),
                    IsSynchronised = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(36)", nullable: false),
                    Steps = table.Column<string>(type: "LONGTEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Scenarios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<string>(type: "VARCHAR(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    EmailAddress = table.Column<string>(type: "VARCHAR(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FtrActnFdbks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FeatureId = table.Column<string>(nullable: false),
                    Feedback = table.Column<string>(type: "VARCHAR(100)", nullable: false),
                    WeightFromIdeal = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FtrActnFdbks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FtrActnFdbks_Ftrs_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Ftrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FtrSmryFdbks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FeatureId = table.Column<string>(nullable: false),
                    Feedback = table.Column<string>(type: "VARCHAR(200)", nullable: false),
                    WeightFromIdeal = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FtrSmryFdbks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FtrSmryFdbks_Ftrs_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Ftrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IntrvQstnFtrMpns",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false),
                    FeatureId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntrvQstnFtrMpns", x => new { x.QuestionId, x.FeatureId });
                    table.ForeignKey(
                        name: "FK_IntrvQstnFtrMpns_Ftrs_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Ftrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IntrvQstnFtrMpns_IntrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "IntrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Jobs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "VARCHAR(100)", nullable: false),
                    ThemeId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Jobs_JobThms_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "JobThms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IntrvQstnRslts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Answered = table.Column<DateTime>(nullable: true),
                    Answering = table.Column<DateTime>(nullable: true),
                    Asking = table.Column<DateTime>(nullable: true),
                    Hidden = table.Column<DateTime>(nullable: true),
                    MatchSessionId = table.Column<string>(nullable: false),
                    QuestionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntrvQstnRslts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IntrvQstnRslts_MtchSsn_MatchSessionId",
                        column: x => x.MatchSessionId,
                        principalTable: "MtchSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IntrvQstnRslts_IntrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "IntrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ObsrvQstnFdbks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Feedback = table.Column<string>(type: "VARCHAR(100)", nullable: false),
                    QuestionId = table.Column<int>(nullable: false),
                    WeightFromIdeal = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObsrvQstnFdbks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ObsrvQstnFdbks_ObsrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "ObsrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ObsrvQstnFtrMpns",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false),
                    FeatureId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObsrvQstnFtrMpns", x => new { x.QuestionId, x.FeatureId });
                    table.ForeignKey(
                        name: "FK_ObsrvQstnFtrMpns_Ftrs_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Ftrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObsrvQstnFtrMpns_ObsrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "ObsrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IntrvQstnTagMpns",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false),
                    TagId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntrvQstnTagMpns", x => new { x.QuestionId, x.TagId });
                    table.ForeignKey(
                        name: "FK_IntrvQstnTagMpns_IntrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "IntrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IntrvQstnTagMpns_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ObsrvQstnTagMpns",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false),
                    TagId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObsrvQstnTagMpns", x => new { x.QuestionId, x.TagId });
                    table.ForeignKey(
                        name: "FK_ObsrvQstnTagMpns_ObsrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "ObsrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObsrvQstnTagMpns_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Auths",
                columns: table => new
                {
                    AuthType = table.Column<string>(type: "VARCHAR(36)", nullable: false),
                    AuthUsrId = table.Column<string>(type: "VARCHAR(36)", nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auths", x => new { x.AuthType, x.AuthUsrId });
                    table.ForeignKey(
                        name: "FK_Auths_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UsrSsn",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsrSsn", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsrSsn_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IntrvQstnJobMpns",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false),
                    JobId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntrvQstnJobMpns", x => new { x.QuestionId, x.JobId });
                    table.ForeignKey(
                        name: "FK_IntrvQstnJobMpns_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IntrvQstnJobMpns_IntrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "IntrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobFtrMpns",
                columns: table => new
                {
                    JobId = table.Column<string>(nullable: false),
                    FeatureId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobFtrMpns", x => new { x.JobId, x.FeatureId });
                    table.ForeignKey(
                        name: "FK_JobFtrMpns_Ftrs_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Ftrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobFtrMpns_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobTagMpns",
                columns: table => new
                {
                    JobId = table.Column<string>(nullable: false),
                    TagId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobTagMpns", x => new { x.JobId, x.TagId });
                    table.ForeignKey(
                        name: "FK_JobTagMpns_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobTagMpns_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ObsrvQstnJobMpns",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false),
                    JobId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObsrvQstnJobMpns", x => new { x.QuestionId, x.JobId });
                    table.ForeignKey(
                        name: "FK_ObsrvQstnJobMpns_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObsrvQstnJobMpns_ObsrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "ObsrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AnlysMtadtas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AnalysisCompleted = table.Column<DateTime>(nullable: true),
                    AnalysisStarted = table.Column<DateTime>(nullable: true),
                    MatchSessionId = table.Column<string>(nullable: false),
                    RecordingEnded = table.Column<DateTime>(nullable: true),
                    RecordingStarted = table.Column<DateTime>(nullable: true),
                    SerializedErrors = table.Column<string>(type: "LONGTEXT", nullable: true),
                    UserSessionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnlysMtadtas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnlysMtadtas_MtchSsn_MatchSessionId",
                        column: x => x.MatchSessionId,
                        principalTable: "MtchSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AnlysMtadtas_UsrSsn_UserSessionId",
                        column: x => x.UserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CstmFdbkUsrMsgs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    IssuerUserSessionId = table.Column<Guid>(nullable: false),
                    MatchSessionId = table.Column<string>(nullable: false),
                    Message = table.Column<string>(type: "VARCHAR(100)", nullable: false),
                    RecipientUserSessionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CstmFdbkUsrMsgs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CstmFdbkUsrMsgs_UsrSsn_IssuerUserSessionId",
                        column: x => x.IssuerUserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CstmFdbkUsrMsgs_MtchSsn_MatchSessionId",
                        column: x => x.MatchSessionId,
                        principalTable: "MtchSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CstmFdbkUsrMsgs_UsrSsn_RecipientUserSessionId",
                        column: x => x.RecipientUserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FtrSmryFdbkStmRstls",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    FeedbackId = table.Column<int>(nullable: false),
                    MatchSessionId = table.Column<string>(nullable: false),
                    UserSessionId = table.Column<Guid>(nullable: false),
                    WeightFromIdeal = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FtrSmryFdbkStmRstls", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FtrSmryFdbkStmRstls_FtrSmryFdbks_FeedbackId",
                        column: x => x.FeedbackId,
                        principalTable: "FtrSmryFdbks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FtrSmryFdbkStmRstls_MtchSsn_MatchSessionId",
                        column: x => x.MatchSessionId,
                        principalTable: "MtchSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FtrSmryFdbkStmRstls_UsrSsn_UserSessionId",
                        column: x => x.UserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MtchSsnUsrMpns",
                columns: table => new
                {
                    MatchSessionId = table.Column<string>(nullable: false),
                    UserSessionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MtchSsnUsrMpns", x => new { x.MatchSessionId, x.UserSessionId });
                    table.ForeignKey(
                        name: "FK_MtchSsnUsrMpns_MtchSsn_MatchSessionId",
                        column: x => x.MatchSessionId,
                        principalTable: "MtchSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MtchSsnUsrMpns_UsrSsn_UserSessionId",
                        column: x => x.UserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ObsrvFdbkUsrRslts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    FeedbackId = table.Column<int>(nullable: false),
                    IssuerUserSessionId = table.Column<Guid>(nullable: false),
                    MatchSessionId = table.Column<string>(nullable: false),
                    RecipientUserSessionId = table.Column<Guid>(nullable: false),
                    Weight = table.Column<float>(nullable: false),
                    WeightFromIdeal = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObsrvFdbkUsrRslts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ObsrvFdbkUsrRslts_ObsrvQstnFdbks_FeedbackId",
                        column: x => x.FeedbackId,
                        principalTable: "ObsrvQstnFdbks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObsrvFdbkUsrRslts_UsrSsn_IssuerUserSessionId",
                        column: x => x.IssuerUserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObsrvFdbkUsrRslts_MtchSsn_MatchSessionId",
                        column: x => x.MatchSessionId,
                        principalTable: "MtchSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObsrvFdbkUsrRslts_UsrSsn_RecipientUserSessionId",
                        column: x => x.RecipientUserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ObsrvQstnRslts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MatchSessionId = table.Column<string>(nullable: false),
                    QuestionId = table.Column<int>(nullable: false),
                    Received = table.Column<DateTime>(nullable: false),
                    Shown = table.Column<DateTime>(nullable: true),
                    Submitted = table.Column<DateTime>(nullable: true),
                    UserSessionId = table.Column<Guid>(nullable: false),
                    Weight = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObsrvQstnRslts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ObsrvQstnRslts_MtchSsn_MatchSessionId",
                        column: x => x.MatchSessionId,
                        principalTable: "MtchSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObsrvQstnRslts_ObsrvQstns_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "ObsrvQstns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObsrvQstnRslts_UsrSsn_UserSessionId",
                        column: x => x.UserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RolSsns",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Ended = table.Column<DateTime>(nullable: true),
                    JobId = table.Column<string>(nullable: false),
                    MatchSessionId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false),
                    UserSessionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolSsns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RolSsns_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolSsns_MtchSsn_MatchSessionId",
                        column: x => x.MatchSessionId,
                        principalTable: "MtchSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolSsns_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolSsns_UsrSsn_UserSessionId",
                        column: x => x.UserSessionId,
                        principalTable: "UsrSsn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AnlysFtrRslts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AnlysMtadtaId = table.Column<int>(nullable: false),
                    Classifier1 = table.Column<float>(nullable: false),
                    Classifier2 = table.Column<float>(nullable: false),
                    Classifier3 = table.Column<float>(nullable: true),
                    FtrId = table.Column<string>(nullable: false),
                    Prediction = table.Column<int>(nullable: false),
                    Weight = table.Column<float>(nullable: false),
                    WeightFromIdeal = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnlysFtrRslts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnlysFtrRslts_AnlysMtadtas_AnlysMtadtaId",
                        column: x => x.AnlysMtadtaId,
                        principalTable: "AnlysMtadtas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AnlysFtrRslts_Ftrs_FtrId",
                        column: x => x.FtrId,
                        principalTable: "Ftrs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FtrActnFdbkStmRstls",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Acknowledged = table.Column<DateTime>(nullable: true),
                    ArchiveAnalysisResultId = table.Column<int>(nullable: false),
                    FeedbackId = table.Column<int>(nullable: false),
                    Received = table.Column<DateTime>(nullable: false),
                    Shown = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FtrActnFdbkStmRstls", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FtrActnFdbkStmRstls_AnlysFtrRslts_ArchiveAnalysisResultId",
                        column: x => x.ArchiveAnalysisResultId,
                        principalTable: "AnlysFtrRslts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FtrActnFdbkStmRstls_FtrActnFdbks_FeedbackId",
                        column: x => x.FeedbackId,
                        principalTable: "FtrActnFdbks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AnlysFtrRslts_FtrId",
                table: "AnlysFtrRslts",
                column: "FtrId");

            migrationBuilder.CreateIndex(
                name: "IX_AnlysFtrRslts_AnlysMtadtaId_FtrId",
                table: "AnlysFtrRslts",
                columns: new[] { "AnlysMtadtaId", "FtrId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AnlysMtadtas_MatchSessionId",
                table: "AnlysMtadtas",
                column: "MatchSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_AnlysMtadtas_UserSessionId_RecordingEnded",
                table: "AnlysMtadtas",
                columns: new[] { "UserSessionId", "RecordingEnded" });

            migrationBuilder.CreateIndex(
                name: "IX_AnlysMtadtas_UserSessionId_RecordingStarted",
                table: "AnlysMtadtas",
                columns: new[] { "UserSessionId", "RecordingStarted" });

            migrationBuilder.CreateIndex(
                name: "IX_AnlysMtadtas_UserSessionId_RecordingStarted_RecordingEnded",
                table: "AnlysMtadtas",
                columns: new[] { "UserSessionId", "RecordingStarted", "RecordingEnded" });

            migrationBuilder.CreateIndex(
                name: "IX_Auths_UserId",
                table: "Auths",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CstmFdbkUsrMsgs_IssuerUserSessionId",
                table: "CstmFdbkUsrMsgs",
                column: "IssuerUserSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_CstmFdbkUsrMsgs_MatchSessionId",
                table: "CstmFdbkUsrMsgs",
                column: "MatchSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_CstmFdbkUsrMsgs_RcpntSsn_IssurSsn_MtchSsnId_Crtd_Msg",
                table: "CstmFdbkUsrMsgs",
                columns: new[] { "RecipientUserSessionId", "IssuerUserSessionId", "MatchSessionId", "Created", "Message" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FtrActnFdbks_FeatureId_Feedback",
                table: "FtrActnFdbks",
                columns: new[] { "FeatureId", "Feedback" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FtrActnFdbks_FeatureId_Feedback_WeightFromIdeal",
                table: "FtrActnFdbks",
                columns: new[] { "FeatureId", "Feedback", "WeightFromIdeal" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FtrActnFdbkStmRstls_FeedbackId",
                table: "FtrActnFdbkStmRstls",
                column: "FeedbackId");

            migrationBuilder.CreateIndex(
                name: "IX_FtrActnFdbkStmRstls_AnlysRslt_Fdbk",
                table: "FtrActnFdbkStmRstls",
                columns: new[] { "ArchiveAnalysisResultId", "FeedbackId" });

            migrationBuilder.CreateIndex(
                name: "IX_FtrActnFdbkStmRstls_AnlysRslt_Fdbk_Rcvd",
                table: "FtrActnFdbkStmRstls",
                columns: new[] { "ArchiveAnalysisResultId", "FeedbackId", "Received" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ftrs_Inverse",
                table: "Ftrs",
                column: "Inverse");

            migrationBuilder.CreateIndex(
                name: "IX_FtrSmryFdbks_FeatureId_Feedback",
                table: "FtrSmryFdbks",
                columns: new[] { "FeatureId", "Feedback" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FtrSmryFdbkStmRstls_FeedbackId",
                table: "FtrSmryFdbkStmRstls",
                column: "FeedbackId");

            migrationBuilder.CreateIndex(
                name: "IX_FtrSmryFdbkStmRstls_MatchSessionId",
                table: "FtrSmryFdbkStmRstls",
                column: "MatchSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_FtrSmryFdbkStmRstls_UsrSssn_MtchSsn_Crtd_Fdbk",
                table: "FtrSmryFdbkStmRstls",
                columns: new[] { "UserSessionId", "MatchSessionId", "Created", "FeedbackId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_IntrvQstnFtrMpns_FeatureId",
                table: "IntrvQstnFtrMpns",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_IntrvQstnJobMpns_JobId",
                table: "IntrvQstnJobMpns",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_IntrvQstnRslts_MatchSessionId",
                table: "IntrvQstnRslts",
                column: "MatchSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_IntrvQstnRslts_QuestionId_MatchSessionId",
                table: "IntrvQstnRslts",
                columns: new[] { "QuestionId", "MatchSessionId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_IntrvQstns_Question",
                table: "IntrvQstns",
                column: "Question",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_IntrvQstnTagMpns_TagId",
                table: "IntrvQstnTagMpns",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_JobFtrMpns_FeatureId",
                table: "JobFtrMpns",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ThemeId",
                table: "Jobs",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_JobTagMpns_TagId",
                table: "JobTagMpns",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_MtchSsnUsrMpns_UserSessionId",
                table: "MtchSsnUsrMpns",
                column: "UserSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvFdbkUsrRslts_FeedbackId",
                table: "ObsrvFdbkUsrRslts",
                column: "FeedbackId");

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvFdbkUsrRslts_IssuerUserSessionId",
                table: "ObsrvFdbkUsrRslts",
                column: "IssuerUserSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvFdbkUsrRslts_MatchSessionId",
                table: "ObsrvFdbkUsrRslts",
                column: "MatchSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvFdbkUsrRslts_RcpntSsn_IsrSsn_MtchSsn_Crtd_Fdbk",
                table: "ObsrvFdbkUsrRslts",
                columns: new[] { "RecipientUserSessionId", "IssuerUserSessionId", "MatchSessionId", "Created", "FeedbackId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstnFdbks_QuestionId_Feedback",
                table: "ObsrvQstnFdbks",
                columns: new[] { "QuestionId", "Feedback" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstnFdbks_QuestionId_Feedback_WeightFromIdeal",
                table: "ObsrvQstnFdbks",
                columns: new[] { "QuestionId", "Feedback", "WeightFromIdeal" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstnFtrMpns_FeatureId",
                table: "ObsrvQstnFtrMpns",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstnJobMpns_JobId",
                table: "ObsrvQstnJobMpns",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstnRslts_MatchSessionId",
                table: "ObsrvQstnRslts",
                column: "MatchSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstnRslts_UserSessionId",
                table: "ObsrvQstnRslts",
                column: "UserSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstnRslts_Qstn_MtchSsn_UsrSsn_Rcvd",
                table: "ObsrvQstnRslts",
                columns: new[] { "QuestionId", "MatchSessionId", "UserSessionId", "Received" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstns_Question",
                table: "ObsrvQstns",
                column: "Question",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ObsrvQstnTagMpns_TagId",
                table: "ObsrvQstnTagMpns",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_RolSsns_JobId",
                table: "RolSsns",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_RolSsns_MatchSessionId",
                table: "RolSsns",
                column: "MatchSessionId");

            migrationBuilder.CreateIndex(
                name: "IX_RolSsns_RoleId",
                table: "RolSsns",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_RolSsns_UsrSsn_Rol",
                table: "RolSsns",
                columns: new[] { "UserSessionId", "RoleId" });

            migrationBuilder.CreateIndex(
                name: "IX_RolSsns_UsrSsn_MtchSsn_Rol",
                table: "RolSsns",
                columns: new[] { "UserSessionId", "MatchSessionId", "RoleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Scenarios_Category_Name_IsSynchronised",
                table: "Scenarios",
                columns: new[] { "Category", "Name", "IsSynchronised" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UsrSsn_UserId",
                table: "UsrSsn",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Auths");

            migrationBuilder.DropTable(
                name: "CstmFdbkUsrMsgs");

            migrationBuilder.DropTable(
                name: "FtrActnFdbkStmRstls");

            migrationBuilder.DropTable(
                name: "FtrSmryFdbkStmRstls");

            migrationBuilder.DropTable(
                name: "IntrvQstnFtrMpns");

            migrationBuilder.DropTable(
                name: "IntrvQstnJobMpns");

            migrationBuilder.DropTable(
                name: "IntrvQstnRslts");

            migrationBuilder.DropTable(
                name: "IntrvQstnTagMpns");

            migrationBuilder.DropTable(
                name: "JobFtrMpns");

            migrationBuilder.DropTable(
                name: "JobTagMpns");

            migrationBuilder.DropTable(
                name: "MtchSsnUsrMpns");

            migrationBuilder.DropTable(
                name: "ObsrvFdbkUsrRslts");

            migrationBuilder.DropTable(
                name: "ObsrvQstnFtrMpns");

            migrationBuilder.DropTable(
                name: "ObsrvQstnJobMpns");

            migrationBuilder.DropTable(
                name: "ObsrvQstnRslts");

            migrationBuilder.DropTable(
                name: "ObsrvQstnTagMpns");

            migrationBuilder.DropTable(
                name: "RolSsns");

            migrationBuilder.DropTable(
                name: "Scenarios");

            migrationBuilder.DropTable(
                name: "AnlysFtrRslts");

            migrationBuilder.DropTable(
                name: "FtrActnFdbks");

            migrationBuilder.DropTable(
                name: "FtrSmryFdbks");

            migrationBuilder.DropTable(
                name: "IntrvQstns");

            migrationBuilder.DropTable(
                name: "ObsrvQstnFdbks");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Jobs");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "AnlysMtadtas");

            migrationBuilder.DropTable(
                name: "Ftrs");

            migrationBuilder.DropTable(
                name: "ObsrvQstns");

            migrationBuilder.DropTable(
                name: "JobThms");

            migrationBuilder.DropTable(
                name: "MtchSsn");

            migrationBuilder.DropTable(
                name: "UsrSsn");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
