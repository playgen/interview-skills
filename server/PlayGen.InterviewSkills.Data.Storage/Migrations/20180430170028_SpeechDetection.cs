﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PlayGen.InterviewSkills.Data.Storage.Migrations
{
    public partial class SpeechDetection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSpeechDetected",
                table: "AnlysMtadtas",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<float>(
                name: "ProbabilitySpeechDetected",
                table: "AnlysMtadtas",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSpeechDetected",
                table: "AnlysMtadtas");

            migrationBuilder.DropColumn(
                name: "ProbabilitySpeechDetected",
                table: "AnlysMtadtas");
        }
    }
}
