﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PlayGen.InterviewSkills.Data.Storage.Migrations
{
    public partial class AnalysisStartedRemoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnalysisStarted",
                table: "AnlysMtadtas");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AnalysisStarted",
                table: "AnlysMtadtas",
                nullable: true);
        }
    }
}
