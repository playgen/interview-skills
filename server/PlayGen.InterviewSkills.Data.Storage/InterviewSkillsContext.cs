﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Logging;
using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Model.Analysis;
using PlayGen.InterviewSkills.Data.Model.Authentications;
using PlayGen.InterviewSkills.Data.Model.FeedbackResults;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Model.Job;
using PlayGen.InterviewSkills.Data.Model.MatchSession;
using PlayGen.InterviewSkills.Data.Model.QuestionResults;
using PlayGen.InterviewSkills.Data.Model.Questions.Interview;
using PlayGen.InterviewSkills.Data.Model.Questions.Observational;
using PlayGen.InterviewSkills.Data.Model.Role;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Storage
{
	public class InterviewSkillsContext : DbContext
	{
		private readonly ILogger<InterviewSkillsContext> _logger;

		// Analysis
		public DbSet<AnalysisMetadata> AnalysesMetadatas { get; set; }
		public DbSet<AnalysisFeatureResult> AnalysisResults { get; set; }

		// Authentications
		public DbSet<Authentication> Authentications { get; set; }

		// Feedback Results
		public DbSet<CustomFeedbackUserMessage> CustomFeedbackUserMessages { get; set; }
		public DbSet<FeatureActionableFeedbackSystemResult> FeatureActionableFeedbackSystemResults { get; set; }
		public DbSet<FeatureSummaryFeedbackSystemResult> FeatureSummaryFeedbackSystemResults { get; set; }
		public DbSet<ObservationalFeedbackUserResult> ObservationalFeedbackUserResults { get; set; }

		// Feedbacks
		public DbSet<FeatureActionableFeedback> FeatureActionableFeedbacks { get; set; }
		public DbSet<FeatureSummaryFeedback> FeatureSummaryFeedbacks { get; set; }
		public DbSet<ObservationalQuestionFeedback> ObservationalQuestionFeedbacks { get; set; }

		// Job
		public DbSet<Job> Jobs { get; set; }
		public DbSet<JobTheme> JobThemes { get; set; }
		public DbSet<JobFeatureMapping> JobFeatureMappings { get; set; }
		public DbSet<JobTagMapping> JobTagMappings { get; set; }

		// Match Session
		public DbSet<MatchSession> MatchSessions { get; set; }
		public DbSet<MatchSessionUserSessionMappings> MatchSessionUserMappings { get; set; }

		// QuestionResults
		public DbSet<InterviewQuestionResult> InterviewQuesitonResults { get; set; }
		public DbSet<ObservationalQuestionResult> ObservationalQuesitonResults { get; set; }

		// Questions.Interview
		public DbSet<InterviewQuestion> InterviewQuestions { get; set; }
		public DbSet<InterviewQuestionFeatureMapping> InterviewQuestionFeatureMappings { get; set; }
		public DbSet<InterviewQuestionJobMapping> InterviewQuestionJobMappings { get; set; }
		public DbSet<InterviewQuestionTagMapping> InterviewQuestionTagMappings { get; set; }

		// Question.Observational
		public DbSet<ObservationalQuestion> ObservationalQuestions { get; set; }
		public DbSet<ObservationalQuestionFeatureMapping> ObservationalQuestionFeatureMappings { get; set; }
		public DbSet<ObservationalQuestionJobMapping> ObservationalQuestionJobMappings { get; set; }
		public DbSet<ObservationalQuestionTagMapping> ObservationalQuestionTagMappings { get; set; }

		// Role
		public DbSet<Role> Roles { get; set; }
		public DbSet<RoleSession> RoleSessions { get; set; }

		// Others
		public DbSet<Feature> Features { get; set; }
		public DbSet<Model.Scenario> Scenarios { get; set; }
		public DbSet<Tag> Tags { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<UserSession> UserSessions { get; set; }

		private const string TextIdColumnType = "VARCHAR(100)";
		private const string ShortMessageColumnType = "VARCHAR(100)";
		private const string MediumMessageColumnType = "VARCHAR(200)";
		private const string ShortStringColumnType = "VARCHAR(36)";
		private const string LongStringColumnType = "LONGTEXT";
		private const string EmailAddressColumnType = "VARCHAR(255)";

		public InterviewSkillsContext(DbContextOptions options, ILogger<InterviewSkillsContext> logger) : base(options)
		{
			_logger = logger;
		}
		public int LogErrorSaveChanges()
		{
			try
			{
				return SaveChanges();
			}
			catch (Exception error)
			{
				_logger.LogError(error, $"Saving changes to the {nameof(InterviewSkillsContext)} {nameof(DbContext)}.");
				throw;
			}
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			// Analysis
			Configure(modelBuilder.Entity<AnalysisMetadata>());
			Configure(modelBuilder.Entity<AnalysisFeatureResult>());

			// Authentication
			Configure(modelBuilder.Entity<Authentication>());

			// Feedback Results
			Configure(modelBuilder.Entity<CustomFeedbackUserMessage>());
			Configure(modelBuilder.Entity<FeatureActionableFeedbackSystemResult>());
			Configure(modelBuilder.Entity<FeatureSummaryFeedbackSystemResult>());
			Configure(modelBuilder.Entity<ObservationalFeedbackUserResult>());

			// Feedbacks
			Configure(modelBuilder.Entity<FeatureActionableFeedback>());
			Configure(modelBuilder.Entity<FeatureSummaryFeedback>());
			Configure(modelBuilder.Entity<ObservationalQuestionFeedback>());

			// Job
			Configure(modelBuilder.Entity<Job>());
			Configure(modelBuilder.Entity<JobTheme>());
			Configure(modelBuilder.Entity<JobFeatureMapping>());
			Configure(modelBuilder.Entity<JobTagMapping>());

			// MatchSession
			Configure(modelBuilder.Entity<MatchSession>());
			Configure(modelBuilder.Entity<MatchSessionUserSessionMappings>());

			// QuestionResults
			Configure(modelBuilder.Entity<InterviewQuestionResult>());
			Configure(modelBuilder.Entity<ObservationalQuestionResult>());

			// Questions.Interview
			Configure(modelBuilder.Entity<InterviewQuestion>());
			Configure(modelBuilder.Entity<InterviewQuestionFeatureMapping>());
			Configure(modelBuilder.Entity<InterviewQuestionJobMapping>());
			Configure(modelBuilder.Entity<InterviewQuestionTagMapping>());

			// Questions.Observational
			Configure(modelBuilder.Entity<ObservationalQuestion>());
			Configure(modelBuilder.Entity<ObservationalQuestionFeatureMapping>());
			Configure(modelBuilder.Entity<ObservationalQuestionJobMapping>());
			Configure(modelBuilder.Entity<ObservationalQuestionTagMapping>());

			// Role
			Configure(modelBuilder.Entity<Role>());
			Configure(modelBuilder.Entity<RoleSession>());

			// Others
			Configure(modelBuilder.Entity<Feature>());
			Configure(modelBuilder.Entity<Model.Scenario>());
			Configure(modelBuilder.Entity<Tag>());
			Configure(modelBuilder.Entity<User>());
			Configure(modelBuilder.Entity<UserSession>());
		}

		private void Configure(EntityTypeBuilder<UserSession> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("UsrSsn");

			entityTypeBuilder.Property(us => us.Created)
				.IsRequired();

			entityTypeBuilder.HasOne(us => us.User)
				.WithMany(u => u.Sessions)
				.HasForeignKey(us => us.UserId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<MatchSessionUserSessionMappings> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("MtchSsnUsrMpns");

			entityTypeBuilder.HasKey(msum => new { msum.MatchSessionId, msum.UserSessionId });

			entityTypeBuilder.HasOne(msum => msum.MatchSession)
				.WithMany(ms => ms.UserSessionMappings)
				.HasForeignKey(msum => msum.MatchSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(msum => msum.UserSession)
				.WithMany(u => u.MatchSessionMappings)
				.HasForeignKey(msum => msum.UserSessionId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<Model.Scenario> entityTypeBuilder)
		{
			entityTypeBuilder.Property(s => s.Category)
				.HasColumnType(ShortStringColumnType)
				.IsRequired();

			entityTypeBuilder.Property(s => s.Name)
				.HasColumnType(ShortStringColumnType)
				.IsRequired();

			entityTypeBuilder.Property(s => s.Steps)
				.HasColumnType(LongStringColumnType)
				.IsRequired();

			entityTypeBuilder.HasIndex(s => new { s.Category, s.Name, s.IsSynchronised })
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<AnalysisMetadata> entityTypeBuilder)
		{
			const string tableName = "AnlysMtadtas";
			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.HasOne(am => am.MatchSession)
				.WithMany()
				.HasForeignKey(am => am.MatchSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(am => am.UserSession)
				.WithMany(us => us.AnalysisMetadatas)
				.HasForeignKey(am => am.UserSessionId)
				.IsRequired();

			entityTypeBuilder.Ignore(am => am.Errors);

			entityTypeBuilder.Property(a => a.SerializedErrors)
				.HasColumnType(LongStringColumnType);

			entityTypeBuilder.HasIndex(am => new { am.UserSessionId, am.RecordingStarted });

			entityTypeBuilder.HasIndex(am => new { am.UserSessionId, am.RecordingEnded });

			entityTypeBuilder.HasIndex(am => new { am.UserSessionId, am.RecordingStarted, am.RecordingEnded });
		}

		private void Configure(EntityTypeBuilder<User> entityTypeBuilder)
		{
			entityTypeBuilder.Property(u => u.EmailAddress)
				.HasColumnType(EmailAddressColumnType);
		}

		private void Configure(EntityTypeBuilder<Tag> entityTypeBuilder)
		{
			entityTypeBuilder.Property(t => t.Id)
				.HasColumnType(TextIdColumnType);
		}

		private void Configure(EntityTypeBuilder<MatchSession> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("MtchSsn");

			entityTypeBuilder.Property(ms => ms.Id)
				.HasColumnType(TextIdColumnType);
		}

		private void Configure(EntityTypeBuilder<RoleSession> entityTypeBuilder)
		{
			const string tableName = "RolSsns";
			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.HasOne(rs => rs.MatchSession)
				.WithMany(ms => ms.RoleSessions)
				.HasForeignKey(rs => rs.MatchSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(rs => rs.UserSession)
				.WithMany(us => us.RoleSessions)
				.HasForeignKey(rs => rs.UserSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(rs => rs.Role)
				.WithMany()
				.HasForeignKey(rs => rs.RoleId)
				.IsRequired();

			entityTypeBuilder.HasOne(rs => rs.Job)
				.WithMany()
				.HasForeignKey(rs => rs.JobId)
				.IsRequired();

			entityTypeBuilder.Property(rs => rs.Created)
				.IsRequired();

			entityTypeBuilder.HasIndex(rs => new { rs.UserSessionId, rs.RoleId })
				.HasName($"IX_{tableName}_UsrSsn_Rol");

			entityTypeBuilder.HasIndex(rs => new { rs.UserSessionId, rs.MatchSessionId, rs.RoleId })
				.HasName($"IX_{tableName}_UsrSsn_MtchSsn_Rol")
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<Role> entityTypeBuilder)
		{
			entityTypeBuilder.Property(r => r.Id)
				.HasColumnType(TextIdColumnType);
		}

		private void Configure(EntityTypeBuilder<Feature> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("Ftrs");

			entityTypeBuilder.Property(f => f.Id)
				.HasColumnType(TextIdColumnType);

			entityTypeBuilder.Property(f => f.Inverse)
				.HasColumnType(TextIdColumnType);

			entityTypeBuilder.HasIndex(f => f.Inverse);
		}

		private void Configure(EntityTypeBuilder<AnalysisFeatureResult> entityTypeBuilder)
		{
			const string tableName = "AnlysFtrRslts";
			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.Property(aar => aar.AnalysisMetadataId)
				.HasColumnName("AnlysMtadtaId")
				.IsRequired();

			entityTypeBuilder.Property(aar => aar.FeatureId)
				.HasColumnName("FtrId");

			entityTypeBuilder.HasOne(aar => aar.AnalysisMetadata)
				.WithMany(aam => aam.FeatureResults)
				.HasForeignKey(aar => aar.AnalysisMetadataId)
				.IsRequired();

			entityTypeBuilder.HasOne(aar => aar.Feature)
				.WithMany()
				.HasForeignKey(aar => aar.FeatureId)
				.IsRequired();

			entityTypeBuilder.Property(aar => aar.Prediction)
				.IsRequired();

			entityTypeBuilder.Property(aar => aar.Classifier1)
				.IsRequired();

			entityTypeBuilder.Property(aar => aar.Classifier2)
				.IsRequired();

			entityTypeBuilder.HasIndex(aar => new { aar.AnalysisMetadataId, aar.FeatureId })
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<ObservationalFeedbackUserResult> entityTypeBuilder)
		{
			const string tableName = "ObsrvFdbkUsrRslts";
			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.HasOne(ofur => ofur.RecipientUserSession)
				.WithMany(u => u.ObservationalFeedbackUserResults)
				.HasForeignKey(ofur => ofur.RecipientUserSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(ofur => ofur.IssuerUserSession)
				.WithMany()
				.HasForeignKey(ofur => ofur.IssuerUserSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(ofur => ofur.MatchSession)
				.WithMany()
				.HasForeignKey(ofur => ofur.MatchSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(ofur => ofur.Feedback)
				.WithMany()
				.HasForeignKey(ofur => ofur.FeedbackId)
				.IsRequired();

			entityTypeBuilder.Property(ofur => ofur.Created)
				.IsRequired();

			entityTypeBuilder.Property(ofur => ofur.Weight)
				.IsRequired();

			entityTypeBuilder.Property(ofur => ofur.WeightFromIdeal)
				.IsRequired();

			entityTypeBuilder.HasIndex(ofur => new { ofur.RecipientUserSessionId, ofur.IssuerUserSessionId, ofur.MatchSessionId, ofur.Created, ofur.FeedbackId })
				.HasName($"IX_{tableName}_RcpntSsn_IsrSsn_MtchSsn_Crtd_Fdbk")
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<FeatureActionableFeedbackSystemResult> entityTypeBuilder)
		{
			const string tableName = "FtrActnFdbkStmRstls";
			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.HasOne(fafsr => fafsr.AnalysisFeatureResult)
				.WithMany()
				.HasForeignKey(fafsr => fafsr.ArchiveAnalysisResultId)
				.IsRequired();

			entityTypeBuilder.HasOne(fafsr => fafsr.Feedback)
				.WithMany()
				.HasForeignKey(fafsr => fafsr.FeedbackId)
				.IsRequired();

			entityTypeBuilder.Property(fafsr => fafsr.Received)
				.IsRequired();

			entityTypeBuilder.HasIndex(fafsr => new { fafsr.ArchiveAnalysisResultId, fafsr.FeedbackId })
				.HasName($"IX_{tableName}_AnlysRslt_Fdbk");

			entityTypeBuilder.HasIndex(fafsr => new { fafsr.ArchiveAnalysisResultId, fafsr.FeedbackId, fafsr.Received })
				.HasName($"IX_{tableName}_AnlysRslt_Fdbk_Rcvd")
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<FeatureSummaryFeedbackSystemResult> entityTypeBuilder)
		{
			const string tableName = "FtrSmryFdbkStmRstls";
			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.HasOne(fsfsr => fsfsr.UserSession)
				.WithMany(u => u.FeatureSummaryFeedbackSystemResults)
				.HasForeignKey(fsfsr => fsfsr.UserSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(fsfsr => fsfsr.MatchSession)
				.WithMany()
				.HasForeignKey(fsfsr => fsfsr.MatchSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(fsfsr => fsfsr.Feedback)
				.WithMany()
				.HasForeignKey(fsfsr => fsfsr.FeedbackId)
				.IsRequired();

			entityTypeBuilder.Property(fsfsr => fsfsr.Created)
				.IsRequired();

			entityTypeBuilder.Property(fsfsr => fsfsr.WeightFromIdeal)
				.IsRequired();

			entityTypeBuilder.HasIndex(fsfsr => new { fsfsr.UserSessionId, fsfsr.MatchSessionId, fsfsr.Created, fsfsr.FeedbackId })
				.HasName($"IX_{tableName}_UsrSssn_MtchSsn_Crtd_Fdbk")
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<CustomFeedbackUserMessage> entityTypeBuilder)
		{
			const string tableName = "CstmFdbkUsrMsgs";
			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.HasOne(cfum => cfum.RecipientUserSession)
				.WithMany(u => u.CustomFeedbackUserMessages)
				.HasForeignKey(cfum => cfum.RecipientUserSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(cfum => cfum.IssuerUserSession)
				.WithMany()
				.HasForeignKey(cfum => cfum.IssuerUserSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(cfum => cfum.MatchSession)
				.WithMany()
				.HasForeignKey(cfum => cfum.MatchSessionId)
				.IsRequired();

			entityTypeBuilder.Property(cfum => cfum.Created)
				.IsRequired();

			entityTypeBuilder.Property(cfum => cfum.Message)
				.HasColumnType(ShortMessageColumnType)
				.IsRequired();

			entityTypeBuilder.HasIndex(cfum => new { cfum.RecipientUserSessionId, IssuerId = cfum.IssuerUserSessionId, cfum.MatchSessionId, cfum.Created, cfum.Message })
				.HasName($"IX_{tableName}_RcpntSsn_IssurSsn_MtchSsnId_Crtd_Msg")
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<ObservationalQuestionTagMapping> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("ObsrvQstnTagMpns");

			entityTypeBuilder.HasKey(oqtm => new { oqtm.QuestionId, oqtm.TagId });

			entityTypeBuilder.HasOne(oqtm => oqtm.Question)
				.WithMany(oq => oq.TagMappings)
				.HasForeignKey(iqtm => iqtm.QuestionId)
				.IsRequired();

			entityTypeBuilder.HasOne(oqtm => oqtm.Tag)
				.WithMany()
				.HasForeignKey(oqtm => oqtm.TagId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<ObservationalQuestionJobMapping> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("ObsrvQstnJobMpns");

			entityTypeBuilder.HasKey(oqjm => new { oqjm.QuestionId, oqjm.JobId });

			entityTypeBuilder.HasOne(oqjm => oqjm.Question)
				.WithMany(oq => oq.JobMappings)
				.HasForeignKey(oqjm => oqjm.QuestionId)
				.IsRequired();

			entityTypeBuilder.HasOne(oqjm => oqjm.Job)
				.WithMany()
				.HasForeignKey(oqjm => oqjm.JobId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<ObservationalQuestionFeatureMapping> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("ObsrvQstnFtrMpns");

			entityTypeBuilder.HasKey(oqfm => new { oqfm.QuestionId, oqfm.FeatureId });

			entityTypeBuilder.HasOne(oqfm => oqfm.Question)
				.WithMany(oq => oq.FeatureMappings)
				.HasForeignKey(oqfm => oqfm.QuestionId)
				.IsRequired();

			entityTypeBuilder.HasOne(oqfm => oqfm.Feature)
				.WithMany()
				.HasForeignKey(oqfm => oqfm.FeatureId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<ObservationalQuestion> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("ObsrvQstns");

			entityTypeBuilder.Property(oq => oq.Question)
				.HasColumnType(ShortMessageColumnType)
				.IsRequired();

			entityTypeBuilder.HasIndex(oq => oq.Question)
				.IsUnique();

			entityTypeBuilder.Property(oq => oq.StartLabel)
				.HasColumnType(ShortStringColumnType)
				.IsRequired();

			entityTypeBuilder.Property(oq => oq.EndLabel)
				.HasColumnType(ShortStringColumnType)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<InterviewQuestionTagMapping> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("IntrvQstnTagMpns");

			entityTypeBuilder.HasKey(iqtm => new { iqtm.QuestionId, iqtm.TagId });

			entityTypeBuilder.HasOne(iqtm => iqtm.Question)
				.WithMany(iq => iq.TagMappings)
				.HasForeignKey(iqtm => iqtm.QuestionId)
				.IsRequired();

			entityTypeBuilder.HasOne(iqtm => iqtm.Tag)
				.WithMany()
				.HasForeignKey(iqtm => iqtm.TagId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<InterviewQuestionJobMapping> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("IntrvQstnJobMpns");

			entityTypeBuilder.HasKey(iqjm => new { iqjm.QuestionId, iqjm.JobId });

			entityTypeBuilder.HasOne(iqjm => iqjm.Question)
				.WithMany(iq => iq.JobMappings)
				.HasForeignKey(iqjm => iqjm.QuestionId)
				.IsRequired();

			entityTypeBuilder.HasOne(iqjm => iqjm.Job)
				.WithMany()
				.HasForeignKey(iqjm => iqjm.JobId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<InterviewQuestionFeatureMapping> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("IntrvQstnFtrMpns");

			entityTypeBuilder.HasKey(iqfm => new { iqfm.QuestionId, iqfm.FeatureId });

			entityTypeBuilder.HasOne(iqfm => iqfm.Question)
				.WithMany(iq => iq.FeatureMappings)
				.HasForeignKey(iqfm => iqfm.QuestionId)
				.IsRequired();

			entityTypeBuilder.HasOne(iqfm => iqfm.Feature)
				.WithMany()
				.HasForeignKey(iqfm => iqfm.FeatureId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<InterviewQuestionResult> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("IntrvQstnRslts");

			entityTypeBuilder.HasOne(iqr => iqr.Question)
				.WithMany()
				.HasForeignKey(iqr => iqr.QuestionId)
				.IsRequired();

			entityTypeBuilder.HasOne(iqr => iqr.MatchSession)
				.WithMany()
				.HasForeignKey(iqr => iqr.MatchSessionId)
				.IsRequired();

			entityTypeBuilder
				.HasIndex(iqr => new { iqr.QuestionId, iqr.MatchSessionId })
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<ObservationalQuestionResult> entityTypeBuilder)
		{
			const string tableName = "ObsrvQstnRslts";

			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.HasOne(oqr => oqr.Question)
				.WithMany()
				.HasForeignKey(oqr => oqr.QuestionId)
				.IsRequired();

			entityTypeBuilder.HasOne(oqr => oqr.MatchSession)
				.WithMany()
				.HasForeignKey(oqr => oqr.MatchSessionId)
				.IsRequired();

			entityTypeBuilder.HasOne(oqr => oqr.UserSession)
				.WithMany()
				.HasForeignKey(oqr => oqr.UserSessionId)
				.IsRequired();

			entityTypeBuilder.Property(oqr => oqr.Received)
				.IsRequired();

			entityTypeBuilder
				.HasIndex(oqr => new { oqr.QuestionId, oqr.MatchSessionId, oqr.UserSessionId, oqr.Received })
				.HasName($"IX_{tableName}_Qstn_MtchSsn_UsrSsn_Rcvd")
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<InterviewQuestion> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("IntrvQstns");

			entityTypeBuilder.Property(iq => iq.Question)
				.HasColumnType(ShortMessageColumnType)
				.IsRequired();

			entityTypeBuilder.HasIndex(iq => iq.Question)
				.IsUnique();

			entityTypeBuilder.Property(iq => iq.Duration)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<JobTagMapping> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("JobTagMpns");

			entityTypeBuilder.HasKey(jtm => new { jtm.JobId, jtm.TagId });

			entityTypeBuilder.HasOne(jtm => jtm.Job)
				.WithMany(j => j.TagMappings)
				.HasForeignKey(jtm => jtm.JobId)
				.IsRequired();

			entityTypeBuilder.HasOne(jtm => jtm.Tag)
				.WithMany()
				.HasForeignKey(jtm => jtm.TagId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<JobFeatureMapping> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("JobFtrMpns");

			entityTypeBuilder.HasKey(jfm => new { jfm.JobId, jfm.FeatureId });

			entityTypeBuilder.HasOne(jfm => jfm.Job)
				.WithMany(j => j.FeatureMappings)
				.HasForeignKey(jfm => jfm.JobId)
				.IsRequired();

			entityTypeBuilder.HasOne(jfm => jfm.Feature)
				.WithMany()
				.HasForeignKey(jfm => jfm.FeatureId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<JobTheme> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("JobThms");

			entityTypeBuilder.Property(jt => jt.Id)
				.HasColumnType(TextIdColumnType);
		}

		private void Configure(EntityTypeBuilder<Job> entityTypeBuilder)
		{
			entityTypeBuilder.Property(j => j.Id)
				.HasColumnType(TextIdColumnType);

			entityTypeBuilder.HasOne(j => j.Theme)
				.WithMany()
				.HasForeignKey(j => j.ThemeId)
				.IsRequired();
		}

		private void Configure(EntityTypeBuilder<ObservationalQuestionFeedback> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("ObsrvQstnFdbks");

			entityTypeBuilder.HasOne(oqf => oqf.Question)
				.WithMany()
				.HasForeignKey(oqf => oqf.QuestionId)
				.IsRequired();

			entityTypeBuilder.Property(oqf => oqf.Feedback)
				.HasColumnType(ShortMessageColumnType)
				.IsRequired();

			entityTypeBuilder.Property(oqf => oqf.WeightFromIdeal)
				.IsRequired();

			entityTypeBuilder.HasIndex(oqf => new { oqf.QuestionId, oqf.Feedback })
				.IsUnique();

			entityTypeBuilder.HasIndex(oqf => new { oqf.QuestionId, oqf.Feedback, oqf.WeightFromIdeal })
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<FeatureActionableFeedback> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("FtrActnFdbks");

			entityTypeBuilder.HasOne(fab => fab.Feature)
				.WithMany()
				.HasForeignKey(fab => fab.FeatureId)
				.IsRequired();

			entityTypeBuilder.Property(fab => fab.Feedback)
				.HasColumnType(ShortMessageColumnType)
				.IsRequired();

			entityTypeBuilder.Property(fab => fab.WeightFromIdeal)
				.IsRequired();

			entityTypeBuilder.HasIndex(fab => new { fab.FeatureId, fab.Feedback })
				.IsUnique();

			entityTypeBuilder.HasIndex(fab => new { fab.FeatureId, fab.Feedback, fab.WeightFromIdeal })
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<FeatureSummaryFeedback> entityTypeBuilder)
		{
			entityTypeBuilder.ToTable("FtrSmryFdbks");

			entityTypeBuilder.HasOne(fsb => fsb.Feature)
				.WithMany()
				.HasForeignKey(fsb => fsb.FeatureId)
				.IsRequired();

			entityTypeBuilder.Property(fsb => fsb.Feedback)
				.HasColumnType(MediumMessageColumnType)
				.IsRequired();

			entityTypeBuilder.HasIndex(fsb => new { fsb.FeatureId, fsb.Feedback })
				.IsUnique();
		}

		private void Configure(EntityTypeBuilder<Authentication> entityTypeBuilder)
		{
			const string tableName = "Auths";
			entityTypeBuilder.ToTable(tableName);

			entityTypeBuilder.HasKey(a => new { a.SerializedAuthenticationType, a.AuthenticationUserId });

			entityTypeBuilder.HasOne(a => a.User)
				.WithMany(u => u.Authentications)
				.HasForeignKey(a => a.UserId)
				.IsRequired();

			entityTypeBuilder.Property(a => a.AuthenticationUserId)
				.HasColumnName("AuthUsrId")
				.HasColumnType(ShortStringColumnType)
				.IsRequired();

			entityTypeBuilder.Ignore(a => a.AuthenticationType);

			entityTypeBuilder.Property(a => a.SerializedAuthenticationType)
				.HasColumnName("AuthType")
				.HasColumnType(ShortStringColumnType)
				.IsRequired();

			entityTypeBuilder.HasIndex(a => a.UserId)
				.IsUnique();
		}
	}
}