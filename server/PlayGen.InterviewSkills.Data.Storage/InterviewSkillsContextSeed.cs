using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Model.Authentications;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Model.Job;
using PlayGen.InterviewSkills.Data.Model.Questions.Interview;
using PlayGen.InterviewSkills.Data.Model.Questions.Observational;
using PlayGen.InterviewSkills.Data.Model.Role;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Data.Storage
{
	public static class InterviewSkillsContextSeed
	{
		// todo find better way of doing this
		public const string GuestAuthenticationId = "defaultguest";

		public static void EnsureSeedData(this InterviewSkillsContext context)
		{
			if (context.AllMigrationsApplied())
			{
				#region Roles
				context.Roles.AddOrUpdate(context, new Role { Id = "candidate" });
				context.Roles.AddOrUpdate(context, new Role { Id = "interviewer" });
				#endregion

				#region Features
				var features = new
				{
					Anger = context.Features.AddOrUpdate(context, new Feature { Id = "anger", Inverse = "pleasantness", IdealWeightMin = 0, IdealWeightMax = 0.25f }),
					Boredom = context.Features.AddOrUpdate(context, new Feature { Id = "boredom", Inverse = "energy", IdealWeightMin = 0, IdealWeightMax = 0.25f }),
					Conflict = context.Features.AddOrUpdate(context, new Feature { Id = "conflict", Inverse = "agreeability", IdealWeightMin = 0, IdealWeightMax = 0.3f }),
					Deception = context.Features.AddOrUpdate(context, new Feature { Id = "deception", IdealWeightMin = 0, IdealWeightMax = 0.3f }),
					Friendliness = context.Features.AddOrUpdate(context, new Feature { Id = "friendliness", IdealWeightMin = 0.7f, IdealWeightMax = 1f }),
					Happiness = context.Features.AddOrUpdate(context, new Feature { Id = "happiness", IdealWeightMin = 0.6f, IdealWeightMax = 1 }),
					Interest = context.Features.AddOrUpdate(context, new Feature { Id = "interest", IdealWeightMin = 0.75f, IdealWeightMax = 1 }),
					Positivity = context.Features.AddOrUpdate(context, new Feature { Id = "positivity", IdealWeightMin = 0.7f, IdealWeightMax = 1 }),
					Sadness = context.Features.AddOrUpdate(context, new Feature { Id = "sadness", Inverse = "cheer", IdealWeightMin = 0, IdealWeightMax = 0.2f }),
					Sincerity = context.Features.AddOrUpdate(context, new Feature { Id = "sincerity", IdealWeightMin = 0.6f, IdealWeightMax = 1 }),
					Sleepiness = context.Features.AddOrUpdate(context, new Feature { Id = "sleepiness", Inverse = "alertness", IdealWeightMin = 0, IdealWeightMax = 0.25f }),
					Stress = context.Features.AddOrUpdate(context, new Feature { Id = "stress", Inverse = "calmness", IdealWeightMin = 0, IdealWeightMax = 0.3f }),
					Volume = context.Features.AddOrUpdate(context, new Feature { Id = "volume", IdealWeightMin = 0.3f, IdealWeightMax = 0.7f })
				};
				#endregion

				#region Tags
				var tags = new
				{
					Character = context.Tags.GetOrAdd(new Tag { Id = "character" }),
					CareerGoal = context.Tags.GetOrAdd(new Tag { Id = "careergoal" }),
					Competency = context.Tags.GetOrAdd(new Tag { Id = "competency" }),
					Curveball = context.Tags.GetOrAdd(new Tag { Id = "curveball" }),

					DuringInterviewQuestions = context.Tags.GetOrAdd(new Tag { Id = "duringinterviewquestions" }),
					PostInterviewQuestions = context.Tags.GetOrAdd(new Tag { Id = "postinterviewquestions" }),

					Serious = context.Tags.GetOrAdd(new Tag { Id = "serious" }),
					Fun = context.Tags.GetOrAdd(new Tag { Id = "fun" })
				};
				#endregion

				#region JobThemes
				var jobThemes = new
				{
					Medical = context.JobThemes.GetOrAdd(new JobTheme { Id = "Medical" }),
					Office = context.JobThemes.GetOrAdd(new JobTheme { Id = "Office" }),
					Laboritory = context.JobThemes.GetOrAdd(new JobTheme { Id = "Laboritory" }),
					Sport = context.JobThemes.GetOrAdd(new JobTheme { Id = "Sport" }),
					Restaurant = context.JobThemes.GetOrAdd(new JobTheme { Id = "Restaurant" }),
					Hollywood = context.JobThemes.GetOrAdd(new JobTheme { Id = "Hollywood" }),
					Nature = context.JobThemes.GetOrAdd(new JobTheme { Id = "Nature" }),
					Urban = context.JobThemes.GetOrAdd(new JobTheme { Id = "Urban" }),
				};
				#endregion

				#region Jobs
				var jobs = new
				{
					// Serious
					//NasaResearchScientist = context.Jobs.AddOrUpdate(context, new Job { Id = "NASA Research Scientist", Theme = jobThemes.Laboritory })
					//	.TryAddTagMappings(context, tags.Serious),

					//AirlinePilot = context.Jobs.AddOrUpdate(context, new Job { Id = "Airline Pilot", Theme = jobThemes.Office })
					//	.TryAddTagMappings(context, tags.Serious),

					PoliceOfficer = context.Jobs.AddOrUpdate(context, new Job { Id = "Police Officer", Theme = jobThemes.Urban })
						.TryAddTagMappings(context, tags.Serious),

					//RecruitmentAgent = context.Jobs.AddOrUpdate(context, new Job { Id = "Recruitment Agent", Theme = jobThemes.Office })
					//	.TryAddTagMappings(context, tags.Serious),

					Teacher = context.Jobs.AddOrUpdate(context, new Job { Id = "Teacher", Theme = jobThemes.Laboritory })
						.TryAddTagMappings(context, tags.Serious),

					//Doctor = context.Jobs.AddOrUpdate(context, new Job { Id = "Doctor", Theme = jobThemes.Medical })
					//	.TryAddTagMappings(context, tags.Serious),

					//Chef = context.Jobs.AddOrUpdate(context, new Job { Id = "Chef", Theme = jobThemes.Restaurant })
					//	.TryAddTagMappings(context, tags.Serious),

					ITSupport = context.Jobs.AddOrUpdate(context, new Job { Id = "IT Support", Theme = jobThemes.Office })
						.TryAddTagMappings(context, tags.Serious),

					Receptionist = context.Jobs.AddOrUpdate(context, new Job { Id = "Receptionist", Theme = jobThemes.Office })
						.TryAddTagMappings(context, tags.Serious),

					//Programmer = context.Jobs.AddOrUpdate(context, new Job { Id = "Programmer", Theme = jobThemes.Office })
					//	.TryAddTagMappings(context, tags.Serious),

					//Accountant = context.Jobs.AddOrUpdate(context, new Job { Id = "Accountant", Theme = jobThemes.Office })
					//	.TryAddTagMappings(context, tags.Serious),

					//Bartender = context.Jobs.AddOrUpdate(context, new Job { Id = "Bartender", Theme = jobThemes.Restaurant })
					//	.TryAddTagMappings(context, tags.Serious),

					Veterinarian = context.Jobs.AddOrUpdate(context, new Job { Id = "Veterinarian", Theme = jobThemes.Medical })
						.TryAddTagMappings(context, tags.Serious),

					// Fun
					//LumberJack = context.Jobs.AddOrUpdate(context, new Job { Id = "Lumberjack", Theme = jobThemes.Nature })
					//	.TryAddTagMappings(context, tags.Fun),

					LionTamer = context.Jobs.AddOrUpdate(context, new Job { Id = "Lion Tamer", Theme = jobThemes.Nature })
						.TryAddTagMappings(context, tags.Fun),

					PetFoodTaster = context.Jobs.AddOrUpdate(context, new Job { Id = "Pet Food Taster", Theme = jobThemes.Laboritory })
						.TryAddTagMappings(context, tags.Fun),

					//FoodCritic = context.Jobs.AddOrUpdate(context, new Job { Id = "Food Critic", Theme = jobThemes.Restaurant })
					//	.TryAddTagMappings(context, tags.Fun),

					//StuntPerson = context.Jobs.AddOrUpdate(context, new Job { Id = "Stuntman/woman", Theme = jobThemes.Hollywood })
					//	.TryAddTagMappings(context, tags.Fun),

					//PetDetective = context.Jobs.AddOrUpdate(context, new Job { Id = "Pet Detective", Theme = jobThemes.Nature })
					//	.TryAddTagMappings(context, tags.Fun),

					//CelebrityPublicist = context.Jobs.AddOrUpdate(context, new Job { Id = "Celebrity Publicist", Theme = jobThemes.Hollywood })
					//	.TryAddTagMappings(context, tags.Fun),

					//SkydivingInstructor = context.Jobs.AddOrUpdate(context, new Job { Id = "Skydiving Instructor", Theme = jobThemes.Sport })
					//	.TryAddTagMappings(context, tags.Fun),

					//ProfessionalAthlete = context.Jobs.AddOrUpdate(context, new Job { Id = "Professional Athlete", Theme = jobThemes.Sport })
					//	.TryAddTagMappings(context, tags.Fun),

					//DJ = context.Jobs.AddOrUpdate(context, new Job { Id = "DJ", Theme = jobThemes.Hollywood })
					//	.TryAddTagMappings(context, tags.Fun),

					//FilmDirector = context.Jobs.AddOrUpdate(context, new Job { Id = "Film Director", Theme = jobThemes.Hollywood })
					//	.TryAddTagMappings(context, tags.Fun),

					//CakeDecorater = context.Jobs.AddOrUpdate(context, new Job { Id = "Cake Decorater", Theme = jobThemes.Restaurant })
					//	.TryAddTagMappings(context, tags.Fun),

					ZooKeeper = context.Jobs.AddOrUpdate(context, new Job { Id = "Zoo Keeper", Theme = jobThemes.Nature })
						.TryAddTagMappings(context, tags.Fun),

					//Cartographer = context.Jobs.AddOrUpdate(context, new Job { Id = "Cartographer", Theme = jobThemes.Nature })
					//	.TryAddTagMappings(context, tags.Fun),

					//CostEstimator = context.Jobs.AddOrUpdate(context, new Job { Id = "Cost Estimator", Theme = jobThemes.Office })
					//	.TryAddTagMappings(context, tags.Fun),

					//ChimneySweep = context.Jobs.AddOrUpdate(context, new Job { Id = "Chimney Sweep", Theme = jobThemes.Urban })
					//	.TryAddTagMappings(context, tags.Fun),

					ParachuteTester = context.Jobs.AddOrUpdate(context, new Job { Id = "Parachute Tester", Theme = jobThemes.Sport })
						.TryAddTagMappings(context, tags.Fun),

					//UsedCarSalesman = context.Jobs.AddOrUpdate(context, new Job { Id = "Used Car Salesman", Theme = jobThemes.Urban })
					//	.TryAddTagMappings(context, tags.Fun),

					//YoutubeVlogger = context.Jobs.AddOrUpdate(context, new Job { Id = "YouTube Vlogger", Theme = jobThemes.Hollywood })
					//	.TryAddTagMappings(context, tags.Fun),

					//Superhero = context.Jobs.AddOrUpdate(context, new Job { Id = "Superhero", Theme = jobThemes.Urban })
					//	.TryAddTagMappings(context, tags.Fun),

					//Sidekick = context.Jobs.AddOrUpdate(context, new Job { Id = "Sidekick", Theme = jobThemes.Urban })
					//	.TryAddTagMappings(context, tags.Fun),

					Supervillan = context.Jobs.AddOrUpdate(context, new Job { Id = "Supervillan", Theme = jobThemes.Urban })
						.TryAddTagMappings(context, tags.Fun),

					//Henchman = context.Jobs.AddOrUpdate(context, new Job { Id = "Henchman", Theme = jobThemes.Urban })
					//	.TryAddTagMappings(context, tags.Fun),

					//Clown = context.Jobs.AddOrUpdate(context, new Job { Id = "Clown", Theme = jobThemes.Hollywood })
					//	.TryAddTagMappings(context, tags.Fun),

					//Acrobat = context.Jobs.AddOrUpdate(context, new Job { Id = "Acrobat", Theme = jobThemes.Hollywood })
					//	.TryAddTagMappings(context, tags.Fun),

					//CEOOfRubberDuckieInc = context.Jobs.AddOrUpdate(context, new Job { Id = "CEO of Rubber Duckie Inc", Theme = jobThemes.Office })
					//	.TryAddTagMappings(context, tags.Fun),

					//Explorer = context.Jobs.AddOrUpdate(context, new Job { Id = "Explorer", Theme = jobThemes.Nature })
					//	.TryAddTagMappings(context, tags.Fun),
				};
				#endregion

				#region InterviewQuestions
				#region Career Goals
				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What are your goals?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Interest, features.Sincerity, features.Sleepiness)
					.TryAddTagMappings(context, tags.CareerGoal);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Why do you want to work here?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Interest, features.Volume, features.Boredom)
					.TryAddTagMappings(context, tags.CareerGoal);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Why are you applying?", Duration = 40 })
					.TryAddFeatureMappings(context, features.Anger, features.Interest, features.Conflict)
					.TryAddTagMappings(context, tags.CareerGoal);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "How does this job fit in with your career path?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Interest, features.Conflict, features.Volume)
					.TryAddTagMappings(context, tags.CareerGoal);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Why do you want to leave your current job?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Sincerity, features.Friendliness, features.Anger)
					.TryAddTagMappings(context, tags.CareerGoal);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What is your dream job?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Interest, features.Sleepiness, features.Deception)
					.TryAddTagMappings(context, tags.CareerGoal);
				#endregion

				#region Character
				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "How would you describe yourself?", Duration = 40 })
					.TryAddFeatureMappings(context, features.Friendliness, features.Deception, features.Positivity)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What are your biggest strengths?", Duration = 40 })
					.TryAddFeatureMappings(context, features.Happiness, features.Sincerity, features.Sleepiness)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What are your biggest weaknesses?", Duration = 40 })
					.TryAddFeatureMappings(context, features.Happiness, features.Sincerity, features.Sadness)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Which websites do you use personally? Why?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Friendliness, features.Anger, features.Happiness)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What did you like and dislike about your previous job?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Conflict, features.Friendliness, features.Stress)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What do your co-workers say about you?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Anger, features.Sadness, features.Sincerity)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What's your ideal work environment?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Volume, features.Happiness, features.Boredom)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Who do you admire and why?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Friendliness, features.Deception, features.Stress)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "How do you maintain a good work/life balance?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Stress, features.Sleepiness, features.Happiness)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Would you rather be liked or feared?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Volume, features.Sincerity, features.Stress)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What motivates you?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Positivity, features.Interest, features.Boredom)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Tell me about something funny that's happened to you at work", Duration = 60 })
					.TryAddFeatureMappings(context, features.Happiness, features.Deception, features.Friendliness)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What do you most dislike about yourself?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Sadness, features.Anger, features.Sincerity)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What do people assume about you that's wrong?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Sincerity, features.Deception, features.Volume)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What are your core values?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Happiness, features.Sleepiness, features.Deception)
					.TryAddTagMappings(context, tags.Character);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What are your hobbies and interests?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Conflict, features.Positivity, features.Happiness)
					.TryAddTagMappings(context, tags.Character);
				#endregion

				#region Competency
				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Why should we hire you?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Positivity, features.Interest, features.Friendliness)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What makes you a good team member?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Friendliness, features.Volume, features.Boredom)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What can you bring to this company?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Interest, features.Stress, features.Conflict)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What's your preferred management style?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Deception, features.Happiness, features.Stress)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What have you achieved elsewhere?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Happiness, features.Positivity, features.Volume)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Why are you a good fit for the company?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Sadness, features.Sleepiness, features.Deception)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "How would you approach this job?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Interest, features.Deception, features.Stress)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "How have you improved in the last year?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Positivity, features.Deception, features.Sleepiness)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Tell me about a time you worked in a team", Duration = 60 })
					.TryAddFeatureMappings(context, features.Sincerity, features.Friendliness, features.Conflict)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What has been the biggest setback of your career?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Sincerity, features.Anger, features.Conflict)
					.TryAddTagMappings(context, tags.Competency);
				
				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What was the last big decision you had to make?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Anger, features.Conflict, features.Stress)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Tell me about a time you supported a member of your team who was struggling", Duration = 60 })
					.TryAddFeatureMappings(context, features.Deception, features.Sincerity, features.Boredom)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Describe the job you've applied for", Duration = 60 })
					.TryAddFeatureMappings(context, features.Interest, features.Conflict, features.Stress)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "How do you respond to stress and pressure?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Sadness, features.Stress, features.Friendliness)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "How do you manage your time and prioritise tasks?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Boredom, features.Interest, features.Stress)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "How do you respond to criticism?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Stress, features.Anger, features.Volume)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Tell me about a time you dealt with a difficult person", Duration = 80 })
					.TryAddFeatureMappings(context, features.Stress, features.Sleepiness, features.Friendliness)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Tell me about a time you worked to or missed a deadline", Duration = 80 })
					.TryAddFeatureMappings(context, features.Volume, features.Stress, features.Anger)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Tell me about one of your work tasks that didn't work and how you learnt from it", Duration = 80 })
					.TryAddFeatureMappings(context, features.Friendliness, features.Deception, features.Sadness)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Tell me about a recent situation where you took initiative and made something happen?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Positivity, features.Interest, features.Boredom)
					.TryAddTagMappings(context, tags.Competency);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What have you learned from your previous jobs?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Happiness, features.Stress, features.Conflict)
					.TryAddTagMappings(context, tags.Competency);
				#endregion

				#region Curveball
				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Have you ever stolen a pen from work?", Duration = 40 })
					.TryAddFeatureMappings(context, features.Conflict, features.Sincerity, features.Friendliness)
					.TryAddTagMappings(context, tags.Curveball);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Could your personal social media presence affect us?", Duration = 60 })
					.TryAddFeatureMappings(context, features.Anger, features.Sincerity, features.Stress)
					.TryAddTagMappings(context, tags.Curveball);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Show me an example of your creativity", Duration = 80 })
					.TryAddFeatureMappings(context, features.Happiness, features.Interest, features.Boredom)
					.TryAddTagMappings(context, tags.Curveball);
				
				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Do you have any questions for me?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Interest, features.Boredom, features.Sleepiness)
					.TryAddTagMappings(context, tags.Curveball);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "Convince me to buy a pen you're selling.", Duration = 80 })
					.TryAddFeatureMappings(context, features.Interest, features.Sincerity, features.Positivity)
					.TryAddTagMappings(context, tags.Curveball);

				context.InterviewQuestions
					.AddOrUpdate(context, new InterviewQuestion { Question = "What haven't I asked you that I should have?", Duration = 80 })
					.TryAddFeatureMappings(context, features.Sadness, features.Boredom, features.Deception)
					.TryAddTagMappings(context, tags.Curveball);
				#endregion
				#endregion

				#region ObservarionalQuestions and ObservationalQuestionFeedbacks
				#region DuringInterviewQuestions
				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Does the candidate appear angry?", IsBinary = true, StartLabel = "No", EndLabel = "Yes" })
					.TryAddFeatureMappings(context, features.Anger)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "The interviewer is finding you calm", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "Try present yourself in a more calm manner", WeightFromIdeal = 0.1f });

				context.ObservationalQuestions.AddOrUpdate(context, new ObservationalQuestion { Question = "Does the candidate sound bored?", IsBinary = true, StartLabel = "No", EndLabel = "Yes" })
					.TryAddFeatureMappings(context, features.Boredom)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "The interviewer has noticed that you sound.interested", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "Try to put more emotion and energy into your voice", WeightFromIdeal = 0.1f });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Does the candidate interrupt and try talk over you?", IsBinary = true, StartLabel = "No", EndLabel = "Yes" })
					.TryAddFeatureMappings(context, features.Conflict)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "The interviewer is finding you to be a good conversational partner", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "Remember not to talk over or interrupt the interviewer", WeightFromIdeal = 0.1f });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Does the candidate appear to be deceptive?", IsBinary = true, StartLabel = "No", EndLabel = "Yes" })
					.TryAddFeatureMappings(context, features.Deception)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "The interviewer is finding you to be honest and sincere", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "The interviewer in noticing a lack of honesty in your voice", WeightFromIdeal = 0.1f });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Does the candidate appear friendly?", IsBinary = true, StartLabel = "No", EndLabel = "Yes" })
					.TryAddFeatureMappings(context, features.Friendliness)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Try present yourself in a more friendly manner", WeightFromIdeal = -0.1f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer is finding you friendly", WeightFromIdeal = 0 });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "How happy does the candidate appear?", IsBinary = false, StartLabel = "Not happy", EndLabel = "Very happy" })
					.TryAddFeatureMappings(context, features.Happiness)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Attempt to present yourself in a more cheerful manner", WeightFromIdeal = -0.6f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer feels that you are relatively happy at the moment", WeightFromIdeal = -0.2f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer has noticed that you're coming accross as happy", WeightFromIdeal = 0 });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "How much interest is the candidate showing for the role?", IsBinary = false, StartLabel = "Low", EndLabel = "High" })
					.TryAddFeatureMappings(context, features.Interest)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "The interviewer feels you are showing low interest in the role", WeightFromIdeal = -0.6f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer feels you are showing mild interest in the role", WeightFromIdeal = -0.2f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer is noticing your high interest in the role", WeightFromIdeal = 0 });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "How positive does the candidate appear?", IsBinary = false, StartLabel = "Negative", EndLabel = "Positive" })
					.TryAddFeatureMappings(context, features.Positivity)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Try to be more positive", WeightFromIdeal = -0.6f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer is finding you neither positive nor negative", WeightFromIdeal = -0.2f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer likes your positive attitude", WeightFromIdeal = 0 });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Does the candidate appear sad?", IsBinary = true, StartLabel = "No", EndLabel = "Yes" })
					.TryAddFeatureMappings(context, features.Sadness)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Well done for being cheerful", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "The interviewer noticed you appear to be in a low mood", WeightFromIdeal = 0.1f });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "How sincere does the candidate appear?", IsBinary = false, StartLabel = "Dishonest", EndLabel = "Sincere" })
					.TryAddFeatureMappings(context, features.Sincerity)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Try to say what you really mean", WeightFromIdeal = -0.6f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer is finding you relatively sincere", WeightFromIdeal = -0.2f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer has noticed that you're sincere in your answers", WeightFromIdeal = 0 });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Does the candidate appear sleepy?", IsBinary = true, StartLabel = "No", EndLabel = "Yes" })
					.TryAddFeatureMappings(context, features.Sleepiness)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Well done on presenting yourself as wide awake and focused", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "You seem a bit sleepy, sit up breathe and concentrate", WeightFromIdeal = 0.1f });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Does it appear that the candidate is finding this interview stressful?", IsBinary = true, StartLabel = "No", EndLabel = "Yes" })
					.TryAddFeatureMappings(context, features.Stress)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "You are coming across as calm and collected", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "The interviewer is noticing you are stressed", WeightFromIdeal = 0.1f });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Is the candidate speaking too loudly/quietly?", IsBinary = false, StartLabel = "Too quetly", EndLabel = "Too loudly" })
					.TryAddFeatureMappings(context, features.Volume)
					.TryAddTagMappings(context, tags.DuringInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "The interviewer feels you are speaking too quietly", WeightFromIdeal = -0.1f },
						new ObservationalQuestionFeedback { Feedback = "The interviewer feels that you are speaking at the correct volume", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "The interviewer feels you are speaking too loudly", WeightFromIdeal = 0.1f });
				#endregion

				#region PostInterviewQuestions
				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Friendliness", IsBinary = false, StartLabel = "Low", EndLabel = "High" })
					.TryAddFeatureMappings(context, features.Friendliness)
					.TryAddTagMappings(context, tags.PostInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Try to present yourself in a more friendly manner", WeightFromIdeal = -0.6f },
						new ObservationalQuestionFeedback { Feedback = "The other player found you fairly friendly during the conversation", WeightFromIdeal = -0.2f },
						new ObservationalQuestionFeedback { Feedback = "The other player thought you were very friendly", WeightFromIdeal = 0 });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Interest", IsBinary = false, StartLabel = "Low", EndLabel = "High" })
					.TryAddFeatureMappings(context, features.Interest)
					.TryAddTagMappings(context, tags.PostInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Next time try to show more interest", WeightFromIdeal = -0.6f },
						new ObservationalQuestionFeedback { Feedback = "The other player felt you showed some interest", WeightFromIdeal = -0.2f },
						new ObservationalQuestionFeedback { Feedback = "The other player was impressed by your level of interest", WeightFromIdeal = 0f });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Sincerity", IsBinary = false, StartLabel = "Low", EndLabel = "High" })
					.TryAddFeatureMappings(context, features.Sincerity)
					.TryAddTagMappings(context, tags.PostInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Try to appear more sincere in future conversations", WeightFromIdeal = -0.6f },
						new ObservationalQuestionFeedback { Feedback = "The other player found you somewhat sincere", WeightFromIdeal = -0.2f },
						new ObservationalQuestionFeedback { Feedback = "The other player thought you were sincere during the interview", WeightFromIdeal = 0 });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Happiness", IsBinary = false, StartLabel = "Low", EndLabel = "High" })
					.TryAddFeatureMappings(context, features.Happiness)
					.TryAddTagMappings(context, tags.PostInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "Try to be more positive in future interviews", WeightFromIdeal = -0.6f },
						new ObservationalQuestionFeedback { Feedback = "The other player thought you seemed indifferent", WeightFromIdeal = -0.2f },
						new ObservationalQuestionFeedback { Feedback = "The other player noticed your level of happiness", WeightFromIdeal = 0 });

				context.ObservationalQuestions
					.AddOrUpdate(context, new ObservationalQuestion { Question = "Calmness", IsInverse = true, IsBinary = false, StartLabel = "Low", EndLabel = "High" })
					.TryAddFeatureMappings(context, features.Stress)
					.TryAddTagMappings(context, tags.PostInterviewQuestions)
					.TryAddOrUpdateFeedbacks(context,
						new ObservationalQuestionFeedback { Feedback = "The other player you were calm", WeightFromIdeal = 0 },
						new ObservationalQuestionFeedback { Feedback = "The other player thought you mostly maintained your composure", WeightFromIdeal = 0.2f },
						new ObservationalQuestionFeedback { Feedback = "In future try to remain relaxed during conversation", WeightFromIdeal = 0.6f });
				#endregion
				#endregion

				#region FeatureActionableFeedbacks
				features.Anger.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Keep an even tone with a varied pitch", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Try to maintain your composure remember to breathe", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Leave external annouances aside and focus on the interview ", WeightFromIdeal = 0.1f });

				features.Boredom.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Keep focussed on the interviewer", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Respond with interest or ask your own questions of the interviewer", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Do your research on the company so you have lots to talk about", WeightFromIdeal = -0.1f });

				features.Conflict.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Don't talk over the interviewer", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Prepare for difficult questions in advance", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Wait for the interviewer to finish talking before you start", WeightFromIdeal = 0.1f });

				features.Deception.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Talk about your own experiences rather than making up stories", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Stick to the essentials, the most impressive parts of your work history", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Be upfront about genuine gaps in your skills ", WeightFromIdeal = 0.1f });

				features.Friendliness.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Relax your body language", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Subtly mimic the interviewer's body language", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Be genuinly interested in what the interviewer is saying", WeightFromIdeal = -0.1f });

				features.Happiness.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Keep a light and positive tone of voice", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Try to Smile more often", WeightFromIdeal = -0.1f });

				features.Interest.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Try to be more open in your your body language", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "React to what the interviewer is saying, even a simple head nod could do", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Make sure to devote your full attention and to focus on the the interviewer", WeightFromIdeal = -0.1f });

				features.Positivity.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Keep your focus on being positive", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Convey a can do attitude", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Let go of bad feelings from past interviews that didn't go well", WeightFromIdeal = -0.1f });

				features.Sadness.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Focus on your achievements rather than failures", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Smiling even when you're not feeling happy can brighten your mood", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "You made it to the interview stage and that's something to feel happy about", WeightFromIdeal = 0.1f });

				features.Sincerity.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Make regular eye contact with the interviewer when talking", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "React tactfully in responding to questions about your weaknesses", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Make sure not to embelish your skills and expertise", WeightFromIdeal = -0.1f });

				features.Sleepiness.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Get enough sleep before the interview so you feel alert and refreshed", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Do some stretching before an interview to wake yourself up", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Pay attention to your diet, avoid a heavy meal before an interview", WeightFromIdeal = 0.1f });

				features.Stress.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Keep your voice calm and even", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Remember to breathe and relax your shoulders", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Avoid too much caffeine as it can induce anxiety ", WeightFromIdeal = 0.1f });

				features.Volume.TryAddOrUpdateActionableFeedbacks(context,
					new FeatureActionableFeedback { Feedback = "Speak up so the interviewer can hear you clearly", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "If you are in a noisy environment check that the inteviewer can hear you ok", WeightFromIdeal = -0.1f },
					new FeatureActionableFeedback { Feedback = "Make sure that the interviewer can hear you if you are in a noisy environment", WeightFromIdeal = 0.1f },
					new FeatureActionableFeedback { Feedback = "Don't talk too loudly", WeightFromIdeal = 0.1f });
				#endregion

				#region FeatureSummaryFeedbacks
				features.Anger.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You maintained your composure throughout the interview.\n" +
									"It's good to show the interviewer that you can control your emotions.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "Congratulations on being calm during your interview.\n" +
									"Keeping calm during an interview allows you to concentrate on how best to present yourself ",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed agitated during the interview.\n" +
									"Instead of holding onto negative emotions try to be in the moment and focus on presenting your best self",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed irritatable in the interview.\n" +
									"You need to show that you can control your emotions.",
						WeightFromIdeal = 0.1f
					});

				features.Boredom.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You kept your enthusiasm throughout the interview.\n" +
									"An interviewer wants to see that you have a genuine interest in the role.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You demonstrated that you are engaged.\n" +
									"Being fully engaged in the conversation ensures that you answer questions ",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You came across as somewhat disinterested.\n" +
									"Remember to be responsive both in your tone and in your body language.",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed bored and disinterested.\n" +
									"Do your research and prepare so that you have lots to talk about",
						WeightFromIdeal = 0.1f
					});

				features.Conflict.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You came across as personable \n" +
									"The interviewer wants to see that you are a team player.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You were seen to be an agreeable \n" +
									"Being liked is important, but make sure you are also staying true to your beliefs. ",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "At times you were seen as a little confrontational.\n" +
									"Showing a strong personality is good, but make sure you are not offending the interviewer.",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You came across as argumentative.\n" +
									"Avoid interrupting or arguing with the interviewer.",
						WeightFromIdeal = 0.1f
					});

				features.Deception.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You demonstrated insincerity in your answers.\n" +
									"It's important to show the interviewer that you are.genuine.",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "At times you were seen to be a little dishonest in your answers.\n" +
									"Only stick to the facts and be open about any parts of the role you would need to improve on.",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "Well done for presenting yourself in a genuine way!\n" +
									"It's important for the interviewere to feel that they can trust you.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You were seen as genuine and honest in your responses.\n" +
									"The interviewer needs to feel confident that you have the skills needed for the role",
						WeightFromIdeal = 0
					});

				features.Friendliness.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed unfriendly.\n" +
									"Present a warm personality and aim to build rapport with the interviewer",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "At times you came across as a little unfriendly.\n" +
									"Throw in a few more smiles and nods and make sure you are actively listening.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You came across as amiable \n" +
									"Likeability scores highly in interviews, as long as you don't go over the top!",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed friendly.\n" +
									"Being personable and having people skills is valuable in many roles.",
						WeightFromIdeal = 0
					});

				features.Happiness.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You were seen as having a happy temprament.\n" +
									"Maintaining a level of happiness even when under stress shows good strength of character.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You came accross as cheerful.\n" +
									"Giving off a happy vibe makes the interview more enjoyable for all parties involved.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "It was noted that you were in an unhappy mood.\n" +
									"You need to try to control your emotions for the duration of an interview.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed in a low mood.\n" +
									"Try to lift your mood by being in the moment and focussing on the positive.",
						WeightFromIdeal = -0.1f
					});

				features.Interest.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed to show a low level of interest.\n" +
									"Showing interest in the role shows the interviewer that you are serious about the role.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You demonstrated some level of interest.\n" +
									"Make sure you listen carefully to the questions so you can focus your answers.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You showed a good level in interest in the role.\n" +
									"Keeping eye contact at the right times shows you are interested.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You were seen as enthusiastic and interested in the role.\n" +
									"Showing interest in the role demonstrates that you are serious about the role.",
						WeightFromIdeal = 0
					});

				features.Positivity.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "At times you came across negatively.\n" +
									"Avoid talking negatively about your past employers or gaps in your skills.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You were seen as airing on the side of negative.\n" +
									"Use a postive tone in your voice and an open  body language to help you feel more positive.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You kept a positive attitude.\n" +
									"Staying positive means your interview demeanor will be more likable.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You displayed a positive attitude.\n" +
									"Staying posiive in stressful situations is a desirable skill in most work environmnets.",
						WeightFromIdeal = 0
					});

				features.Sadness.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed to be in a low mood.\n" +
									"It's important to show the interviewer that you can maintain a professional attitude.",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "It seemed that you were not in the best of moods today.\n" +
									"Practice smiling on your way to the interview it will release endorphins and lift your mood!",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You came across as a little indifferent.\n" +
									"It's good to show some emotion in your tone to keep the interviewer engaged.",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "Your upbeat mood came across well.\n" +
									"Maintaining a good mood in an interview creates a more pleasant setting and eases tension.",
						WeightFromIdeal = 0
					});

				features.Sincerity.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You came across as insincere.\n" +
									"Stick to the facts and avoid over embelishment.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You were seen as incincere in some of your answers.\n" +
									"Sincerity comes across not only in your answers but also your body language.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You were good at being sincere.\n" +
									"The interviewer needs to feel confident in you and your skills so they can make the best hire.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "Your sincerity came across well.\n" +
									"Staying sincere in an interview demonstrates that you are trustworthy.",
						WeightFromIdeal = 0
					});

				features.Sleepiness.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed alert and awake.\n" +
									"Maintaining focus on the interviewer is essential in showing that you are serious about the role.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You showed a good level of energy.\n" +
									"Your level of energy is a good indicator of your work life balance.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You looked sleepy!.\n" +
									"No one wants to hire someone who will fall asleep on the job! Next time, have a coffee!",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You came across as being tired.\n" +
									"Being tired will hinder your ability to keep focussed. Next time make sure you get a good night's sleep.",
						WeightFromIdeal = 0.1f
					});

				features.Stress.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed relaxed.\n" +
									"Being relaxed in an interview shows that you are confident in your ability to fulfill the role",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You came across as fairly relaxed.\n" +
									"A small level of anxiety can motivate you and make you feel more alert and energised.",
						WeightFromIdeal = 0
					},
					new FeatureSummaryFeedback
					{
						Feedback = "Your demomstrated a level of stress.\n" +
									"Being mindful and remembering to breathe goes a long way in reducing anxiety.",
						WeightFromIdeal = 0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You seemed stressed.\n" +
									"It is natural to feel some stress at an interview, but too much of it will impact your performance.",
						WeightFromIdeal = 0.1f
					});

				features.Volume.TryAddOrUpdateSummaryFeedbacks(context,
					new FeatureSummaryFeedback
					{
						Feedback = "You spoke too softly.\n" +
									"Projecting your voice at the right level promotes confidence.",
						WeightFromIdeal = -0.1f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You spoke at a good volume.\n" +
									"Guaging the right volume to speak shows you are sensitive and responsive to your environment.",
						WeightFromIdeal = 0f
					},
					new FeatureSummaryFeedback
					{
						Feedback = "You spoke too loudly.\n" +
									"It's good to show enthusiasm but make sure you're not being too loud or aggressive.",
						WeightFromIdeal = 0.1f
					});
				#endregion

				#region Scenarios
				context.Scenarios.AddOrUpdate(context, new Scenario.Implementation.Scenarios.JobInterview.Default().ToModel());
				context.Scenarios.AddOrUpdate(context, new Scenario.Implementation.Scenarios.JobInterview.Synchronised.Default().ToModel());
				#endregion

				#region Authentications

				context.Authentications
					.TryAdd(context,
						new Authentication
						{
							AuthenticationType = AuthenticationType.Guest,
							AuthenticationUserId = GuestAuthenticationId
						});
				#endregion

				context.LogErrorSaveChanges();
			}
		}
	}
}