﻿using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
	public class GuestAuthenticationController : Controller
	{
		private readonly Core.Authentication.GuestAuthenticationController _coreAuthenticationController;

		public GuestAuthenticationController(Core.Authentication.GuestAuthenticationController coreAuthenticationController)
		{
			_coreAuthenticationController = coreAuthenticationController;
		}

		// POST api/guestauthentication
		[HttpPost("authenticate")]
		public IActionResult Authenticate()
		{
			var authenticatedUser = _coreAuthenticationController.Get();
			return new OkObjectResult(authenticatedUser);
		}
	}
}