﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PlayGen.InterviewSkills.WebAPI.ActionResults;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
	public class GoogleAuthenticationController : Controller
	{
		private readonly Core.Authentication.GoogleAuthenticationController _coreAuthenticationController;

		public GoogleAuthenticationController(Core.Authentication.GoogleAuthenticationController coreAuthenticationController)
		{
			_coreAuthenticationController = coreAuthenticationController;
		}

		// POST api/googleAuthentication
		// {token: token}
		[HttpPost("authenticate")]
		public async Task<IActionResult> Authenticate([FromBody]string token)
		{
			if (string.IsNullOrEmpty(token))
			{
				return new BadRequestResult();
			}

			var (isValid, googleUserId, emailAddress, error) = await _coreAuthenticationController.TryValidate(token);
			if (isValid)
			{
				var authenticatedUser = _coreAuthenticationController.GetOrCreate(googleUserId, emailAddress);
				return new OkObjectResult(authenticatedUser); 
			}
			else
			{
				return new AuthenticationFailedResult(error);
			}
		}
	}
}
