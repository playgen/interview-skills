﻿using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class FeatureSummaryFeedbackController
	{
		private readonly Core.Feedbacks.FeatureSummaryFeedbackController _coreFeedbackController;

		public FeatureSummaryFeedbackController(Core.Feedbacks.FeatureSummaryFeedbackController coreFeedbackController)
		{
			_coreFeedbackController = coreFeedbackController;
		}

		// GET api/featureSummaryFeedback/list
		// GET api/featureSummaryFeedback/list/[feature]
		// GET api/featureSummaryFeedback/list/[feature]/[weightFromIdeal]
		[HttpGet("list/")]
		[HttpGet("list/{feature}")]
		[HttpGet("list/{feature}/{weightFromIdeal}")]
		public IActionResult List(string feature, float? weightFromIdeal = null, string[] includes = null)
		{
			var feedbacks = _coreFeedbackController.List(feature, weightFromIdeal, includes);
			return new OkObjectResult(feedbacks);
		}
	}
}
