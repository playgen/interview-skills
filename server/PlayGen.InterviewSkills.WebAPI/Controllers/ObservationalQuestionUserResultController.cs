﻿using Microsoft.AspNetCore.Mvc;
using PlayGen.InterviewSkills.Data.Contracts.FeedbackResults;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
	public class ObservationalQuestionUserResultController
    {
		private readonly Core.FeedbackResults.ObservationalQuestionUserResultController _coreResultController;

		public ObservationalQuestionUserResultController(Core.FeedbackResults.ObservationalQuestionUserResultController coreResultController)
		{
			_coreResultController = coreResultController;
		}

		// POST api/observationalQuestionUserResultController/add/
		[HttpPost("add")]
		public IActionResult Add([FromBody] ObservationalFeedbackUserResultRequest request)
		{
			var model = request.ToModel();
			_coreResultController.Add(model);
			return new OkObjectResult(model);
		}
	}
}