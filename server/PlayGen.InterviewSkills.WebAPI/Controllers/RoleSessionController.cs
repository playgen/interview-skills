﻿using System;
using Microsoft.AspNetCore.Mvc;
using PlayGen.InterviewSkills.Data.Contracts.Role;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
	public class RoleSessionController
    {
		private readonly Core.Role.RoleSessionController _coreSessionController;

		public RoleSessionController(Core.Role.RoleSessionController coreSessionController)
		{
			_coreSessionController = coreSessionController;
		}

		// POST api/rolesession/add
		[HttpPost("add")]
		public IActionResult Add([FromBody] RoleSessionRequest request)
		{
			var model = request.ToModel();
			_coreSessionController.Add(model);
			return new OkObjectResult(model);
		}

		// POST api/rolesession/end/ceb58e50-2f9c-47a7-8ec0-9a8d55ca32fa/5325345
		[HttpPost("end/{roleSessionId}/{ended}")]
	    public IActionResult End(string roleSessionId, long ended)
	    {
		    var model = _coreSessionController.End(Guid.Parse(roleSessionId), EpochUtil.FromUnixTimestamp(ended));
		    return new OkObjectResult(model);
	    }
	}
}