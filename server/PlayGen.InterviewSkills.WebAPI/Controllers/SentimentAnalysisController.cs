﻿using System;
using Microsoft.AspNetCore.Mvc;
using PlayGen.InterviewSkills.Data.Contracts.SentimentAnalysis;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class SentimentAnalysisController : Controller
    {
        private readonly Core.SentimentAnalysisController _coreController;

        public SentimentAnalysisController(Core.SentimentAnalysisController coreController)
        {
            _coreController = coreController;
        }

		// GET api/SentimentAnalysis/later/[user id]/[session id]/[later than time]?excludeFeatureIds=deception
		[HttpGet("later/{userSessionId}/{matchSessionId}/{recordedLaterThan}")]
	    public IActionResult GetLater(
			string userSessionId, 
			string matchSessionId, 
			DateTime recordedLaterThan, 
			string[] excludeFeatureIds = null)
	    {
			if (_coreController.TryGetLater(Guid.Parse(userSessionId), matchSessionId, recordedLaterThan, out var archiveAnalysisMetadata, excludeFeatureIds))
			{
				return new OkObjectResult(archiveAnalysisMetadata);
			}
			else
			{
				return new NoContentResult();
			}
		}

		// GET api/SentimentAnalysis/RangeExclusive/[userSessionId]/[matchSessionId]/[start]/[end]
		// GET api/SentimentAnalysis/RangeExclusive/[userSessionId]/[matchSessionId]/[start]/[end]?excludeFeatureIds=[happiness]&excludeFeatureIds=[boredom]
		[HttpGet("RangeExclusive/{userSessionId}/{matchSessionId}/{start}/{end}")]
		public IActionResult GetRangeExclusive(
			string userSessionId,
			string matchSessionId,
			DateTime start,
			DateTime? end,
			string[] excludeFeatureIds = null)
		{
			var resultsByFeature = _coreController.GetOrderedExclusive(Guid.Parse(userSessionId), matchSessionId, start, end, excludeFeatureIds);
			if (resultsByFeature.Count > 0)
			{
				return new OkObjectResult(resultsByFeature.ToContract());
			}
			else
			{
				return new NoContentResult();
			}
		}

		// GET api/SentimentAnalysis/RangeInclusive/[userSessionId]/[matchSessionId]/[start]/[end]
		// GET api/SentimentAnalysis/RangeInclusive/[userSessionId]/[matchSessionId]/[start]/[end]?excludeFeatureIds=[happiness]&excludeFeatureIds=[boredom]
		[HttpGet("RangeInclusive/{userSessionId}/{matchSessionId}/{start}/{end}")]
		public IActionResult GetRangeInclusive(
			string userSessionId,
			string matchSessionId,
			DateTime start,
			DateTime? end,
			string[] includeFeatureIds = null)
		{
			var resultsByFeature = _coreController.GetOrderedInclusive(Guid.Parse(userSessionId), matchSessionId, start, end, includeFeatureIds);
			if (resultsByFeature.Count > 0)
			{
				return new OkObjectResult(resultsByFeature.ToContract());
			}
			else
			{
				return new NoContentResult();
			}
		}

		// GET api/SentimentAnalysis/AveragesInclusive/userSessionId/matchSessionId/start/end
		// GET api/SentimentAnalysis/AveragesInclusive/userSessionId/matchSessionId/start/end?excludeFeatureIds=happiness&excludeFeatureIds=boredom
		[HttpGet("AveragesExclusive/{userSessionId}/{matchSessionId}/{start}/{end}")]
	    public IActionResult GetAveragesExclusive(
		    string userSessionId,
		    string matchSessionId,
		    DateTime start,
		    DateTime? end,
		    string[] excludeFeatureIds = null)
	    {
		    var averages = _coreController.GetAveragesExclusive(Guid.Parse(userSessionId), matchSessionId, start, end, excludeFeatureIds);
		    if (averages.Count > 0)
		    {
			    return new OkObjectResult(averages.ToContract());
		    }
		    else
		    {
			    return new NoContentResult();
		    }
	    }

		// GET api/SentimentAnalysis/AveragesInclusive/userSessionId/matchSessionId/start/end?includeFeatureIds=happiness&includeFeatureIds=boredom
		[HttpGet("AveragesInclusive/{userSessionId}/{matchSessionId}/{start}/{end}")]
	    public IActionResult GetAveragesInclusive(
		    string userSessionId,
		    string matchSessionId,
		    DateTime start,
		    DateTime? end,
		    string[] includeFeatureIds = null)
	    {
		    var averages = _coreController.GetAveragesInclusive(Guid.Parse(userSessionId), matchSessionId, start, end, includeFeatureIds);
		    if (averages.Count > 0)
		    {
			    return new OkObjectResult(averages.ToContract());
		    }
		    else
		    {
			    return new NoContentResult();
		    }
	    }
	}
}
