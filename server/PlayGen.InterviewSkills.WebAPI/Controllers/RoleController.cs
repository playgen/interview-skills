﻿using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class RoleController
    {
	    private readonly Core.Role.RoleController _roleCoreController;

	    public RoleController(Core.Role.RoleController roleCoreController)
	    {
		    _roleCoreController = roleCoreController;
	    }

		// GET api/role/list
		[HttpGet("list")]
	    public IActionResult List()
	    {
			var roles = _roleCoreController.List();
		    return new OkObjectResult(roles);
	    }
    }
}
