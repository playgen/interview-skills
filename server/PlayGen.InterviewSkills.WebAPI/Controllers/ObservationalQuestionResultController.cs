﻿using Microsoft.AspNetCore.Mvc;
using System;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class ObservationalQuestionResultController
	{
		private readonly Core.QuestionResults.ObservationalQuestionResultController _coreResultController;

		public ObservationalQuestionResultController(Core.QuestionResults.ObservationalQuestionResultController coreResultController)
		{
			_coreResultController = coreResultController;
		}

        // POST api/observationalQuestionResult/[questionId]/[matchSessionId]/[userSessionId]/received/[received]
		[HttpPost("{questionId}/{matchSessionId}/{userSessionId}/received/{received:datetime}")]
		public IActionResult SystemResultReceived(
			int questionId, 
			string matchSessionId, 
			string userSessionId, 
			DateTime received)
        {
            var result = _coreResultController.AddReceived(
				questionId, 
				matchSessionId, 
				Guid.Parse(userSessionId), 
				received);

            return new OkObjectResult(result);
        }

		[HttpPost("{questionId}/{matchSessionId}/{userSessionId}/received/{received:long}")]
		public IActionResult SystemResultReceived(
			int questionId, 
			string matchSessionId,
			string userSessionId, 
			long received)
		{
			var result = _coreResultController.AddReceived(
				questionId, 
				matchSessionId, 
				Guid.Parse(userSessionId), 
				EpochUtil.FromUnixTimestamp(received));

			return new OkObjectResult(result);
		}

		// POST api/observationalQuestionResult/[questionId]/[matchSessionId]/[userSessionId]/[received]/shown/[shown]
		[HttpPost("{questionId}/{matchSessionId}/{userSessionId}/{received:datetime}/shown/{shown:datetime}")]
		public IActionResult SystemResultShown(
			int questionId, 
			string matchSessionId, 
			string userSessionId, 
			DateTime received, 
			DateTime shown)
		{
			var result = _coreResultController.AddShown(
				questionId, 
				matchSessionId, 
				Guid.Parse(userSessionId), 
				received, 
				shown);

			return new OkObjectResult(result);
		}

		[HttpPost("{questionId}/{matchSessionId}/{userSessionId}/{received:long}/shown/{shown:long}")]
		public IActionResult SystemResultShown(
			int questionId, 
			string matchSessionId, 
			string userSessionId, 
			long received, 
			long shown)
		{
			var result = _coreResultController.AddShown(
				questionId, 
				matchSessionId, 
				Guid.Parse(userSessionId), 
				EpochUtil.FromUnixTimestamp(received), 
				EpochUtil.FromUnixTimestamp(shown));

			return new OkObjectResult(result);
		}

		// POST api/observationalQuestionResult/[questionId]/[matchSessionId]/[userSessionId]/[received]/submitted/[weight]/[submitted]
		[HttpPost("{questionId}/{matchSessionId}/{userSessionId}/{received:datetime}/submitted/{weight}/{submitted:datetime}")]
		public IActionResult SystemResultSubmitted(
			int questionId, 
			string matchSessionId, 
			string userSessionId, 
			DateTime received, 
			float weight, 
			DateTime submitted)
		{
			var result = _coreResultController.AddSubmitted(
				questionId, 
				matchSessionId, 
				Guid.Parse(userSessionId), 
				received, 
				weight, 
				submitted);

			return new OkObjectResult(result);
		}

		[HttpPost("{questionId}/{matchSessionId}/{userSessionId}/{received:long}/submitted/{weight}/{submitted:long}")]
		public IActionResult SystemResultSubmitted(
			int questionId, 
			string matchSessionId, 
			string userSessionId, 
			long received, 
			float weight, 
			long submitted)
		{
			var result = _coreResultController.AddSubmitted(
				questionId,
				matchSessionId, 
				Guid.Parse(userSessionId), 
				EpochUtil.FromUnixTimestamp(received), 
				weight, 
				EpochUtil.FromUnixTimestamp(submitted));

			return new OkObjectResult(result);
		}
	}
}
