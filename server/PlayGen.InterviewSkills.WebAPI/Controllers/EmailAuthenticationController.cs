﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PlayGen.InterviewSkills.WebAPI.ActionResults;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
	public class EmailAuthenticationController : Controller
	{
		private readonly Core.Authentication.EmailAuthenticationController _coreAuthenticationController;

		public EmailAuthenticationController(Core.Authentication.EmailAuthenticationController coreAuthenticationController)
		{
			_coreAuthenticationController = coreAuthenticationController;
		}

		// POST api/emailAuthentication 
		// {emailAddress: id@domain}
		[HttpPost("authenticate")]
		public IActionResult Authenticate([FromBody] string emailAddress)
		{
			var (isValid, error) = _coreAuthenticationController.TryValidate(emailAddress);

			if(isValid)
			{
				var authenticatedUser = _coreAuthenticationController.GetOrCreate(emailAddress);
				return new OkObjectResult(authenticatedUser); 
			}
			else
			{
				return new AuthenticationFailedResult(error);
			}
		}
	}
}
