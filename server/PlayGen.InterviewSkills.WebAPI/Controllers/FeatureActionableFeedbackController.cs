﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class FeatureActionableFeedbackController
	{
		private readonly Core.Feedbacks.FeatureActionableFeedbackController _coreFeedbackController;

		public FeatureActionableFeedbackController(Core.Feedbacks.FeatureActionableFeedbackController coreFeedbackController)
		{
			_coreFeedbackController = coreFeedbackController;
		}

		// GET api/featureActionableFeedback/list
		// GET api/featureActionableFeedback/list/[feature]
		// GET api/featureActionableFeedback/list/[feature]/[weightFromIdeal]
		[HttpGet("list/")]
		[HttpGet("list/{feature}")]
		[HttpGet("list/{feature}/{weightFromIdeal}")]
		public IActionResult List(string feature, float? weightFromIdeal = null, string[] includes = null)
		{
			var feedbacks = _coreFeedbackController.List(feature, weightFromIdeal, includes);
			return new OkObjectResult(feedbacks);
		}
    }
}
