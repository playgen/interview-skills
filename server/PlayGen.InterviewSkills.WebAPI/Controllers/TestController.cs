﻿using System;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        // GET
        [HttpGet]
		public IActionResult Get()
        {
            return new OkObjectResult($"{DateTime.UtcNow}: {typeof(TestController).GetTypeInfo().Assembly}.");
        }
    }
}
