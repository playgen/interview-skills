﻿using Microsoft.AspNetCore.Mvc;
using System;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class FeatureActionableFeedbackSystemResultController
	{
		private readonly Core.FeedbackResults.FeatureActionableFeedbackSystemResultController _coreResultController;

		public FeatureActionableFeedbackSystemResultController(Core.FeedbackResults.FeatureActionableFeedbackSystemResultController coreResultController)
		{
			_coreResultController = coreResultController;
		}

        // POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/received/[received]
		[HttpPost("{feedbackId}/{analysisResultId}/received/{received:datetime}")]
		public IActionResult SystemResultReceived(int feedbackId, int analysisResultId, DateTime received)
        {
            var result = _coreResultController.Create(feedbackId, analysisResultId, received);
            return new OkObjectResult(result);
        }

		// POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/received/[received]
		[HttpPost("{feedbackId}/{analysisResultId}/received/{received:long}")]
		public IActionResult SystemResultReceived(int feedbackId, int analysisResultId, long received)
		{
			return SystemResultReceived(feedbackId, analysisResultId, EpochUtil.FromUnixTimestamp(received));
		}

		// POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/shown/[shown]
		[HttpPost("{feedbackId}/{analysisResultId}/shown/{shown:datetime}")]
        public IActionResult SystemResultShown(int feedbackId, int analysisResultId, DateTime shown)
        {
            var result = _coreResultController.AddShown(feedbackId, analysisResultId, shown);
            return new OkObjectResult(result);
        }

		// POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/shown/[shown]
		[HttpPost("{feedbackId}/{analysisResultId}/shown/{shown:long}")]
		public IActionResult SystemResultShown(int feedbackId, int analysisResultId, long shown)
		{
			return SystemResultShown(feedbackId, analysisResultId, EpochUtil.FromUnixTimestamp(shown));
		}

		// POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/acknowledged/[acknowledged]
		[HttpPost("{feedbackId}/{analysisResultId}/acknowledged/{acknowledged:datetime}")]
        public IActionResult SystemResultAcknowledged(int feedbackId, int analysisResultId, DateTime acknowledged)
        {
            var result = _coreResultController.AddAcknowledged(feedbackId, analysisResultId, acknowledged);
            return new OkObjectResult(result);
        }

		// POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/acknowledged/[acknowledged]
		[HttpPost("{feedbackId}/{analysisResultId}/acknowledged/{acknowledged:long}")]
		public IActionResult SystemResultAcknowledged(int feedbackId, int analysisResultId, long acknowledged)
		{
			return SystemResultAcknowledged(feedbackId, analysisResultId, EpochUtil.FromUnixTimestamp(acknowledged));
		}
	}
}
