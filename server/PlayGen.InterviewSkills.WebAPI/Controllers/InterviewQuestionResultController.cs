﻿using System;
using Microsoft.AspNetCore.Mvc;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class InterviewQuestionResultController
    {
	    private readonly Core.QuestionResults.InterviewQuestionResultController _questionResultCoreController;

	    public InterviewQuestionResultController(Core.QuestionResults.InterviewQuestionResultController questionResultCoreController)
	    {
		    _questionResultCoreController = questionResultCoreController;
	    }

		// POST api/interviewQuestionResult/[questionId]/[matchSessionid]/updated/[state]/[updated]
		[HttpPost("{questionId}/{matchSessionId}/updated/{state}/{updated:datetime}")]
	    public IActionResult QuestionUpdated(int questionId, string matchSessionId, string state, DateTime updated)
	    {
		    var result = _questionResultCoreController.AddUpdated(questionId, matchSessionId, state, updated);
		    return new OkObjectResult(result);
	    }

	    [HttpPost("{questionId}/{matchSessionId}/updated/{state}/{updated:long}")]
	    public IActionResult QuestionUpdated(int questionId, string matchSessionId, string state, long updated)
	    {
		    return QuestionUpdated(questionId, matchSessionId, state, EpochUtil.FromUnixTimestamp(updated));
	    }

		// POST api/interviewQuestionResult/[questionId]/[matchSessionid]/hidden/[hidden]
		[HttpPost("{questionId}/{matchSessionId}/hidden/{hidden:datetime}")]
	    public IActionResult QuestionHidden(int questionId, string matchSessionId, DateTime hidden)
	    {
		    var result = _questionResultCoreController.AddHidden(questionId, matchSessionId, hidden);
		    return new OkObjectResult(result);
	    }

	    [HttpPost("{questionId}/{matchSessionId}/hidden/{hidden:long}")]
	    public IActionResult QuestionHidden(int questionId, string matchSessionId, long hidden)
	    {
		    return QuestionHidden(questionId, matchSessionId, EpochUtil.FromUnixTimestamp(hidden));
	    }
	}
}
