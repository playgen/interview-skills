﻿using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class InterviewQuestionController
    {
	    private readonly Core.Questions.InterviewQuestionController _questionCoreController;

	    public InterviewQuestionController(Core.Questions.InterviewQuestionController questionCoreController)
	    {
		    _questionCoreController = questionCoreController;
	    }

		// GET api/interviewquestion/list
		// GET api/interviewquestion/list?features=Happiness
		// GET api/interviewquestion/list?tags=round1
		// GET api/interviewquestion/list?jobs=pilot
		// GET api/interviewquestion/list?includes=TagMappings
		[HttpGet("list")]
	    public IActionResult List(string[] features = null, string[] tags = null, string[] jobs = null,
		    bool requireAllFeatures = true, bool requireAllTags = true, bool requireAllJobs = true, 
			string[] includes = null)
	    {
		    var questions = _questionCoreController.List(features, tags, jobs, 
				requireAllFeatures, requireAllTags, requireAllJobs,
				includes);

		    return new OkObjectResult(questions);
	    }
	}
}
