﻿using Microsoft.AspNetCore.Mvc;
using PlayGen.InterviewSkills.Data.Contracts.Feedbacks;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class ObservationalQuestionFeedbackController
	{
		private readonly Core.Feedbacks.ObservationalQuestionFeedbackController _coreFeedbackController;

		public ObservationalQuestionFeedbackController(Core.Feedbacks.ObservationalQuestionFeedbackController coreFeedbackController)
		{
			_coreFeedbackController = coreFeedbackController;
		}

		// GET api/observationalquestionfeedback/list
		// GET api/observationalquestionfeedback/list/[questionId]
		[HttpGet("list")]
		[HttpGet("list/{questionId}")]
		public IActionResult List(int? questionId, string[] includes = null)
		{
			var feedbacks = _coreFeedbackController.List(questionId, includes);
			return new OkObjectResult(feedbacks);
		}

		// GET api/observationalquestionfeedback/list/[questionId]/[featureId]
		[HttpGet("list/{questionId}/{featureId}")]
		public IActionResult ListForFeature(int questionId, string featureId, string[] includes = null)
		{
			var feedbacks = _coreFeedbackController.ListForFeature(questionId, featureId, includes);
			return new OkObjectResult(feedbacks);
		}

		// GET api/observationalquestionfeedback/list/[questionId]/[featureId]/[weight]
		[HttpGet("list/{questionId}/{featureId}/{weight}")]
		public IActionResult ListForFeatureAndWeight(int questionId, string featureId, float weight, string[] includes = null)
		{
			var (feedbacks, weightFromIdeal) = _coreFeedbackController.ListForFeatureAndWeight(questionId, featureId, weight, includes);

			return new OkObjectResult(new ObservationalQuestionFeedbacksForWeightResponse
			{
				Feedbacks = feedbacks,
				WeightFromIdeal = weightFromIdeal
			});
		}
	}
}
