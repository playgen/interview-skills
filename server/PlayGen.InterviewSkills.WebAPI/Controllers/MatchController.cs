﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PlayGen.InterviewSkills.Core.Match.MatchMaking;
using PlayGen.InterviewSkills.Data.Contracts.Match;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
	public class MatchController : Controller
	{
		private readonly MatchMakingController _coreMatchController;

		public MatchController(MatchMakingController coreMatchController)
		{
			_coreMatchController = coreMatchController;

		}

		// GET api/match/find/[participantId]/[scenarioCategory]/[scenarioName]?canMatchPrevious=true
		[HttpGet("find/{participantId}/{scenarioCategory}/{scenarioName}")]
		public IActionResult Find(string participantId, string scenarioCategory, string scenarioName, bool cantMatchPrevious = false)
		{
			_coreMatchController.TryFindMatch(participantId, scenarioCategory, scenarioName, cantMatchPrevious, out var matches);
			var response = matches.Select(m => m.ToContract());
			return new OkObjectResult(response);
		}

		// GET api/match/new/[participantId]/[scenarioCategory]/[scenarioName]?canMatchPrevious=true
		[HttpGet("new/{participantId}/{scenarioCategory}/{scenarioName}")]
		public IActionResult New(string participantId, string scenarioCategory, string scenarioName, bool cantMatchPrevious = false)
		{
			_coreMatchController.TryFindNewMatch(participantId, scenarioCategory, scenarioName, cantMatchPrevious, out var match);
			var response = match?.ToContract();
			return new OkObjectResult(response);
		}

		// GET api/match/existing/[participantId]
		[HttpGet("existing/{participantId}")]
		public IActionResult Existing(string participantId)
		{
			_coreMatchController.TryFindExistingMatches(participantId, out var matches);
			var response = matches.Select(m => m.ToContract());
			return new OkObjectResult(response);
		}

		// POST api/match/create/{scenarioCategory}/{scenarioName}?participantIds=[user 1 session Id]&participantIds=[user 2 session Id]
		[HttpPost("create/{scenarioCategory}/{scenarioName}")]
		public IActionResult Create(string scenarioCategory, string scenarioName, List<string> participantIds)
		{
			var match = _coreMatchController.CreateMatch(participantIds.ToList(), scenarioCategory, scenarioName);
			var response = match?.ToContract();
			return new OkObjectResult(response);
		}

		// Post api/match/leave/
		[HttpPost("leave/{matchSessionId}/{participantId}")]
		public IActionResult Leave(string matchId, string participantId)
		{
			var didLeave = _coreMatchController.RemoveParticipant(matchId, participantId);
			return new OkObjectResult(didLeave);
		}

		// Post api/match/leaveall/1
		[HttpPost("leaveall/{participantId}")]
		public IActionResult LeaveAll(string participantId)
		{
			var didLeave = _coreMatchController.RemoveParticipant(participantId);
			return new OkObjectResult(didLeave);
		}
	}
}
