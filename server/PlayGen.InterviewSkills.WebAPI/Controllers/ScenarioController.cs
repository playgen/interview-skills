﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
	public class ScenarioController
	{
		private readonly Core.Scenario.ScenarioController _scenarioCoreController;

		public ScenarioController(Core.Scenario.ScenarioController scenarioCoreController)
		{
			_scenarioCoreController = scenarioCoreController;
		}

		// GET api/scenario/list
		// GET api/scenario/list/[category]
		[HttpGet("list/{category}")]
		public IActionResult List(string category)
		{
			throw new NotImplementedException();
		}

		// GET api/scenario/[category]/[name]/[isSynchronised]
		[HttpGet("{category}/{name}/{isSynchronised:bool}")]
		public IActionResult Get(string category, string name, bool isSynchronised)
		{
			var scenario = _scenarioCoreController.Get(category, name, isSynchronised);
			return new OkObjectResult(scenario);
		}
	}
}
