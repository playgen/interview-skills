﻿using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
    public class JobController
    {
	    private readonly Core.JobController _jobCoreController;

	    public JobController(Core.JobController jobCoreController)
	    {
		    _jobCoreController = jobCoreController;
	    }

		// GET api/job/list
		// GET api/job/list?tags=tag1&tags=tag2
		// GET api/job/list?includes=Theme
		[HttpGet("list")]
	    public IActionResult List(
			string[] tags = null,
			bool requireAllTags = true,
			string[] includes = null)
	    {
			var jobs = _jobCoreController.List(tags, requireAllTags, includes);
		    return new OkObjectResult(jobs);
	    }
    }
}
