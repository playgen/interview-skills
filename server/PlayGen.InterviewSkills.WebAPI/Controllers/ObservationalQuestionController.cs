﻿using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.Controllers
{
	[Route("api/[controller]")]
	public class ObservationalQuestionController
	{
		private readonly Core.Questions.ObservationalQuestionController _coreQuestionController;

		public ObservationalQuestionController(Core.Questions.ObservationalQuestionController coreQuestionController)
		{
			_coreQuestionController = coreQuestionController;
		}

		// GET api/observationalQuestion/list
		// GET api/observationalQuestion/list?features=Happiness
		// GET api/observationalQuestion/list?tags=round1
		// GET api/observationalQuestion/list?jobs=pilot
		[HttpGet("list")]
		public IActionResult List(string[] features = null, string[] tags = null, string[] jobs = null, 
			bool requireAllFeatures = true, bool requireAllTags = true, bool requireAllJobs = true,
			string[] includes = null)
		{
			var questions = _coreQuestionController.List(features, tags, jobs, 
				requireAllFeatures, requireAllTags, requireAllJobs,
				includes);

			return new OkObjectResult(questions);
		}
	}
}
