﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PlayGen.InterviewSkills.Data.Storage;
using Serilog;
using Serilog.Events;

namespace PlayGen.InterviewSkills.WebAPI
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Log.Logger = new LoggerConfiguration()
				.MinimumLevel.Debug()
				.MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
				.Enrich.FromLogContext()
				.WriteTo.Console()
				.WriteTo.File("logs/.log", rollingInterval: RollingInterval.Day)
				.CreateLogger();

			Log.Information("Application Startup.");

			try
			{
				var host = BuildWebHost(args);
				SetupDatabase(host);
				host.Run();
			}
			catch (Exception startupFailure)
			{
				Log.Fatal(startupFailure, "Running Host: {0}");
			}
			finally
			{
				Log.CloseAndFlush();
			}
		}
		
		public static void SetupDatabase(IWebHost host)
		{
			using (var scope = host.Services.CreateScope())
			using (var context = scope.ServiceProvider.GetService<InterviewSkillsContext>())
			{
				context.Database.Migrate();
				context.EnsureSeedData();
			}
		}

		public static IWebHost BuildWebHost(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.UseSerilog()
				.Build();
	}
}