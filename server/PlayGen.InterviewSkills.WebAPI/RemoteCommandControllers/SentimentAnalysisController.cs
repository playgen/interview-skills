﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PlayGen.InterviewSkills.Data.Model.Analysis;
using PlayGen.RemoteCommands;
using PlayGen.RemoteCommands.Command;
using PlayGen.RemoteCommands.Connection.WebSocket;

namespace PlayGen.InterviewSkills.WebAPI.RemoteCommandControllers
{
	public class SentimentAnalysisController
	{
		public const string UpdateId = "SentimentAnalysisResultUpdate";

		private readonly ReaderWriterLockSlim _connectionsForSessionsLock = new ReaderWriterLockSlim();
		private readonly Dictionary<string, List<WebSocketConnection>> _connectionsForSessions = new Dictionary<string, List<WebSocketConnection>>();
		private readonly ConcurrentDictionary<string, WebSocketConnection> _connectionsForUserSessions = new ConcurrentDictionary<string, WebSocketConnection>();
		private readonly ILogger<SentimentAnalysisController> _logger;

		public event Action<string> FirstConnectionsForSessionAddedEvent;
		public event Action<string> AllConnectionsForSessionRemovedEvent;

		public SentimentAnalysisController(ILogger<SentimentAnalysisController> logger)
		{
			_logger = logger;
		}

		[RemoteCommand]
		public bool StartAnalyzing(string sessionId, WebSocketConnection connection)
		{
			var didAdd = false;

			connection.ConnectionClosed += _ => RemoveSessionConnection(sessionId, connection);

			_connectionsForSessionsLock.EnterWriteLock();

			try
			{
				if (!_connectionsForSessions.TryGetValue(sessionId, out var connections))
				{
					_connectionsForSessions.TryAdd(sessionId, new List<WebSocketConnection> { connection });
					Task.Run(() => FirstConnectionsForSessionAddedEvent?.Invoke(sessionId));
					didAdd = true;
				}
				else if (!connections.Contains(connection))
				{
					connections.Add(connection);
					didAdd = true;
				}
			}
			finally
			{
				_connectionsForSessionsLock.ExitWriteLock();
			}

			return didAdd;
		}

		[RemoteCommand]
		public bool StopAnalyzing(string sessionId, WebSocketConnection connection)
		{
			return RemoveSessionConnection(sessionId, connection);
		}

		[RemoteCommand]
		public bool StartListening(string userSessionId, WebSocketConnection connection)
		{
			var didStart = false;

			if (!_connectionsForUserSessions.TryGetValue(userSessionId, out var _))
			{
				_connectionsForUserSessions[userSessionId] = connection;
				connection.ConnectionClosed += _ => _connectionsForUserSessions.TryRemove(userSessionId, out var _);
				didStart = true;
			}

			return didStart;
		}

		[RemoteCommand]
		public bool StopListening(string userSessionId)
		{
			return _connectionsForUserSessions.TryRemove(userSessionId, out var _);
		}

		public void OnAnalysisResultReceived(AnalysisMetadata analysis, Dictionary<string, object> debugInfo)
		{
			_logger.LogDebug($"Got {nameof(AnalysisMetadata)} for Session Id: {analysis.MatchSessionId} and User Session Id: {analysis.UserSessionId}.");

			if (_connectionsForUserSessions.TryGetValue(analysis.UserSessionId.ToString(), out var connection))
			{
				var update = new UpdateResponse
				{
					Id = UpdateId,

					Result = new Dictionary<string, object>
					{
						{"analysis", analysis},
						{"debugInfo", debugInfo}
					}
				};

				connection.SendUpdateResponse(update);

				_logger.LogDebug($"Sent {nameof(AnalysisMetadata)} for Session Id: {analysis.MatchSessionId} and User Session Id: {analysis.UserSessionId}.");
			}
		}

		private bool RemoveSessionConnection(string sessionId, WebSocketConnection connection)
		{
			var didRemove = false;

			_connectionsForSessionsLock.EnterWriteLock();

			try
			{
				if (_connectionsForSessions.TryGetValue(sessionId, out var connections))
				{
					if (connections.Remove(connection))
					{
						didRemove = true;

						if (!connections.Any())
						{
							_connectionsForSessions.Remove(sessionId, out var _);
							Task.Run(() => AllConnectionsForSessionRemovedEvent?.Invoke(sessionId));
						}
					}
				}
			}
			finally
			{
				_connectionsForSessionsLock.ExitWriteLock();
			}

			return didRemove;
		}
	}
}
