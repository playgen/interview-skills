﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlayGen.InterviewSkills.Data.Model.Job;
using PlayGen.RemoteCommands.Command;

namespace PlayGen.InterviewSkills.WebAPI.RemoteCommandControllers
{
    public class JobController
    {
		private readonly Core.JobController _controller;

		public JobController(Core.JobController controller)
		{
			_controller = controller;
		}

		[RemoteCommand]
		public List<Job> List(string[] tags = null, bool requireAllTags = true, string[] includes = null)
		{
			return _controller.List(tags, requireAllTags, includes);
		}

	}
}
