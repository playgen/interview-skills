﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PlayGen.InterviewSkills.Core;
using PlayGen.InterviewSkills.Core.Authentication;
using PlayGen.InterviewSkills.Core.OpenTok;
using PlayGen.InterviewSkills.Core.Scenario;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.SentimentAnalysis.Session;
using System;
using System.Linq;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Playgen.Serialization;
using PlayGen.InterviewSkills.Core.FeedbackResults;
using PlayGen.InterviewSkills.Core.Feedbacks;
using PlayGen.InterviewSkills.Core.Match.MatchMaking;
using PlayGen.InterviewSkills.Core.Match.Session;
using PlayGen.InterviewSkills.Core.QuestionResults;
using PlayGen.InterviewSkills.Core.Questions;
using PlayGen.InterviewSkills.Core.User;
using PlayGen.InterviewSkills.Core.Role;
using PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResponse;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingSubmitter;
using PlayGen.InterviewSkills.SentimentAnalysis.SessionAnalyzer;
using PlayGen.OpenTok.Server;
using PlayGen.RemoteCommands;
using PlayGen.RemoteCommands.Connection;
using PlayGen.RemoteCommands.Connection.WebSocket;
using JsonSerializer = Playgen.Serialization.JsonSerializer;

namespace PlayGen.InterviewSkills.WebAPI
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container
		public void ConfigureServices(IServiceCollection services)
		{
			ConfigureFormatters(services);

			AddSentimentAnalysisControllers(services);

			AddCoreControllers(services);

			AddStorageControllers(services);

			var keepAliveInterval = Configuration.GetValue<int>("WebSocket:KeepAliveInterval");
			services.Configure<WebSocketOptions>(options =>
				options.KeepAliveInterval = TimeSpan.FromMilliseconds(keepAliveInterval));

			AddRemoteCommandControllers(services);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			app.UseDefaultFiles();
			app.UseStaticFiles();
			app.UseMvc();

			app.UseWebSockets(app.ApplicationServices.GetService<IOptions<WebSocketOptions>>().Value);
			app.UseRemoteCommands<WebSocketConnectionMiddleware>();

			ConfigureCoreControllers(app.ApplicationServices);

			ConfigureSentimentAnalysisControllers(app.ApplicationServices);

			ConfigureRemoteCommandControllers(app.ApplicationServices);
		}

		private void AddRemoteCommandControllers(IServiceCollection services)
		{
			services.AddSingleton<RemoteCommandControllers.JobController>();
			services.AddSingleton<RemoteCommandControllers.SentimentAnalysisController>();

			services.AddSingleton<WebSocketConnectionFactory>();
			services.AddSingleton<WebSocketConnectionRouter>();
			services.AddRemoteCommands<CommandRouter, JsonSerializer>();
		}

		private void AddSentimentAnalysisControllers(IServiceCollection services)
		{
			var pruneInterval = Configuration.GetValue<int>("SentimentAnalysis:PruneInterval");
			var sessionMaxTimeout = Configuration.GetValue<int>("SentimentAnalysis:MaxTimeout");
			services.AddSingleton(serviceProvider => new SessionController(
			    pruneInterval, 
			    sessionMaxTimeout, 
			    serviceProvider.GetService<ILogger<SessionController>>()));

			var recordingUriTemplate = Configuration.GetValue<string>("SentimentAnalysis:RecordingUriTemplate");
			var socketReceiveBufferSize = Configuration.GetValue<int>("SentimentAnalysis:SocketReceiveBufferSize");
			services.AddSingleton(serviceProvider => new RecordingReceiverFactory(
				recordingUriTemplate,
				socketReceiveBufferSize,
				serviceProvider.GetService<OpenTokManager>(),
				serviceProvider.GetService<ILoggerFactory>()));

			var analysisUri = Configuration.GetValue<string>("SentimentAnalysis:AnalysisUri");
			services.AddSingleton(serviceProvider => new RecordingSubmitterFactory(
			    analysisUri, 
			    socketReceiveBufferSize, 
			    serviceProvider.GetService<ILoggerFactory>()));

			var voiceActivityKey = Configuration.GetValue<string>("SentimentAnalysis:VoiceActivityKey");
		    var errorsKey = Configuration.GetValue<string>("SentimentAnalysis:ErrorsKey");
            services.AddSingleton(serviceProvider => new AnalysisResponseDeserializerFactory(
                serviceProvider.GetService<ISerializer>(), 
                voiceActivityKey,
                errorsKey,
                serviceProvider.GetService<ILoggerFactory>()));

			services.AddSingleton<SessionAnalyzerFactory>();
			services.AddSingleton<SessionAnalyzerController>();
		}

		private void ConfigureSentimentAnalysisControllers(IServiceProvider serviceProvider)
		{
			var recordingController = serviceProvider.GetService<SessionAnalyzerController>();
			var sessionController = serviceProvider.GetService<SessionController>();

			sessionController.SessionStartedEvent += recordingController.StartRecording;
			sessionController.SessionStoppedEvent += recordingController.StopRecording;

			sessionController.Start();
		}

		private void ConfigureRemoteCommandControllers(IServiceProvider serviceProvider)
		{
			var sentimentAnalysisRemoteCommandController =
				serviceProvider.GetService<RemoteCommandControllers.SentimentAnalysisController>();
			var sentimentAnalysisCoreController = serviceProvider.GetService<SentimentAnalysisController>();

			sentimentAnalysisCoreController.AnalysisResultParsedEvent +=
				sentimentAnalysisRemoteCommandController.OnAnalysisResultReceived;

			var sessionController = serviceProvider.GetService<SessionController>();
			sentimentAnalysisRemoteCommandController.FirstConnectionsForSessionAddedEvent += sessionId => sessionController.Start(sessionId);
			sentimentAnalysisRemoteCommandController.AllConnectionsForSessionRemovedEvent += sessionId => sessionController.Stop(sessionId);
		}

		private void ConfigureCoreControllers(IServiceProvider serviceProvider)
		{
			var matchMakingController = serviceProvider.GetService<MatchMakingController>();
			matchMakingController.Start();

			var scenarioController = serviceProvider.GetService<ScenarioController>();
			matchMakingController.MatchMadeEvent += scenarioController.AddScenario;

			var openTokController = serviceProvider.GetService<OpenTokController>();
			matchMakingController.MatchMadeEvent += openTokController.AddNewOpenTokSession;

			var matchSessionController = serviceProvider.GetService<MatchSessionController>();
			matchMakingController.MatchMadeEvent += matchSessionController.CreateAndAddMatchSession;

			var userSessionController = serviceProvider.GetService<UserSessionController>();

			// Authenticaion
			var googleAuthController = serviceProvider.GetService<GoogleAuthenticationController>();
			googleAuthController.UserAuthenticatedEvent += userSessionController.CreateSession;

			var guestAuthController = serviceProvider.GetService<GuestAuthenticationController>();
			guestAuthController.UserAuthenticatedEvent += userSessionController.CreateSession;

			var emailAuthController = serviceProvider.GetService<EmailAuthenticationController>();
			emailAuthController.UserAuthenticatedEvent += userSessionController.CreateSession;

			// Sentiment Analysis
			var sentimentAnalysisController = serviceProvider.GetService<SentimentAnalysisController>();

			var sessionAnalysisController = serviceProvider.GetService<SessionAnalyzerController>();
			sessionAnalysisController.AnalysisResultReceived += sentimentAnalysisController.AddRange;
		}

		private void ConfigureFormatters(IServiceCollection services)
		{
			var jsonSettings = new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
			};

			jsonSettings.Converters.Add(new StringEnumConverter { CamelCaseText = true });

			jsonSettings.ContractResolver = new DefaultContractResolver
			{
				NamingStrategy = new CamelCaseNamingStrategy
				{
					ProcessDictionaryKeys = false,
					OverrideSpecifiedNames = true
				}
			};

			services.AddSingleton<ISerializer>(new JsonSerializer(jsonSettings));

			services.AddMvcCore()
				.AddJsonFormatters(settings =>
				{
					settings.TypeNameHandling = jsonSettings.TypeNameHandling;
					settings.ReferenceLoopHandling = jsonSettings.ReferenceLoopHandling;
					settings.ContractResolver = jsonSettings.ContractResolver;
					settings.Converters = jsonSettings.Converters;
				});
		}

		private void AddStorageControllers(IServiceCollection services)
		{
			var connectionString = Configuration.GetConnectionString("InterviewSkillsDatabase");
			services.AddSingleton(serviceProvider => new InterviewSkillsContextFactory(
				connectionString,
				serviceProvider.GetService<ILoggerFactory>()));

			services.AddDbContext<InterviewSkillsContext>((serviceProvider, options) =>
				serviceProvider.GetService<InterviewSkillsContextFactory>().ApplyOptions(options));
		}

		private void AddCoreControllers(IServiceCollection services)
		{
			// Authentication
			services.AddSingleton<GoogleAuthenticationController>();
			services.AddSingleton<GuestAuthenticationController>();
			services.AddSingleton<EmailAuthenticationController>();

			// Feedback Results
			services.AddSingleton<FeatureActionableFeedbackSystemResultController>();

			// Feedbacks
			services.AddSingleton<FeatureActionableFeedbackController>();
			services.AddSingleton<FeatureSummaryFeedbackController>();
			services.AddSingleton<ObservationalQuestionFeedbackController>();

			// Match
			services.AddSingleton<MatchMakingParticipantUserDataFactory>();
			services.AddSingleton<MatchMakingParticipantPreviouslyMatchedUsersDataFactory>();

			var unmatchedKeepAliveTimeout = Configuration.GetValue<int>("MatchMaking:UnmatchedKeepAliveTimeout");
			var unmatchedPruneInterval = Configuration.GetValue<int>("MatchMaking:UnmatchedPruneInterval");

			services.AddSingleton(serviceProvider => new MatchMakingController(
				serviceProvider.GetService<MatchMakingParticipantUserDataFactory>(),
				serviceProvider.GetService<MatchMakingParticipantPreviouslyMatchedUsersDataFactory>(),
				unmatchedKeepAliveTimeout,
				unmatchedPruneInterval));

			services.AddSingleton<MatchSessionController>();

			// Open Tok
			var openTokApiKey = Configuration.GetValue<int>("OpenTok:APIKey");
			var openTokApiSecret = Configuration.GetValue<string>("OpenTok:APISecret");
			services.AddSingleton(new OpenTokManager(openTokApiKey, openTokApiSecret));
			services.AddSingleton<OpenTokController>();

			// Question Results
			services.AddSingleton<InterviewQuestionResultController>();
			services.AddSingleton<ObservationalQuestionResultController>();

			// Questions 
			services.AddSingleton<InterviewQuestionController>();
			services.AddSingleton<ObservationalQuestionController>();

			// Role
			services.AddSingleton<RoleController>();
			services.AddSingleton<RoleSessionController>();

			// Scenario
			services.AddSingleton<ScenarioController>();

			// Sentiment Analysis
			services.AddSingleton<SentimentAnalysisController>();

			// User
			services.AddSingleton<UserSessionController>();

			// Other
			services.AddSingleton<JobController>();
		}
	}
}
