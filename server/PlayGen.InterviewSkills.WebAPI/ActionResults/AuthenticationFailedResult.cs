﻿using System;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.ActionResults
{
	public class AuthenticationFailedResult : UnauthorizedResult
    {
		public string Reason { get; }

	    public AuthenticationFailedResult(string reason)
	    {
		    Reason = reason.Replace(Environment.NewLine, "");
	    }

	    public override void ExecuteResult(ActionContext context)
	    {
		    base.ExecuteResult(context);
		    context.HttpContext.Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = Reason;
		}
	}
}
