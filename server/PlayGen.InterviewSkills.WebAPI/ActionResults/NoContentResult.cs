﻿using System;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;

namespace PlayGen.InterviewSkills.WebAPI.ActionResults
{
	public class NoContentResult : Microsoft.AspNetCore.Mvc.NoContentResult
    {
		public string Reason { get; }

	    public NoContentResult(string reason)
	    {
		    Reason = reason.Replace(Environment.NewLine, "");
	    }

	    public override void ExecuteResult(ActionContext context)
	    {
		    base.ExecuteResult(context);
		    context.HttpContext.Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = Reason;
		}
	}
}
