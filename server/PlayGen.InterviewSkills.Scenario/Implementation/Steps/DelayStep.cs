﻿using PlayGen.Scenario.Step;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps
{
	public class DelayStep : Step
	{
		public DelayStep(float delay)
		{
			SetDelay((int)delay);
		}

		public DelayStep(int delay)
		{
			SetDelay(delay);
		}

		/// <summary>
		/// Used for deserialization
		/// </summary>
		private DelayStep()
		{
		}

		private void SetDelay(int delay)
		{
			Actions = new[]
			{
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstTrue,
						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[] {new IsConditionElapsedInterval {Interval = delay}}
				}
			};
		}
	}
}