﻿using PlayGen.InterviewSkills.Scenario.Model.Commands;
using PlayGen.InterviewSkills.Scenario.Model.Commands.FeatureActionableFeedback;
using PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewQuestion;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.FeatureActionableFeedback;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.InterviewQuestion;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.SentimentAnalysis;
using PlayGen.Scenario.Step;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps
{
	public sealed class QuestionsStep : Step
	{
		public QuestionsStep(string[] interviewQuestionTags, float duration, float timeScale = 1)
		{
			Actions = new[]
			{
				// Keep blocked until question duration elapses
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstTrue,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[]
					{
						new Conditions.IsConditionElapsedInterval(duration)
					}
				},

				// Show interview questions while the duration hasn't elapsed
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.Never,

						BlockingType = BlockingType.None
					},

					Conditions = new ICondition[]
					{
						new None
						{
							Conditions = new ICondition[]
							{
								new IsInterviewQuestionShown(),

								new Conditions.IsConditionElapsedInterval(duration),
							}
						}
					},

					OnConditionCommands = new IOnConditionCommand[]
					{
						// Answering
						new ShowInterviewQuestion
						{
							State = InterviewQuestionStates.Answering,

							Tags = interviewQuestionTags,

							Options = new[] {"answered"}
						},

						new EnqueueActions
						{
							Actions = new[]
							{
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.FirstTrue,

										BlockingType = BlockingType.Step
									},

									Conditions = new ICondition[]
									{
										new Any
										{
											Conditions = new ICondition[]
											{
												new IsInterviewQuestionSelection {Selection = new[] {"answered"}},

												new HasInterviewQuestionDurationElapsed()
											}
										}
									},

									OnConditionCommands = new IOnConditionCommand[]
									{
										// Answered
										new UpdateInterviewQuestion
										{
											State = InterviewQuestionStates.Answered,

											Options = new[] {"next"}
										},

										new EnqueueActions
										{
											Actions = new[]
											{
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstTrue,

														BlockingType = BlockingType.Step
													},

													Conditions = new ICondition[]
													{
														new Any
														{
															Conditions = new ICondition[]
															{
																new IsInterviewQuestionSelection {Selection = new[] {"next"}},

																new Conditions.IsConditionElapsedInterval(10 * timeScale)
															}
														}
													},

													OnConditionCommands = new IOnConditionCommand[]
													{
														new HideInterviewQuestion()
													}
												}
											}
										}
									}
								}
							}
						}
					}
				},

				// Periodically check for Analysis Results
				new Action
				{
					Config = new Config
					{
						BlockingType = BlockingType.None,

						DiscardType = DiscardType.Never
					},

					Conditions = new ICondition[] { new Conditions.IsConditionElapsedInterval(20 * timeScale, repeatCount: -1) },

					OnConditionCommands = new IOnConditionCommand[]
					{
						new EnqueueActions
						{
							Actions = new []
							{
								new Action
								{
									Config = new Config
									{
										BlockingType = BlockingType.None,

										DiscardType = DiscardType.FirstEvaluation
									},

									Conditions = new ICondition[]
									{
										new HasNewSentimentAnalysisResult()
									},

									OnConditionCommands = new IOnConditionCommand[]
									{
										// Get Feature Actionable Feedback
										new GetFeatureActionableFeedback(),

										new EnqueueActions
										{
											Actions = new []
											{
												// Show Feature Actionable Feedback
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstTrue,

														BlockingType = BlockingType.None
													},

													Conditions = new ICondition[]
													{
														new HasFeatureActionableFeedback()
													},

													OnConditionCommands = new IOnConditionCommand[]
													{
														new ShowFeatureActionableFeedback(),

														new EnqueueActions
														{
															Actions = new []
															{
																// Hide Feature Actionable Feedback
																new Action
																{
																	Config = new Config
																	{
																		DiscardType = DiscardType.FirstTrue,

																		BlockingType = BlockingType.None
																	},

																	Conditions = new ICondition[]
																	{
																		new Any
																		{
																			Conditions = new ICondition[]
																			{
																				new None
																				{
																					Conditions = new ICondition[]
																					{
																						new IsFeatureActionableFeedbackSelection
																						{
																							Selection = Selection.NoSelection
																						}
																					}
																				},

																				new Conditions.IsConditionElapsedInterval(20 * timeScale),
																			}
																		}
																	},

																	OnDiscardCommands = new IOnDiscardCommand[]
																	{
																		new HideFeatureActionableFeedback()
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								},
							}
						}
					}
				}
			};
		}
	}
}