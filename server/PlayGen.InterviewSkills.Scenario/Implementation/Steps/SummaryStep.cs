﻿using PlayGen.InterviewSkills.Scenario.Model.Commands;
using PlayGen.InterviewSkills.Scenario.Model.Commands.CandidateInterviewSummary;
using PlayGen.InterviewSkills.Scenario.Model.Conditions;
using PlayGen.Scenario.Step;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps
{
	public sealed class SummaryStep : Step
	{
		public SummaryStep()
		{
			Actions = new[]
			{
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					OnConditionCommands = new IOnConditionCommand[]
					{
						new ShowCandidateInterviewSummary { Options = new [] {"quit", "new game"} },

						new EnqueueActions
						{
							Actions = new []
							{
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.FirstTrue,

										BlockingType = BlockingType.Step
									},

									Conditions = new ICondition[]
									{
										new None
										{
											Conditions = new ICondition[]
											{
												new IsCandidateInterviewSummarySelection{ Selection = Selection.NoSelection }
											}
										}
									},

									OnConditionCommands = new IOnConditionCommand[]
									{
										new HideCandidateInterviewSummary(),

										new EnqueueActions
										{
											Actions = new []
											{
												// Stop
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstEvaluation,

														BlockingType = BlockingType.Step
													},

													Conditions = new ICondition[] { new IsCandidateInterviewSummarySelection { Selection = new[] {"quit"} } },

													OnConditionCommands = new IOnConditionCommand[] { new Stop() }
												},

												// New Game
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstEvaluation,

														BlockingType = BlockingType.Step
													},

													Conditions = new ICondition[] { new IsCandidateInterviewSummarySelection { Selection = new[] {"new game"} } },

													OnConditionCommands = new IOnConditionCommand[] { new NewGame() }
												}
											}
										}
									}
								}
							}
						}
					}
				}
			};
		}
	}
}
