﻿using PlayGen.InterviewSkills.Scenario.Model.Commands;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Job;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Waiting;
using PlayGen.InterviewSkills.Scenario.Model.Conditions;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.Job;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps.Synchronised
{
	public sealed class JobSelectStep : OtherLeftStep
	{
		public JobSelectStep(bool masterIsCandidate)
		{
			Actions = new[]
			{
				// Candidate: Job Selection
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[]
					{
						new IsMaster {Value = masterIsCandidate}
					},

					OnConditionCommands = new IOnConditionCommand[]
					{
						new ShowJobSelect(),

						new EnqueueActions
						{
							Actions = new[]
							{
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.FirstTrue,

										BlockingType = BlockingType.Step
									},

									Conditions = new ICondition[]
									{
										new None {Conditions = new ICondition[] {new IsJobSelection {Selection = Selection.NoSelection}}}
									},

									OnConditionCommands = new IOnConditionCommand[] {new HideJobSelect()}
								}
							}
						}
					}
				},

				// Interviewer: Block until a job has been selected
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[]
					{
						new IsMaster {Value = !masterIsCandidate}
					},

					OnConditionCommands = new IOnConditionCommand[]
					{
						new ShowWaiting {Body = "Waiting for the Candidate to select a job to interview for..."},

						new EnqueueActions
						{
							Actions = new[]
							{
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.FirstTrue,

										BlockingType = BlockingType.Step
									},

									Conditions = new ICondition[]
									{
										new HasJobResult()
									},

									OnConditionCommands = new IOnConditionCommand[]
									{
										new ApplyJobResult(),

										new EnqueueActions
										{
											Actions = new[]
											{
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstTrue,

														BlockingType = BlockingType.Step
													},

													Conditions = new ICondition[] 
													{
														new None {Conditions = new ICondition[] {new IsJobSelection {Selection = Selection.NoSelection}}}
													},

													OnConditionCommands = new IOnConditionCommand[] {new HideWaiting()}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			};
		}
	}
}
