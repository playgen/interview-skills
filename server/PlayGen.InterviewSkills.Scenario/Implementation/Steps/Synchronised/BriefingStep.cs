﻿using PlayGen.InterviewSkills.Scenario.Model.Commands;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Brief;
using PlayGen.InterviewSkills.Scenario.Model.Commands.SentimentAnalysis;
using PlayGen.InterviewSkills.Scenario.Model.Conditions;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps.Synchronised
{
	public sealed class BriefingStep : OtherLeftStep
	{
		public BriefingStep(bool masterIsCandidate)
		{
			Actions = new[]
			{
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					OnConditionCommands = new IOnConditionCommand[]
					{
						new ShowSentimentAnalysis()
					}
				},

			    // Candidate Briefing
			    new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[] {new IsMaster {Value = masterIsCandidate}},

					OnConditionCommands = new IOnConditionCommand[]
					{
						new SetRole{ RoleId = "candidate"},
						new ShowBrief {Title = "You are the candidate applying for a job as:"}
					}
				},

			    // Interviewer Briefing
			    new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[] {new IsMaster {Value = !masterIsCandidate } },

					OnConditionCommands = new IOnConditionCommand[]
					{
						new SetRole{ RoleId = "interviewer"},
						new ShowBrief {Title = "You are the interviewer hiring for a role as:"}
					},
				},
			};
		}
	}
}
