﻿using System.Linq;
using PlayGen.Scenario.Step.Action;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps.Synchronised
{
	public class DelayStep : Steps.DelayStep
	{
		private Action[] _actions;

		public override Action[] Actions
		{
			get => _actions;
			set
			{
				_actions = new OtherLeftStep().Actions;
				_actions = _actions.Concat(value).ToArray();
			}
		}

		public DelayStep(float delay) : base(delay)
		{
		}

		public DelayStep(int delay) : base(delay)
		{
		}
	}
}