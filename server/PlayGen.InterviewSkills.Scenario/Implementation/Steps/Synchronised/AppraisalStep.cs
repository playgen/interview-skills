﻿using PlayGen.InterviewSkills.Scenario.Model.Commands;
using PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewerCandidateAppraisal;
using PlayGen.InterviewSkills.Scenario.Model.Commands.PostInterviewSwapPrompt;
using PlayGen.InterviewSkills.Scenario.Model.Commands.PostInterviewWaiting;
using PlayGen.InterviewSkills.Scenario.Model.Conditions;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.InterviewerCandidateAppraisal;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps.Synchronised
{
	public sealed class AppraisalStep : OtherLeftStep
	{
		public AppraisalStep(bool masterIsCandidate, string[] options, bool showCandidateSwapPrompt)
		{
			Actions = new[]
			{
			    // Interviewer Appraisal of Candidate
			    new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[] {new IsMaster {Value = !masterIsCandidate}},

					OnConditionCommands = new IOnConditionCommand[]
					{
						// Show appraisal until something is selected. If it is stop, then issue the stop command
						new ShowInterviewerCandidateAppraisal { Options = options },

						new EnqueueActions
						{
							Actions = new []
							{
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.FirstTrue,

										BlockingType = BlockingType.Step
									},

									Conditions = new ICondition[]
									{
										new None
										{
											Conditions = new ICondition[]
											{
												new IsInterviewerCandidateAppraisalSelection { Selection = Selection.NoSelection }
											}
										}
									},

									OnConditionCommands = new IOnConditionCommand[]
									{
										new HideInterviewerCandidateAppraisal(),

										new EnqueueActions
										{
											Actions = new []
											{
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstEvaluation,

														BlockingType = BlockingType.Step
													},

													Conditions = new ICondition[] { new IsInterviewerCandidateAppraisalSelection { Selection = new [] {"quit"} } },

													OnConditionCommands = new IOnConditionCommand[] { new Stop() }
												}
											}
										}
									}
								}
							}
						}
					}
				},

				// Candidate waiting for Interviewer Appraisal
				new Action
				{

					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[] {new IsMaster {Value = masterIsCandidate}},

					OnConditionCommands = showCandidateSwapPrompt
						// Show prompt until something is selected. If it is stop, then issue the stop command
						? new IOnConditionCommand[]
							{
								new ShowPostInterviewSwapPrompt { Options = options },

								new EnqueueActions
								{
									Actions = new []
									{
										new Action
										{
											Config = new Config
											{
												DiscardType = DiscardType.FirstTrue,

												BlockingType = BlockingType.None
											},

											Conditions = new ICondition[]
											{
												new None
												{
													Conditions = new ICondition[]
													{
														new IsPostInterviewSwapPromptSelection { Selection = Selection.NoSelection }
													}
												}
											},

											OnConditionCommands = new IOnConditionCommand[]
											{
												new EnqueueActions
												{
													Actions = new []
													{
														new Action
														{
															Config = new Config
															{
																DiscardType = DiscardType.FirstEvaluation,

																BlockingType = BlockingType.Step
															},

															Conditions = new ICondition[] { new IsPostInterviewSwapPromptSelection { Selection = new [] {"quit"} } },

															OnConditionCommands = new IOnConditionCommand[] { new Stop() }
														},

														new Action
														{
															Config = new Config
															{
																DiscardType = DiscardType.FirstEvaluation,

																BlockingType = BlockingType.Step
															},

															Conditions = new ICondition[]
															{
																new None
																{
																	Conditions = new ICondition[]
																	{
																		new IsPostInterviewSwapPromptSelection { Selection = new [] {"quit"} }
																	}
																}
															},

															OnConditionCommands = new IOnConditionCommand[]
															{
																new ShowPostInterviewWaiting(),

																new EnqueueActions
																{
																	Actions = new []
																	{
																		new Action
																		{
																			Config = new Config
																			{
																				DiscardType = DiscardType.FirstTrue,

																				BlockingType = BlockingType.Step
																			},

																			Conditions = new ICondition[] {new HasInterviewerCandidateAppraisal() },

																			OnDiscardCommands = new IOnDiscardCommand[] { new HidePostInterviewWaiting() }
																		}
																	}
																}
															}
														}
													}
												}
											},

											OnDiscardCommands = new IOnDiscardCommand[] { new HidePostInterviewSwapPrompt() }
										}
									}
								}
							}
						// Go straight to showing waiting
						: new IOnConditionCommand[]
						{
							new ShowPostInterviewWaiting(),

							new EnqueueActions
							{
								Actions = new []
								{
									new Action
									{
										Config = new Config
										{
											DiscardType = DiscardType.FirstTrue,

											BlockingType = BlockingType.Step
										},

										Conditions = new ICondition[] {new HasInterviewerCandidateAppraisal() },

										OnDiscardCommands = new IOnDiscardCommand[] { new HidePostInterviewWaiting() }
									}
								}
							}
						}
				}
			};
		}

		/// <summary>
		/// Used for deserialization
		/// </summary>
		private AppraisalStep()
		{
		}
	}
}
