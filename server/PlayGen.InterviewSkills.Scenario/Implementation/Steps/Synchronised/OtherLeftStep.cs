﻿using System.Linq;
using PlayGen.InterviewSkills.Scenario.Model.Commands;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Brief;
using PlayGen.InterviewSkills.Scenario.Model.Commands.SentimentAnalysis;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Synchronisation;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Title;
using PlayGen.InterviewSkills.Scenario.Model.Commands.VideoCalling;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.Synchronisation;
using PlayGen.Scenario.Step;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;
using PlayGen.Scenario.Synchronised.Conditions;
using Action = PlayGen.Scenario.Step.Action.Action;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps.Synchronised
{
	public class OtherLeftStep : Step
	{
		private Action[] _actions;

		public override Action[] Actions
		{
			get => _actions;
			set
			{
				_actions = new[]
				{
					new Action
					{
						Config = new Config
						{
							BlockingType = BlockingType.None,

							DiscardType = DiscardType.FirstTrue
						},

						Conditions = new ICondition[]
						{
							new HasOtherLeft()
						},

						OnConditionCommands = new IOnConditionCommand[]
						{
							new StopVideoCalling(),

							new HideTitle(),

							new HideBrief(),

							new HideSentimentAnalysis(),

							new ShowOtherLeft
							{
								Title= "Other player left.",

								Body = "The other player has left the match or lost connection.",

								Options = new[] {"quit", "new game"}
							},

							new EnqueueActions
							{
								Actions = new[]
								{
									new Action
									{
										Config = new Config
										{
											BlockingType = BlockingType.Step,

											DiscardType = DiscardType.FirstTrue
										},

										Conditions = new ICondition[]
										{
											new None
											{
												Conditions = new ICondition[]
												{
													new IsOtherLeftSelection {Selection = Selection.NoSelection}
												}
											}
										},

										OnConditionCommands = new IOnConditionCommand[]
										{
											new EnqueueActions
											{
												Actions = new []
												{
													// Stop
													new Action
													{
														Config = new Config
														{
															DiscardType = DiscardType.FirstEvaluation,

															BlockingType = BlockingType.Step
														},

														Conditions = new ICondition[] { new IsOtherLeftSelection { Selection = new[] {"quit"} } },

														OnConditionCommands = new IOnConditionCommand[] { new Stop() }
													},

													// New Game
													new Action
													{
														Config = new Config
														{
															DiscardType = DiscardType.FirstEvaluation,

															BlockingType = BlockingType.Step
														},

														Conditions = new ICondition[] { new IsOtherLeftSelection { Selection = new[] {"new game"} } },

														OnConditionCommands = new IOnConditionCommand[] { new NewGame() }
													}
												}
											}
										},

										OnDiscardCommands = new IOnDiscardCommand[]
										{
											new HideOtherLeft()
										}
									}
								}
							}
						}
					}
				};

				_actions = _actions.Concat(value).ToArray();
			}
		}
	}
}
