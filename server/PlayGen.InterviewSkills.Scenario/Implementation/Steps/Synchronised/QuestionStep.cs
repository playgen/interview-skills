﻿using PlayGen.InterviewSkills.Scenario.Model.Commands;
using PlayGen.InterviewSkills.Scenario.Model.Commands.FeatureActionableFeedback;
using PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewQuestion;
using PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewQuestionRespond;
using PlayGen.InterviewSkills.Scenario.Model.Commands.ObservationalQuestion;
using PlayGen.InterviewSkills.Scenario.Model.Commands.ObservationalQuestionFeedback;
using PlayGen.InterviewSkills.Scenario.Model.Conditions;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.FeatureActionableFeedback;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.InterviewQuestion;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.ObservationalQuestion;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.ObservationalQuestionFeedback;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.SentimentAnalysis;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.Synchronisation;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Steps.Synchronised
{
	public sealed class QuestionsStep : OtherLeftStep
	{
		public QuestionsStep(bool masterIsCandidate, string[] interviewQuestionTags, float duration, float timeScale = 1)
		{
			Actions = new[]
			{
				// Interviewer
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[]
					{
						new IsMaster {Value = !masterIsCandidate }
					},

					OnConditionCommands = new IOnConditionCommand[]
					{
						new EnqueueActions
						{
							Actions = new []
							{
								// Keep blocked until question duration elapses
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.FirstTrue,

										BlockingType = BlockingType.Step
									},

									Conditions = new ICondition[]
									{
										new Conditions.IsConditionElapsedInterval(duration)
									}
								},

								// Show interview questions while the duration hasn't elapsed
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.Never,

										BlockingType = BlockingType.None
									},

									Conditions = new ICondition[]
									{
										new None
										{
											Conditions = new ICondition[]
											{
												new IsInterviewQuestionShown(),

												new Conditions.IsConditionElapsedInterval(duration),
											}
										}
									},

									OnConditionCommands = new IOnConditionCommand[]
									{
										// Asking 
										new ShowInterviewQuestion
										{
											State = InterviewQuestionStates.Asking,

											Tags = interviewQuestionTags,

											Options = new[] {"asked"}
										},

										new EnqueueActions
										{
											Actions = new[]
											{
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstTrue,

														BlockingType = BlockingType.Step
													},

													Conditions = new ICondition[]
													{
														new Any
														{
															Conditions = new ICondition[]
															{
																new IsInterviewQuestionSelection {Selection = new[] {"asked"}},

																new Conditions.IsConditionElapsedInterval(10 * timeScale)
															}
														}
													},

													OnConditionCommands = new IOnConditionCommand[]
													{
														// Answering
														new UpdateInterviewQuestion
														{
															State = InterviewQuestionStates.Answering,

															Options = new[] {"answered"}
														},

														new EnqueueActions
														{
															Actions = new[]
															{
																new Action
																{
																	Config = new Config
																	{
																		DiscardType = DiscardType.FirstTrue,

																		BlockingType = BlockingType.Step
																	},

																	Conditions = new ICondition[]
																	{
																		new Any
																		{
																			Conditions = new ICondition[]
																			{
																				new IsInterviewQuestionSelection {Selection = new[] {"answered"}},

																				new HasInterviewQuestionDurationElapsed()
																			}
																		}
																	},

																	OnConditionCommands = new IOnConditionCommand[]
																	{
																		// Answered
																		new UpdateInterviewQuestion
																		{
																			State = InterviewQuestionStates.Answered,

																			Options = new[] {"next"}
																		},

																		new EnqueueActions
																		{
																			Actions = new[]
																			{
																				new Action
																				{
																					Config = new Config
																					{
																						DiscardType = DiscardType.FirstTrue,

																						BlockingType = BlockingType.Step
																					},

																					Conditions = new ICondition[]
																					{
																						new Any
																						{
																							Conditions = new ICondition[]
																							{
																								new IsInterviewQuestionSelection {Selection = new[] {"next"}},

																								new Conditions.IsConditionElapsedInterval(10 * timeScale)
																							}
																						}
																					},

																					OnConditionCommands = new IOnConditionCommand[]
																					{
																						new HideInterviewQuestion()
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								},

								// Show observational questions while the duration hasn't elapsed
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.Never,

										BlockingType = BlockingType.None
									},

									Conditions = new ICondition[]
									{
										new None { Conditions = new ICondition[] { new Conditions.IsConditionElapsedInterval(duration) } },

										new Conditions.IsConditionElapsedInterval(40 * timeScale, -1)
									},

									OnConditionCommands = new IOnConditionCommand[]
									{
										// Get Observational Question
										new GetObservationalQuestion(),

										new EnqueueActions
										{
											Actions = new []
											{
												// Show Observational Question Evaluation
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstTrue,

														BlockingType = BlockingType.None
													},

													Conditions = new ICondition[]
													{
														new HasObservationalQuestion()
													},

													OnConditionCommands = new IOnConditionCommand[]
													{
														new ShowObservationalQuestion(),

														new EnqueueActions
														{
															Actions = new []
															{
																// Hide Observational Question Evaluation
																new Action
																{
																	Config = new Config
																	{
																		DiscardType = DiscardType.FirstTrue,

																		BlockingType = BlockingType.None
																	},

																	Conditions = new ICondition[]
																	{
																		new None
																		{
																			Conditions = new ICondition[]
																			{
																				new IsObservationalQuestionSelection
																				{
																					Selection = Selection.NoSelection
																				}
																			}
																		}
																	},

																	OnDiscardCommands = new IOnDiscardCommand[]
																	{
																		new HideObservationalQuestion()
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				},

				// Candidate
				new Action
				{
					Config = new Config
					{
						DiscardType = DiscardType.FirstEvaluation,

						BlockingType = BlockingType.Step
					},

					Conditions = new ICondition[]
					{
						new IsMaster {Value = masterIsCandidate}
					},

					OnConditionCommands = new IOnConditionCommand[]
					{
						new EnqueueActions
						{
							Actions = new []
							{
								// Block while interviewer is blocked
								new Action
								{
									Config = new Config
									{
										DiscardType = DiscardType.FirstTrue,

										BlockingType = BlockingType.Step
									},

									Conditions = new ICondition[]
									{
										new IsOtherBlocked() // Beacuase other has questoin active
									}
								},

								// Show respond interview questions
								new Action
								{
									Config = new Config
									{
										BlockingType = BlockingType.None,

										DiscardType = DiscardType.Never
									},

									Conditions = new ICondition[] {new HasInterviewQuestionRespond() },

									OnConditionCommands = new IOnConditionCommand[] { new ShowInterviewQuestionRespond() },

									OnDiscardCommands = new IOnDiscardCommand[] { new HideInterviewQuestionRespond() },
								},

								// Periodically check for Analysis Results
								new Action
								{
									Config = new Config
									{
										BlockingType = BlockingType.None,

										DiscardType = DiscardType.Never
									},

									Conditions = new ICondition[] { new Conditions.IsConditionElapsedInterval(20 * timeScale, repeatCount: -1) },

									OnConditionCommands = new IOnConditionCommand[]
									{
										new EnqueueActions
										{
											Actions = new []
											{
												new Action
												{
													Config = new Config
													{
														BlockingType = BlockingType.None,

														DiscardType = DiscardType.FirstEvaluation
													},

													Conditions = new ICondition[]
													{
														new HasNewSentimentAnalysisResult()
													},

													OnConditionCommands = new IOnConditionCommand[]
													{
														// Get Feature Actionable Feedback
														new GetFeatureActionableFeedback(),

														new EnqueueActions
														{
															Actions = new []
															{
																// Show Feature Actionable Feedback
																new Action
																{
																	Config = new Config
																	{
																		DiscardType = DiscardType.FirstTrue,

																		BlockingType = BlockingType.None
																	},

																	Conditions = new ICondition[]
																	{
																		new HasFeatureActionableFeedback()
																	},

																	OnConditionCommands = new IOnConditionCommand[]
																	{
																		new ShowFeatureActionableFeedback(),

																		new EnqueueActions
																		{
																			Actions = new []
																			{
																				// Hide Feature Actionable Feedback
																				new Action
																				{
																					Config = new Config
																					{
																						DiscardType = DiscardType.FirstTrue,

																						BlockingType = BlockingType.None
																					},

																					Conditions = new ICondition[]
																					{
																						new Any
																						{
																							Conditions = new ICondition[]
																							{
																								new None
																								{
																									Conditions = new ICondition[]
																									{
																										new IsFeatureActionableFeedbackSelection
																										{
																											Selection = Selection.NoSelection
																										}
																									}
																								},

																								new Conditions.IsConditionElapsedInterval(20 * timeScale),
																							}
																						}
																					},

																					OnDiscardCommands = new IOnDiscardCommand[]
																					{
																						new HideFeatureActionableFeedback()
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
											}
										}
									}
								},

								// Check for Observational Question Results
								new Action
								{
									Config = new Config
									{
										BlockingType = BlockingType.None,

										DiscardType = DiscardType.Never
									},

									Conditions = new ICondition[]
									{
										new HasObservationalQuestionResult()
									},

									OnConditionCommands = new IOnConditionCommand[]
									{
										// Get Observational Question Feedback
										new GetObservationalQuestionFeedback(),

										new EnqueueActions
										{
											Actions = new []
											{
												// Show Observational Question Feedback
												new Action
												{
													Config = new Config
													{
														DiscardType = DiscardType.FirstTrue,

														BlockingType = BlockingType.None
													},

													Conditions = new ICondition[]
													{
														new HasObservationalQuestionFeedback()
													},

													OnConditionCommands = new IOnConditionCommand[]
													{
														new ShowObservationalQuestionFeedback(),

														new EnqueueActions
														{
															Actions = new []
															{
																// Hide Observational Question Feedback
																new Action
																{
																	Config = new Config
																	{
																		DiscardType = DiscardType.FirstTrue,

																		BlockingType = BlockingType.None
																	},

																	Conditions = new ICondition[]
																	{
																		new Any
																		{
																			Conditions = new ICondition[]
																			{
																				new None
																				{
																					Conditions = new ICondition[]
																					{
																						new IsObservationalQuestionFeedbackSelection
																						{
																							Selection = Selection.NoSelection
																						}
																					}
																				},

																				new Conditions.IsConditionElapsedInterval(20 * timeScale),
																			}
																		}
																	},

																	OnDiscardCommands = new IOnDiscardCommand[]
																	{
																		new HideObservationalQuestionFeedback()
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			};
		}
	}
}