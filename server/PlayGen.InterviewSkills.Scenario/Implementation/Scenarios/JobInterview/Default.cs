﻿using PlayGen.InterviewSkills.Scenario.Implementation.Steps;
using PlayGen.InterviewSkills.Scenario.Model.Commands;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Brief;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Job;
using PlayGen.InterviewSkills.Scenario.Model.Commands.SentimentAnalysis;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Title;
using PlayGen.InterviewSkills.Scenario.Model.Commands.VideoCalling;
using PlayGen.InterviewSkills.Scenario.Model.Conditions.Job;
using PlayGen.Scenario.Step;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;
using PlayGen.Scenario.Step.Action.Config;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Scenarios.JobInterview
{
	public class Default : Model.Scenario
	{
		private const float TimeScale = 1;

		public Default()
		{
			Category = "JobInterview";

			Name = nameof(Default);

			IsSynchronised = false;

			Steps = new[]
			{
				new Step
				{
					Id = "Show Title",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new ShowTitle()
							}
						}
					}
				},

				new Step
				{
					Id = "Job Select",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new ShowJobSelect(),

								new EnqueueActions
								{
									Actions = new[]
									{
										new Action
										{
											Config = new Config
											{
												DiscardType = DiscardType.FirstTrue,

												BlockingType = BlockingType.Step
											},

											Conditions = new ICondition[]
											{
												new None {Conditions = new ICondition[] {new IsJobSelection {Selection = Selection.NoSelection}}}
											},

											OnConditionCommands = new IOnConditionCommand[] {new HideJobSelect()}
										}
									}
								}
							}
						}
					}
				},

				new Step
				{
					Id = "Setup",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new StartVideoCalling(),

								new ShowSentimentAnalysis(),

								new SetRole{ RoleId = "candidate"},

								new ShowBrief {Title = "You are applying for a job as:"}
							}
						}
					}
				},

				new DelayStep(3 * TimeScale) {Id = "Pre Question Delay" },

				new QuestionsStep(interviewQuestionTags: new[] {"character"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Character Questions" },

				new QuestionsStep(interviewQuestionTags: new[] {"careergoal"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Career Goal Questions" },

				new QuestionsStep(interviewQuestionTags: new[] {"competency"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Competency Questions" },

				new QuestionsStep(interviewQuestionTags: new[] {"curveball"}, duration: 10 * TimeScale, timeScale: TimeScale) { Id = "Curveball Questions" },

				new Step
				{
					Id = "Stop Video Calling",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new HideSentimentAnalysis(),

								new HideBrief(),

								new HideTitle(),

								new StopVideoCalling(),
							}
						}
					}
				},

				new SummaryStep
				{
					Id = "Summary"
				}
			};
		}
	}
}
