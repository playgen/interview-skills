﻿using PlayGen.InterviewSkills.Scenario.Implementation.Steps;
using PlayGen.InterviewSkills.Scenario.Implementation.Steps.Synchronised;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Brief;
using PlayGen.InterviewSkills.Scenario.Model.Commands.SentimentAnalysis;
using PlayGen.InterviewSkills.Scenario.Model.Commands.Title;
using PlayGen.InterviewSkills.Scenario.Model.Commands.VideoCalling;
using PlayGen.Scenario.Step;
using PlayGen.Scenario.Step.Action;
using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Config;
using QuestionsStep = PlayGen.InterviewSkills.Scenario.Implementation.Steps.Synchronised.QuestionsStep;

namespace PlayGen.InterviewSkills.Scenario.Implementation.Scenarios.JobInterview.Synchronised
{
	public class Default : Model.Scenario
	{
		private const float TimeScale = 1;

		public Default()
		{
			Category = "JobInterview";

			Name = nameof(Default);

			IsSynchronised = true;

			Steps = new Step[]
			{
				// -------------------------------------------------------------------------------------------------------------------
				// Playthrough 1: Master is the Candidate, Slave is the Interviewer
				// -------------------------------------------------------------------------------------------------------------------
				new OtherLeftStep
				{
					Id = "Show Title",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new ShowTitle()
							}
						}
					}
				},

				new JobSelectStep(masterIsCandidate: true)
				{
					Id = "Job Select 1"
				},

				new OtherLeftStep
				{
					Id = "Start Video Calling",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new StartVideoCalling()
							}
						}
					}
				},

				new BriefingStep(masterIsCandidate: true) { Id = "Briefing 1" },

				new QuestionsStep(masterIsCandidate: true, interviewQuestionTags: new[] {"character"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Character Questions 1" },

				new QuestionsStep(masterIsCandidate: true, interviewQuestionTags: new[] {"careergoal"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Career Goal Questions 1" },

				new QuestionsStep(masterIsCandidate: true, interviewQuestionTags: new[] {"competency"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Competency Questions 1" },

				new QuestionsStep(masterIsCandidate: true, interviewQuestionTags: new[] {"curveball"}, duration: 10 * TimeScale, timeScale: TimeScale) { Id = "Curveball Questions 1" },

				new OtherLeftStep
				{
					Id = "Suspend Video Calling Audio",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new HideSentimentAnalysis(),

								new HideBrief(),

								new SuspendVideoCallingAudio()
							}
						}
					}
				},

				new AppraisalStep(masterIsCandidate: true, options: new []{"quit", "swap"}, showCandidateSwapPrompt: true) { Id = "Appraisal 1" },
				
				// -------------------------------------------------------------------------------------------------------------------
				// Playthrough 2: Master is the Interviewer, Slave is the Candidate
				// -------------------------------------------------------------------------------------------------------------------

				new JobSelectStep(masterIsCandidate: false)
				{
					Id = "Job Select 2"
				},

				new OtherLeftStep
				{
					Id = "Resume Video Calling",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new ResumeVideoCallingAudio()
							}
						}
					}
				},

				new BriefingStep(masterIsCandidate: false) { Id = "Briefing 2" },

				new QuestionsStep(masterIsCandidate: false, interviewQuestionTags: new[] {"character"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Character Questions 2" },

				new QuestionsStep(masterIsCandidate: false, interviewQuestionTags: new[] {"careergoal"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Career Goal Questions 2" },

				new QuestionsStep(masterIsCandidate: false, interviewQuestionTags: new[] {"competency"}, duration: 60 * TimeScale, timeScale: TimeScale) { Id = "Competency Questions 2" },

				new QuestionsStep(masterIsCandidate: false, interviewQuestionTags: new[] {"curveball"}, duration: 10 * TimeScale, timeScale: TimeScale) { Id = "Curveball Questions 2" },

				new OtherLeftStep
				{
					Id = "Suspend Video Calling Audio",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new HideSentimentAnalysis(),

								new HideBrief(),

								new SuspendVideoCallingAudio()
							}
						}
					}
				},

				new AppraisalStep(masterIsCandidate: false, options: new []{"results"}, showCandidateSwapPrompt: false) { Id = "Appraisal 2" },

				new OtherLeftStep
				{
					Id = "Stop Video Calling",

					Actions = new []
					{
						new Action
						{
							Config = new Config
							{
								DiscardType = DiscardType.FirstEvaluation,

								BlockingType = BlockingType.Step
							},

							OnConditionCommands = new IOnConditionCommand[]
							{
								new StopVideoCalling(),

								new HideTitle(),
							}
						}
					}
				},

				new SummaryStep
				{
					Id = "Summary"
				}
			};
		}
	}
}
