﻿namespace PlayGen.InterviewSkills.Scenario.Implementation.Conditions
{
	public class IsConditionElapsedInterval : PlayGen.Scenario.Step.Action.Conditions.IsConditionElapsedInterval
    {
	    public IsConditionElapsedInterval(float time, int repeatCount = 0)
	    {
		    Interval = (int)time;
		    RepeatCount = repeatCount;
	    }

		public IsConditionElapsedInterval(int interval, int repeatCount = 0)
	    {
		    Interval = interval;
		    RepeatCount = repeatCount;
	    }

	    /// <summary>
	    /// Used for deserialization
	    /// </summary>
	    private IsConditionElapsedInterval()
	    {
	    }
	}
}
