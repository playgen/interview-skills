﻿using PlayGen.Scenario.Step.Action.Command;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands.Waiting
{
    public class HideWaiting : IOnConditionCommand, IOnDiscardCommand
	{
    }
}