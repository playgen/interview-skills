﻿using PlayGen.Scenario.Step.Action.Command;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands
{
    public class Selection : IOnConditionCommand
    {
	    public static readonly string[] NoSelection = new string[0];

	    public virtual string[] InitialSelection => NoSelection;
	}
}
