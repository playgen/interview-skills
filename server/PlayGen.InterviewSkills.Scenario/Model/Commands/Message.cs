﻿using PlayGen.Scenario.Step.Action.Command;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands
{
    public abstract class Message : IOnConditionCommand
    {
		public string Title { get; set; }

		public string Body { get; set; }
    }
}
