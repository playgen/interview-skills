﻿using PlayGen.Scenario.Step.Action.Command;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands.FeatureActionableFeedback
{
    public class HideFeatureActionableFeedback : IOnConditionCommand, IOnDiscardCommand
    {
    }
}
