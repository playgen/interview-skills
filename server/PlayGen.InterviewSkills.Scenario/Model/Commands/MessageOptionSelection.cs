﻿namespace PlayGen.InterviewSkills.Scenario.Model.Commands
{
    public abstract class MessageOptionSelection : OptionSelection
    {
		public string Title { get; set; }

		public string Body { get; set; }
    }
}
