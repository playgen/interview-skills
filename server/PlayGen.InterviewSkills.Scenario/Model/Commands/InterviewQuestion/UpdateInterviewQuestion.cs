﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewQuestion
{
    public class UpdateInterviewQuestion : OptionSelection
	{
		public InterviewQuestionStates State { get; set; }
    }
}
