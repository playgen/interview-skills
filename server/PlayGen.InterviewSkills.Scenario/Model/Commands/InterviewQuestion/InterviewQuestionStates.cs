﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewQuestion
{
    public enum InterviewQuestionStates
    {
		Asking,
		Answering,
		Answered
    }
}
