﻿namespace PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewQuestion
{
    public class ShowInterviewQuestion : UpdateInterviewQuestion
    {
		public string[] Tags { get; set; }
    }
}
