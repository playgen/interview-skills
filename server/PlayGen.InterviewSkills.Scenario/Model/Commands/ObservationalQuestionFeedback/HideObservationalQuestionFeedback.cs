﻿using PlayGen.Scenario.Step.Action.Command;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands.ObservationalQuestionFeedback
{
	public class HideObservationalQuestionFeedback : IOnConditionCommand, IOnDiscardCommand
    {
    }
}
