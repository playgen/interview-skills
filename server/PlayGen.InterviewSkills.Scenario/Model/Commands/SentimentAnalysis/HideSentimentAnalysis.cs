﻿using PlayGen.Scenario.Step.Action.Command;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands.SentimentAnalysis
{
    public class HideSentimentAnalysis : IOnConditionCommand, IOnDiscardCommand
	{
    }
}
