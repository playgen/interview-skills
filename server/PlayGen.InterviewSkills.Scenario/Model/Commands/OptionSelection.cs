﻿namespace PlayGen.InterviewSkills.Scenario.Model.Commands
{
    public abstract class OptionSelection : Selection
    {
	    public virtual string[] Options { get; set; }
	}
}
