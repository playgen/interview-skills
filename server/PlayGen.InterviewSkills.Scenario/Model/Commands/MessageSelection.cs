﻿namespace PlayGen.InterviewSkills.Scenario.Model.Commands
{
    public class MessageSelection : Selection
    {
	    public string Title { get; set; }

	    public string Body { get; set; }
	}
}
