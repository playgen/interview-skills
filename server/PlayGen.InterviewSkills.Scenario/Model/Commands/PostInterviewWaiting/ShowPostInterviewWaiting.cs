﻿using PlayGen.Scenario.Step.Action.Command;

namespace PlayGen.InterviewSkills.Scenario.Model.Commands.PostInterviewWaiting
{
	public class ShowPostInterviewWaiting : IOnConditionCommand, IOnDiscardCommand
	{
    }
}
