﻿namespace PlayGen.InterviewSkills.Scenario.Model
{
	public class Scenario : PlayGen.Scenario.Scenario
    {
		public string Category { get; set; }

	    public string Name { get; set; }

		public bool IsSynchronised { get; set; }
	}
}
