﻿using PlayGen.Scenario.Step.Action.Conditions;

namespace PlayGen.InterviewSkills.Scenario.Model.Conditions
{
    public class IsMaster : ICondition
    {
		public bool Value { get; set; }
    }
}
