﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PlayGen.InterviewSkills.Core.Exceptions;
using PlayGen.InterviewSkills.Data.Model.FeedbackResults;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.FeedbackResults
{
	public class FeatureActionableFeedbackSystemResultController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public FeatureActionableFeedbackSystemResultController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public FeatureActionableFeedbackSystemResult Create(
			int feedbackId,
			int analysisResultId,
			DateTime received)
		{
			using (var context = _contextFactory.Create())
			{
				var result = context.FeatureActionableFeedbackSystemResults.AddOrUpdate(
					context,
					new FeatureActionableFeedbackSystemResult
					{
						FeedbackId = feedbackId,
						ArchiveAnalysisResultId = analysisResultId,
						Received = received
					});

				context.LogErrorSaveChanges();
				return result;
			}
		}

		public FeatureActionableFeedbackSystemResult AddShown(
			int feedbackId,
			int analysisResultId,
			DateTime shown)
		{
			using (var context = _contextFactory.Create())
			{
				var result = context.FeatureActionableFeedbackSystemResults.AddOrUpdate(
					context,
					new FeatureActionableFeedbackSystemResult
					{
						FeedbackId = feedbackId,
						ArchiveAnalysisResultId = analysisResultId,
						Shown = shown
					});

				context.LogErrorSaveChanges();
				return result;
			}
		}

		public FeatureActionableFeedbackSystemResult AddAcknowledged(
			int feedbackId,
			int analysisResultId,
			DateTime acknowledged)
		{
			using (var context = _contextFactory.Create())
			{
				var result = context.FeatureActionableFeedbackSystemResults.AddOrUpdate(
					context,
					new FeatureActionableFeedbackSystemResult
					{
						FeedbackId = feedbackId,
						ArchiveAnalysisResultId = analysisResultId,
						Acknowledged = acknowledged
					});

				context.LogErrorSaveChanges();
				return result;
			}
		}
	}
}
