﻿using PlayGen.InterviewSkills.Data.Model.FeedbackResults;
using PlayGen.InterviewSkills.Data.Storage;

namespace PlayGen.InterviewSkills.Core.FeedbackResults
{
	public class ObservationalQuestionUserResultController
    {
	    private readonly InterviewSkillsContextFactory _contextFactory;

	    public ObservationalQuestionUserResultController(InterviewSkillsContextFactory contextFactory)
	    {
		    _contextFactory = contextFactory;
	    }

	    public void Add(ObservationalFeedbackUserResult newEntry)
	    {
		    using (var context = _contextFactory.Create())
			{
				context.ObservationalFeedbackUserResults.Add(newEntry);
			}
	    }
    }
}

