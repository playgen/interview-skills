﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Authentications;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.Authentication
{
	public class GuestAuthenticationController : AuthenticationController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public GuestAuthenticationController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public AuthenticatedUser Get()
		{
			using (var context = _contextFactory.Create())
			{
				var authentication = context.Authentications
					.Where(g => g.AuthenticationType == AuthenticationType.Guest && g.AuthenticationUserId == InterviewSkillsContextSeed.GuestAuthenticationId)
					.Include(a => a.User)
					.Single();

				return CreateAuthenticatedUser(authentication.User, AuthenticationType.Guest);
			}
		}
	}
}