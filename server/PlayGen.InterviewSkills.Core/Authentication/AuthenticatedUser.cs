﻿using PlayGen.CustomData;
using PlayGen.InterviewSkills.Data.Model.Authentications;

namespace PlayGen.InterviewSkills.Core.Authentication
{
	public class AuthenticatedUser : CustomDataContainer<IUserCustomData>
	{
		public Data.Model.User.User User { get; set; } 

		public AuthenticationType AuthenticationType { get; set; }
    }
}
