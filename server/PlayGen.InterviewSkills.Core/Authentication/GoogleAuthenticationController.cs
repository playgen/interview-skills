﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Authentications;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.Authentication
{
	public class GoogleAuthenticationController : AuthenticationController
    {
		private readonly InterviewSkillsContextFactory _contextFactory;

	    public GoogleAuthenticationController(InterviewSkillsContextFactory contextFactory)
	    {
		    _contextFactory = contextFactory;
	    }

	    public async Task<(bool isValid, string tokenId, string emailAddress, string error)> TryValidate(string token)
	    {
		    bool isValid;
			string error, googleUserId, emailAddress;

			try
			{
				var result = await GoogleJsonWebSignature.ValidateAsync(token);
				googleUserId = result.Subject;
				emailAddress = result.Email;
				error = null;
				isValid = true;
			}
			catch (Exception exception)
			{
				error = exception.ToString();
				googleUserId = null;
				emailAddress = null;
				isValid = false;
			}

		    return (isValid, googleUserId, emailAddress, error);
	    }

	    public AuthenticatedUser GetOrCreate(string googleUserId, string emailAddress)
	    {
		    using (var context = _contextFactory.Create())
		    {
			    var authentication = context.Authentications
					.Where(g => g.AuthenticationUserId == googleUserId && g.AuthenticationType == AuthenticationType.Google)
					.Include(a => a.User)
					.SingleOrDefault();

				if (authentication == null)
				{
					var user = new Data.Model.User.User
					{
						EmailAddress = emailAddress
					};
					context.Users.Add(user);

					authentication = new Data.Model.Authentications.Authentication
					{
						AuthenticationUserId = googleUserId,
						User = user,
						AuthenticationType = AuthenticationType.Google
					};
					context.Authentications.Add(authentication);

					context.LogErrorSaveChanges();
				}

			    return CreateAuthenticatedUser(authentication.User, AuthenticationType.Google);
			}
	    }
	}
}
