﻿using PlayGen.InterviewSkills.Data.Model.Authentications;

namespace PlayGen.InterviewSkills.Core.Authentication
{
	public abstract class AuthenticationController
    {
	    public delegate void UserAuthenticatedHandler(AuthenticatedUser authenticatedUser);
	    public event UserAuthenticatedHandler UserAuthenticatedEvent;
		
	    protected AuthenticatedUser CreateAuthenticatedUser(Data.Model.User.User user, AuthenticationType authenticationType)
	    {
		    var authUser = new AuthenticatedUser
		    {
			    User = user,
				AuthenticationType = authenticationType
		    };

		    UserAuthenticatedEvent?.Invoke(authUser);

			return authUser;
	    }
	}
}
