﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Data.Model.Authentications;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.Authentication
{
	public class EmailAuthenticationController : AuthenticationController
    {
		private readonly InterviewSkillsContextFactory _contextFactory;

	    public EmailAuthenticationController(InterviewSkillsContextFactory contextFactory)
	    {
		    _contextFactory = contextFactory;
	    }

	    public (bool isValid, string error) TryValidate(string emailAddress)
	    {
			var emailAddressValidator = new EmailAddressAttribute();
		    return (emailAddressValidator.IsValid(emailAddress), emailAddressValidator.ErrorMessage);
	    }

		public AuthenticatedUser GetOrCreate(string emailAddress)
	    {
		    using (var context = _contextFactory.Create())
		    {
			    var authentication = context.Authentications
					.Where(g => g.AuthenticationUserId == emailAddress && g.AuthenticationType == AuthenticationType.Email)
					.Include(a => a.User)
					.SingleOrDefault();

				if (authentication == null)
				{
					var user = new Data.Model.User.User
					{
						EmailAddress = emailAddress
					};
					context.Users.Add(user);

					authentication = new Data.Model.Authentications.Authentication
					{
						AuthenticationUserId = emailAddress,
						User = user,
						AuthenticationType = AuthenticationType.Email
					};
					context.Authentications.Add(authentication);

					context.LogErrorSaveChanges();
				}

			    return CreateAuthenticatedUser(authentication.User, AuthenticationType.Email);
			}
	    }
    }
}
