﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.InterviewSkills.Core.Exceptions
{
    public class AlreadyHasValue : CoreException
    {
        public AlreadyHasValue()
        {
        }

        public AlreadyHasValue(string message) : base(message)
        {
        }

        public AlreadyHasValue(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
