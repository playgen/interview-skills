﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.InterviewSkills.Core.Exceptions
{
    public class EntryNotFound : CoreException
    {
        public EntryNotFound(string message) : base(message)
        {
        }

        public EntryNotFound(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
