﻿using PlayGen.MatchMaking.Matching;

namespace PlayGen.InterviewSkills.Core.Match.MatchMaking
{
    public class MatchMakingParticipantScenarioData : IMatchingParticipantCustomData
    {
        public string Category { get; }

        public string Name { get; }

		public bool IsSynchronised { get; }

	    public MatchMakingParticipantScenarioData(
			string category, 
			string name,
			bool isSynchronised)
	    {
		    Category = category;
		    Name = name;
			IsSynchronised = isSynchronised;
		}

	    public bool IsMatch(MatchMakingParticipantScenarioData other)
	    {
		    return 
				other.Category == Category 
				&& other.Name == Name
				&& other.IsSynchronised == IsSynchronised;
	    }
    }
}
