﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlayGen.MatchMaking;
using PlayGen.MatchMaking.Matched;
using PlayGen.MatchMaking.Matching;

namespace PlayGen.InterviewSkills.Core.Match.MatchMaking
{
	public class MatchMakingController : IDisposable
	{
		public const int MinParticipants = 2;

		private readonly MatchManager _matchManager;
		private readonly MatchMakingComparer _participantComparer = new MatchMakingComparer();
		private readonly MatchMakingParticipantPreviouslyMatchedUsersDataFactory _participantPreviouslyMatchedUsersDataFactory;
		private readonly MatchMakingParticipantUserDataFactory _participantUserDataFactory;

		public event MatchManager.MatchMade MatchMadeEvent;
		public event MatchManager.MatchMakingParticipantCreated MatchMakingParticipantCreated;

		private bool _isDisposed;

		public MatchMakingController(
			MatchMakingParticipantUserDataFactory participantUserDataFactory,
			MatchMakingParticipantPreviouslyMatchedUsersDataFactory participantPreviouslyMatchedUsersDataFactory,
			int unmatchedKeepAliveTimeout,
			int unmatchedPruneInterval)
		{
			_participantUserDataFactory = participantUserDataFactory;
			_participantPreviouslyMatchedUsersDataFactory = participantPreviouslyMatchedUsersDataFactory;

			_matchManager = new MatchManager(MinParticipants, unmatchedKeepAliveTimeout, unmatchedPruneInterval);
			_matchManager.MatchCreatedEvent += OnMatchMade;
			_matchManager.MatchedParticipantCreatedEvent += OnMatchParticpantCreated;
		}

		~MatchMakingController()
		{
			Dispose();
		}

		public void Dispose()
		{
			if (_isDisposed) return;

			_matchManager.Dispose();

			_isDisposed = true;
		}

		public void Start()
		{
			_matchManager.Start();
		}

		public bool TryFindMatch(string participantId, string scenarioCategory, string scenarioName, bool cantMatchPrevious, out List<PlayGen.MatchMaking.Matched.Match> matches)
		{
			var participantConfig = GenerateParticpantConfig(participantId, scenarioCategory, scenarioName, true, cantMatchPrevious);
			return _matchManager.TryFindMatches(participantConfig, out matches);
		}

		public bool TryFindNewMatch(string participantId, string scenarioCategory, string scenarioName, bool cantMatchPrevious, out PlayGen.MatchMaking.Matched.Match match)
		{
			var participantConfig = GenerateParticpantConfig(participantId, scenarioCategory, scenarioName, true, cantMatchPrevious);
			return _matchManager.TryFindNewMatch(participantConfig, out match);
		}

		public PlayGen.MatchMaking.Matched.Match CreateMatch(List<string> participantIds, string scenarioCategory, string scenarioName)
		{
			var isSynchronised = participantIds.Count > 1;
			var participantConfigs = participantIds
				.Select(pId => GenerateParticpantConfig(pId, scenarioCategory, scenarioName, isSynchronised, false))
				.ToList();
			
			return _matchManager.CreateMatch(participantConfigs);
		}

		public bool TryFindExistingMatches(string participantId, out List<PlayGen.MatchMaking.Matched.Match> matches)
		{
			return _matchManager.TryFindExistingMatches(participantId, out matches);
		}

		public bool RemoveParticipant(string matchId, string participantId)
		{
			return _matchManager.RemoveParticipant(participantId, matchId);
		}

		public bool RemoveParticipant(string participantId)
		{
			return _matchManager.RemoveParticipant(participantId);
		}

		private MatchingParticipant GenerateParticpantConfig(string participantId, string scenarioCategory, string scenarioName, bool isSynchronised, bool cantMatchPrevious)
		{
			var participantConfig = new MatchingParticipant(participantId, _participantComparer);

			var scenarioData = new MatchMakingParticipantScenarioData(scenarioCategory, scenarioName, isSynchronised);
			participantConfig.AddCustomData(scenarioData);

			var userData = _participantUserDataFactory.Create(participantId);
			participantConfig.AddCustomData(userData);

			var previouslyMatchedUsersData = _participantPreviouslyMatchedUsersDataFactory.Create(userData.UserId, cantMatchPrevious);
			participantConfig.AddCustomData(previouslyMatchedUsersData);

			return participantConfig;
		}

		private void OnMatchMade(PlayGen.MatchMaking.Matched.Match match)
		{
			MatchMadeEvent?.Invoke(match);
		}

		private void OnMatchParticpantCreated(MatchedParticipant matchedparticipant)
		{
			MatchMakingParticipantCreated?.Invoke(matchedparticipant);
		}
	}
}

