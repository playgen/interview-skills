﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.MatchMaking.Matching;

namespace PlayGen.InterviewSkills.Core.Match.MatchMaking
{
    public class MatchMakingParticipantUserData : IMatchingParticipantCustomData
    {
	    public string UserId { get; }

		public MatchMakingParticipantUserData(string userId)
	    {
		    UserId = userId;
		}
    }
}
