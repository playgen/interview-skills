﻿using PlayGen.MatchMaking.Matching;

namespace PlayGen.InterviewSkills.Core.Match.MatchMaking
{
    public class MatchMakingComparer : IMatchMakingParticipantComparer
    {
        public bool AreMatch(MatchingParticipant a, MatchingParticipant b)
        {
            var storyConfigA = a.GetCustomData<MatchMakingParticipantScenarioData>();
            var storyConfigB = b.GetCustomData<MatchMakingParticipantScenarioData>();

	        if (!storyConfigA.IsMatch(storyConfigB))
	        {
		        return false;
	        }

	        var userA = a.GetCustomData<MatchMakingParticipantUserData>();
	        var userB = b.GetCustomData<MatchMakingParticipantUserData>();

			var previouslyMatchedUsersA = a.GetCustomData<MatchMakingParticipantPreviouslyMatchedUsersData>();
	        var previouslyMatchedUsersB = b.GetCustomData<MatchMakingParticipantPreviouslyMatchedUsersData>();

	        if (!previouslyMatchedUsersA.IsMatch(userB) || !previouslyMatchedUsersB.IsMatch(userA))
	        {
		        return false;
	        }

			return true;
        }
    }
}
