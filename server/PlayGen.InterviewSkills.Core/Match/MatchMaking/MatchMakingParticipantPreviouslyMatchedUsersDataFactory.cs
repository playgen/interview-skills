﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PlayGen.InterviewSkills.Core.Match.Session;

namespace PlayGen.InterviewSkills.Core.Match.MatchMaking
{
    public class MatchMakingParticipantPreviouslyMatchedUsersDataFactory
    {
	    private readonly MatchSessionController _matchController;

	    public MatchMakingParticipantPreviouslyMatchedUsersDataFactory(MatchSessionController matchController)
	    {
		    _matchController = matchController;
	    }

	    public MatchMakingParticipantPreviouslyMatchedUsersData Create(string userId, bool cantMatchPrevious)
	    {
		    var previouslyMatchedUserIds = _matchController.GetPreviouslyMatchedUserIds(Guid.Parse(userId));
			return new MatchMakingParticipantPreviouslyMatchedUsersData(
				previouslyMatchedUserIds.Select(p => p.ToString()).ToArray(), 
				cantMatchPrevious);
	    }
	}
}
