﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.InterviewSkills.Core.User;

namespace PlayGen.InterviewSkills.Core.Match.MatchMaking
{
    public class MatchMakingParticipantUserDataFactory
    {
	    private readonly UserSessionController _userSessionController;

	    public MatchMakingParticipantUserDataFactory(UserSessionController userSessionController)
	    {
		    _userSessionController = userSessionController;
	    }

	    public MatchMakingParticipantUserData Create(string userSessionId)
	    {
		    var userId = _userSessionController.GetUserId(Guid.Parse(userSessionId));
		    return new MatchMakingParticipantUserData(userId.ToString());
	    }
    }
}
