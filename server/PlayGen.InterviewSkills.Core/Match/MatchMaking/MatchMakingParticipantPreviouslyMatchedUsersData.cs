﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PlayGen.MatchMaking.Matching;

namespace PlayGen.InterviewSkills.Core.Match.MatchMaking
{
    public class MatchMakingParticipantPreviouslyMatchedUsersData : IMatchingParticipantCustomData
	{
		public string[] PreviouslyMatchedUserIds { get; }

		public bool CantMatchPrevious { get; }

		public MatchMakingParticipantPreviouslyMatchedUsersData(string[] previouslyMatchedUserIds, bool cantMatchPrevious)
		{
			PreviouslyMatchedUserIds = previouslyMatchedUserIds;
			CantMatchPrevious = cantMatchPrevious;
		}

		public bool IsMatch(MatchMakingParticipantUserData other)
		{
			return !CantMatchPrevious || PreviouslyMatchedUserIds.All(pmuid => pmuid != other.UserId);
		}
	}
}
