﻿using PlayGen.MatchMaking.Matched;

namespace PlayGen.InterviewSkills.Core.Match.Session
{
    public class MatchSessionData : IMatchCustomData
    {
		public string Id { get; set; }

	    public MatchSessionData(string id)
	    {
		    Id = id;
	    }
    }
}
