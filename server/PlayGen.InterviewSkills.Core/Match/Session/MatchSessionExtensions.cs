﻿namespace PlayGen.InterviewSkills.Core.Match.Session
{
    public static class MatchSessionExtensions
    {
	    public static MatchSessionData ToMatchSessionData(this Data.Model.MatchSession.MatchSession matchSession)
	    {
		    return new MatchSessionData(matchSession.Id);
	    }
    }
}
