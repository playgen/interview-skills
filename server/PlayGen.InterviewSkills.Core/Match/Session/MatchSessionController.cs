﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlayGen.InterviewSkills.Core.OpenTok;
using PlayGen.InterviewSkills.Data.Model.MatchSession;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.Match.Session
{
	public class MatchSessionController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public MatchSessionController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public void CreateAndAddMatchSession(PlayGen.MatchMaking.Matched.Match match)
		{
			var openTokMatchData = match.GetCustomData<MatchOpenTokData>();

			var userSessionIds = match.Participants
				.Select(p => p.MatchingParticipant.Id)
				.ToArray();

			var matchSession = CreateMatchSession(openTokMatchData.Session.Id, userSessionIds);

			match.AddCustomData(matchSession.ToMatchSessionData());
		}

		public bool DoesHaveSessionId(PlayGen.MatchMaking.Matched.Match match, string id)
		{
			var matchSessionData = match.GetCustomData<MatchSessionData>();
			return matchSessionData.Id == id;
		}

		private MatchSession CreateMatchSession(string id, string[] userSessionIds)
		{
			var matchSession = new MatchSession
			{
				Id = id
			};

			using (var context = _contextFactory.Create())
			{
				context.MatchSessions.Add(matchSession).Entity
					.AddUserSessionMappings(context, userSessionIds.Select(Guid.Parse).ToArray());

				context.LogErrorSaveChanges();
			}

			return matchSession;
		}

		public Guid[] GetPreviouslyMatchedUserIds(Guid userId)
		{
			using (var context = _contextFactory.Create())
			{
				return GetPreviouslyMatchedOther(context, userId)
					.Select(usm => usm.UserSession.UserId)
					.Distinct()
					.ToArray();
			}
		}

		private IQueryable<MatchSessionUserSessionMappings> GetPreviouslyMatched(InterviewSkillsContext context, Guid userId)
		{
			return context.MatchSessions
				.Where(ms => ms.UserSessionMappings.Any(usm => usm.UserSession.UserId == userId))
				.SelectMany(ms => ms.UserSessionMappings);
		}

		private IQueryable<MatchSessionUserSessionMappings> GetPreviouslyMatchedOther(InterviewSkillsContext context, Guid userId)
		{
			return GetPreviouslyMatched(context, userId)
				// It's possible to have matched another user or oneself. In the case of matching oneself, return both entries.
				.Where(usm => usm.UserSession.UserId != userId
					|| usm.MatchSession.UserSessionMappings.All(musm => musm.UserSession.UserId == userId));
		}
	}
}
