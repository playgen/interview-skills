﻿using System;
using PlayGen.InterviewSkills.Data.Model.Role;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.Role
{
	public class RoleSessionController
    {
	    private readonly InterviewSkillsContextFactory _contextFactory;

	    public RoleSessionController(InterviewSkillsContextFactory contextFactory)
	    {
		    _contextFactory = contextFactory;
	    }

	    public void Add(RoleSession newEntry)
	    {
		    using (var context = _contextFactory.Create())
			{
				context.RoleSessions.Add(newEntry);
				context.LogErrorSaveChanges();
			}
	    }

	    public RoleSession End(Guid roleSessionId, DateTime ended)
	    {
		    using (var context = _contextFactory.Create())
		    {
			    var roleSession = context.RoleSessions.Find(roleSessionId);
			    if (roleSession != null)
			    {
				    roleSession.Ended = ended;
				    context.LogErrorSaveChanges();
				}

				return roleSession;
		    }
	    }
    }
}

