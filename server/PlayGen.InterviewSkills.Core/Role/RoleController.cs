﻿using System.Collections.Generic;
using System.Linq;
using PlayGen.InterviewSkills.Data.Storage;

namespace PlayGen.InterviewSkills.Core.Role
{
	public class RoleController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public RoleController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public List<Data.Model.Role.Role> List()
		{
			using (var context = _contextFactory.Create())
			{
				return context.Roles.ToList();
			}
		}
	}
}
