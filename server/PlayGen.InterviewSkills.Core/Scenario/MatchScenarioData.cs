﻿using PlayGen.MatchMaking.Matched;

namespace PlayGen.InterviewSkills.Core.Scenario
{
    public class MatchScenarioData : IMatchCustomData
    {
		public Data.Model.Scenario Scenario { get; }

	    public MatchScenarioData(Data.Model.Scenario scenario)
	    {
		    Scenario = scenario;
	    }
    }
}
