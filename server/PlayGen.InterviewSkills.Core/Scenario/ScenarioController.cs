﻿using System.Linq;
using PlayGen.InterviewSkills.Core.Match.MatchMaking;
using PlayGen.InterviewSkills.Data.Storage;

namespace PlayGen.InterviewSkills.Core.Scenario
{
	public class ScenarioController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public ScenarioController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public void AddScenario(MatchMaking.Matched.Match match)
		{
			var masterScenarioConfig = match.Participants
				.Single(p => p.IsMaster)
				.MatchingParticipant.GetCustomData<MatchMakingParticipantScenarioData>();

			var scenario = Get(masterScenarioConfig.Category, masterScenarioConfig.Name, masterScenarioConfig.IsSynchronised);

			match.AddCustomData(new MatchScenarioData(scenario));
		}

		public Data.Model.Scenario Get(string category, string name, bool isSynchronised)
		{
			using (var context = _contextFactory.Create())
			{
				return context.Scenarios.First(s =>
					s.Category == category
					&& s.Name == name
					&& s.IsSynchronised == isSynchronised);
			}
		}
	}
}