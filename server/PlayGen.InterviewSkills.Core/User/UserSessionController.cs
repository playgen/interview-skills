﻿using System;
using PlayGen.InterviewSkills.Core.Authentication;
using PlayGen.InterviewSkills.Data.Model.User;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.User
{
    public class UserSessionController
    {
	    private readonly InterviewSkillsContextFactory _contextFactory;

	    public UserSessionController(InterviewSkillsContextFactory contextFactory)
	    {
		    _contextFactory = contextFactory;
	    }

	    public void CreateSession(AuthenticatedUser authUser)
	    {
			using (var context = _contextFactory.Create())
			{
				var session = new UserSession
				{
					UserId = authUser.User.Id,
					Created = DateTime.UtcNow
				};
				context.UserSessions.Add(session);
				context.LogErrorSaveChanges();
				var userSessionData = new UserSessionData(session.Id.ToString());
				authUser.AddCustomData(userSessionData);
			}
		}

	    public Guid  GetUserId(Guid userSessionId)
	    {
			using (var context = _contextFactory.Create())
			{
				return context.UserSessions.Find(userSessionId).UserId;
			}
		}
    }
}
