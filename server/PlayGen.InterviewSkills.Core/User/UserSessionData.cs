﻿using PlayGen.InterviewSkills.Core.Authentication;

namespace PlayGen.InterviewSkills.Core.User
{
    public class UserSessionData : IUserCustomData
	{
		public string Id { get; }

		public UserSessionData(string id)
		{
			Id = id;
		}
	}
}
