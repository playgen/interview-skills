﻿using System.Collections.Generic;
using System.Linq;
using PlayGen.InterviewSkills.Data.Model.Questions.Interview;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.Questions
{
	public class InterviewQuestionController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public InterviewQuestionController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public IList<InterviewQuestion> List(string[] features = null, string[] tags = null, string[] jobs = null, 
			bool requireAllFeatures = true, bool requireAllTags = true, bool requireAllJobs = true,
			string[] includes = null)
		{
			using (var context = _contextFactory.Create())
			{
				IQueryable<InterviewQuestion> questions = context.InterviewQuestions;

				if (features?.Length > 0)
				{
					if (requireAllFeatures)
					{
						questions = questions.Where(
							oq => features.All(
								feature => oq.FeatureMappings.Any(
									fm => fm.Feature.Id == feature)));
					}
					else
					{
						questions = questions.Where(
							oq => features.Any(
								feature => oq.FeatureMappings.Any(
									fm => fm.Feature.Id == feature)));
					}
				}

				if (tags?.Length > 0)
				{
					if (requireAllTags)
					{
						questions = questions.Where(
							oq => tags.All(
								tag => oq.TagMappings.Any(
									tm => tm.Tag.Id == tag)));
					}
					else
					{
						questions = questions.Where(
							oq => tags.Any(
								tag => oq.TagMappings.Any(
									tm => tm.Tag.Id == tag)));
					}
				}

				if (jobs?.Length > 0)
				{
					if (requireAllJobs)
					{
						questions = questions.Where(
							oq => jobs.All(
								job => oq.JobMappings.Any(
									jm => jm.Job.Id == job)));
					}
					else
					{
						questions = questions.Where(
							oq => jobs.Any(
								job => oq.JobMappings.Any(
									jm => jm.Job.Id == job)));
					}
				}

				return questions.Include(includes).ToList();
			}
		}
	}
}
