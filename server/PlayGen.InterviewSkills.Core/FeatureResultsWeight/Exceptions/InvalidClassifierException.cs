﻿using System;
using PlayGen.InterviewSkills.Core.Exceptions;

namespace PlayGen.InterviewSkills.Core.FeatureResultsWeight.Exceptions
{
	public class InvalidClassifierException : CoreException
	{
		public InvalidClassifierException()
		{
		}

		public InvalidClassifierException(string message) : base(message)
		{
		}

		public InvalidClassifierException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
