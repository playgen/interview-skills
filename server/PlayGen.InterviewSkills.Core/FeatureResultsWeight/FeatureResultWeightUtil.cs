﻿using PlayGen.InterviewSkills.Core.FeatureResultsWeight.Exceptions;
using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Storage;

namespace PlayGen.InterviewSkills.Core.FeatureResultsWeight
{
	public static class FeatureResultWeightUtil
	{
		public static float CalculateWeightFromIdeal(float weight, Feature feature)
		{
			var (distance, range) = DistanceFromIdeal(weight, feature.IdealWeightMin, feature.IdealWeightMax);

			return distance == 0
				? 0
				: distance / range;
		}

		public static float CalculateWeightFromIdeal(InterviewSkillsContext context, string featureId, float weight)
		{
			var feature = context.Features.Find(featureId);
			return CalculateWeightFromIdeal(weight, feature);
		}

		public static (float distance, float range) DistanceFromIdeal(float weight, float idealMin, float idealMax)
		{
			float distance, range;

			if (weight < idealMin)
			{
				distance = idealMin - weight;
				range = - idealMin;

			}
			else if (weight > idealMax)
			{
				distance = weight - idealMax;
				range = 1 - idealMax;

			}
			else
			{
				distance = 0;
				range = 0;
			}

			return (distance, range);
		}

		public static float CalculateWeight(int prediction, float class1Prob, float class2Prob, float? class3Prob)
		{
			return class3Prob.HasValue
				? Calculate3ClassifierWeight(prediction, class1Prob, class2Prob, class3Prob.Value)
				: Calculate2ClassifierWeight(prediction, class1Prob, class2Prob);
		}

		public static float Calculate2ClassifierWeight(int prediction, float class1Prob, float class2Prob)
		{
			return class2Prob;
		}

		public static float Calculate3ClassifierWeight(int prediction, float class1Prob, float class2Prob, float class3Prob)
		{
			float weight;

			switch (prediction)
			{
				case 1:
					// 0 <- 1/3
					weight = 1f / 3 - class1Prob * 1f / 3;
					break;

				case 2:
					if (class1Prob > class3Prob)
					{
						// 1/3 -> 1/2
						weight = 1f / 3 + class2Prob * 1f / 6;
					}
					else
					{
						// 1/2 -> 2/3
						weight = 2f / 3 - class2Prob * 1f / 6;
					}
					break;

				case 3:
					// 2/3 -> 1
					weight = 2f / 3 + class3Prob * 1f / 3;
					break;

				default:
					throw new InvalidClassifierException($"Unhandled Prediction value: \"{prediction}\".");
			}

			return weight;
		}
	}
}