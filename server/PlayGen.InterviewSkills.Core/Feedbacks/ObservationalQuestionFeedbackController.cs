﻿using System.Collections.Generic;
using System.Linq;
using PlayGen.InterviewSkills.Core.Extensions;
using PlayGen.InterviewSkills.Core.FeatureResultsWeight;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.Feedbacks
{
	public class ObservationalQuestionFeedbackController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public ObservationalQuestionFeedbackController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public IList<ObservationalQuestionFeedback> List(int? questionId = null, string[] includes = null)
		{
			using (var context = _contextFactory.Create())
			{
				var feedbacks = questionId == null
					? context.ObservationalQuestionFeedbacks
					: context.ObservationalQuestionFeedbacks.Where(oqf => oqf.QuestionId == questionId);

				return feedbacks.Include(includes).ToList();
			}
		}

		public IList<ObservationalQuestionFeedback> ListForFeature(int questionId, string featureId, string[] includes = null)
		{
			using (var context = _contextFactory.Create())
			{
				return ListForFeature(context, questionId, featureId)
					.Include(includes)
					.ToList();
			}
		}

		public (IList<ObservationalQuestionFeedback>, float weightFromIdeal) ListForFeatureAndWeight(int questionId, string featureId, float weight,
			string[] includes = null)
		{
			using (var context = _contextFactory.Create())
			{
				var weightFromIdeal = FeatureResultWeightUtil.CalculateWeightFromIdeal(context, featureId, weight);
				var feedbacks = ListForFeature(context, questionId, featureId)
					.ListClosest(weightFromIdeal, oqf => oqf.Include(includes))
					.ToList();

				return (feedbacks, weightFromIdeal);
			}
		}

		private static IQueryable<ObservationalQuestionFeedback> ListForFeature(InterviewSkillsContext context, int questionId, string featureId)
		{
			return context.ObservationalQuestionFeedbacks
				.Where(oqf => oqf.QuestionId == questionId
							&& oqf.Question.FeatureMappings
								.Any(fm => fm.FeatureId == featureId));
		}
	}
}