﻿using System.Collections.Generic;
using System.Linq;
using PlayGen.InterviewSkills.Core.Extensions;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.Feedbacks
{
	public class FeatureActionableFeedbackController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public FeatureActionableFeedbackController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public IList<FeatureActionableFeedback> List(string feature, float? weightFromIdeal, string[] includes = null)
		{
			using (var context = _contextFactory.Create())
			{
				if (feature == null)
				{
					var feedbacks = context.FeatureActionableFeedbacks;
					return feedbacks.Include(includes).ToList();
				}
				else
				{
					var feedbacks = context.FeatureActionableFeedbacks
						.Where(fab => fab.Feature.Id == feature);

					if (!weightFromIdeal.HasValue)
					{
						feedbacks = feedbacks.Include(includes);
						return feedbacks.ToList();
					}
					else
					{
						return feedbacks
							.ListClosest(weightFromIdeal.Value, fab => fab.Include(includes))
							.ToList();
					}
				}
			}
		}
	}
}
