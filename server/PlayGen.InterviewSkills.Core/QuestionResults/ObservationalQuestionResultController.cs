﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using PlayGen.InterviewSkills.Data.Model.QuestionResults;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core.QuestionResults
{
	public class ObservationalQuestionResultController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;

		public ObservationalQuestionResultController(InterviewSkillsContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		public ObservationalQuestionResult AddReceived(
			int questionId,
			string matchSessionId,
			Guid userSessionId,
			DateTime received)
		{
			using (var context = _contextFactory.Create())
			{
				var result = context.ObservationalQuesitonResults.AddOrUpdate(
					context,
					new ObservationalQuestionResult
					{
						QuestionId = questionId,
						MatchSessionId = matchSessionId,
						UserSessionId = userSessionId,
						Received = received
					});

				context.LogErrorSaveChanges();

				return result;
			}
		}

		public ObservationalQuestionResult AddShown(
			int questionId,
			string matchSessionId,
			Guid userSessionId,
			DateTime received,
			DateTime shown)
		{
			using (var context = _contextFactory.Create())
			{
				var result = context.ObservationalQuesitonResults.AddOrUpdate(
					context,
					new ObservationalQuestionResult
					{
						QuestionId = questionId,
						MatchSessionId = matchSessionId,
						UserSessionId = userSessionId,
						Received = received,
						Shown = shown
					});

				context.LogErrorSaveChanges();
				return result;
			}
		}

		public ObservationalQuestionResult AddSubmitted(
			int questionId,
			string matchSessionId,
			Guid userSessionId,
			DateTime received,
			float weight,
			DateTime submitted)
		{
			using (var context = _contextFactory.Create())
			{
				var result = context.ObservationalQuesitonResults.AddOrUpdate(
					context,
					new ObservationalQuestionResult
					{
						QuestionId = questionId,
						MatchSessionId = matchSessionId,
						UserSessionId = userSessionId,
						Received = received,
						Weight = weight,
						Submitted = submitted
					});

				context.LogErrorSaveChanges();
				return result;
			}
		}
	}
}
