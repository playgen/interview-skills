﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PlayGen.InterviewSkills.Core.Exceptions;
using PlayGen.InterviewSkills.Data.Model.QuestionResults;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;
using PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewQuestion;

namespace PlayGen.InterviewSkills.Core.QuestionResults
{
    public class InterviewQuestionResultController
    {
	    private readonly InterviewSkillsContextFactory _contextFactory;

	    public InterviewQuestionResultController(InterviewSkillsContextFactory contextFactory)
	    {
		    _contextFactory = contextFactory;
	    }

	    public InterviewQuestionResult AddUpdated(int quesitonId, string matchSessionId, string state, DateTime updated)
	    {
			var result = new InterviewQuestionResult
			{
				QuestionId = quesitonId,
				MatchSessionId = matchSessionId
			};

			state = char.ToUpper(state[0]) + state.Substring(1);

			switch (state)
			{
				case nameof(InterviewQuestionStates.Asking):
					result.Asking = updated;
					break;

				case nameof(InterviewQuestionStates.Answering):
					result.Answering = updated;
					break;

				case nameof(InterviewQuestionStates.Answered):
					result.Answered = updated;
					break;

				default:
					throw new CoreException($"No case for {nameof(InterviewQuestionResult)} state: \"{state}\".");
			}

		    using (var context = _contextFactory.Create())
		    {
			    result = context.InterviewQuesitonResults.AddOrUpdate(context, result);
			    context.LogErrorSaveChanges();
			    return result;
			}
	    }

		public InterviewQuestionResult AddHidden(int quesitonId, string matchSessionId, DateTime hidden)
	    {
		    using (var context = _contextFactory.Create())
		    {
			    var result = context.InterviewQuesitonResults.AddOrUpdate(context, new InterviewQuestionResult
			    {
				    QuestionId = quesitonId,
				    MatchSessionId = matchSessionId,
					Hidden = hidden
				});
			    context.LogErrorSaveChanges();
			    return result;
		    }
		}
	}
}
