﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using PlayGen.InterviewSkills.Core.FeatureResultsWeight;
using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Model.Analysis;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResult;
using PlayGen.OpenTok.Server;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.Core
{
	public class SentimentAnalysisController
	{
		private readonly InterviewSkillsContextFactory _contextFactory;
		private readonly OpenTokManager _openTokManager;

		public event Action<AnalysisMetadata, Dictionary<string, object>> AnalysisResultParsedEvent;

		public SentimentAnalysisController(InterviewSkillsContextFactory contextFactory, OpenTokManager openTokManager)
		{
			_contextFactory = contextFactory;
			_openTokManager = openTokManager;
		}

		public bool TryGetLater(
			Guid userSessionId,
			string matchSessionId,
			DateTime startedLaterThan,
			out AnalysisMetadata result,
			string[] excludeFeatureIds = null)
		{
			using (var context = _contextFactory.Create())
			{
				result = context.AnalysesMetadatas
					.Where(
						aam => aam.UserSessionId == userSessionId
								&& aam.MatchSessionId == matchSessionId
								&& aam.RecordingStarted > startedLaterThan)
					.Include(aam => aam.FeatureResults)
					.ThenInclude(ar => ar.Feature)
					.OrderBy(aam => aam.RecordingStarted)
					.FirstOrDefault();

				if (result != default(AnalysisMetadata))
				{
					// EF Core doesn't have support conditional includes. When it does, this should be moved
					// to the include function above
					if (excludeFeatureIds != null && excludeFeatureIds.Length > 0)
					{
						var filtered = result.FeatureResults.ToList();
						filtered.RemoveAll(ar => excludeFeatureIds.Contains(ar.FeatureId));
						result.FeatureResults = filtered;
					}

					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public void AddRange(AnalysisResult[] analysisResults)
		{
			foreach (var analysisResult in analysisResults)
			{
				Task.Run(() => Add(analysisResult));
			}
		}

		public void Add(AnalysisResult analysisResult)
		{
			using (var context = _contextFactory.Create())
			{
				var userSessionId = _openTokManager.ExtractClientId(analysisResult.ConnectionData);

				var analysisMetadata = context.AnalysesMetadatas.Add(
					new AnalysisMetadata
					{
						UserSessionId = Guid.Parse(userSessionId),
						MatchSessionId = analysisResult.SessionId,

						RecordingStarted = analysisResult.RecordingStarted,
						RecordingEnded = analysisResult.RecordingEnded,

					    IsSpeechDetected = analysisResult.IsSpeechDetected,
					    ProbabilitySpeechDetected = analysisResult.ProbabilitySpeechDetected,

                        Errors = analysisResult.Errors?.Length > 0 ? analysisResult.Errors : null

					}).Entity;

				if (analysisResult.Results != null)
				{
					foreach (var featureResult in analysisResult.Results)
					{
						var feature = featureResult.Feature.ToLower();

						var weight = FeatureResultWeightUtil.CalculateWeight(
							featureResult.Prediction,
							featureResult.Classifier1,
							featureResult.Classifier2,
							featureResult.Classifier3);

						var weightFromIdeal = FeatureResultWeightUtil.CalculateWeightFromIdeal(context, feature, weight);

						context.AnalysisResults.Add(new AnalysisFeatureResult
						{
							AnalysisMetadata = analysisMetadata,

							FeatureId = feature,
							Classifier1 = featureResult.Classifier1,
							Classifier2 = featureResult.Classifier2,
							Classifier3 = featureResult.Classifier3,
							Prediction = featureResult.Prediction,
							Weight = weight,
							WeightFromIdeal = weightFromIdeal
						});
					}
				}

				context.LogErrorSaveChanges();

				if (analysisResult.DebugInfo != null)
				{
					var serverDebugInfo = (List<object[]>)analysisResult.DebugInfo["is-server"];
					serverDebugInfo.Add(new object[] { "addedToDatabase", DateTime.UtcNow.ToUnixTimestamp() });
				}

				AnalysisResultParsedEvent?.Invoke(analysisMetadata, analysisResult.DebugInfo);
			}
		}

		public Dictionary<Feature, AnalysisFeatureResult[]> GetOrderedInclusive(
			Guid userSessionId,
			string matchSessionId,
			DateTime start,
			DateTime? end,
			string[] includeFeatureIds)
		{
			using (var context = _contextFactory.Create())
			{
				var range = GetAnalysisResultsByFeatureBetween(context, userSessionId, matchSessionId, start, end);
				var filteredInclusive = IncludeFeatures(range, includeFeatureIds);

				return OrderByStarted(filteredInclusive);
			}
		}

		public Dictionary<Feature, AnalysisFeatureResult[]> GetOrderedExclusive(
			Guid userSessionId,
			string matchSessionId,
			DateTime start,
			DateTime? end,
			string[] excludeFeatureIds)
		{
			using (var context = _contextFactory.Create())
			{
				var range = GetAnalysisResultsByFeatureBetween(context, userSessionId, matchSessionId, start, end);
				var filteredExclusive = ExcludeFeatures(range, excludeFeatureIds);

				return OrderByStarted(filteredExclusive);
			}
		}

		public Dictionary<Feature, (float averageWeight, float averageWeightFromIdeal)> GetAveragesExclusive(
			Guid userSessionId,
			string matchSessionId,
			DateTime start,
			DateTime? end,
			string[] excludeFeatureIds)
		{
			using (var context = _contextFactory.Create())
			{
				var range = GetAnalysisResultsByFeatureBetween(context, userSessionId, matchSessionId, start, end);
				var filteredExclusive = ExcludeFeatures(range, excludeFeatureIds);

				return GetAverages(filteredExclusive);
			}
		}

		public Dictionary<Feature, (float averageWeight, float averageWeightFromIdeal)> GetAveragesInclusive(
			Guid userSessionId,
			string matchSessionId,
			DateTime start,
			DateTime? end,
			string[] includeFeatureIds)
		{
			using (var context = _contextFactory.Create())
			{
				var range = GetAnalysisResultsByFeatureBetween(context, userSessionId, matchSessionId, start, end);
				var filteredInclusive = IncludeFeatures(range, includeFeatureIds);

				return GetAverages(filteredInclusive);
			}
		}

		public List<AnalysisMetadata> GetMetadatas(string matchSessionId)
		{
			using (var context = _contextFactory.Create())
			{
				return context.AnalysesMetadatas
					.Where(aam => aam.MatchSessionId == matchSessionId)
					.ToList();
			}
		}

		public List<AnalysisMetadata> GetMetadatas(string matchSessionId, Guid userSessionId)
		{
			using (var context = _contextFactory.Create())
			{
				return context.AnalysesMetadatas
					.Where(
						aam => aam.MatchSessionId == matchSessionId
								&& aam.UserSessionId == userSessionId)
					.ToList();
			}
		}

		private IQueryable<AnalysisFeatureResult> ExcludeFeatures(IQueryable<AnalysisFeatureResult> resultsAndFeatures, string[] excludeFeatureIds)
		{
			return resultsAndFeatures
				.Include(afr => afr.Feature)
				.Where(aar => excludeFeatureIds == null
							|| excludeFeatureIds.Length == 0
							|| !excludeFeatureIds.Contains(aar.Feature.Id));
		}

		private IQueryable<AnalysisFeatureResult> IncludeFeatures(IQueryable<AnalysisFeatureResult> resultsAndFeatures, string[] includeFeatureIds)
		{
			return resultsAndFeatures
				.Include(afr => afr.Feature)
				.Where(aar => includeFeatureIds != null
							&& includeFeatureIds.Length > 0
							&& includeFeatureIds.Contains(aar.Feature.Id));
		}

		private Dictionary<Feature, (float averageWeight, float averageWeightFromIdeal)> GetAverages(IEnumerable<AnalysisFeatureResult> resultsById)
		{
			return resultsById
				.GroupBy(aar => aar.Feature)
				.ToDictionary(
					g => g.Key,
					g => (g.Average(aar => aar.Weight),
						g.Average(aar => aar.WeightFromIdeal)));
		}

		private Dictionary<Feature, AnalysisFeatureResult[]> OrderByStarted(IQueryable<AnalysisFeatureResult> resultsById)
		{
			return resultsById
					.Include(afr => afr.AnalysisMetadata)
					.GroupBy(aar => aar.Feature)
					.ToDictionary(
						g => g.Key,
						g => g.OrderBy(aar => aar.AnalysisMetadata.RecordingStarted).ToArray());
		}

		private IQueryable<AnalysisFeatureResult> GetAnalysisResultsByFeatureBetween(
			InterviewSkillsContext context,
			Guid userSessionId,
			string matchSessionId,
			DateTime start,
			DateTime? end)
		{
			return context.AnalysesMetadatas
				.Where(
					am => am.UserSessionId == userSessionId && am.MatchSessionId == matchSessionId
							&& start <= am.RecordingStarted && (end == null || am.RecordingStarted <= end))
				.Include(am => am.FeatureResults)
				.SelectMany(am => am.FeatureResults);
		}
	}
}