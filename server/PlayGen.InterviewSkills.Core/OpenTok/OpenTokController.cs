﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenTok.Server;
using PlayGen.OpenTok.Server;

namespace PlayGen.InterviewSkills.Core.OpenTok
{
	public class OpenTokController
    {
		private readonly OpenTokManager _openTokManager;

		public OpenTokController(OpenTokManager openTokManager)
		{
			_openTokManager = openTokManager;
		}

		public void AddNewOpenTokSession(MatchMaking.Matched.Match match)
        {
			// todo find way to do this without using .Result - has to be blocking to ensure correct event execution order
	        var openTokMatchData = new MatchOpenTokData(_openTokManager.CreateSessionAsync().Result, _openTokManager.ApiKey);

            match.AddCustomData(openTokMatchData);

            match.Participants.ForEach(p =>
            {
	            var token = _openTokManager.GenerateToken(openTokMatchData.Session.Id, p.MatchingParticipant.Id);
	            p.AddCustomData(new MatchedParticipantOpenTokData(token));
            });
        }
	}
}
