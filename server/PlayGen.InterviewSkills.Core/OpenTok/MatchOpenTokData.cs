﻿using OpenTok.Server;
using PlayGen.MatchMaking.Matched;

namespace PlayGen.InterviewSkills.Core.OpenTok
{
    public class MatchOpenTokData : IMatchCustomData
    {
	    public Session Session { get; }
        
        public int ApiKey { get; }

	    public MatchOpenTokData(Session session, int apiKey)
	    {
		    Session = session;
		    ApiKey = apiKey;
	    }
    }
}
