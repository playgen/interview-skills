﻿using PlayGen.MatchMaking.Matched;

namespace PlayGen.InterviewSkills.Core.OpenTok
{
	public class MatchedParticipantOpenTokData : IMatchedParticipantCustomData
    {
		public string Token { get; }

	    public MatchedParticipantOpenTokData(string token)
	    {
		    Token = token;
	    }
    }
}