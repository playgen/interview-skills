﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlayGen.InterviewSkills.Data.Model;

namespace PlayGen.InterviewSkills.Core.Extensions
{
	public static class IFloatValueExtensions
    {
	    public static IEnumerable<T> ListClosest<T>(this IQueryable<T> items, float value, Func<IQueryable<T>, IQueryable<T>> retrieveItems)
			where T : IWeightFromIdeal
	    {
		    var orderedItems = items.OrderBy(item => Math.Abs(value - item.WeightFromIdeal));
		    var retrievedOrderedItems = retrieveItems(orderedItems).ToList();

		    var closestValue = retrievedOrderedItems[0].WeightFromIdeal;
		    var closestItems = retrievedOrderedItems.TakeWhile(oqf => oqf.WeightFromIdeal == closestValue);
		    return closestItems;
	    }
    }
}
