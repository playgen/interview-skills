﻿using System.Collections.Generic;
using System.Linq;
using PlayGen.InterviewSkills.Data.Model.Job;
using PlayGen.InterviewSkills.Data.Storage;
using PlayGen.InterviewSkills.Data.Storage.Extensions;

namespace PlayGen.InterviewSkills.Core
{
	public class JobController
    {
		protected readonly InterviewSkillsContextFactory _contextFactory;

	    public JobController(InterviewSkillsContextFactory contextFactory)
	    {
		    _contextFactory = contextFactory;
	    }
		
	    public List<Job> List(string[] tags = null, bool requireAllTags = true, string[] includes = null)
	    {
		    using (var context = _contextFactory.Create())
		    {
			    IQueryable<Job> jobs = context.Jobs;

			    if (tags?.Length > 0)
			    {
				    if (requireAllTags)
				    {
					    jobs = jobs.Where(
						    job => tags.All(
							    tag => job.TagMappings.Any(
								    tm => tm.Tag.Id == tag)));
					}
				    else
				    {
						jobs = jobs.Where(
							job => tags.Any(
								tag => job.TagMappings.Any(
									tm => tm.Tag.Id == tag)));
					}
			    }

			    jobs = jobs.Include(includes);

			    return jobs.ToList();
		    }
	    }
	}
}
