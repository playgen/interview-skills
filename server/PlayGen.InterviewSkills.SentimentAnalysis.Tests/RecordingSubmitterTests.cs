﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Playgen.Serialization;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingSubmitter;
using PlayGen.InterviewSkills.WebAPI.Tests;
using Xunit;

namespace PlayGen.InterviewSkills.SentimentAnalysis.Tests
{
    public class RecordingSubmitterTests : WebAPITestsBase
    {
		private readonly RecordingSubmitterFactory _recordingSubmitterFactory;
		private readonly ISerializer _serialzier;

		private static string VideoPath
		{
			get
			{
				var type = typeof(RecordingSubmitterTests);
				var assembly = type.GetTypeInfo().Assembly;
				var uriBuilder = new UriBuilder(assembly.CodeBase);
				var rootDir = Path.GetDirectoryName(
					Path.GetDirectoryName(
						Path.GetDirectoryName(
							Path.GetDirectoryName(
								Path.GetDirectoryName(
									Path.GetDirectoryName(
										Uri.UnescapeDataString(uriBuilder.Path)))))));

				return rootDir + "/sewa/test_data/test_short_audio_and_video.webm";
			}
		}

		[Fact]
		private async Task CanConnect()
		{
			string error = null;
			using (var submitter = _recordingSubmitterFactory.Create())
			{
				submitter.ErrorEvent += response => error = response;
				await submitter.ConnectAsync();
			}

			// Assert
			Assert.Null(error);
		}

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		public async Task<(byte[], List<RecordingMetadata>)> CanSubmitVideo(int count)
		{
			// Arrange
			var delay = 1000;
			var timeout = delay * 10 * count;
			var serialized = Array.Empty<byte>();
			var video = File.ReadAllBytes(VideoPath);
			var recordingMetadats = new List<RecordingMetadata>();
			
			for (var i = 0; i < count; i++)
			{
				var metadata = new RecordingMetadata
				{
					Started = 102030405060708090,

					Ended = 908070605040302010,

					BlobLength = video.Length,

					ConnectionData = $"{nameof(CanSubmitVideo)}-connection-data-{i}",

					MimeType = "video/webm",

					StreamId = $"{nameof(CanSubmitVideo)}-stream-id-{i}"
				};

				recordingMetadats.Add(metadata);

				var serializedMetadata = _serialzier.SerializeBinary(metadata).ToArray();

				const byte byteSize = 8;
				var serializedMetadataLength = new byte[2];
				var unserializedLength = serializedMetadata.Length;

				for (var j = serializedMetadataLength.Length - 1; j >= 0; j--)
				{
					serializedMetadataLength[j] = (byte) (unserializedLength & byte.MaxValue);
					unserializedLength = unserializedLength >> byteSize;
				}

				serialized = serialized
					.Concat(serializedMetadataLength)
					.Concat(serializedMetadata)
					.Concat(video)
					.ToArray();
			}

			// Act
			byte[] result = null;
			string error = null;

			using (var submitter = _recordingSubmitterFactory.Create())
			{
				submitter.ErrorEvent += response => error = response;
				submitter.ResultReceivedEvent += response => result = response;
				await submitter.ConnectAsync();

				if (error == null)
				{
					submitter.SubmitRecording(serialized);
				}

				var elapsed = 0;
				while (result == null && error == null)
				{
					await Task.Delay(delay);
					elapsed += delay;

					if (elapsed > timeout)
					{
						throw new TimeoutException();
					}
				}
			}

			// Assert
			Assert.Null(error);
			Assert.True(serialized.Length - (video.Length * count) < result?.Length);

			return (result, recordingMetadats);
		}

		public RecordingSubmitterTests(WebAPITestsFixture fixture) : base(fixture)
		{
			_recordingSubmitterFactory = (RecordingSubmitterFactory)Fixture.Server.Host.Services.GetService(typeof(RecordingSubmitterFactory));
			_serialzier = (ISerializer)Fixture.Server.Host.Services.GetService(typeof(ISerializer));
		}
	}
}
