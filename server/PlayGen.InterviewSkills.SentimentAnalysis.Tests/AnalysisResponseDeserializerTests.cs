﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResponse;
using PlayGen.InterviewSkills.WebAPI.Tests;
using Xunit;

namespace PlayGen.InterviewSkills.SentimentAnalysis.Tests
{
	public class AnalysisResponseDeserializerTests : WebAPITestsBase
	{
		private static readonly Dictionary<string, string[]> ExpectedData = new Dictionary<string, string[]>
		{
			{"Anger", new[] {"Class1prob", "Class2prob", "Prediction"}},
			{"Boredom", new[] {"Class1prob", "Class2prob", "Class3prob", "Prediction"}},
			{"Conflict", new[] {"Class1prob", "Class2prob", "Prediction"}},
			{"Deception", new[] {"Class1prob", "Class2prob", "Prediction"}},
			{"Friendliness", new[] {"Class1prob", "Class2prob", "Prediction"}},
			{"Happiness", new[] {"Class1prob", "Class2prob", "Class3prob", "Prediction"}},
			{"Interest", new[] {"Class1prob", "Class2prob", "Class3prob", "Prediction"}},
			{"Positivity", new[] {"Class1prob", "Class2prob", "Class3prob", "Prediction"}},
			{"Sadness", new[] {"Class1prob", "Class2prob", "Prediction"}},
			{"Sincerity", new[] {"Class1prob", "Class2prob", "Class3prob", "Prediction"}},
			{"Sleepiness", new[] {"Class1prob", "Class2prob", "Class3prob", "Prediction"}},
			{"Stress", new[] {"Class1prob", "Class2prob", "Class3prob", "Prediction"}},
			{"Volume", new[] {"Class1prob", "Class2prob", "Class3prob", "Prediction"}}
		};

		private readonly AnalysisResponseDeserializerFactory _analysisResponseDeserializerFactory;

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		public async Task CanParseResults(int count)
		{
			// Arrange
			var deserializer = _analysisResponseDeserializerFactory.Create();
			var (serializedResponse, sentMetadatas) = await new RecordingSubmitterTests(Fixture).CanSubmitVideo(count);
			
			// Act
			var analysisResponses = deserializer.Deserialize(serializedResponse);

			// Assert
			foreach (var analysisResponse in analysisResponses)
			{
				var sentMetadata = sentMetadatas.Single(sm => sm.StreamId == analysisResponse.RecordingMetadata.StreamId);

				Assert.Equal(analysisResponse.RecordingMetadata.ConnectionData, sentMetadata.ConnectionData);
				Assert.Equal(analysisResponse.RecordingMetadata.BlobLength, sentMetadata.BlobLength);
				Assert.Equal(analysisResponse.RecordingMetadata.Ended, sentMetadata.Ended);
				Assert.Equal(analysisResponse.RecordingMetadata.MimeType, sentMetadata.MimeType);
				Assert.Equal(analysisResponse.RecordingMetadata.Started, sentMetadata.Started);

				// Assert
				Assert.Empty(analysisResponse.Errors);
				Assert.NotEmpty(analysisResponse.FeatureResults);

				foreach (var featureData in ExpectedData)
				{
					var featureResult = analysisResponse.FeatureResults.Single(r => r.Feature == featureData.Key);

					Assert.Equal(featureData.Value.Contains("Class3prob"), featureResult.Classifier3.HasValue);
				}
			}
		}

		public AnalysisResponseDeserializerTests(WebAPITestsFixture fixture) : base(fixture)
		{
			_analysisResponseDeserializerFactory = (AnalysisResponseDeserializerFactory)Fixture.Server.Host.Services.GetService(typeof(AnalysisResponseDeserializerFactory));
		}
	}
}
