﻿using PlayGen.InterviewSkills.WebAPI.Tests;
using Xunit;

namespace PlayGen.InterviewSkills.SentimentAnalysis.Tests
{
	[Collection(nameof(WebAPITestsFixture))]
	public class WebAPITestBase
	{
		protected readonly WebAPITestsFixture Fixture;

		public WebAPITestBase(WebAPITestsFixture fixture)
		{
			Fixture = fixture;
		}
	}
	
	[CollectionDefinition(nameof(WebAPITestsFixture))]
	public class WebAPITestCollection : ICollectionFixture<WebAPITestsFixture>
	{
		// This class has no code, and is never created. Its purpose is simply
		// to be the place to apply [CollectionDefinition] and all the
		// ICollectionFixture<> interfaces.
	}
}
