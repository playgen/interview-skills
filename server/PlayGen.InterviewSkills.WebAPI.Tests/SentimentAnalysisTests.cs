﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Playgen.Serialization;
using PlayGen.InterviewSkills.WebAPI.RemoteCommandControllers;
using PlayGen.RemoteCommands.Command;
using Xunit;

namespace PlayGen.InterviewSkills.WebAPI.Tests
{
	public class SentimentAnalysisTests : ClientTestsBase
	{
		private readonly ISerializer _serializer;

		[Theory]
		[InlineData(null, null)]
		public async Task CanStartListening(string userSessionId = null, WebSocket connection = null)
		{
			// Arrange
			var didCreateConnection = false;

			userSessionId = userSessionId ?? Guid.NewGuid().ToString();
			if (connection == null)
			{
				connection = await Fixture.WebSocketClient.CreateConnection(Fixture.Server);
				didCreateConnection = true;
			}

			var request = new CommandRequest
			{
				Id = "CanStartListening",

				Command = $"{nameof(SentimentAnalysisController)}.{nameof(SentimentAnalysisController.StartListening)}",

				Parameters = new object[] { userSessionId }
			};

			// Act
			await connection.SendText(request, _serializer);
			
			var respone = await connection.AwaitOneCommandResponse(_serializer);

			if (didCreateConnection)
			{
				await connection.Close();
			}

			// Assert
			Assert.Equal(request.Id, respone.RequestId);

			var didStartListening = (bool)respone.Result;
			Assert.True(didStartListening);
		}

		[Fact]
		public async Task CanStopListening()
		{
			// Arrange
			var connection = await Fixture.WebSocketClient.CreateConnection(Fixture.Server);
			var userSessionId = Guid.NewGuid().ToString();

			await CanStartListening(userSessionId, connection);

			var request = new CommandRequest
			{
				Id = "CanStopListening",

				Command = $"{nameof(SentimentAnalysisController)}.{nameof(SentimentAnalysisController.StopListening)}",

				Parameters = new object[] { userSessionId }
			};

			// Act
			await connection.SendText(request, _serializer);
			
			var respone = await connection.AwaitOneCommandResponse(_serializer);

			await connection.Close();
			
			// Assert
			Assert.Equal(request.Id, respone.RequestId);

			var didStopListening = (bool)respone.Result;
			Assert.True(didStopListening);
		}

		public SentimentAnalysisTests(WebAPITestsFixture fixture) : base(fixture)
		{
			_serializer = (ISerializer)Fixture.Server.Host.Services.GetService(typeof(ISerializer));
		}
	}
}
