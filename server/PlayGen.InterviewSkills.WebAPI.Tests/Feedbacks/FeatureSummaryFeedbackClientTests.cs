﻿using System.Collections.Generic;
using System.Linq;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Feedbacks
{
	public class FeatureSummaryFeedbackClientTests : ClientTestsBase
	{
	    [Fact]
	    public async void CanList()
	    {
			// Act 
		    var feedbacks = (await Fixture.HttpClient.GetAsync<IEnumerable<FeatureSummaryFeedback>>("api/featureSummaryFeedback/list" +
																				"?includes=Feature")).ToArray();

			// Assert
		    Assert.NotNull(feedbacks);
		    Assert.NotEmpty(feedbacks);
		    Assert.True(feedbacks.All(f => !string.IsNullOrWhiteSpace(f.Feedback)));
			Assert.True(feedbacks.All(f => f.Feature != null));
			Assert.True(feedbacks.All(f => !string.IsNullOrWhiteSpace(f.Feature.Id)));
		}

	    [Theory]
		[InlineData("sincerity")]
	    [InlineData("happiness")]
		public async void FilterByFeature(string feature)
	    {
			// Arrange 
		    var allFeedbacks = (await Fixture.HttpClient.GetAsync<IEnumerable<FeatureSummaryFeedback>>("api/featureSummaryfeedback/list")).ToArray();

			// Act 
			var filteredFeedbacks = (await Fixture.HttpClient.GetAsync<IEnumerable<FeatureSummaryFeedback>>($"api/featureSummaryFeedback/list" +
																						$"/{feature}" +
																						"?includes=Feature")).ToArray();

		    // Assert
		    Assert.NotNull(filteredFeedbacks);
		    Assert.NotEmpty(filteredFeedbacks);
		    Assert.True(filteredFeedbacks.All(f => f.Feature.Id == feature));
			Assert.True(allFeedbacks.Length > filteredFeedbacks.Length);
	    }

	    [Theory]
		[InlineData("sincerity", -0.5f, -0.1f, 2)]
		[InlineData("sincerity", 0, 0, 2)]
		[InlineData("volume", -0.2, -0.1f, 1)]
	    [InlineData("volume", -0.7, -0.1f, 1)]
	    [InlineData("volume", 0, 0, 1)]
		[InlineData("volume", 0.2, 0.1f, 1)]
		[InlineData("volume", 0.7, 0.1f, 1)]
		public async void FilterByFeatureAndValue(string feature, float weightFromIdeal, float expectedValue, int expectedCount)
	    {
		    // Act 
		    var filteredFeedbacks = (await Fixture.HttpClient.GetAsync<IEnumerable<FeatureSummaryFeedback>>($"api/featureSummaryFeedback/list" +
																									$"/{feature}" +
																									$"/{weightFromIdeal}" +
																									"?includes=Feature")).ToArray();

		    // Assert
		    Assert.NotNull(filteredFeedbacks);
		    Assert.NotEmpty(filteredFeedbacks);
		    Assert.True(filteredFeedbacks.All(f => f.Feature.Id == feature));
		    Assert.True(filteredFeedbacks.All(f => f.WeightFromIdeal == expectedValue));
			Assert.Equal(expectedCount, filteredFeedbacks.Length);
	    }

		public FeatureSummaryFeedbackClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
