﻿//using System;
//using System.Threading.Tasks;
//using FluentAssertions;
//using PlayGen.CustomData;
//using PlayGen.InterviewSkills.Core.Match.Session;
//using PlayGen.InterviewSkills.Core.User;
//using PlayGen.InterviewSkills.Data.Contracts.Role;
//using PlayGen.InterviewSkills.Data.Model.Role;
//using PlayGen.InterviewSkills.WebAPI.Tests.Authentication;
//using PlayGen.InterviewSkills.WebAPI.Tests.Match;
//using PlayGen.MatchMaking.Matched;
//using PlayGen.TimeUtils;
//using Xunit;
//using PlayGen.WebUtils;

//namespace PlayGen.InterviewSkills.WebAPI.Tests.Role
//{
//	public class RoleSessionTests : ClientTestsBase
//	{
//	    [Fact]
//	    public async Task<(RoleSession, RoleSession)> CanGetRoleSession()
//	    {
//			// Arrange
//		    var firstUser = await new GuestAuthenticationClientTestses().CanAuthenticate();
//		    var secondUser = await new GuestAuthenticationClientTestses().CanAuthenticate();

//			var firstUserSessionId = firstUser.GetCustomData<UserSessionData>().Id;
//		    var secondUserSessionId = secondUser.GetCustomData<UserSessionData>().Id;

//			var (firstUserMatchResponse, secondUserMatchResponse) = await new MatchClientTestses().MatchesMatching(firstUserSessionId, secondUserSessionId);
//			var roles = await new RoleClientTestses().CanList();
//		    var jobs = await new JobTests().CanList();

//		    var firstUserMatchSessionId = firstUserMatchResponse.CustomData.GetCustomData<IMatchCustomData, MatchSessionData>().Id;
//		    var secondUserMatchSessionId = secondUserMatchResponse.CustomData.GetCustomData<IMatchCustomData, MatchSessionData>().Id;

//			var before = EpochUtil.FromMilliseconds(EpochUtil.ToMilliseconds(DateTime.UtcNow));
//		    var jobId = jobs[0].Id;
//		    var firstUserRoleId = roles[0].Id;
//		    var secondUserRoleId = roles[1].Id;

//			// Act
//			var firstUserRoleSession = await Fixture.Client.PostAsync<RoleSession>($"api/rolesession/add", new RoleSessionRequest
//		    {
//			    UserSessionId = firstUserSessionId,
//				MatchSessionId = firstUserMatchSessionId,
//				Created = EpochUtil.ToMilliseconds(DateTime.UtcNow),
//				JobId = jobId,
//				RoleId = firstUserRoleId
//		    });

//		    var secondUserRoleSession = await Fixture.Client.PostAsync<RoleSession>($"api/rolesession/add", new RoleSessionRequest
//		    {
//			    UserSessionId = secondUserSessionId,
//			    MatchSessionId = secondUserMatchSessionId,
//			    Created = EpochUtil.ToMilliseconds(DateTime.UtcNow),
//				JobId = jobId,
//			    RoleId = secondUserRoleId
//		    });

//		    var after = EpochUtil.FromMilliseconds(EpochUtil.ToMilliseconds(DateTime.UtcNow));

//			// Assert
//		    firstUserRoleSession.Should().NotBeNull();
//		    firstUserSessionId.Should().Be(firstUserRoleSession.UserSessionId.ToString());
//			firstUserMatchSessionId.Should().Be(firstUserRoleSession.MatchSessionId);
//		    firstUserRoleSession.JobId.Should().Be(jobId);
//		    firstUserRoleSession.RoleId.Should().Be(firstUserRoleId);
//		    firstUserRoleSession.Created.Should().BeOnOrAfter(before).And.BeOnOrBefore(after);

//			secondUserRoleSession.Should().NotBeNull();
//		    secondUserSessionId.Should().Be(secondUserRoleSession.UserSessionId.ToString());
//		    secondUserMatchSessionId.Should().Be(secondUserRoleSession.MatchSessionId);
//		    secondUserRoleSession.JobId.Should().Be(jobId);
//		    secondUserRoleSession.RoleId.Should().Be(secondUserRoleId);
//		    secondUserRoleSession.Created.Should().BeOnOrAfter(before).And.BeOnOrBefore(after);

//			return (firstUserRoleSession, secondUserRoleSession);
//	    }

//	    [Fact]
//	    public async Task<(RoleSession, RoleSession)> CanEnd()
//	    {
//		    // Arrange
//		    var (firstUserRoleSession, secondUserRoleSession) = await CanGetRoleSession();

//			// Act
//		    var before = EpochUtil.FromMilliseconds(EpochUtil.ToMilliseconds(DateTime.UtcNow));

//		    var ended = EpochUtil.ToMilliseconds(DateTime.UtcNow);
//			var firstUserEndedRoleSession = await Fixture.Client.PostAsync<RoleSession>($"api/rolesession/end/{firstUserRoleSession.Id}/{ended}");
//			var secondUserEndedRoleSession = await Fixture.Client.PostAsync<RoleSession>($"api/rolesession/end/{secondUserRoleSession.Id}/{ended}");

//		    var after = EpochUtil.FromMilliseconds(EpochUtil.ToMilliseconds(DateTime.UtcNow));

//			// Assert
//			firstUserRoleSession.Should().NotBeNull();
//		    firstUserRoleSession.Id.Should().Be(firstUserEndedRoleSession.Id);
//		    firstUserEndedRoleSession.Ended.Should().BeOnOrAfter(before).And.BeOnOrBefore(after);

//			secondUserRoleSession.Should().NotBeNull();
//		    secondUserRoleSession.Id.Should().Be(secondUserEndedRoleSession.Id);
//		    secondUserEndedRoleSession.Ended.Should().BeOnOrAfter(before).And.BeOnOrBefore(after);

//			return (firstUserEndedRoleSession, secondUserEndedRoleSession);
//		}
//    }
//}
