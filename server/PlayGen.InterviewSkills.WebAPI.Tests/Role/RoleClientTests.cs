﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Role
{
    public class RoleClientTests : ClientTestsBase
	{
	    [Fact]
	    public async Task<List<Data.Model.Role.Role>> CanList()
	    {
			// Act
			var roles = await Fixture.HttpClient.GetAsync<List<Data.Model.Role.Role>>("api/role/list");

			// Assert
			Assert.NotNull(roles);
			Assert.NotEmpty(roles);

		    return roles;
	    }

		public RoleClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
