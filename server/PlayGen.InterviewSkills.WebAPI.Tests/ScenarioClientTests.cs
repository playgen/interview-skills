﻿using FluentAssertions;
using Newtonsoft.Json;
using PlayGen.Scenario.Step;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests
{
	public class ScenarioClientTests : ClientTestsBase
    {
	    [Theory]
	    [InlineData("JobInterview", "Default", false)]
		[InlineData("JobInterview", "Default", true)]
		public async void CanDeserializeScenarioSteps(string category, string name, bool isSynchronised)
	    {
		    // Act
		    var scenario = await Fixture.HttpClient.GetAsync<Data.Model.Scenario>($"/api/scenario/{category}/{name}/{isSynchronised}");

		    // Assert
			scenario.Should().NotBeNull();
			scenario.Category.Should().Be(category);
			scenario.Name.Should().Be(name);
			scenario.IsSynchronised.Should().Be(isSynchronised);
			
		    var deserializedSteps = JsonConvert.DeserializeObject<Step[]>(scenario.Steps);
			deserializedSteps.Should().NotBeNullOrEmpty();
	    }

	    public ScenarioClientTests(WebAPITestsFixture fixture) : base(fixture)
	    {
	    }
    }
}
