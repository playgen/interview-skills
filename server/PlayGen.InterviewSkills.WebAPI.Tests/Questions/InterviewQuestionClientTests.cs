﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using PlayGen.InterviewSkills.Data.Model.Questions.Interview;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Questions
{
	public class InterviewQuestionClientTests : ClientTestsBase
	{
		[Fact]
		public async Task<List<InterviewQuestion>> CanList()
		{
			// Act 
			var questions = await Fixture.HttpClient.GetAsync<List<InterviewQuestion>>("api/interviewquestion/list");

			// Assert
			Assert.NotNull(questions);
			Assert.NotEmpty(questions);
			Assert.True(questions.All(q => !string.IsNullOrWhiteSpace(q.Question)));

			return questions;
		}

		[Theory]
		[InlineData("careergoal")]
		[InlineData("character")]
		[InlineData("curveball")]
		[InlineData("competency")]
		public async void FilterByTag(params string[] tags)
		{
			// Act 
			var firstTag = tags[0];
			var remainingTags = tags.Skip(1).Take(tags.Length - 1).ToArray();

			var filteredQuestions = await Fixture.HttpClient.GetAsync<List<InterviewQuestion>>($"/api/interviewQuestion/list" +
															$"?tags={firstTag}" +
															$"{string.Join("&tags=", remainingTags)}" +
															"&includes=TagMappings");
			
			// Assert
			filteredQuestions.Should().NotBeEmpty();
			filteredQuestions.Should().OnlyContain(q => tags.All(t => q.TagMappings.Any(qtm => qtm.Tag.Id == t)));
		}

		public InterviewQuestionClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
