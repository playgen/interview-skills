﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlayGen.InterviewSkills.Data.Model.Questions.Observational;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Questions
{
	public class ObservationalQuestionClientTests : ClientTestsBase
	{
		[Fact]
		public async Task<ObservationalQuestion[]> CanList()
		{
			// Act 
			var questions = (await Fixture.HttpClient.GetAsync<IEnumerable<ObservationalQuestion>>("api/observationalquestion/list")).ToArray();

			// Assert
			Assert.NotNull(questions);
			Assert.NotEmpty(questions);
			Assert.True(questions.All(q => !string.IsNullOrWhiteSpace(q.Question)));
			Assert.True(questions.All(q => !string.IsNullOrWhiteSpace(q.StartLabel)));
			Assert.True(questions.All(q => !string.IsNullOrWhiteSpace(q.EndLabel)));

			return questions;
		}

		[Theory]
		[InlineData("happiness")]
		[InlineData("sincerity")]
		public async void FilterByFeature(params string[] features)
		{
			// Act 
			var firstFeature = features[0];
			var remainingFeatures = features.Skip(1).Take(features.Length - 1).ToArray();

			var filteredQuestions = (await Fixture.HttpClient.GetAsync<IEnumerable<ObservationalQuestion>>("/api/observationalQuestion/list" +
																				$"?features={firstFeature}" +
																				$"{string.Join("&features=", remainingFeatures)}" +
																				"&includes=FeatureMappings")).ToArray();
			
			// Assert
			Assert.NotNull(filteredQuestions);
			Assert.NotEmpty(filteredQuestions);
			Assert.True(filteredQuestions.All(q => features.All(t => q.FeatureMappings.Any(qtm => qtm.Feature.Id == t))));
		}

		public ObservationalQuestionClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
