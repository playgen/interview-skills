﻿using System;
using Xunit;

namespace PlayGen.InterviewSkills.WebAPI.Tests
{
	[Collection(nameof(WebAPITestsFixture))]
	public class ClientTestsBase
	{
		protected readonly WebAPITestsFixture Fixture;

		public ClientTestsBase(WebAPITestsFixture fixture)
		{
			Fixture = fixture;
		}
	}
	
	[CollectionDefinition(nameof(WebAPITestsFixture))]
	public class ClientTestsCollection : ICollectionFixture<WebAPITestsFixture>
	{
		// This class has no code, and is never created. Its purpose is simply
		// to be the place to apply [CollectionDefinition] and all the
		// ICollectionFixture<> interfaces.
	}
}
