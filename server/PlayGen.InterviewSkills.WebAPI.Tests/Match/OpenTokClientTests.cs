﻿using PlayGen.CustomData;
using PlayGen.InterviewSkills.Core.OpenTok;
using PlayGen.InterviewSkills.Core.User;
using PlayGen.InterviewSkills.Data.Contracts.Match.CustomData.OpenTok;
using PlayGen.InterviewSkills.WebAPI.Tests.Authentication;
using PlayGen.MatchMaking.Matched;
using Xunit;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Match
{
	public class OpenTokClientTests : MatchClientTestsBase
    {
		[Fact]
		private async void GetsOpenTokSession()
		{
			// Arrange
			var firstParticipantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;
			var secondParticipantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;
			var (firstParticipantMatchResponse, secondParticipantMatchResponse) = await FindMatch(firstParticipantId: firstParticipantId, secondParticipantId: secondParticipantId);

			// Assert
			var firstMatchOpenTokData = firstParticipantMatchResponse[0].CustomData.GetCustomData<IMatchCustomData, MatchOpenTokDataResponse>();
		    var secondMatchOpenTokData = secondParticipantMatchResponse[0].CustomData.GetCustomData<IMatchCustomData, MatchOpenTokDataResponse>();

			Assert.True(firstMatchOpenTokData.ApiKey != default(int));
		    Assert.False(string.IsNullOrWhiteSpace(firstMatchOpenTokData.SessionId));

		    Assert.Equal(firstMatchOpenTokData.ApiKey, secondMatchOpenTokData.ApiKey);
		    Assert.True(firstMatchOpenTokData.SessionId == secondMatchOpenTokData.SessionId);

		    var firstUserOpenTokData = firstParticipantMatchResponse[0].GetParticipantById(firstParticipantId)
		        .GetCustomData<MatchedParticipantOpenTokData>();
		    var secondUserOpenTokData = secondParticipantMatchResponse[0].GetParticipantById(secondParticipantId)
		        .GetCustomData<MatchedParticipantOpenTokData>();

			Assert.False(string.IsNullOrWhiteSpace(firstUserOpenTokData.Token));
			Assert.False(string.IsNullOrWhiteSpace(secondUserOpenTokData.Token));

			Assert.False(firstUserOpenTokData.Token == secondUserOpenTokData.Token);
		}

	    public OpenTokClientTests(WebAPITestsFixture fixture) : base(fixture)
	    {
	    }
    }
}
