﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlayGen.InterviewSkills.Core.User;
using PlayGen.InterviewSkills.Data.Contracts.Match;
using PlayGen.InterviewSkills.WebAPI.Tests.Authentication;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Match
{
	public abstract class MatchClientTestsBase : ClientTestsBase
	{
		public const string FindMatchesUri = "/api/match/find/{0}/{1}/{2}?cantMatchPrevious={3}";
		public const string NewMatchUri = "/api/match/new/{0}/{1}/{2}?cantMatchPrevious={3}";
		public const string ExistingMatchesUri = "/api/match/existing/{0}";
		public const string CreateMatchUri = "/api/match/create/{0}/{1}?participantIds={2}";

		public const string DefaultScenarioCategory = "JobInterview";
		public const string DefaultScenarioName = "Default";

		public async Task<(List<MatchResponse>, List<MatchResponse>)> FindMatch(string scenarioCategory = DefaultScenarioCategory, string scenarioName = DefaultScenarioName, string firstParticipantId = null, string secondParticipantId = null)
		{
			// Arrange
			firstParticipantId = firstParticipantId ?? (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;
			secondParticipantId = secondParticipantId ?? (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;

			// Act
			// Because there are no available players at this point, this initial request 
			// shouldn't return any match data
			// but if it does, burn through all the matches until no matches are found
			MatchResponse firstParticipantMatchResponse;
			do
			{
				firstParticipantMatchResponse = await Fixture.HttpClient.GetAsync<MatchResponse>(
					string.Format(
						NewMatchUri,
						firstParticipantId,
						scenarioCategory,
						scenarioName,
						false));
			} while (firstParticipantMatchResponse != null);


			var secondParticipantMatchResponses = await Fixture.HttpClient.GetAsync<List<MatchResponse>>(
				string.Format(FindMatchesUri, secondParticipantId, scenarioCategory, scenarioName, false));

			var firstParticipantMatchResponses = await Fixture.HttpClient.GetAsync<List<MatchResponse>>(
				string.Format(FindMatchesUri, firstParticipantId, scenarioCategory, scenarioName, false));

			return (firstParticipantMatchResponses, secondParticipantMatchResponses);
		}

		protected MatchClientTestsBase(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}