﻿using FluentAssertions;
using PlayGen.CustomData;
using PlayGen.InterviewSkills.Core.Scenario;
using PlayGen.MatchMaking.Matched;
using Xunit;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Match
{
	public class ScenarioClientTests : MatchClientTestsBase
    {
        [Fact]
        public async void GetsScenarioForMatch()
        {
            // Arrange
			var (firstParticipantMatchResponse, secondParticipantMatchResponse) = await FindMatch(DefaultScenarioCategory, DefaultScenarioName);

            // Assert
		    var firstParticipantScenarioData = firstParticipantMatchResponse[0].CustomData.GetCustomData<IMatchCustomData, MatchScenarioData>();
			var secondParticipantScenarioData = secondParticipantMatchResponse[0].CustomData.GetCustomData<IMatchCustomData, MatchScenarioData>();

			Assert.NotNull(firstParticipantScenarioData.Scenario);
			Assert.NotNull(secondParticipantScenarioData.Scenario);

            Assert.Equal(firstParticipantScenarioData.Scenario.Category, DefaultScenarioCategory);
            Assert.Equal(firstParticipantScenarioData.Scenario.Category, secondParticipantScenarioData.Scenario.Category);
            Assert.Equal(firstParticipantScenarioData.Scenario.Name, secondParticipantScenarioData.Scenario.Name);
        }

		[Fact]
		public async void GetsNonSynchronisedScenarioForOneParticipantMatch()
		{
			// Act
			var matchResponse = await new MatchClientTests(Fixture).CanCreateVariableParticipantMatch(1);

			// Assert
			var matchScenarioData = matchResponse.CustomData.GetCustomData<IMatchCustomData, MatchScenarioData>();
			matchScenarioData.Scenario.IsSynchronised.Should().BeFalse();
		}

		[Fact]
		public async void GetsSynchronisedScenarioForTwoParticipantMatch()
		{
			// Act
			var matchResponse = await new MatchClientTests(Fixture).CanCreateVariableParticipantMatch(2);

			// Assert
			var matchScenarioData = matchResponse.CustomData.GetCustomData<IMatchCustomData, MatchScenarioData>();
			matchScenarioData.Scenario.IsSynchronised.Should().BeTrue();
		}

		public ScenarioClientTests(WebAPITestsFixture fixture) : base(fixture)
	    {
	    }
    }
}
