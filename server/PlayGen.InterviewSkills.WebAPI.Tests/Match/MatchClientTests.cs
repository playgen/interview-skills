﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using PlayGen.InterviewSkills.Core.User;
using PlayGen.InterviewSkills.Data.Contracts.Match;
using PlayGen.InterviewSkills.WebAPI.Tests.Authentication;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Match
{
	public class MatchClientTests : MatchClientTestsBase
	{
		[Theory]
		[InlineData(null, null)]
		public async Task<(List<MatchResponse>, List<MatchResponse>)> MatchesMatching(string firstParticipantId = null, string secondParticipantId = null)
		{
			// Arrange
			firstParticipantId = firstParticipantId ?? (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;
			secondParticipantId = secondParticipantId ?? (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;

			// Act
			var (firstParticipantMatchResponses, secondParticipantMatchResponses) = await FindMatch(firstParticipantId: firstParticipantId, secondParticipantId: secondParticipantId);

			// Assert
			Assert.NotNull(firstParticipantMatchResponses);
			Assert.NotNull(secondParticipantMatchResponses);

			Assert.Equal(2, firstParticipantMatchResponses[0].Participants.Length);
			Assert.Contains(firstParticipantId, firstParticipantMatchResponses[0].Participants.Select(p => p.MatchingParticipant.Id));
			Assert.Contains(secondParticipantId, firstParticipantMatchResponses[0].Participants.Select(p => p.MatchingParticipant.Id));

			Assert.False(firstParticipantMatchResponses[0].GetParticipantById(firstParticipantId).IsMaster);
			Assert.True(firstParticipantMatchResponses[0].GetParticipantById(secondParticipantId).IsMaster);

			Assert.Equal(firstParticipantMatchResponses[0].Participants.Select(p => p.MatchingParticipant.Id),
				secondParticipantMatchResponses[0].Participants.Select(p => p.MatchingParticipant.Id));

			return (firstParticipantMatchResponses, secondParticipantMatchResponses);
		}

		[Fact]
		public async void DoesntMatchNonMatching()
		{
			// Arrange
			var firstParticipantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;
			var secondParticipantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;

			// Act
			// Because there are no available players at this point, this initial request 
			// shouldn't return any match data
			await Fixture.HttpClient.GetAsync<List<MatchResponse>>(string.Format(FindMatchesUri, firstParticipantId, DefaultScenarioCategory, "one", false));
			var secondParticipantMatchResponses =
				await Fixture.HttpClient.GetAsync<List<MatchResponse>>(string.Format(FindMatchesUri, secondParticipantId, DefaultScenarioCategory, "two", false));
			var firstParticipantMatchResponses =
				await Fixture.HttpClient.GetAsync<List<MatchResponse>>(string.Format(FindMatchesUri, firstParticipantId, DefaultScenarioCategory, "one", false));

			// Assert
			firstParticipantMatchResponses.Should().BeEmpty();
			secondParticipantMatchResponses.Should().BeEmpty();
		}

		[Fact]
		public async void RemovesMatchWhenAParticipantFindsNew()
		{
			// Arrange
			var firstParticipantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;
			var secondParticipantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;

			// Need to have been previously matched
			await MatchesMatching(firstParticipantId, secondParticipantId);

			// Act
			var firstParticipantMatchResponse = await Fixture.HttpClient.GetAsync<MatchResponse>(
				string.Format(NewMatchUri, firstParticipantId, DefaultScenarioCategory, DefaultScenarioName, false));
			var secondParticipantMatchResponses = await Fixture.HttpClient.GetAsync<List<MatchResponse>>(string.Format(ExistingMatchesUri, secondParticipantId));

			// Assert
			firstParticipantMatchResponse.Should().BeNull();
			secondParticipantMatchResponses.Should().BeEmpty();
		}

		[Fact]
		public async void DoesntMatchPreviouslyMatchedIfCantMatchSpecified()
		{
			// Arrange
			var firstParticipantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;
			var secondParticipantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;

			// Need to have been previously matched
			await MatchesMatching(firstParticipantId, secondParticipantId);

			// Set to searching for match
			var firstParticipantMatchResponse = await Fixture.HttpClient.GetAsync<MatchResponse>(
				string.Format(NewMatchUri, firstParticipantId, DefaultScenarioCategory, DefaultScenarioName, false));

			const bool cantMatchPrevious = true;

			// Act
			// Set to searching for match, shouldn't find as already matched
			var secondParticipantMatchResponse = await Fixture.HttpClient.GetAsync<MatchResponse>(
				string.Format(NewMatchUri, secondParticipantId, DefaultScenarioCategory, DefaultScenarioName, cantMatchPrevious));

			// Assert
			firstParticipantMatchResponse.Should().BeNull();
			secondParticipantMatchResponse.Should().BeNull();
		}

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		public async Task<MatchResponse> CanCreateVariableParticipantMatch(int participantCount)
		{
			// Arrange
			var participantIds = new List<string>();
			for (var i = 0; i < participantCount; i++)
			{
				var participantId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate())
					.GetCustomData<UserSessionData>().Id;

				participantIds.Add(participantId);
			}

			// Act
			var uri = string.Format(CreateMatchUri, DefaultScenarioCategory, DefaultScenarioName, participantIds[0]);

			for(var i = 1; i < participantIds.Count; i++)
			{
				uri += $"&participantIds={participantIds[i]}";
			}

			var matchResponse = await Fixture.HttpClient.PostAsync<MatchResponse>(uri);

			// Assert
			matchResponse.Should().NotBeNull();
			matchResponse.Participants.Should().HaveCount(participantCount);
			matchResponse.Participants.Select(p => p.MatchingParticipant.Id).Should().BeSubsetOf(participantIds);

			return matchResponse;
		}

		public MatchClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
