﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace PlayGen.InterviewSkills.WebAPI.Tests
{
    public class WebSocketTests : WebAPITestsBase
    {
		[Fact]
		public async Task CanConnect()
		{
			await Fixture.WebSocketClient.CreateConnection(Fixture.Server);
		}

		[Fact]
		public async Task CanConnectSecurely()
		{
			await Fixture.WebSocketClient.CreateSecureConnection(Fixture.Server);
		}

		public WebSocketTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}

		public const int BufferSize = 10 * 1024 * 1024;
	}
}
