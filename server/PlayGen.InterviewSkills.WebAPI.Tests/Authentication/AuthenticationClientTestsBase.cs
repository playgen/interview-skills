﻿using System.Threading.Tasks;
using PlayGen.InterviewSkills.Core.Authentication;
using PlayGen.InterviewSkills.Core.User;
using Xunit;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Authentication
{
	public abstract class AuthenticationClientTestsBase : ClientTestsBase
	{
	    [Fact]
	    public abstract Task<AuthenticatedUser> CanAuthenticate();

	    [Fact]
	    public async void CanGetUserSessionData()
	    {
		    // Act
		    var authenticated = await CanAuthenticate();
		    var userSessionData = authenticated.GetCustomData<UserSessionData>();

		    // Assert
		    Assert.NotNull(userSessionData);
		    Assert.NotNull(userSessionData.Id);
		    Assert.NotEmpty(userSessionData.Id);
	    }

		protected AuthenticationClientTestsBase(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
