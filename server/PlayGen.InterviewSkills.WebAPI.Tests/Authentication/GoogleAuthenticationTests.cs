﻿using PlayGen.WebUtils;
﻿using System.Net;
using Xunit;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Authentication
{
	public class GoogleAuthenticationClientTests : ClientTestsBase// AuthenticationClientTestsBase
	{
		[Fact]
		public async void DoesntAuthenticateInvalid()
		{
			// Arrange
			const string token = "somenonsense";

			// Act
			using (var responseMessage = await Fixture.HttpClient.PostAsync($"/api/googleauthentication/authenticate", token))
			{

				// Assert			
				Assert.Equal(HttpStatusCode.Unauthorized, responseMessage.StatusCode);
				Assert.NotEmpty(responseMessage.ReasonPhrase);
			}
		}

		//[Fact]
		//public override AuthenticatedUser CanAuthenticate()
		//{
		//	// Act
		//	var authenticated = TestUtil.Client.Post<AuthenticatedUser>("/api/googleauthentication/");

		//	// Assert
		//	Assert.NotNull(authenticated);

		//	return authenticated;
		//}		
		public GoogleAuthenticationClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
