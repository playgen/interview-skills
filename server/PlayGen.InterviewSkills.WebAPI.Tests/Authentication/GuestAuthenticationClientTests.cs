﻿using System.Threading.Tasks;
using PlayGen.InterviewSkills.Core.Authentication;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.Authentication
{
	public class GuestAuthenticationClientTests : AuthenticationClientTestsBase
	{
		[Fact]
		public override async Task<AuthenticatedUser> CanAuthenticate()
		{
			// Act
			var authenticated = await Fixture.HttpClient.PostAsync<AuthenticatedUser>("/api/guestauthentication/authenticate");

			// Assert
			Assert.NotNull(authenticated);

			return authenticated;
		}

		public GuestAuthenticationClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
