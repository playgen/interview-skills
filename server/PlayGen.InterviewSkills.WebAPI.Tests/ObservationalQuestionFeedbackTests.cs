﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using PlayGen.InterviewSkills.Data.Contracts.Feedbacks;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Model.Questions.Observational;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests
{
	public class ObservationalQuestionTFeedbackClientTestses : ClientTestsBase
	{
		[Fact]
		public async void CanList()
		{
			// Act 
			var feedbacks = (await Fixture.HttpClient.GetAsync<IEnumerable<ObservationalQuestionFeedback>>("api/observationalquestionfeedback/list" +
																				"?includes=Question")).ToArray();

			// Assert
			Assert.NotNull(feedbacks);
			Assert.NotEmpty(feedbacks);
			Assert.True(feedbacks.All(f => !string.IsNullOrWhiteSpace(f.Feedback)));
			Assert.True(feedbacks.All(f => f.Question != null));
			Assert.True(feedbacks.All(f => !string.IsNullOrWhiteSpace(f.Question.Question)));
		}

		[Fact]
		public async void CanListFeedbacksForQuestion()
		{
			// Arrange
			var questions = await Fixture.HttpClient.GetAsync<List<ObservationalQuestion>>("api/observationalquestion/list");

			// Act 
			foreach (var question in questions)
			{
				var feedbacks = await Fixture.HttpClient.GetAsync<List<ObservationalQuestionFeedback>>("api/observationalquestionfeedback/list" +
																						$"?questionId={question.Id}");

				// Assert
				feedbacks.Should().NotBeNull();
				feedbacks.Should().NotBeEmpty();
				feedbacks.ForEach(f => f.QuestionId.Should().Be(question.Id));
				
				feedbacks.Should().HaveCount(question.IsBinary ? 2 : 3, $"Question is: \"{question.Question}\" and IsBinary: {question.IsBinary}");
			}
		}

		[Theory]
		[InlineData(0f)]
		[InlineData(0.5)]
		[InlineData(1)]
		public async void CanListFeedbackForQuestionAndWeight(float weight)
		{
			// Arrange
			var questions = await Fixture.HttpClient.GetAsync<List<ObservationalQuestion>>("api/observationalquestion/list?includes=FeatureMappings");

			// Act 
			foreach (var question in questions)
			{
				var featureId = question.FeatureMappings.ElementAt(0).Feature.Id;

				var closestFeedbacksForQuestionAndValue = await Fixture.HttpClient.GetAsync<ObservationalQuestionFeedbacksForWeightResponse>(
					"api/observationalquestionfeedback/list" +
					$"/{question.Id}" +
					$"/{featureId}" +
					$"/{weight}");

				Assert.NotEmpty(closestFeedbacksForQuestionAndValue.Feedbacks);
			}
		}

		public ObservationalQuestionTFeedbackClientTestses(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
