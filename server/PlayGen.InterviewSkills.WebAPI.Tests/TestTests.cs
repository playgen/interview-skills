﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlayGen.InterviewSkills.Data.Model.Job;
using PlayGen.InterviewSkills.WebAPI.Controllers;
using Xunit;
using PlayGen.WebUtils;
using System.Net.WebSockets;
using System.Threading;
using PlayGen.RemoteCommands.Command;

namespace PlayGen.InterviewSkills.WebAPI.Tests
{
	public class TestTests : ClientTestsBase
	{
		[Fact]
		public async Task<string> CanGetHttpResponse()
		{
			// Act
			var response = await Fixture.HttpClient.GetStringAsync("/api/test");

			// Assert
			Assert.NotNull(response);
			Assert.True(response.Length > 0);

			return response;
		}

		public TestTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
