﻿using System;
using System.Net.Http;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Xunit;

namespace PlayGen.InterviewSkills.WebAPI.Tests
{
	public class WebAPITestsBase : IClassFixture<WebAPITestsFixture>
	{
		protected readonly WebAPITestsFixture Fixture;

		public WebAPITestsBase(WebAPITestsFixture fixture)
		{
			Fixture = fixture;
		}
	}

	public class WebAPITestsFixture : IDisposable
	{
		public readonly TestServer Server;
		public readonly HttpClient HttpClient;
		public readonly WebSocketClient WebSocketClient;

		public WebAPITestsFixture()
		{
			var builder = WebHost.CreateDefaultBuilder()
				.UseStartup<Startup>()
				.UseEnvironment("Development");

			Server = new TestServer(builder);
			HttpClient = Server.CreateClient();
			WebSocketClient = Server.CreateWebSocketClient();

			Program.SetupDatabase(Server.Host);

			JsonConvert.DefaultSettings = () => new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.All
			};
		}

		public void Dispose()
		{
			HttpClient.Dispose();
			Server.Dispose();
		}
	}
}
