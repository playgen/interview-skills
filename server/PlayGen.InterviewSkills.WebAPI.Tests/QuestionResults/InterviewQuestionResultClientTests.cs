﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using PlayGen.CustomData;
using PlayGen.InterviewSkills.Core.Match.Session;
using PlayGen.InterviewSkills.Data.Model.QuestionResults;
using PlayGen.InterviewSkills.Scenario.Model.Commands.InterviewQuestion;
using PlayGen.InterviewSkills.WebAPI.Tests.Match;
using PlayGen.InterviewSkills.WebAPI.Tests.Questions;
using PlayGen.MatchMaking.Matched;
using Xunit;
using PlayGen.WebUtils;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.QuestionResults
{
	public class InterviewQuestionResultClientTests : ClientTestsBase
	{
		[Fact]
		public async Task<InterviewQuestionResult> CanSetAllStates()
		{
			var result = await CanSetUpdateStates();
			return await CanSetHidden(result.QuestionId, result.MatchSessionId);
		}

		[Fact]
		public async Task<InterviewQuestionResult> CanSetUpdateStates()
		{
			var (questionId, matchSessionId) = await GetQuestionAndMatch();
			return await CanSetUpdateStates(questionId, matchSessionId);
		}

		public async Task<InterviewQuestionResult> CanSetUpdateStates(int questionId, string matchSessionId)
		{ 
			// Arrange
			var updated = new Dictionary<string, long>();
			InterviewQuestionResult updatedResult = null;

			// Act
			foreach (var state in Enum.GetNames(typeof(InterviewQuestionStates)))
			{
				updated[state] = EpochUtil.ToUnixTimestamp(DateTime.UtcNow);

				updatedResult = await Fixture.HttpClient.PostAsync<InterviewQuestionResult>($"api/interviewQuestionResult" +
																				$"/{questionId}" +
																				$"/{matchSessionId}" +
																				$"/updated" +
																				$"/{state}" +
																				$"/{updated[state]}");
			}

			// Assert
			updatedResult.Should().NotBeNull();
			updatedResult.QuestionId.Should().Be(questionId);
			updatedResult.MatchSessionId.Should().Be(matchSessionId);

			updatedResult.Asking.Should().HaveValue();
			updatedResult.Asking.Value.Should()
				.BeCloseTo(EpochUtil.FromUnixTimestamp(updated[nameof(InterviewQuestionStates.Asking)]), 1000);

			updatedResult.Answering.Should().HaveValue();
			updatedResult.Answering.Value.Should()
				.BeCloseTo(EpochUtil.FromUnixTimestamp(updated[nameof(InterviewQuestionStates.Answering)]), 1000);

			updatedResult.Answered.Should().HaveValue();
			updatedResult.Answered.Value.Should()
				.BeCloseTo(EpochUtil.FromUnixTimestamp(updated[nameof(InterviewQuestionStates.Answered)]), 1000);

			return updatedResult;
		}

		[Fact]
		public async Task<InterviewQuestionResult> CanSetHidden()
		{
			var (questionId, matchSessionId) = await GetQuestionAndMatch();
			return await CanSetHidden(questionId, matchSessionId);
		}

		public async Task<InterviewQuestionResult> CanSetHidden(int questionId, string matchSessionId)
		{
			// Arrange
			var hidden = EpochUtil.ToUnixTimestamp(DateTime.UtcNow);

			// Act
			var hiddenResult = await Fixture.HttpClient.PostAsync<InterviewQuestionResult>($"api/interviewQuestionResult" +
																				$"/{questionId}" +
																				$"/{matchSessionId}" +
																				$"/hidden" +
																				$"/{hidden}");

			// Assert
			hiddenResult.QuestionId.Should().Be(questionId);
			hiddenResult.MatchSessionId.Should().Be(matchSessionId);
			hiddenResult.Hidden.Should().HaveValue();
			hiddenResult.Hidden.Value.Should().BeCloseTo(EpochUtil.FromUnixTimestamp(hidden), 1000);

			return hiddenResult;
		}

		private async Task<(int questionId, string matchSessionId)> GetQuestionAndMatch()
		{
			var questions = await new InterviewQuestionClientTests(Fixture).CanList();
			var(firstParticipantResponses, _) = await new MatchClientTests(Fixture).MatchesMatching();

			var questionId = questions[0].Id;
			var matchSessionId = firstParticipantResponses[0].CustomData.GetCustomData<IMatchCustomData, MatchSessionData>().Id;

			return (questionId, matchSessionId);
		}

		public InterviewQuestionResultClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
