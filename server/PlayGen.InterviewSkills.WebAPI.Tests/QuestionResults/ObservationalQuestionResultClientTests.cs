﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using PlayGen.CustomData;
using PlayGen.InterviewSkills.Core.Match.Session;
using PlayGen.InterviewSkills.Core.User;
using PlayGen.InterviewSkills.Data.Model.QuestionResults;
using PlayGen.InterviewSkills.WebAPI.Tests.Authentication;
using PlayGen.InterviewSkills.WebAPI.Tests.Match;
using PlayGen.InterviewSkills.WebAPI.Tests.Questions;
using PlayGen.MatchMaking.Matched;
using PlayGen.TimeUtils;
using Xunit;
using PlayGen.WebUtils;

namespace PlayGen.InterviewSkills.WebAPI.Tests.QuestionResults
{
	public class ObservationalQuestionResultClientTests : ClientTestsBase
	{
		[Fact]
		public async Task<ObservationalQuestionResult> CanPostAll()
		{
			var received = await CanPostReceived();
			var shown = await CanPostShown(received.QuestionId, received.UserSessionId.ToString(), received.MatchSessionId, received.Received);
			var submitted = await CanPostShown(shown.QuestionId, shown.UserSessionId.ToString(), shown.MatchSessionId, shown.Received);
			return submitted;
		}

		[Fact]
		public async Task<ObservationalQuestionResult> CanPostReceived()
		{
			var (questionId, userSessionId, matchSessionId) = await GetQuestionAndUserSessionAndMatch();
			return await CanPostReceived(questionId, userSessionId, matchSessionId);
		}

		private async Task<ObservationalQuestionResult> CanPostReceived(int questionId, string userSessionId ,string matchSessionId)
		{
			// Arrange
			var received = EpochUtil.ToUnixTimestamp(DateTime.UtcNow);

			// Act
			var receivedResult = await Fixture.HttpClient.PostAsync<ObservationalQuestionResult>($"api/observationalQuestionResult" +
																				$"/{questionId}" +
																				$"/{matchSessionId}" +
																				$"/{userSessionId}" +
																				$"/received" +
																				$"/{received}");

			// Assert
			receivedResult.QuestionId.Should().Be(questionId);
			receivedResult.UserSessionId.Should().Be(userSessionId);
			receivedResult.MatchSessionId.Should().Be(matchSessionId);
			receivedResult.Received.Should().BeCloseTo(EpochUtil.FromUnixTimestamp(received), 1000);

			return receivedResult;
		}

		[Fact]
		public async Task<ObservationalQuestionResult> CanPostShown()
		{
			var (questionId, userSessionId, matchSessionId) = await GetQuestionAndUserSessionAndMatch();
			return await CanPostShown(questionId, userSessionId, matchSessionId, DateTime.UtcNow);
		}

		private async Task<ObservationalQuestionResult> CanPostShown(int questionId, string userSessionId, string matchSessionId, DateTime received)
		{
			// Arrange
			var receivedMS = EpochUtil.ToUnixTimestamp(received);
			var shown = EpochUtil.ToUnixTimestamp(DateTime.UtcNow);

			// Act
			var shownResult = await Fixture.HttpClient.PostAsync<ObservationalQuestionResult>($"api/observationalQuestionResult" +
																				$"/{questionId}" +
																				$"/{matchSessionId}" +
																				$"/{userSessionId}" +
																				$"/{receivedMS}" +
																				$"/shown" +
																				$"/{shown}");

			// Assert
			shownResult.QuestionId.Should().Be(questionId);
			shownResult.UserSessionId.Should().Be(userSessionId);
			shownResult.MatchSessionId.Should().Be(matchSessionId);
			shownResult.Received.Should().BeCloseTo(EpochUtil.FromUnixTimestamp(receivedMS), 1000);
			shownResult.Shown.Should().HaveValue();
			shownResult.Shown.Value.Should().BeCloseTo(EpochUtil.FromUnixTimestamp(shown), 1000);

			return shownResult;
		}

		[Fact]
		public async Task<ObservationalQuestionResult> CanPostSubmitted()
		{
			var (questionId, userSessionId, matchSessionId) = await GetQuestionAndUserSessionAndMatch();
			return await CanPostSubmitted(questionId, userSessionId, matchSessionId, DateTime.UtcNow);
		}

		public async Task<ObservationalQuestionResult> CanPostSubmitted(int questionId, string userSessionId, string matchSessionId, DateTime received)
		{
			// Arrange
			var receivedMS = EpochUtil.ToUnixTimestamp(received);
			var submitted = EpochUtil.ToUnixTimestamp(DateTime.UtcNow);
			var rating = 0.5f;

			// Act
			var submittedResult = await Fixture.HttpClient.PostAsync<ObservationalQuestionResult>($"api/observationalQuestionResult" +
																				$"/{questionId}" +
																				$"/{matchSessionId}" +
																				$"/{userSessionId}" +
																				$"/{receivedMS}" +
																				$"/submitted" +
																				$"/{rating}" +
																				$"/{submitted}");

			// Assert
			submittedResult.QuestionId.Should().Be(questionId);
			submittedResult.UserSessionId.Should().Be(userSessionId);
			submittedResult.MatchSessionId.Should().Be(matchSessionId);
			submittedResult.Received.Should().BeCloseTo(EpochUtil.FromUnixTimestamp(receivedMS), 1000);
			submittedResult.Weight.Should().HaveValue();
			submittedResult.Weight.Value.Should().Be(rating);
			submittedResult.Submitted.Should().HaveValue();
			submittedResult.Submitted.Value.Should().BeCloseTo(EpochUtil.FromUnixTimestamp(submitted), 1000);

			return submittedResult;
		}

		private async Task<(int questionId, string userSessionId, string matchSessionId)> GetQuestionAndUserSessionAndMatch()
		{
			var questions = await new ObservationalQuestionClientTests(Fixture).CanList();
			var firstUserSessionId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;
			var secondUserSessionId = (await new GuestAuthenticationClientTests(Fixture).CanAuthenticate()).GetCustomData<UserSessionData>().Id;

			var (firstParticipantResponse, _) = await new MatchClientTests(Fixture).MatchesMatching(firstUserSessionId, secondUserSessionId);

			var questionId = questions[0].Id;
			var matchSessionId = firstParticipantResponse[0].CustomData.GetCustomData<IMatchCustomData, MatchSessionData>().Id;

			return (questionId, firstUserSessionId, matchSessionId);
		}

		public ObservationalQuestionResultClientTests(WebAPITestsFixture fixture) : base(fixture)
		{
		}
	}
}
