﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Playgen.Serialization;
using PlayGen.RemoteCommands.Command;

namespace PlayGen.InterviewSkills.WebAPI.Tests
{
	public static class WebSocketExtensions
	{
		public static async Task<WebSocket> CreateConnection(this WebSocketClient client, TestServer server)
		{
			var wsUri = new UriBuilder(server.BaseAddress)
			{
				Scheme = "ws"
			}.Uri;

			return await client.ConnectAsync(wsUri, CancellationToken.None);
		}

		public static async Task<WebSocket> CreateSecureConnection(this WebSocketClient client, TestServer server)
		{
			var wsUri = new UriBuilder(server.BaseAddress)
			{
				Scheme = "wss"
			}.Uri;

			return await client.ConnectAsync(wsUri, CancellationToken.None);
		}

		public static async Task SendText(this WebSocket connection, object payload, ISerializer serializer)
		{
			using (var serializedPayload = serializer.SerializeText(payload))
			{
				await connection.SendAsync(
					serializedPayload.ToArray(),
					WebSocketMessageType.Text,
					true,
					CancellationToken.None);
			}
		}

		public static async Task SendBinary(this WebSocket connection, object payload, ISerializer serializer)
		{
			using (var serializedPayload = serializer.SerializeBinary(payload))
			{
				await connection.SendAsync(
					serializedPayload.ToArray(),
					WebSocketMessageType.Binary,
					true,
					CancellationToken.None);
			}
		}

		public static async Task<CommandResponse> AwaitOneCommandResponse(this WebSocket connection, ISerializer serializer)
		{
			CommandResponse response;
			
			var buffer = new byte[WebSocketTests.BufferSize];
			var message = await connection.ReceiveAsync(buffer, CancellationToken.None);
				
			switch (message.MessageType)
			{
				case WebSocketMessageType.Binary:
					response = serializer.DeserializeBinary<CommandResponse>(buffer, message.Count);

					break;

				case WebSocketMessageType.Text:
					response = serializer.DeserializeText<CommandResponse>(buffer, message.Count);

					break;

				default:
					throw new ArgumentOutOfRangeException();
			}

			return response;
		}
		
		public static async Task Close(this WebSocket connection)
		{
			await connection.CloseAsync(
				WebSocketCloseStatus.NormalClosure,
				"Connection Closed.",
				CancellationToken.None);
		}
	}
}
