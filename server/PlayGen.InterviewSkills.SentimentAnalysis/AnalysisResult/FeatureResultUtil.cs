﻿using System.Collections.Generic;

namespace PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResult
{
	public static class FeatureResultUtil
	{
		private const string PredictionKey = "Prediction";

		private const string Class1ProbKey = "Class1prob";

		private const string Class2ProbKey = "Class2prob";

		private const string Class3ProbKey = "Class3prob";

		public static FeatureResult FromDictionary(
			KeyValuePair<string,
			Dictionary<string, float>> resultClassifierProbabilities)
		{
			return FromDictionary(resultClassifierProbabilities.Key, resultClassifierProbabilities.Value);
		}

		public static FeatureResult FromDictionary(
			string feature, 
			Dictionary<string, float> classifierProbabilities)
		{
			var result = new FeatureResult
			{
				Feature = feature,

				Classifier1 = classifierProbabilities[Class1ProbKey],

				Classifier2 = classifierProbabilities[Class2ProbKey],

				Prediction = (int)classifierProbabilities[PredictionKey]
			};

			if (classifierProbabilities.TryGetValue(Class3ProbKey, out var classifier3))
			{
				result.Classifier3 = classifier3;
			}

			return result;
		}
	}
}
