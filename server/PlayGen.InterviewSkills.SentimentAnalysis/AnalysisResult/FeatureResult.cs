﻿namespace PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResult
{
	public class FeatureResult
	{
		public string Feature { get; set; }

		public float Classifier1 { get; set; }

		public float Classifier2 { get; set; }

		public float? Classifier3 { get; set; }

		public int Prediction { get; set; }
	}
}
