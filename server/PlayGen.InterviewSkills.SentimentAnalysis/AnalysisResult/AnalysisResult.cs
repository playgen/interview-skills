﻿using System;
using System.Collections.Generic;

namespace PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResult
{
    public class AnalysisResult
    {
		public string SessionId { get; set; }

		public string StreamId { get; set; }

		public string ConnectionData { get; set; }
		
		public DateTime? RecordingStarted { get; set; }

		public DateTime? RecordingEnded { get; set; }

        public bool IsSpeechDetected { get; set; }

        public float ProbabilitySpeechDetected { get; set; }
        
        public FeatureResult[] Results { get; set; }

		public string[] Errors { get; set; }

		public Dictionary<string, object> DebugInfo { get; set; }
    }
}
