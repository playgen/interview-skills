﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using Microsoft.Extensions.Logging;
using PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResponse;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingSubmitter;

namespace PlayGen.InterviewSkills.SentimentAnalysis.SessionAnalyzer
{
    public class SessionAnalyzerFactory
    {
		private readonly RecordingReceiverFactory _receiverFactory;
		private readonly RecordingSubmitterFactory _submitterFactory;
		private readonly AnalysisResponseDeserializerFactory _responseDeserializerFactory;
		private readonly ILoggerFactory _loggerFactory;

		public SessionAnalyzerFactory(
			RecordingReceiverFactory receiverFactory, 
			RecordingSubmitterFactory submitterFactory,
			AnalysisResponseDeserializerFactory responseDeserializerFactory,
			ILoggerFactory loggerFactory)
		{
			_receiverFactory = receiverFactory;
			_submitterFactory = submitterFactory;
			_responseDeserializerFactory = responseDeserializerFactory;

			_loggerFactory = loggerFactory;
		}
		
		public SessionAnalyzer Create(string sessionId)
		{
			var receiver = _receiverFactory.Create(sessionId);
			var submitter = _submitterFactory.Create();
			var responseParser = _responseDeserializerFactory.Create();
			var logger = _loggerFactory.CreateLogger<SessionAnalyzer>();
			return new SessionAnalyzer(sessionId, receiver, submitter, responseParser, logger);
		}
    }
}
