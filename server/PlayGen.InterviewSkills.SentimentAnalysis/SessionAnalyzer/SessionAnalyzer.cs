﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResponse;
using PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResult;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.SentimentAnalysis.SessionAnalyzer
{
	public sealed class SessionAnalyzer : IDisposable
	{
		private readonly string _sessionId;
		private readonly RecordingReceiver.RecordingReceiver _recorder;
		private readonly RecordingSubmitter.RecordingSubmitter _submitter;
		private readonly AnalysisResponseDeserializer _deserializer;
		private readonly ILogger _logger;
		private readonly CancellationTokenSource _cancellationSource = new CancellationTokenSource();

		public event Action<AnalysisResult.AnalysisResult[]> AnalysisResultEvent;

		public SessionAnalyzer(
			string sessionId,
			RecordingReceiver.RecordingReceiver recorder,
			RecordingSubmitter.RecordingSubmitter submitter,
			AnalysisResponseDeserializer deserializer,
			ILogger<SessionAnalyzer> logger)
		{
			_sessionId = sessionId;
			_recorder = recorder;
			_submitter = submitter;
			_deserializer = deserializer;
			_logger = logger;

			_recorder.RecordingReceivedEvent += _submitter.SubmitRecording;
			_submitter.ResultReceivedEvent += OnResultReceived;
		}

		private void OnResultReceived(byte[] serializedResults)
		{
		    try
		    {
		        var analysisResponses = _deserializer.Deserialize(serializedResults);

		        var analysisResults = analysisResponses.Select(analysisResponse => new AnalysisResult.AnalysisResult
		        {
		            SessionId = _sessionId,

		            StreamId = analysisResponse.RecordingMetadata.StreamId,

		            ConnectionData = analysisResponse.RecordingMetadata.ConnectionData,

		            RecordingStarted = analysisResponse.RecordingMetadata.Started.FromUnixTimestamp(),

		            RecordingEnded = analysisResponse.RecordingMetadata.Ended.FromUnixTimestamp(),

		            IsSpeechDetected = analysisResponse.IsSpeechDetected,

		            ProbabilitySpeechDetected = analysisResponse.ProbabilitySpeechDetected,

		            Results = analysisResponse.FeatureResults,

		            Errors = analysisResponse.Errors,

		            DebugInfo = analysisResponse.RecordingMetadata.DebugInfo
		        }).ToArray();

		        _logger.LogDebug(
		            $"Succesfully constructed ${analysisResults.Length} ${nameof(AnalysisResult)}(s) for Session Id: {_sessionId}.");

		        AnalysisResultEvent?.Invoke(analysisResults);
		    }
		    catch (Exception desarializationFailure)
		    {
                _logger.LogError(desarializationFailure, "Failed to deserialize results from bytes.");
		    }
		}

		public async Task StartAsync()
		{
			await Task.WhenAll(
				_recorder.StartAsync(),
				_submitter.ConnectAsync());
		}

		public void Dispose()
		{
			_cancellationSource.Cancel();
			_recorder.Dispose();
			_submitter.Dispose();
		}
	}
}
