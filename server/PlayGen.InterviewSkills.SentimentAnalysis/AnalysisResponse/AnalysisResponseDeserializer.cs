﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Playgen.Serialization;
using PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResult;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResponse
{
	public class AnalysisResponseDeserializer
	{
		private readonly ISerializer _serializer;
		private readonly string _errorsKey;
	    private readonly string _voiceActivityKey;
	    private readonly ILogger _logger;

	    public AnalysisResponseDeserializer(ISerializer serializer, string voiceActivityKey, string errorsKey, ILogger<AnalysisResponseDeserializer> logger)
		{
		    _logger = logger;
			_serializer = serializer;
		    _voiceActivityKey = voiceActivityKey;
            _errorsKey = errorsKey;
		}

		public List<AnalysisResponse> Deserialize(byte[] serializedResponse)
		{
			var responses = new List<AnalysisResponse>();

			var root = _serializer.DeserializeBinary<JObject>(serializedResponse, serializedResponse.Length);
			var analysis = root["analysis"];
			foreach (var analysisEntry in analysis)
			{
				var recordingMetadataJtoken = analysisEntry["metadata"];
				var resultsAndErrorsJToken = analysisEntry["result"];

				var recordingMetadata = recordingMetadataJtoken.ToObject<RecordingMetadata>();

				if (recordingMetadata.DebugInfo != null)
				{
					recordingMetadata.DebugInfo["is-server"] = new List<object[]>
					{
						new object[]{ "deserializedMetadata", DateTime.UtcNow.ToUnixTimestamp()}
					};
				}

			    var analysisResponse = new AnalysisResponse
			    {
			        RecordingMetadata = recordingMetadata
			    };

                try
			    {
			        var (isSpeechDetected, probabilitySpeechDetected, results, errors) = Parse(resultsAndErrorsJToken);
			        analysisResponse.IsSpeechDetected = isSpeechDetected;
			        analysisResponse.ProbabilitySpeechDetected = probabilitySpeechDetected;
			        analysisResponse.FeatureResults = results;
			        analysisResponse.Errors = errors;
                }
			    catch (Exception deserializationFailure)
			    {
			        analysisResponse.Errors = new[] {deserializationFailure.ToString()};
                    _logger.LogError(deserializationFailure, $"Failed to deserialize results for ConnectionData: {recordingMetadata.ConnectionData}, Started: {recordingMetadata.Started}.");
			    }

			    responses.Add(analysisResponse);
			}

			return responses;
		}

		private (bool, float, FeatureResult[], string[]) Parse(JToken resultsAndErrors)
		{
		    var isSpeechDetected = false;
		    float probabilitySpeechDetected = 0;
            string[] errors = null;
			FeatureResult[] features = null;
            var results = new Dictionary<string, Dictionary<string, float>>();

			if (resultsAndErrors != null)
			{
				foreach (var jItem in resultsAndErrors.Children<JProperty>())
				{
				    if (jItem.Name == _voiceActivityKey)
				    {
				        isSpeechDetected = jItem.Value["Prediction"].ToObject<bool>();
                        probabilitySpeechDetected = jItem.Value["Probability"].ToObject<float>();
				    }
				    else if (jItem.Name == _errorsKey)
					{
						errors = jItem.Value.ToObject<string[]>();
					}
					else
					{
						results[jItem.Name] = jItem.Value.ToObject<Dictionary<string, float>>();
					}
				}

				features = results.Select(FeatureResultUtil.FromDictionary).ToArray();
			}
            
            return (isSpeechDetected, probabilitySpeechDetected, features, errors);
		}
	}
}
