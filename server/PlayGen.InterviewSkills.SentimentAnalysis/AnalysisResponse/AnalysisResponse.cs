﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResult;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver;

namespace PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResponse
{
    public class AnalysisResponse
    {
		public RecordingMetadata RecordingMetadata { get; set; }

        public bool IsSpeechDetected { get; set; }

        public float ProbabilitySpeechDetected { get; set; }

        public FeatureResult[] FeatureResults { get; set; }

		public string[] Errors { get; set; }
    }
}
