﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Playgen.Serialization;

namespace PlayGen.InterviewSkills.SentimentAnalysis.AnalysisResponse
{
    public class AnalysisResponseDeserializerFactory
    {
		private readonly ISerializer _serializer;
        private readonly string _voiceActivityKey;
        private readonly string _errorsKey;
        private readonly ILoggerFactory _loggerFactory;

        public AnalysisResponseDeserializerFactory(ISerializer serializer, string voiceActivityKey, string errorsKey, ILoggerFactory loggerFactory)
		{
			_serializer = serializer;
		    _voiceActivityKey = voiceActivityKey;
			_errorsKey = errorsKey;
		    _loggerFactory = loggerFactory;
		}

		public AnalysisResponseDeserializer Create()
		{
			return new AnalysisResponseDeserializer(_serializer, _voiceActivityKey,  _errorsKey, _loggerFactory.CreateLogger<AnalysisResponseDeserializer>());
		}
    }
}