﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PlayGen.InterviewSkills.SentimentAnalysis.SessionAnalyzer;
using Serilog;

namespace PlayGen.InterviewSkills.SentimentAnalysis.Session
{
	public sealed class SessionAnalyzerController : IDisposable
	{
		private readonly ReaderWriterLockSlim _recorderBySessionIdLock = new ReaderWriterLockSlim();
		private readonly Dictionary<string, SessionAnalyzer.SessionAnalyzer> _recordersBySessionId = new Dictionary<string, SessionAnalyzer.SessionAnalyzer>();
		private readonly SessionAnalyzerFactory _analyzerFactory;
		private readonly ILogger<SessionAnalyzerController> _logger;

		public event Action<AnalysisResult.AnalysisResult[]> AnalysisResultReceived;

		private bool _isDisposed;

		public SessionAnalyzerController(
			SessionAnalyzerFactory analyzerFactory, 
			ILogger<SessionAnalyzerController> logger)
		{
			_analyzerFactory = analyzerFactory;
			_logger = logger;
		}

		~SessionAnalyzerController()
		{
			Dispose();
		}

		public void Dispose()
		{
			if (_isDisposed) return;

			_recorderBySessionIdLock.EnterWriteLock();

			try
			{
				_recordersBySessionId.Values.ToList().ForEach(DisposeAnalyzer);
				_recordersBySessionId.Clear();
			}
			finally
			{
				_recorderBySessionIdLock.ExitWriteLock();
			}

			_isDisposed = true;
		}

		public void StartRecording(string sessionId)
		{
			if (_recordersBySessionId.ContainsKey(sessionId))
			{
				_logger.LogWarning($"There is already a recorder for Session Id: {sessionId}");
				return;
			}

			_logger.LogDebug($"Starting recording for Session Id: {sessionId}");

			_recorderBySessionIdLock.EnterWriteLock();

			try
			{
				if (!_recordersBySessionId.ContainsKey(sessionId))
				{
					var analyzer = _analyzerFactory.Create(sessionId);
					analyzer.AnalysisResultEvent += OnAnalysisResultEvent;
					_recordersBySessionId.Add(sessionId, analyzer);
					Task.Run(analyzer.StartAsync);
				}
			}
			finally
			{
				_recorderBySessionIdLock.ExitWriteLock();
			}
		}

		private void OnAnalysisResultEvent(AnalysisResult.AnalysisResult[] analysisResults)
		{
			AnalysisResultReceived?.Invoke(analysisResults);
		}

		public void StopRecording(string sessionId)
		{
			RemoveAnalyzer(sessionId);
		}
		
		private void RemoveAnalyzer(string sessionId)
		{
			SessionAnalyzer.SessionAnalyzer analyzer;
			_recorderBySessionIdLock.EnterWriteLock();

			try
			{
				if (_recordersBySessionId.TryGetValue(sessionId, out analyzer))
				{
					_recordersBySessionId.Remove(sessionId);
				}
			}
			finally
			{
				_recorderBySessionIdLock.ExitWriteLock();
			}

			if (analyzer != null)
			{
				_logger.LogDebug($"Stopping recording for Session Id: {sessionId}");
				DisposeAnalyzer(analyzer);
			}
			else
			{
				_logger.LogWarning($"Didn't remove a recorder for Session Id: {sessionId}.");
			}
		}

		private void DisposeAnalyzer(SessionAnalyzer.SessionAnalyzer analyzer)
		{
			analyzer.Dispose();
		}
	}
}
