﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace PlayGen.InterviewSkills.SentimentAnalysis.Session
{
	public sealed class SessionController : IDisposable
	{
		private readonly int _pruneInterval;
		private readonly int _maxTimeout;
		private readonly ILogger _logger;
		private readonly Dictionary<string, DateTime> _aliveSessions = new Dictionary<string, DateTime>();
		private readonly ReaderWriterLockSlim _aliveSessionsLock = new ReaderWriterLockSlim();
		
		private readonly Thread _pruneThread;
		private readonly ManualResetEventSlim _pruneSignal = new ManualResetEventSlim(false);
		private volatile bool _isDisposed;
		
		public delegate void SessionStartedHandler(string sessionId);
		public event SessionStartedHandler SessionStartedEvent;

		public delegate void SessionStoppedHandler(string sessionId);
		public event SessionStoppedHandler SessionStoppedEvent;

		public SessionController(int pruneInterval, int maxTimeout, ILogger<SessionController> logger)
		{
			_pruneInterval = pruneInterval;
			_maxTimeout = maxTimeout;
			_logger = logger;
			_pruneThread = new Thread(PruneLoop) {
				IsBackground = true
			};
		}

		~SessionController()
		{
			Dispose();
		}

		public bool Start(string sessionId)
		{
			var didStart = false;
			_aliveSessionsLock.EnterWriteLock();

			try
			{
				if(!_aliveSessions.ContainsKey(sessionId))
				{
					_aliveSessions[sessionId] = DateTime.UtcNow;
					didStart = true;
					OnSessionStarted(sessionId);
				}
			}
			finally
			{
				_aliveSessionsLock.ExitWriteLock();
			}

			return didStart;
		}

		public bool Stop(string sessionId)
		{
			var didStop = false;
			_aliveSessionsLock.EnterWriteLock();

			try
			{
				if (_aliveSessions.Remove(sessionId))
				{
					didStop = true;
					OnSessionStopped(sessionId);
				}
			}
			finally
			{
				_aliveSessionsLock.ExitWriteLock();
			}

			return didStop;
		}

		public bool KeepAlive(string sessionId)
		{
			var isAlive = false;

			_aliveSessionsLock.EnterWriteLock();
			try
			{
				if (_aliveSessions.ContainsKey(sessionId))
				{
					_aliveSessions[sessionId] = DateTime.UtcNow;
					isAlive = true;
				}
			}
			finally
			{
				_aliveSessionsLock.ExitWriteLock();
			}

			return isAlive;
		}

		public void Start()
		{
			_pruneThread.Start();
		}

		public void Dispose()
		{
			if (_isDisposed) return;

			_pruneSignal.Dispose();

			_isDisposed = true;
		}

		private void PruneLoop()
		{
			while (true)
			{
				_pruneSignal.Wait(_pruneInterval);

				if (_isDisposed)
				{
					break;
				}
				else
				{
					PruneSessions();
				}
			}
		}

		private void PruneSessions()
		{
			var timeout = DateTime.UtcNow - TimeSpan.FromMilliseconds(_maxTimeout);

			_aliveSessionsLock.EnterUpgradeableReadLock();

			try
			{
				foreach (var sessionId in _aliveSessions.Keys.ToList())
				{
					if (_aliveSessions[sessionId] < timeout)
					{
						_aliveSessionsLock.EnterWriteLock();

						try
						{
							if (_aliveSessions.Remove(sessionId, out var startTime))
							{
								_logger.LogWarning($"Session Timed Out: {sessionId}. Started at: {startTime}. Timeout set to: {timeout}.");
								OnSessionStopped(sessionId);
							}
						}
						finally
						{
							_aliveSessionsLock.ExitWriteLock();
						}
					}
				}
			}
			finally
			{
				_aliveSessionsLock.ExitUpgradeableReadLock();
			}
		}

		private void OnSessionStarted(string sessionId)
		{
			_logger.LogDebug($"Session started: {sessionId}");
			Task.Run(() => SessionStartedEvent?.Invoke(sessionId));
		}

		private void OnSessionStopped(string sessionId)
		{
			_logger.LogDebug($"Session stopped: {sessionId}");
			Task.Run(() => SessionStoppedEvent?.Invoke(sessionId));
		}		
	}
}
