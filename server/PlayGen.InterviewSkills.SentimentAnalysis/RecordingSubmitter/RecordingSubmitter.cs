﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver;
using WebSocket4Net;
using ErrorEventArgs = SuperSocket.ClientEngine.ErrorEventArgs;

namespace PlayGen.InterviewSkills.SentimentAnalysis.RecordingSubmitter
{
	public sealed class RecordingSubmitter : IDisposable
	{
		private readonly ILogger<RecordingSubmitter> _logger;
		private readonly WebSocket _websocket;

		public event Action<byte[]> ResultReceivedEvent;
		public event Action<string> ErrorEvent;

		public RecordingSubmitter(string uri, int receiveBufferSize, ILogger<RecordingSubmitter> logger)
		{
			_logger = logger;

			_websocket = new WebSocket($"ws://{uri}", receiveBufferSize: receiveBufferSize);
			_websocket.Error += OnError;
			_websocket.Opened += OnOpened;
			_websocket.Closed += OnClosed;
			_websocket.DataReceived += OnDataReceived;
			_websocket.MessageReceived += OnMessageReceived;
		}

		~RecordingSubmitter()
		{
			Dispose();
		}

		public async Task ConnectAsync()
		{
			await _websocket.OpenAsync();
		}

		public void Dispose()
		{
			_websocket.Dispose();
		}

		public void SubmitRecording(byte[] data)
		{
			_websocket.Send(data, 0, data.Length);
		}

		private void OnDataReceived(object sender, DataReceivedEventArgs dataReceivedEventArgs)
		{
			ResultReceivedEvent?.Invoke(dataReceivedEventArgs.Data);
		}

		private void OnMessageReceived(object sender, MessageReceivedEventArgs messageReceivedEventArgs)
		{
			_logger.LogWarning("Shouldn't receive messages but got: {0}", messageReceivedEventArgs.Message);
		}

		private void OnClosed(object sender, EventArgs eventArgs)
		{
			_logger.LogDebug("WebSocket connection closed: {0}", eventArgs.ToString());
		}

		private void OnOpened(object sender, EventArgs eventArgs)
		{
			_logger.LogDebug("WebSocket connection opened: {0}", eventArgs.ToString());
		}

		private void OnError(object sender, ErrorEventArgs errorEventArgs)
		{
			_logger.LogError(errorEventArgs.Exception, "For WebSocket connection.");
			ErrorEvent?.Invoke(errorEventArgs.Exception.ToString());
		}
	}
}
