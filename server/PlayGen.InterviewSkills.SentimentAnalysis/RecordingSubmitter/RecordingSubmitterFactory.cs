﻿using Microsoft.Extensions.Logging;

namespace PlayGen.InterviewSkills.SentimentAnalysis.RecordingSubmitter
{
    public class RecordingSubmitterFactory
    {
		private readonly string _uri;
		private readonly int _socketReceiveBufferSize; 
		private readonly ILoggerFactory _loggerFactory;

		public RecordingSubmitterFactory(string uri, int socketReceiveBufferSize, ILoggerFactory loggerFactory)
		{
			_uri = uri;
			_socketReceiveBufferSize = socketReceiveBufferSize;
			_loggerFactory = loggerFactory;
		}

		public RecordingSubmitter Create()
		{
			return new RecordingSubmitter(_uri, _socketReceiveBufferSize, _loggerFactory.CreateLogger<RecordingSubmitter>());
		}
	}
}
