﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver
{
	public class RecordingMetadata
	{
		public long Started { get; set; }

		public long Ended { get; set; }

		public string StreamId { get; set; }

		public string ConnectionData { get; set; }

		public string MimeType { get; set; }

		public int BlobLength { get; set; }

		public Dictionary<string, object> DebugInfo { get; set; }
	}
}
