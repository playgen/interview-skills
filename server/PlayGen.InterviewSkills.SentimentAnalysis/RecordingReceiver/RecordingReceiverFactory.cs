﻿using Microsoft.Extensions.Logging;
using OpenTok.Server;
using PlayGen.OpenTok.Server;

namespace PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver
{
    public class RecordingReceiverFactory
    {
		private readonly string _uriTemplate;
		private readonly int _socketReceiveBufferSize;
		private readonly OpenTokManager _openTokManager;
		private readonly ILoggerFactory _loggerFactory;

		public RecordingReceiverFactory(string uriTemplate, int socketReceiveBufferSize, OpenTokManager openTokManager, ILoggerFactory loggerFactory)
		{
			_uriTemplate = uriTemplate;
			_socketReceiveBufferSize = socketReceiveBufferSize;
			_openTokManager = openTokManager;
			_loggerFactory = loggerFactory;
		}

		public RecordingReceiver Create(string sessionId)
		{
			var recorderToken = _openTokManager.GenerateToken(sessionId, "recorder", Role.SUBSCRIBER);
			var uri = string.Format(_uriTemplate, sessionId, recorderToken);
			var logger = _loggerFactory.CreateLogger<RecordingReceiver>();
			return new RecordingReceiver(uri, _socketReceiveBufferSize, logger);
		}
    }
}
