﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SuperSocket.ClientEngine;
using WebSocket4Net;

namespace PlayGen.InterviewSkills.SentimentAnalysis.RecordingReceiver
{
	public sealed class RecordingReceiver : IDisposable
	{
		private readonly WebSocket _websocket;
		private readonly ILogger _logger;

		public event Action<byte[]> RecordingReceivedEvent;
		public event Action<string> ErrorEvent;

		public RecordingReceiver(string uri, int receiveBufferSize, ILogger<RecordingReceiver> logger)
		{
			_logger = logger;

			_websocket = new WebSocket($"ws://{uri}", receiveBufferSize: receiveBufferSize);
			_websocket.Error += OnError;
			_websocket.Opened += OnOpened;
			_websocket.Closed += OnClosed;
			_websocket.DataReceived += OnDataReceived;
			_websocket.MessageReceived += OnMessageReceived;
		}

		~RecordingReceiver()
		{
			Dispose();
		}

		public async Task StartAsync()
		{
			await _websocket.OpenAsync();
		}

		public void Dispose()
		{
			_websocket.Dispose();
		}

		private void OnDataReceived(object sender, DataReceivedEventArgs dataReceivedEventArgs)
		{
			RecordingReceivedEvent?.Invoke(dataReceivedEventArgs.Data);
		}

		private void OnMessageReceived(object sender, MessageReceivedEventArgs messageReceivedEventArgs)
		{
			_logger.LogWarning("Shouldn't receive messages but got: {0}", messageReceivedEventArgs.Message);
		}

		private void OnClosed(object sender, EventArgs eventArgs)
		{
			_logger.LogDebug("WebSocket connection closed: {0}", eventArgs.ToString());
		}

		private void OnOpened(object sender, EventArgs eventArgs)
		{
			_logger.LogDebug("WebSocket connection opened: {0}", eventArgs.ToString());
		}

		private void OnError(object sender, ErrorEventArgs errorEventArgs)
		{
			_logger.LogError(errorEventArgs.Exception, "For WebSocket connection.");
			ErrorEvent?.Invoke(errorEventArgs.Exception.ToString());
		}
	}
}
