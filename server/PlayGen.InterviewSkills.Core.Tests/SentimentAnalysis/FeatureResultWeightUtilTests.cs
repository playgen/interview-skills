using System.Collections.Generic;
using PlayGen.InterviewSkills.Core.FeatureResultsWeight;
using Xunit;

namespace PlayGen.InterviewSkills.Core.Tests.SentimentAnalysis
{
	public class FeatureResultWeightUtilTests
	{
		[Theory]
		[InlineData(0, 1, 1, 0)]
		[InlineData(0.4f, 1, 0.6f, 0.4f)]
		[InlineData(0.6f, 2, 0.4f, 0.6f)]
		[InlineData(1, 2, 0, 1)]
		[InlineData(0, 1, 0, 0)]
		[InlineData(0.2f, 1, 0.4f, 0.3f, 0.3f)]
		[InlineData(0.4f, 2, 0.35f, 0.4f, 0.25f)]
		[InlineData(0.6f, 2, 0.25f, 0.4f, 0.35f)]
		[InlineData(0.5f, 2, 0, 1, 0)]
		[InlineData(0.8f, 3, 0.3f, 0.3f, 0.4f)]
		[InlineData(1, 3, 0, 0, 1)]
		public void CanCalculateWeight(float expected, int prediction, float class1Prob, float class2Prob, float? class3Prob = null)
		{
			// Act
			var weight = FeatureResultWeightUtil.CalculateWeight(prediction, class1Prob, class2Prob, class3Prob);

			// Assert
			Assert.Equal(expected, weight, 2);
		}


		[Theory]
		[InlineData(0, 0, 0, 1)]
		[InlineData(0.2, 0, 0.2, 1)]
		[InlineData(0.3, 0.1, 0.4, 1)]
		[InlineData(0, 0.4, 0.2, 1)]
		[InlineData(0, 0.7, 0.2, 1)]
		[InlineData(0.2, 0.7, 0.2, 0.5)]
		[InlineData(0.1, 0.9, 0.2, 0.8)]
		[InlineData(0.2, 1, 0.2, 0.8)]
		[InlineData(0, 1, 0.2, 1)]
		public void CanCalulateDistanceFromIdeal(float expected, float weight, float idealMin, float idealMax)
		{
			// Act
			var (distance, range) = FeatureResultWeightUtil.DistanceFromIdeal(weight, idealMin, idealMax);

			// Assert
			Assert.Equal(expected, distance, 2);
		}
	}
}