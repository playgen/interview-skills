﻿namespace PlayGen.InterviewSkills.Data.Model.Job
{
    public class JobFeatureMapping
    {
	    public string JobId { get; set; }

	    public Job Job { get; set; }

	    public string FeatureId { get; set; }

	    public Feature Feature { get; set; }
	}
}
