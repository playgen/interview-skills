﻿namespace PlayGen.InterviewSkills.Data.Model.Job
{
    public class JobTagMapping
    {
	    public string JobId { get; set; }

	    public Job Job { get; set; }

	    public string TagId { get; set; }

	    public Tag Tag { get; set; }
	}
}
