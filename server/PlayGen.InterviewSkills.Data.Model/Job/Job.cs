﻿using System.Collections.Generic;

namespace PlayGen.InterviewSkills.Data.Model.Job
{
    public class Job
    {
		public string Id { get; set; }
		
		public string ThemeId { get; set; }

		public virtual JobTheme Theme { get; set; }

	    public virtual ICollection<JobFeatureMapping> FeatureMappings { get; set; }

	    public virtual ICollection<JobTagMapping> TagMappings { get; set; }
	}
}
