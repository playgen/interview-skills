﻿namespace PlayGen.InterviewSkills.Data.Model.Authentications
{
	public enum AuthenticationType
	{
		Guest = 0,
		Google = 1,
		Email = 2,
	}
}