﻿using System;

namespace PlayGen.InterviewSkills.Data.Model.Authentications
{
    public class Authentication
    {
	    public AuthenticationType AuthenticationType { get; set; }

		public string AuthenticationUserId { get; set; }

	    public Guid UserId { get; set; }

	    public virtual User.User User { get; set; }

		public string SerializedAuthenticationType
	    {
			get => AuthenticationType.ToString();
			set => AuthenticationType = (AuthenticationType)Enum.Parse(typeof(AuthenticationType), value, true);
		}
	}
}
