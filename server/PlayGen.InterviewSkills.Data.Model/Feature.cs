﻿namespace PlayGen.InterviewSkills.Data.Model
{
	public class Feature
    {
		public string Id { get; set; }

		public string Inverse { get; set; }

		public float IdealWeightMin { get; set; }

		public float IdealWeightMax { get; set; }
    }
}