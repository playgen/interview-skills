﻿using PlayGen.InterviewSkills.Data.Model.FeedbackResults;
using PlayGen.InterviewSkills.Data.Model.User;
using System;
using System.Collections.Generic;

namespace PlayGen.InterviewSkills.Data.Model.Role
{
    public class RoleSession
    {
		public Guid Id { get; set; }

		public Guid UserSessionId { get; set; }

		public virtual UserSession UserSession { get; set; }

		public string MatchSessionId { get; set; }

		public virtual MatchSession.MatchSession MatchSession { get; set; }

	    public string RoleId { get; set; }

		public virtual Role Role { get; set; }		

	    public string JobId { get; set; }

	    public virtual Job.Job Job { get; set; }

	    public DateTime Created { get; set; }

		public DateTime? Ended { get; set; }
	}
}
