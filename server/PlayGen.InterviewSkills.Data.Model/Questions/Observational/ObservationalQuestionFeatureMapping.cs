﻿namespace PlayGen.InterviewSkills.Data.Model.Questions.Observational
{
    public class ObservationalQuestionFeatureMapping
	{
		public int QuestionId { get; set; }

		public ObservationalQuestion Question { get; set; }

		public string FeatureId { get; set; }

		public Feature Feature { get; set; }
    }
}
