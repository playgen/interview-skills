﻿namespace PlayGen.InterviewSkills.Data.Model.Questions.Observational
{
    public class ObservationalQuestionTagMapping
	{
	    public int QuestionId { get; set; }

	    public ObservationalQuestion Question { get; set; }

	    public string TagId { get; set; }

	    public Tag Tag { get; set; }
	}
}
