﻿using System.Collections.Generic;

namespace PlayGen.InterviewSkills.Data.Model.Questions.Observational
{
    public class ObservationalQuestion
    {
		public int Id { get; set; }

		public string Question { get; set; }

		public bool IsBinary { get; set; }

	    public bool IsInverse { get; set; }

		public string StartLabel { get; set; }

		public string EndLabel { get; set; }

		public virtual ICollection<ObservationalQuestionFeatureMapping> FeatureMappings { get; set; }

		public virtual ICollection<ObservationalQuestionTagMapping> TagMappings { get; set; }

		public virtual ICollection<ObservationalQuestionJobMapping> JobMappings { get; set; }		
    }
}
