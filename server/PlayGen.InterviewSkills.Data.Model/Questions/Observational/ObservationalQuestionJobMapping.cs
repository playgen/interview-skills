﻿namespace PlayGen.InterviewSkills.Data.Model.Questions.Observational
{
    public class ObservationalQuestionJobMapping
	{
		public int QuestionId { get; set; }

		public ObservationalQuestion Question { get; set; }

		public string JobId { get; set; }

		public Job.Job Job { get; set; }
    }
}
