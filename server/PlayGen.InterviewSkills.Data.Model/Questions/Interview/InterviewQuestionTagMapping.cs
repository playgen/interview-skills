﻿namespace PlayGen.InterviewSkills.Data.Model.Questions.Interview
{
    public class InterviewQuestionTagMapping
    {
	    public int QuestionId { get; set; }

	    public InterviewQuestion Question{ get; set; }

	    public string TagId { get; set; }

	    public Tag Tag { get; set; }
	}
}
