﻿using System.Collections.Generic;

namespace PlayGen.InterviewSkills.Data.Model.Questions.Interview
{
	public class InterviewQuestion
    {
	    public int Id { get; set; }

	    public string Question { get; set; }

		public int Duration { get; set; }

	    public virtual ICollection<InterviewQuestionFeatureMapping> FeatureMappings { get; set; }

	    public virtual ICollection<InterviewQuestionTagMapping> TagMappings { get; set; }

	    public virtual ICollection<InterviewQuestionJobMapping> JobMappings { get; set; }
	}
}
