﻿namespace PlayGen.InterviewSkills.Data.Model.Questions.Interview
{
    public class InterviewQuestionFeatureMapping
    {
		public int QuestionId { get; set; }

		public InterviewQuestion Question { get; set; }

		public string FeatureId { get; set; }

		public Feature Feature { get; set; }
    }
}
