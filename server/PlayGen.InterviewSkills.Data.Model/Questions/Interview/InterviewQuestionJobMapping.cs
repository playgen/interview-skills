﻿namespace PlayGen.InterviewSkills.Data.Model.Questions.Interview
{
    public class InterviewQuestionJobMapping
    {
		public int QuestionId { get; set; }

		public InterviewQuestion Question { get; set; }

		public string JobId { get; set; }

		public Job.Job Job { get; set; }
    }
}
