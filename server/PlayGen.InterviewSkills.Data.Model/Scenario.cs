﻿namespace PlayGen.InterviewSkills.Data.Model
{
    public class Scenario
    {
		public int Id { get; set; }

		public string Category { get; set; }

	    public string Name { get; set; }

		public bool IsSynchronised { get; set; }

		public string Steps { get; set; }
	}
}
