﻿namespace PlayGen.InterviewSkills.Data.Model
{
    public interface IWeightFromIdeal
    {
		float WeightFromIdeal { get; set; }
    }
}
