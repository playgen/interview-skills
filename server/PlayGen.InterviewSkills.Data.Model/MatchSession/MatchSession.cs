﻿using System.Collections.Generic;
using PlayGen.InterviewSkills.Data.Model.Role;

namespace PlayGen.InterviewSkills.Data.Model.MatchSession
{
	public class MatchSession
    {
		public string Id { get; set; }
		
		public virtual ICollection<MatchSessionUserSessionMappings> UserSessionMappings { get; set; }

	    public virtual ICollection<RoleSession> RoleSessions { get; set; }
    }
}
