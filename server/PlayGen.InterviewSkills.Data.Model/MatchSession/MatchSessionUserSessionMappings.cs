﻿using System;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Model.MatchSession
{
	public class MatchSessionUserSessionMappings
    {
	    public string MatchSessionId { get; set; }

	    public MatchSession MatchSession { get; set; }

		public Guid UserSessionId { get; set; }

		public UserSession UserSession { get; set; }
    }
}
