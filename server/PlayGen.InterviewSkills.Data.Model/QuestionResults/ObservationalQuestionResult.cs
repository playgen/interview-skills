﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.InterviewSkills.Data.Model.Questions.Observational;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Model.QuestionResults
{
    public class ObservationalQuestionResult
    {
		public int Id { get; set; }

		public int QuestionId { get; set; }

		public virtual ObservationalQuestion Question { get; set; }

		public string MatchSessionId { get; set; }

		public virtual MatchSession.MatchSession MatchSession { get; set; }

		public Guid UserSessionId { get; set; }

		public virtual UserSession UserSession { get; set; }

	    public DateTime Received { get; set; }

		public DateTime? Shown { get; set; }

	    public DateTime? Submitted { get; set; }

		public float? Weight { get; set; }
    }
}
