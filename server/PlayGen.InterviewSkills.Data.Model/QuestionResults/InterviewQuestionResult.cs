﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.InterviewSkills.Data.Model.Questions.Interview;

namespace PlayGen.InterviewSkills.Data.Model.QuestionResults
{
    public class InterviewQuestionResult
    {
		public int Id { get; set; }

		public int QuestionId { get; set; }

		public virtual InterviewQuestion Question { get; set;}

		public string MatchSessionId { get; set; }

		public virtual MatchSession.MatchSession MatchSession { get; set; }

		public DateTime? Asking { get; set; }

		public DateTime? Answering { get; set; }

		public DateTime? Answered { get; set; }

		public DateTime? Hidden { get; set; }
    }
}
