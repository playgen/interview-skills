﻿using System;
using PlayGen.InterviewSkills.Data.Model.Role;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Model.FeedbackResults
{
    public class CustomFeedbackUserMessage
    {
		public int Id { get; set; }

		public Guid RecipientUserSessionId { get; set; }

		public virtual UserSession RecipientUserSession { get; set; }

		public Guid IssuerUserSessionId { get; set; }
		
		public virtual UserSession IssuerUserSession { get; set; }

	    public string MatchSessionId { get; set; }

		public virtual MatchSession.MatchSession MatchSession { get; set; }

		public DateTime Created { get; set; }

		public string Message { get; set; }
    }
}
