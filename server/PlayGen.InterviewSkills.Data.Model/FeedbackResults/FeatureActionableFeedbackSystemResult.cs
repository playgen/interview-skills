﻿using System;
using System.Reflection.PortableExecutable;
using PlayGen.InterviewSkills.Data.Model.Analysis;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Model.FeedbackResults
{
    public class FeatureActionableFeedbackSystemResult
    {
	    public int Id { get; set; }

		public int ArchiveAnalysisResultId { get; set; }

	    public virtual AnalysisFeatureResult AnalysisFeatureResult { get; set; }

		public int FeedbackId { get; set; }

	    public virtual FeatureActionableFeedback Feedback { get; set; }

	    public DateTime Received { get; set; }

        public DateTime? Shown { get; set; }

        public DateTime? Acknowledged { get; set; }
    }
}
