﻿using System;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Model.FeedbackResults
{
    public class FeatureSummaryFeedbackSystemResult
    {
	    public int Id { get; set; }

		public Guid UserSessionId { get; set; }

	    public virtual UserSession UserSession { get; set; }

	    public string MatchSessionId { get; set; }

		public virtual MatchSession.MatchSession MatchSession { get; set; }

	    public DateTime Created { get; set; }

	    public int FeedbackId { get; set; }

	    public virtual FeatureSummaryFeedback Feedback { get; set; }

		public float WeightFromIdeal { get; set; }
	}
}
