﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using PlayGen.InterviewSkills.Data.Model.User;

namespace PlayGen.InterviewSkills.Data.Model.Analysis
{
    public class AnalysisMetadata
    {
		public int Id { get; set; }

	    public string MatchSessionId { get; set; }

		public MatchSession.MatchSession MatchSession { get; set; }

		public DateTime? RecordingStarted { get; set; }

		public DateTime? RecordingEnded { get; set; }

		public Guid UserSessionId { get; set; }

		public virtual UserSession UserSession { get; set; }

        public bool IsSpeechDetected { get; set; }

        public float ProbabilitySpeechDetected { get; set; }

        public string[] Errors { get; set; }

	    public string SerializedErrors
	    {
		    get => JsonConvert.SerializeObject(Errors);
		    set => Errors = JsonConvert.DeserializeObject<string[]>(value);
	    }
		
		public virtual ICollection<AnalysisFeatureResult> FeatureResults { get; set; }
    }
}
