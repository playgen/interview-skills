﻿namespace PlayGen.InterviewSkills.Data.Model.Analysis
{
	public class AnalysisFeatureResult
	{
		public int Id { get; set; }

		public int AnalysisMetadataId { get; set; }

		public virtual AnalysisMetadata AnalysisMetadata { get; set; }

		public string FeatureId { get; set; }

		public virtual Feature Feature { get; set; }

		public float Classifier1 { get; set; }

		public float Classifier2 { get; set; }

		public float? Classifier3 { get; set; }

		public int Prediction { get; set; }

		public float Weight { get; set; }

		public float WeightFromIdeal { get; set; }
	}
}
