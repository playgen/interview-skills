﻿using System;
using System.Collections.Generic;
using PlayGen.InterviewSkills.Data.Model.Authentications;

namespace PlayGen.InterviewSkills.Data.Model.User
{
    public class User
    {
		public Guid Id { get; set; }

		public string EmailAddress { get; set; }

	    public virtual ICollection<Authentication> Authentications { get; set; }

		public virtual ICollection<UserSession> Sessions { get; set; }
	}
}
