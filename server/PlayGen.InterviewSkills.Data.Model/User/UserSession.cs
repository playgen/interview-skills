﻿using System;
using System.Collections.Generic;
using PlayGen.InterviewSkills.Data.Model.Analysis;
using PlayGen.InterviewSkills.Data.Model.MatchSession;
using PlayGen.InterviewSkills.Data.Model.Role;
using PlayGen.InterviewSkills.Data.Model.FeedbackResults;

namespace PlayGen.InterviewSkills.Data.Model.User
{
    public class UserSession
    {
		public Guid Id { get; set; }

		public Guid UserId { get; set; }

		public virtual User User { get; set; }

		public DateTime Created { get; set; }

		public virtual ICollection<MatchSessionUserSessionMappings> MatchSessionMappings { get; set; }

	    public virtual ICollection<RoleSession> RoleSessions { get; set; }

		public virtual ICollection<AnalysisMetadata> AnalysisMetadatas { get; set; }

		public virtual ICollection<CustomFeedbackUserMessage> CustomFeedbackUserMessages { get; set; }

	    public virtual ICollection<FeatureSummaryFeedbackSystemResult> FeatureSummaryFeedbackSystemResults { get; set; }

		public virtual ICollection<ObservationalFeedbackUserResult> ObservationalFeedbackUserResults { get; set; }
	}
}
