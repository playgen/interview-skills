﻿namespace PlayGen.InterviewSkills.Data.Model.Feedbacks
{
    public class FeatureActionableFeedback : IWeightFromIdeal
    {
		public int Id { get; set; }

		public string FeatureId { get; set; }

		public virtual Feature Feature { get; set; }

		public string Feedback { get; set; }
		
		public float WeightFromIdeal { get; set; }
    }
}
