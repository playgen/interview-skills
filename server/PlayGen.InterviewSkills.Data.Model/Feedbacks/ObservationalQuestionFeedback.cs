﻿using PlayGen.InterviewSkills.Data.Model.Questions.Observational;

namespace PlayGen.InterviewSkills.Data.Model.Feedbacks
{
	public class ObservationalQuestionFeedback : IWeightFromIdeal
    {
		public int Id { get; set; }

		public int QuestionId { get; set; }

		public virtual ObservationalQuestion Question { get; set; }

		public string Feedback { get; set; }

		public float WeightFromIdeal { get; set; }
    }
}
