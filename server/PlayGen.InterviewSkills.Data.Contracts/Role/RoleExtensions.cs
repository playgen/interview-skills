﻿using System;
using PlayGen.InterviewSkills.Data.Model.Role;
using PlayGen.TimeUtils;

namespace PlayGen.InterviewSkills.Data.Contracts.Role
{
    public static class RoleExtensions
    {
	    public static RoleSession ToModel(this RoleSessionRequest request)
	    {
		    return new RoleSession
		    {
				UserSessionId = Guid.Parse(request.UserSessionId),
				MatchSessionId = request.MatchSessionId,
				Created = EpochUtil.FromUnixTimestamp(request.Created),
				JobId = request.JobId,
			    RoleId = request.RoleId
			};
	    }
	}
}
