﻿namespace PlayGen.InterviewSkills.Data.Contracts.Role
{
    public class RoleSessionRequest
    {
		public string UserSessionId { get; set; }

	    public string MatchSessionId { get; set; }

		public long Created { get; set; }

	    public string RoleId { get; set; }

		public string JobId { get; set; }
	}
}
