﻿using System;
using PlayGen.InterviewSkills.Data.Model.FeedbackResults;

namespace PlayGen.InterviewSkills.Data.Contracts.FeedbackResults
{
    public static class FeedbackExtensions
    {
	    public static ObservationalFeedbackUserResult ToModel(this ObservationalFeedbackUserResultRequest request)
	    {
		    return new ObservationalFeedbackUserResult
		    {
			    RecipientUserSessionId = Guid.Parse(request.RecipientUserSessionId),
			    IssuerUserSessionId = Guid.Parse(request.IssuerUserSessionId),
			    MatchSessionId = request.MatchSessionId,
			    Created = request.Time,
			    FeedbackId = request.FeedbackId,
			    Weight = request.Value
		    };
	    }
	}
}
