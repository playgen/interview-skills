﻿using System;

namespace PlayGen.InterviewSkills.Data.Contracts.FeedbackResults
{
    public class ObservationalFeedbackUserResultRequest
    {
	    public string RecipientUserSessionId { get; set; }

	    public string IssuerUserSessionId { get; set; }

	    public string MatchSessionId { get; set; }

	    public DateTime Time { get; set; }

	    public int FeedbackId { get; set; }

	    public float Value { get; set; }
	}
}
