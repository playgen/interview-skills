﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.InterviewSkills.Data.Model;

namespace PlayGen.InterviewSkills.Data.Contracts.SentimentAnalysis
{
    public class ResultResponse
    {
		public float Weight { get; set; }

		public float WeightFromIdeal { get; set; }

		public DateTime Started { get; set; }

		public DateTime Ended { get; set; }
	}
}
