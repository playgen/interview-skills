﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.InterviewSkills.Data.Model;

namespace PlayGen.InterviewSkills.Data.Contracts.SentimentAnalysis
{
    public class FeatureResultResponse
    {
		public ResultResponse Result { get; set; }

		public Feature Feature { get; set; }
	}
}
