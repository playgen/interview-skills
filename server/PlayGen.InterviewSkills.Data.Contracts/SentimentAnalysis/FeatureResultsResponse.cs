﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.InterviewSkills.Data.Model;

namespace PlayGen.InterviewSkills.Data.Contracts.SentimentAnalysis
{
    public class FeatureResultsResponse
    {
		public ResultResponse[] Results { get; set; }

		public Feature Feature { get; set; }
	}
}
