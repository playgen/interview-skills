﻿using System.Collections.Generic;
using System.Linq;
using PlayGen.InterviewSkills.Data.Model;
using PlayGen.InterviewSkills.Data.Model.Analysis;

namespace PlayGen.InterviewSkills.Data.Contracts.SentimentAnalysis
{
	public static class SentimentAnalysisExtensions
	{
		public static FeatureResultsResponse[] ToContract(this Dictionary<Feature, AnalysisFeatureResult[]> averagesFromIdeal)
		{
			return averagesFromIdeal.Select(kvp => new FeatureResultsResponse
			{
				Feature = kvp.Key,

				Results = kvp.Value.ToContracts(),
			}).ToArray();
		}

		public static ResultResponse[] ToContracts(this AnalysisFeatureResult[] models)
		{
			var contracts = new ResultResponse[models.Length];
			for (var i = 0; i < models.Length; i++)
			{
				contracts[i] = models[i].ToContract();
			}

			return contracts;
		}

		public static ResultResponse ToContract(this AnalysisFeatureResult model)
		{
			return new ResultResponse
			{
				Weight = model.Weight,

				Started = model.AnalysisMetadata.RecordingStarted.Value,

				Ended = model.AnalysisMetadata.RecordingEnded.Value
			};
		}

		public static FeatureResultResponse[] ToContract(
			this Dictionary<Feature, (float averageWeight, float averageWeightFromIdeal)> averages)
		{
			return averages
				.Select(kvp => new FeatureResultResponse
				{
					Feature = kvp.Key,

					Result = new ResultResponse
					{
						Weight = kvp.Value.averageWeight,

						WeightFromIdeal = kvp.Value.averageWeightFromIdeal
					}
				}).ToArray();
		}
	}
}
