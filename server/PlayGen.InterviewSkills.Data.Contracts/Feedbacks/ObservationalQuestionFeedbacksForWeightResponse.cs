﻿using System;
using System.Collections.Generic;
using System.Text;
using PlayGen.InterviewSkills.Data.Model.Feedbacks;

namespace PlayGen.InterviewSkills.Data.Contracts.Feedbacks
{
    public class ObservationalQuestionFeedbacksForWeightResponse
    {
		public IEnumerable<ObservationalQuestionFeedback> Feedbacks { get; set; }

		public float WeightFromIdeal { get; set; }
    }
}
