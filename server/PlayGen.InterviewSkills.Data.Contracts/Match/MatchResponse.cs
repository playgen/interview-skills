﻿using System.Collections.Generic;
using System.Linq;
using PlayGen.MatchMaking.Matched;

namespace PlayGen.InterviewSkills.Data.Contracts.Match
{
    public class MatchResponse
    {
		public string Id { get; set; }

        public IDictionary<string, IMatchCustomData> CustomData { get; set; }

        public MatchedParticipant[] Participants { get; set; }
        
	    public MatchedParticipant GetParticipantById(string participantId)
	    {
	        return Participants?.Single(p => p.MatchingParticipant.Id == participantId);
	    }
    }
}