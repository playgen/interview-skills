﻿using PlayGen.InterviewSkills.Core.OpenTok;

namespace PlayGen.InterviewSkills.Data.Contracts.Match.CustomData.OpenTok
{
    public static class OpenTokMatchDataExtensions
    {
        public static MatchOpenTokDataResponse ToOpenTokContract(this MatchOpenTokData model)
        {
	        return new MatchOpenTokDataResponse(model.Session.Id, model.ApiKey);
        }
    }
}
