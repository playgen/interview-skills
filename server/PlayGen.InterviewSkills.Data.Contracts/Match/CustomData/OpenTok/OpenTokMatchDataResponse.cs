﻿namespace PlayGen.InterviewSkills.Data.Contracts.Match.CustomData.OpenTok
{
    public class MatchOpenTokDataResponse : IMatchCustomDataResponse
    {
	    public string SessionId { get; }

	    public int ApiKey { get; }

		public MatchOpenTokDataResponse(string sessionId, int apiKey)
		{
			SessionId = sessionId;
			ApiKey = apiKey;
		}
    }
}
