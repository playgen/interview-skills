﻿using System.Collections.Generic;
using PlayGen.InterviewSkills.Core.OpenTok;
using PlayGen.InterviewSkills.Data.Contracts.Match.CustomData.OpenTok;
using PlayGen.MatchMaking.Matched;

namespace PlayGen.InterviewSkills.Data.Contracts.Match.CustomData
{
    public static class CustomMatchDataExtensions
    {
        public static IDictionary<string, IMatchCustomData> ToContract(this IReadOnlyDictionary<string, IMatchCustomData> model)
        {
            var customMatchDataContracts = new Dictionary<string, IMatchCustomData>();

            foreach (var kvp in model)
            {
                var contract = kvp.Value.ToContract();
                customMatchDataContracts[contract.GetType().FullName] = contract;
            }

            return customMatchDataContracts;
        }

        private static IMatchCustomData ToContract(this IMatchCustomData matchCustomData)
        {
            if (matchCustomData is MatchOpenTokData)
            {
                return ((MatchOpenTokData) matchCustomData).ToOpenTokContract();
            }
            else
            {
                return matchCustomData;
            }
        }
    }
}