﻿using PlayGen.MatchMaking.Matched;

namespace PlayGen.InterviewSkills.Data.Contracts.Match.CustomData
{
    public interface IMatchCustomDataResponse : IMatchCustomData
    {
    }
}