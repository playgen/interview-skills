﻿using PlayGen.InterviewSkills.Data.Contracts.Match.CustomData;

namespace PlayGen.InterviewSkills.Data.Contracts.Match
{
    public static class MatchExtensions
    {
        public static MatchResponse ToContract(this MatchMaking.Matched.Match model)
        {
            return new MatchResponse
            {
				Id = model.Id,
                Participants = model.Participants.ToArray(),
                CustomData = model.CustomData.ToContract(),
            };
        }
    }
}
