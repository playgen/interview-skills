### Requirements
- Windows OS
- Visual Studio 2017 with the "ASP.NET Core" workload.
- MySQL database host (we use MariaDB). Uid: root, Pwd: b3BetTer.
- Nginx
- website/ReadMe.md followed and built.
- sewa/ReadMe.md followed and runnig.
- opentok-subscriber-server/ReadMe.md followed and runnig.

### Steps
1. In server\PlayGen.InterviewSkills.WebAPI\ open appsettings.json and appsettings.Development.json.  
1.1. Set the "OpenTok" "APIKey" to your OpenTok API Key.  
1.2. Set the "OpenTok" "APISecret" to your OpenTok API Secret.
2. Edit your hosts file to map "127.0.0.1" to "interview-skills-mariadb".
3. Edit your hosts file to map "127.0.0.1" to "interview-skills-local.playgen.internal".
4. Install Nginx.
5. Copy the contents of hosting/local/nginx.conf to your installed Nginx config.
6. Restart/Run Nginx.
7. Open and run server\PlayGen.InterviewSkills.sln.
8. Browse to interview-skills-local.playgen.internal

See the Nginx logs for any Nginx configuration issues.

Interview Skills Server logs are written to server\PlayGen.InterviewSkills\WebAPI\logs

### Other

To Initialise the Database Migrations:

1. Open the project in Visual Studio
2. Open the Package Manager Console
3. Make sure the project is set to: PlayGen.InterviewSkills.Data.Storage
4. Run: Add-Migration InitialCreate

To Add a new Database Migration after modifying the model in code:

1. Open the project in Visual Studio
2. Open the Package Manager Console
3. Make sure the project is set to: PlayGen.InterviewSkills.Data.Storage
4. Run: Add-Migration

Note: Migrations are automatically applied to the database when the server is started. This is done in the InterviewSkillsContextFactory

To manually apply the created Migrations to the Database:

1. Open the project in Visual Studio
2. Open the Package Manager Console
3. Make sure the project is set to: PlayGen.InterviewSkills.Data.Storage
4. Run: Update-Database