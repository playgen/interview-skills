## This project is compose of the following components:

- Website
- Interview Skills Server
- OpenTok Subscriber Server
- SEWA Server

# Publishing
## Build
### Requirements:
- Linux OS.
- Docker.
- Docker Compose v2 min.
- An OpenTok Account.

### Steps:
1. Add your OpenTok API Key and API Secret to:  
1.1. server\PlayGen.InterviewSkills.WebAPI\appsettings.json.  
1.2. opentok-headless-subscriber\config.json.
2. Run `sh docker-compose_build_deploy.sh`

## Hosting
Due to Google Chrome requireing WebRTC communications to take place over SSL you will also need to host Interview Skills behind SSL. We use Nginx as a reverse proxy to do this.

### Requirements:
- Nginx with extras.
- Container IP Lookup script (see hosting/internet/get_container_ip.lua).
- Nginx config (see hosting/internet/nginx.conf).
- Due to chrome requireing WebRTC communications to take place over SSL you will also need a SSL certificate.
- SSL DH param file.

Alternatively you can hardcode the IP address of the Interview Skills container in your Nginx config instead of requiring "Nginx with extras" and the "Container IP Lookup script".

### Steps:
Use hosting/internet/nginx.conf as a template. Keep th file open as you go through the following steps as it has commented steps to help you.

1. See the Nginx docs on how to setup Nginx.
2. Copy the Container IP Lookup script to the correct location.
3. Copy your SSL certificate to the correct location.
4. Copy your DH param file to the correct location.
5. Copy the nginx.conf to the correct location.  
6. Follow the commented steps in the commented nginx.conf to set the correct paths for the files you copied above.
7. Start Nginx and browse to your website location (make sure you've set up your DNS).

See the Nginx logs to help you debug any issues with your configuration.


# Development
You will need to run each of the 4 components mentioned above.

## Website
Follow the instructions in [website/source/ReadMe.md](website/source/ReadMe.md)

## OpenTok Headless Subscriber Server: 
1. Follow the instructions in [opentok-headless-subscriber/ReadMe.md](opentok-headless-subscriber/ReadMe.md)
2. Edit your hosts file to map "127.0.0.1" to "opentok-headless-subscriber".

## SEWA Server 
1. In sewa/ build the server with: `docker build . -t sewa-image`
2. Then run it with: `docker run -p 8080:8080 sewa-image --name sewa-image`
3. Edit your hosts file to map "127.0.0.1" to "sewa-image".
4. In your web browser, got to http://sewa-image:8080/upload to make sure the hosts mapping is correct and pointing to the running SEWA container.

## Interview Skills Server
Follow the instructions in server/ReadMe.md

### Note:
If you need to modify the WebSocket bufffer sizes: The server's appsettings.json has a socket received buffer size that should match the headless subscriber server's config.json max message received size.

# Issues and Solutions:
Each server logs to the active directory for that server to a folder called /logs.

## Nginx
Check the Nginx access and error logs.

## sewa-image
If ffmpeg fails to install when building the docker image, build just that docker image with --no-cache and the start the deploy process again.