# Development
### Requirements
- NPM 5+
- VSCode with the gulp extension.

### Steps
1. Run `npm install`.

2. Open this folder in VSCode.
3. Run the "development-build-watch" task.

Or if you don't want to use VSCode

2. Run `gulp development-build-watch`

