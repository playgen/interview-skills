﻿angular
	.module("app")
	.component("isScenarioPlayerSoloContainer", {
		templateUrl: "/is-scenario-player/components/solo-player-container/is-scenario-player-solo-container.html",
		bindings: {
			onScenarioPlayerStopped: "<",
			newGame: "<"
		},
		controller: [
			"appContext",
			"MatchWebClient",
			"matchConfig",
			"MatchData",
			"VideoCalling",
			"subscriberConfig",
			"$animate",
			"ISScenarioPlayer",
			function (
				appContext,
				MatchWebClient,
				matchConfig,
				MatchData,
				VideoCalling,
				subscriberConfig,
				$animate,
				ISScenarioPlayer
			) {
				const ctrl = this;
				
				// private variables
				let subscriberElement = null;
				let soloMatch = null;
				let isDestroyed = false;
				const subscriberPlaceholderClass = "subscriber-placeholder";

				// angular hooks
				ctrl.$onInit = function () {
					getMatch()
						.then(() => {
							if (isDestroyed) {
								Logger.debug("This component has already been destroyed.");
							} else {
								VideoCalling.initialisedEvent.subscribe(setupOtherPlayerPlaceholder);
								ISScenarioPlayer.initialisedEvent.subscribe(setupScenarioPlayer);
								ctrl.hasMatch = true;
							}
						});
				};

				ctrl.$onDestroy = function () {
					VideoCalling.initialisedEvent.unsubscribe(setupOtherPlayerPlaceholder);
					ISScenarioPlayer.initialisedEvent.unsubscribe(setupScenarioPlayer);

					if (soloMatch) {
						MatchWebClient.leaveMatch(soloMatch.id, appContext.user.session.id);
						soloMatch = null;
					}

					if (VideoCalling.getIsInitialised()) {
						VideoCalling.streamingResumedEvent.unsubscribe(showOtherPlayerPlaceholder);
						VideoCalling.streamingSuspendedEvent.unsubscribe(hideOtherPlayerPlaceholder);
					}

					if (ISScenarioPlayer.getIsInitialised()) {
						ISScenarioPlayer.player.stoppedEvent.unsubscribe(ctrl.onScenarioPlayerStopped);
					}

					isDestroyed = true;
				};

				// private functions
				function setupScenarioPlayer() {
					ISScenarioPlayer.player.stoppedEvent.subscribe(ctrl.onScenarioPlayerStopped);
				}

				function getMatch() {
					const userSessionId = appContext.user.session.id;
					const scenarioCategory = matchConfig.scenario.category;
					const scenarioName = matchConfig.scenario.name;

					const createMatchPromise = MatchWebClient.createMatch(scenarioCategory, scenarioName, userSessionId, [userSessionId])
						.then(newSoloMatch => {
							appContext.user.session.match = newSoloMatch;
							appContext.user.session.match.session = MatchData.getSession(newSoloMatch);
							soloMatch = newSoloMatch;

							Logger.debug("Got match id: " + appContext.user.session.match.session.id);
						});

					return createMatchPromise;
				}

				function setupOtherPlayerPlaceholder() {
					VideoCalling.initialisedEvent.unsubscribe(setupOtherPlayerPlaceholder);

					VideoCalling.streamingResumedEvent.subscribe(showOtherPlayerPlaceholder);
					VideoCalling.streamingSuspendedEvent.subscribe(hideOtherPlayerPlaceholder);
				}

				function showOtherPlayerPlaceholder() {
					if (subscriberElement === null) {
						subscriberElement = angular.element(document.querySelector("#" + subscriberConfig.elementId));
					}

					$animate.addClass(subscriberElement, subscriberPlaceholderClass);
				}

				function hideOtherPlayerPlaceholder() {
					$animate.removeClass(subscriberElement, subscriberPlaceholderClass);
				}
			}]
	});