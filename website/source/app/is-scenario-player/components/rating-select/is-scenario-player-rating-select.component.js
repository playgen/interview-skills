angular
	.module("app")
	.component("isScenarioPlayerRatingSelect", {
		templateUrl: "/is-scenario-player/components/rating-select/is-scenario-player-rating-select.html",
		bindings: {
			rating: "<",
			disabled: "<",
			ratings: "<",
			onSelect: "&",
			reverse: "<"
		},
		controller: [function () {
			const ctrl = this;

			// public variables
			ctrl.rating = null;

			// public methods
			ctrl.select = function (rating) {
				ctrl.rating = rating;
				ctrl.onSelect({ rating: rating });
			};

			ctrl.isPassedSelected = function (rating) {
				return ctrl.rating === null || (ctrl.processedRatings.indexOf(ctrl.rating) < ctrl.processedRatings.indexOf(rating));
			};

			// angular hooks
			ctrl.$onChanges = function (changes) {
				if (changes.ratings || changes.reverse) {
					ctrl.rating = null;

					const ratings = changes.ratings ? changes.ratings.currentValue : ctrl.ratings;
					const reverse = changes.reverse ? changes.reverse.currentValue : ctrl.reverse;

					if (ratings) {
						if (reverse) {
							ctrl.processedRatings = ratings.slice().reverse();

						} else {
							ctrl.processedRatings = ratings;
						}
					}
				}
			};
		}]
	});