angular
    .module("app")
    .component("isScenarioPlayerInfoPanel", {
        templateUrl: "/is-scenario-player/components/info-panel/is-scenario-player-info-panel.html",
        transclude: {
            icon: "?infoPanelIcon",
            title: "?infoPanelTitle",
            content: "infoPanelContent"
        }
    });