angular
	.module("app")
	.value("isScenarioPlayerInterviewQuestionConfig", {
		stateMappings: new ScenarioPlayerData({
			synchronised: {
				asking: {
					title: "Ask",
					icon: "ask-question",
					instruction: "Ask question:",
					button: "asked"
				},
				answering: {
					title: "Answering",
					icon: "timer-filling",
					instruction: "Candidate answering question:",
					button: "answered"
				}
			},
			answering: {
				title: "Answer",
				icon: "timer-filling",
				instruction: "Answer the question:",
				button: "answered"
			},
			answered: {
				title: "Time's up!",
				icon: "timer-full",
				instruction: "",
				button: "next"
			}
		})
	});