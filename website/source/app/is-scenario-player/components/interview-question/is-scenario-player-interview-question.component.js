angular
	.module("app")
	.component("isScenarioPlayerInterviewQuestion", {
		templateUrl: "/is-scenario-player/components/interview-question/is-scenario-player-interview-question.html",
		bindings: {
			onSelect: "&",
			question: "<",
			state: "<",
			isSynchronised: "<",
			length: "<"
		},
		controller: ["isScenarioPlayerInterviewQuestionConfig",
			function (isScenarioPlayerInterviewQuestionConfig) {
				const ctrl = this;

				// public variables
				ctrl.title = null;
				ctrl.instruction = null;
				ctrl.icon = null;
				ctrl.button = null;

				// angular hooks
				ctrl.$onChanges = function (changes) {
					if (changes.state) {
						const mappings = isScenarioPlayerInterviewQuestionConfig.stateMappings
							.getValue(changes.state.currentValue, ctrl.isSynchronised);

						ctrl.title = mappings.title;
						ctrl.instruction = mappings.instruction;
						ctrl.icon = mappings.icon;
						ctrl.button = mappings.button;
					}
				};

				// public methods
				ctrl.submit = function () {
					ctrl.onSelect({ option: ctrl.button });
				};
			}]
	});