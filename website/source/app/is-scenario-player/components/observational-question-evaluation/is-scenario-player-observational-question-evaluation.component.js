angular
	.module("app")
	.component("isScenarioPlayerObservationalQuestionEvaluation", {
		templateUrl: "/is-scenario-player/components/observational-question-evaluation/is-scenario-player-observational-question-evaluation.html",
		bindings: {
			question: "<",
			isBinary: "<",
			isInverse: "<",
			startLabel: "<",
			endLabel: "<",
			onSubmit: "&"
		},
		controller: ["isScenarioPlayerConfig",
			function (isScenarioPlayerConfig) {
				const ctrl = this;

				// public variables
				ctrl.ratings = null;
				ctrl.rating = null;

				// private variables

				// Angular Hooks
				ctrl.$onInit = function() {
					ctrl.ratings = isScenarioPlayerConfig.ratingValues;
				};

				ctrl.$onChanges = function (changes) {
					ctrl.rating = null;
				};

				// public methods
				ctrl.select = function (rating) {
					ctrl.rating = rating;
					ctrl.onSubmit({ rating: ctrl.rating });
				};
			}]
	});