﻿angular
	.module("app")
	.value("isScenarioPlayerCandidateInterviewSummaryConfig", {
		title: "Your {0} interview Results and Suggestions",
		interviewer: {
			ratingsTitle: "The other player rated you as:",
			messageTitle: "Other player's suggestions:",
		},
		candidate : {
			bestTitle: "Our robot detected your best performance on:",
			bestCount: 2,
			worstTitle: "Our robot detected your worst performance on:",
			worstCount: 2,
			error: "No results available :( \nPlease check your webcam volume and video."
		}
	});