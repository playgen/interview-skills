angular
	.module("app")
	.component("isScenarioPlayerCandidateInterviewSummary",
	{
		templateUrl: "/is-scenario-player/components/candidate-interview-summary/is-scenario-player-candidate-interview-summary.html",
		bindings: {
			options: "<",
			onSelect: "&",
			job: "<",
			interviewerObservationalRatings: "<",
			interviewerMessage: "<",
			userSessionId: "<",
			matchSessionId: "<",
			candidateStart: "<",
			candidateEnd: "<",
		},
		controller: [
			"ObservationalQuestionFeedbackWebClient",
			"isScenarioPlayerCandidateInterviewSummaryConfig",
			"SentimentAnalysisWebClient",
			function (ObservationalQuestionFeedbackWebClient,
				isScenarioPlayerCandidateInterviewSummaryConfig,
				SentimentAnalysisWebClient) {
				const ctrl = this;

				// public variables
				ctrl.interviewer = {
					observationalFeedbacks: [],
					message: null,
					ratingsTitle: isScenarioPlayerCandidateInterviewSummaryConfig.interviewer.ratingsTitle,
					messageTitle: isScenarioPlayerCandidateInterviewSummaryConfig.interviewer.messageTitle
				};
				ctrl.title = "";
				ctrl.candidate = {
					best: [],
					worst: [],
					bestTitle: isScenarioPlayerCandidateInterviewSummaryConfig.candidate.bestTitle,
					worstTitle: isScenarioPlayerCandidateInterviewSummaryConfig.candidate.worstTitle,
					error: null
			};

				// Angular Hooks
				ctrl.$onInit = function () {
					ctrl.title = String.format(isScenarioPlayerCandidateInterviewSummaryConfig.title, ctrl.job);
					getAverageResults(ctrl.userSessionId, ctrl.matchSessionId, ctrl.candidateStart, ctrl.candidateEnd);
					ctrl.interviewer.message = ctrl.interviewerMessage;
				};

				ctrl.$onChanges = function (changes) {
					if (changes.interviewerObservationalRatings && changes.interviewerObservationalRatings.currentValue !== undefined) {
						getObservationalQuestionFeedback(changes.interviewerObservationalRatings.currentValue,
							ctrl.interviewer.observationalFeedbacks);
					}
				};

				// public methods
				ctrl.select = function (option) {
					ctrl.onSelect({ option: option });
				};

				// private methods
				function getAverageResults(userSessionId, matchSessionId, start, end) {
					SentimentAnalysisWebClient.getAverages(userSessionId, matchSessionId, start, end)
						.then(averages => {
								if (!averages || averages.length === 0) {
									ctrl.candidate.error = isScenarioPlayerCandidateInterviewSummaryConfig.candidate.error;

								} else {
									ctrl.candidate.error = null;

									averages.forEach(a => a.result.absoluteWeightFromIdeal = Math.abs(a.result.weightFromIdeal));

									ctrl.candidate.best = getBest(averages, isScenarioPlayerCandidateInterviewSummaryConfig.candidate.bestCount);
									ctrl.candidate.worst = getWorst(averages, isScenarioPlayerCandidateInterviewSummaryConfig.candidate.worstCount);
								}
							},
							error => {
								ctrl.candidate.error = isScenarioPlayerCandidateInterviewSummaryConfig.candidate.error;
								Logger.warn(error);
							});
				}
				

				function getBest(results, bestCount) {
					const best = results.sort((a, b) => a.result.absoluteWeightFromIdeal - b.result.absoluteWeightFromIdeal)
						.slice(0, bestCount);
					return best;
				}

				function getWorst(results, worstCount) {
					const worst = results.sort((a, b) => b.result.absoluteWeightFromIdeal - a.result.absoluteWeightFromIdeal)
						.slice(0, worstCount);
					return worst;
				}

				function getObservationalQuestionFeedback(questionRatings, feedbacks) {
					feedbacks.length = 0;
					/*jshint loopfunc:true */
					for (let questionId in questionRatings) {
						const questionRating = questionRatings[questionId];

						ObservationalQuestionFeedbackWebClient.listFeedbacks(questionRating.question.id,
								questionRating.question.featureMappings[0].featureId,
								questionRating.rating)
							.then(response => {
									if (response.feedbacks.length === 0) {
										Logger.warn("No ObervationalQuestionFeedbacks returned for ObservationalQuestion: " +
											questionRating.question.id);

									} else {
										const randomFeedback = Array.takeRandom(response.feedbacks);
										feedbacks.push({
											feedback: randomFeedback,
											weight: questionRating.weight,
											weightFromIdeal: response.weightFromIdeal
										});
									}
								},
								error => Logger.warn(error));
					}
				}
			}]
	});