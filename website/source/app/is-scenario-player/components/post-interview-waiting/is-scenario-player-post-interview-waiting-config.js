﻿angular
	.module("app")
	.value("isScenarioPlayerPostInterviewWaitingConfig", {
		info: "Waiting for other player to give you feedback...",
		loading: "Loading results...",
		error: "No results available :( \nPlease check your webcam volume and video."
	});