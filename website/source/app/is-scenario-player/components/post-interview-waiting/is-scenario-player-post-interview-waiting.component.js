﻿angular
	.module("app")
	.component("isScenarioPlayerPostInterviewWaiting", {
		templateUrl: "/is-scenario-player/components/post-interview-waiting/is-scenario-player-post-interview-waiting.html",
		bindings: {
			userSessionId: "<",
			matchSessionId: "<",
			candidateStart: "<",
			candidateEnd: "<",
			interviewRound: "<"
		},
		controller: [
			"isScenarioPlayerPostInterviewWaitingConfig",
			"SentimentAnalysisWebClient",
			function (isScenarioPlayerPostInterviewWaitingConfig, SentimentAnalysisWebClient) {
				const ctrl = this;

				// public variables
				ctrl.error = null;
				ctrl.info = isScenarioPlayerPostInterviewWaitingConfig.info;
				ctrl.selectedAverage = null;
				ctrl.loading = isScenarioPlayerPostInterviewWaitingConfig.loading;
				
				// private variables
				let selectedAverageIndex = null;
				let averages = [];

				// angular hooks
				ctrl.$onInit = function () {
					getAverageResults(ctrl.userSessionId, ctrl.matchSessionId, ctrl.candidateStart, ctrl.candidateEnd);
				};

				// public methods
				ctrl.nextAverage = function() {
					selectedAverageIndex++;
					selectedAverageIndex %= averages.length;
					ctrl.selectedAverage = averages[selectedAverageIndex];
				};

				ctrl.previousAverage = function() {
					selectedAverageIndex--;

					if (selectedAverageIndex < 0) {
						selectedAverageIndex = averages.length - 1;
					}

					selectedAverageIndex %= averages.length;

					ctrl.selectedAverage = averages[selectedAverageIndex];
				};

				// private methods
				function getAverageResults(userSessionId, matchSessionId, start, end) {
					SentimentAnalysisWebClient.getAverages(userSessionId, matchSessionId, start, end)
						.then(averagesResponse => {
							if (!averagesResponse || averagesResponse.length === 0) {
								ctrl.error = isScenarioPlayerPostInterviewWaitingConfig.error;

							} else {
								ctrl.error = null;
								
								averages = averagesResponse;
								averages.forEach(a => a.result.absoluteWeightFromIdeal = Math.abs(a.result.weightFromIdeal));

								selectedAverageIndex = 0;								
								ctrl.selectedAverage = averages[selectedAverageIndex];
							}
						},
						error => {
							ctrl.error = isScenarioPlayerPostInterviewWaitingConfig.error;
							Logger.warn(error);
						});
				}
			}]
	});