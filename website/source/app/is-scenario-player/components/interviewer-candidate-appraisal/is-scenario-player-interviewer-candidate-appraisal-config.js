﻿angular
	.module("app")
	.value("isScenarioPlayerInterviewerCandidateAppraisalConfig", {
		tags: ["postinterviewquestions"],
		instruction: "Please rate the candidate on:",
		messagePlaceholder: "Enter a constructive feedback message for the other player"
	});