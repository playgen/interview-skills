﻿angular
	.module("app")
	.component("isScenarioPlayerInterviewerCandidateAppraisal", {
		templateUrl: "/is-scenario-player/components/interviewer-candidate-appraisal/is-scenario-player-interviewer-candidate-appraisal.html",
		bindings: {
			interviewRound: "<",
			options: "<",
			onSubmit: "&"
		},
		controller: [
			"ObservationalQuestionWebClient",
			"isScenarioPlayerConfig",
			"isScenarioPlayerInterviewerCandidateAppraisalConfig",
			function (ObservationalQuestionWebClient, isScenarioPlayerConfig, isScenarioPlayerInterviewerCandidateAppraisalConfig) {
				const ctrl = this;

				// public variables
				ctrl.instruction = isScenarioPlayerInterviewerCandidateAppraisalConfig.instruction;
				ctrl.ratingValues = isScenarioPlayerConfig.ratingValues;
				ctrl.questions = [];
				ctrl.questionRatings = {};
				ctrl.message = "";
				ctrl.messagePlaceholder = isScenarioPlayerInterviewerCandidateAppraisalConfig.messagePlaceholder;
				
				// angular hooks
				ctrl.$onInit = function () {
					ObservationalQuestionWebClient.listQuestions(undefined, isScenarioPlayerInterviewerCandidateAppraisalConfig.tags, false, true, true)
						.then(questions => {
							ctrl.questions = questions;
						});
				};

				// public methods
				ctrl.ratingSelect = function (question, rating) {
					ctrl.questionRatings[question.id] = {
						question: question,
						rating: rating
					};
				};

				ctrl.isValid = function () {
					return ctrl.questions.length === Object.keys(ctrl.questionRatings).length;// && ctrl.message.length > 0;
				};

				ctrl.submit = function(option) {
					ctrl.onSubmit({
						option: option,
						appraisalResult: {
							questionRatings: ctrl.questionRatings,
							message: ctrl.message
						}
					});
				};
			}]
	});