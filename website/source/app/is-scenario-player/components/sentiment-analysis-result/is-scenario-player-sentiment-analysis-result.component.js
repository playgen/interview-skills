angular
	.module("app")
	.component("isScenarioPlayerSentimentAnalysisResult", {
		templateUrl: "/is-scenario-player/components/sentiment-analysis-result/is-scenario-player-sentiment-analysis-result.html",
		bindings: {
			feature: "<",
			weight: "<",
			weightFromIdeal: "<"			
		},
		controller: [
			"isScenarioPlayerSentimentAnalysisResultConfig", 
			"Interpolate",
			function (isScenarioPlayerSentimentAnalysisResultConfig, Interpolate) {
			const ctrl = this;

			// private variables
			var feature = null;

			var currentWeight = null;
			var currentWeightFromIdeal = null;
			var weightInterpolation = null;
			var weightFromIdealInterpolation = null;

			// public variables
			ctrl.label = null;
			ctrl.minSegmentWidth = 0;
			ctrl.idealSegmentWidth = 0;
			ctrl.maxSegmentWidth = 0;
			ctrl.color = null;

			ctrl.colorRange = isScenarioPlayerSentimentAnalysisResultConfig.colors.join(", ");
			ctrl.idealColor = isScenarioPlayerSentimentAnalysisResultConfig.colors[0];
						
			// angular hooks 	
			ctrl.$onDestroy = function() {
				if(weightInterpolation) {
					weightInterpolation.cancel();
				}
				
				if(weightFromIdealInterpolation) {
					weightFromIdealInterpolation.cancel();
				}
			};

			ctrl.$onChanges = function (changes) {
				if (changes.feature) {					
					feature = changes.feature.currentValue;

					var label = feature.id;
					var idealWeightMin = feature.idealWeightMin;
					var idealWeightMax = feature.idealWeightMax;
					
					if(feature.inverse) {
						label = feature.inverse;
						idealWeightMin = 1 - feature.idealWeightMax;
						idealWeightMax = 1 - feature.idealWeightMin;
					}

					ctrl.label = String.title(label);
					ctrl.minSegmentWidth = 100 * idealWeightMin;
					ctrl.idealSegmentWidth = 100 * (idealWeightMax - idealWeightMin);					
					ctrl.maxSegmentWidth = 100 - (ctrl.minSegmentWidth + ctrl.idealSegmentWidth);
				}

				if (changes.weight && changes.weightFromIdeal) {
					var newWeight = changes.weight.currentValue;
					var newWeightFromIdeal = changes.weightFromIdeal.currentValue;

					if(!feature) {
						Logger.warn("No feature set.");

					} else if (feature.inverse) {
						newWeight = 1 - changes.weight.currentValue;
					} 

					if(weightInterpolation) {
						weightInterpolation.cancel();
					}
					
					if(weightFromIdealInterpolation) {
						weightFromIdealInterpolation.cancel();
					}

					var distance = Math.abs(currentWeight - newWeight);
					var duration = isScenarioPlayerSentimentAnalysisResultConfig.lerpTime * distance;

					weightInterpolation = Interpolate.start(currentWeight, newWeight, duration, onWeightChanged, true);
					weightFromIdealInterpolation = Interpolate.start(currentWeightFromIdeal, newWeightFromIdeal, duration, onWeightFromIdealChanged, true);
				}				
			};

			// private methods		
			function onWeightChanged(weight) {
				ctrl.weight = weight;
				currentWeight = weight;
			}

			function onWeightFromIdealChanged(weightFromIdeal) {
				currentWeightFromIdeal = Math.abs(weightFromIdeal);								

				const lastIndex = isScenarioPlayerSentimentAnalysisResultConfig.colors.length - 1;
				let fromIdealIndex = currentWeightFromIdeal * lastIndex;
				fromIdealIndex = Math.ceil(fromIdealIndex);

				ctrl.color = isScenarioPlayerSentimentAnalysisResultConfig.colors[fromIdealIndex];
			}
		}]
	});