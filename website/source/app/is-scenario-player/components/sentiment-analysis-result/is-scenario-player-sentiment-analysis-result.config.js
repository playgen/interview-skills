angular
	.module("app")
	.value("isScenarioPlayerSentimentAnalysisResultConfig", {
		colors: [
			"#36D900",
			"#6DD900",
			"#A3D900", 
			"#D9D900", 
			"#D9A300",
			"#D96D00",
			"#D93600"
		],
		lerpTime: 3000
	});