angular
    .module("app")
    .component("isScenarioPlayerInfoBox", {
        templateUrl: "/is-scenario-player/components/info-box/is-scenario-player-info-box.html",
        transclude: {
            icon: "?infoBoxIcon",
            title: "?infoBoxTitle",
            content: "infoBoxContent"
        }
    });