﻿angular
	.module("app")
	.value("isScenarioPlayerIconButtonConfig", {
			typeMappings: {
				"quit": {
					icon: "logout",
					button: "secondary"
				},
				"home": {
					icon: "logout",
					button: "secondary"
				},
				"new game": {
					icon: "new-game",
					button: "primary"
				},
				"results": {
					icon: "results",
					button: "primary"
				},
				"swap": {
					icon: "swap",
					button: "primary"
				},
				"asked": {
					icon: "tick",
					button: "primary"
				},
				"answered": {
					icon: "tick",
					button: "primary"
				},
				"next": {
					icon: "tick",
					button: "primary"
				}
			}
		});