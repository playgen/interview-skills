﻿angular
	.module("app")
	.component("isScenarioPlayerIconButton", {
		templateUrl: "/is-scenario-player/components/icon-button/is-scenario-player-icon-button.html",
		bindings: {
			type: "<",
			onClick: "&",
			isDisabled: "<"
		},
		controller: [
			"isScenarioPlayerIconButtonConfig",
			function (isScenarioPlayerIconButtonConfig) {
				const ctrl = this;

				// public variables
				ctrl.buttonClass = null;
				ctrl.iconClass = null;

				// angular hooks
				ctrl.$onChanges = function (changes) {
					if (changes.type) {
						const type = changes.type.currentValue;
						ctrl.label = type.toUpperCase();
						
						const typeMapping = isScenarioPlayerIconButtonConfig.typeMappings[type];
						ctrl.buttonClass = typeMapping.button;
						ctrl.iconClass = typeMapping.icon;
					}
				};
			}]
	});