angular
    .module("app")
    .component("isScenarioPlayerBriefingBox", {
        templateUrl: "/is-scenario-player/components/briefing-box/is-scenario-player-briefing-box.html",
		bindings: {
			title: "<",
			body: "<"
		}
    });