﻿angular
	.module("app")
	.component("isScenarioPlayerPostInterviewSwapPrompt", {
		templateUrl: "/is-scenario-player/components/post-interview-swap-prompt/is-scenario-player-post-interview-swap-prompt.html",
		bindings: {
			userSessionId: "<",
			matchSessionId: "<",
			candidateStart: "<",
			candidateEnd: "<",
			interviewRound: "<",
			options: "<",
			onSelect: "&"
		},
		controller: [
			"isScenarioPlayerPostInterviewSwapPromptConfig",
			"SentimentAnalysisWebClient",
			function (isScenarioPlayerPostInterviewSwapPromptConfig, SentimentAnalysisWebClient) {
				const ctrl = this;

				// public variables
				ctrl.title = isScenarioPlayerPostInterviewSwapPromptConfig.title;
				ctrl.error = null;
				ctrl.averages = [];

				// angular hooks
				ctrl.$onInit = function () {
					getAverageResults(ctrl.userSessionId, ctrl.matchSessionId, ctrl.candidateStart, ctrl.candidateEnd);
				};

				// public methods
				ctrl.select = function (option) {
					ctrl.onSelect({ option: option });
				};

				// private methods
				function getAverageResults(userSessionId, matchSessionId, start, end) {
					SentimentAnalysisWebClient.getAverages(userSessionId, matchSessionId, start, end)
						.then(averages => {
							if (!averages || averages.length === 0) {
								ctrl.error = isScenarioPlayerPostInterviewSwapPromptConfig.error;

							} else {
								ctrl.error = null;

								averages.forEach(a => a.result.absoluteWeightFromIdeal = Math.abs(a.result.weightFromIdeal));
								
								ctrl.averages = averages.sort((a, b) => a.result.absoluteWeightFromIdeal - b.result.absoluteWeightFromIdeal)
									.slice(0, isScenarioPlayerPostInterviewSwapPromptConfig.resultsCount);
							}
						},
						error => {
							ctrl.error = isScenarioPlayerPostInterviewSwapPromptConfig.error;
							Logger.warn(error);
						});
				}
			}]
	});