﻿angular
	.module("app")
	.value("isScenarioPlayerPostInterviewSwapPromptConfig", {
		title: "Well done!" +
			"\nAs the candidate you did really well on:",
		resultsCount: 3,
		error: "No results available :( \nPlease check your webcam volume and video."
	});