angular
	.module("app")
	.component("isScenarioPlayerJobSelect", {
		templateUrl: "/is-scenario-player/components/job-select/is-scenario-player-job-select.html",
		bindings: {
			onSubmit: "&"
		},
		controller: ["isScenarioPlayerJobSelectConfig",
			function (isScenarioPlayerJobSelectConfig) {
				const ctrl = this;

				// public variables
				ctrl.selected = null;

				ctrl.tagStates = isScenarioPlayerJobSelectConfig.tags.reduce((map, tag) => {
					map[tag] = true;
					return map;
				}, {});

				updateActiveTags();

				// public methods
				ctrl.onTagStateChange = function(tag, state) {
					updateActiveTags();
				};

				// private methods
				function updateActiveTags() {
					ctrl.activeTags = Object.keys(ctrl.tagStates).map(key => {
						return ctrl.tagStates[key] ? key : null;
					});
				}
			}]
	});