angular
    .module("app")
    .component("isScenarioPlayerIconBox", {
        templateUrl: "/is-scenario-player/components/icon-box/is-scenario-player-icon-box.html",
        transclude: {
            icon: "iconBoxIcon",
            title: "?iconBoxTitle",
            content: "iconBoxContent"
        }
	});