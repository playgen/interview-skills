angular
    .module("app")
    .component("isScenarioPlayerSubmitIconBox", {
        templateUrl: "/is-scenario-player/components/submit-icon-box/is-scenario-player-submit-icon-box.html",
        bindings: {
            onSubmit: "&",
            canSubmit: "<"
        },
        transclude: {
            icon: "submitIconBoxIcon",
            title: "?submitIconBoxTitle",
            content: "submitIconBoxContent"
        },
        controller: function() {
	        const ctrl = this;

	        // public
            ctrl.isSubmitDisabled = function() {
	            const enabled = ctrl.canSubmit === undefined || ctrl.canSubmit;
	            return !enabled;
			};

	        ctrl.submit = function() {
		        ctrl.onSubmit();
	        };
        }
	});