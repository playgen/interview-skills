﻿angular
	.module("app")
	.component("isScenarioPlayerContainer", {
		templateUrl: "/is-scenario-player/components/player-container/is-scenario-player-container.html",
		bindings: {
			isSynchronised: "<",
			newGame: "<",
			onScenarioPlayerStopped: "<"
		},
		controller: [
			"appContext",
			"MatchData",
			"VideoCalling",
			"SentimentAnalysis",
			"$state",
			"ISScenarioPlayer",
			"ISScenarioPlayerLogging",
			"ScenarioStepper",
			"PeerSignalling",
			"$timeout",
			"RemoteCommands",
			function (
				appContext,
				MatchData,
				VideoCalling,
				SentimentAnalysis,
				$state,
				ISScenarioPlayer,
				ISScenarioPlayerLogging,
				ScenarioStepper,
				PeerSignalling,
				$timeout,
				RemoteCommands
			) {
				const ctrl = this;

				// angular hooks
				ctrl.$onInit = function () {
					if (appContext.user.session.match === undefined) {
						Logger.error("No valid match data. Executing stopped.");
						$timeout(ctrl.onScenarioPlayerStopped());
						return;
					}

					const openTokMatchData = MatchData.getOpenTokMatch(appContext.user.session.match);
					const openTokUserData = MatchData.getOpenTokUser(appContext.user.session.match);
					const scenario = MatchData.getScenario(appContext.user.session.match);
					const matchSessionId = appContext.user.session.match.session.id;
					const userSessionId = appContext.user.session.id;

					RemoteCommands.initialize();
					VideoCalling.initialize(openTokMatchData.apiKey, openTokMatchData.sessionId, openTokUserData.token);
					SentimentAnalysis.initialize(userSessionId, matchSessionId, RemoteCommands);

					if (ctrl.isSynchronised) {
						const clientIds = {
							all: Array.select(appContext.user.session.match.participants, p => p.matchingParticipant.id),
							me: appContext.user.session.match.thisParticipant.matchingParticipant.id,
							master: Array.first(appContext.user.session.match.participants, p => p.isMaster).matchingParticipant.id
						};
						clientIds.other = Array.where(clientIds.all, id => id !== clientIds.me);

						PeerSignalling.initialize(VideoCalling, clientIds.me);

						if (appContext.user.session.match.thisParticipant.isMaster) {
							ScenarioStepper.initialize(PeerSignalling.sendSignal, PeerSignalling.signalReceivedEvent, clientIds.all);
						}

						VideoCalling.connectedEvent.subscribe(PeerSignalling.sendPending, PeerSignalling);
						VideoCalling.signalReceivedEvent.subscribe(PeerSignalling.onSignalReceived, PeerSignalling);

						ISScenarioPlayer.initializeSynchronised(
							scenario,
							clientIds.me,
							matchSessionId,
							ctrl.newGame,
							VideoCalling,
							PeerSignalling.sendSignal,
							PeerSignalling.signalReceivedEvent,
							clientIds.master,
							clientIds.other);

					} else {
						ISScenarioPlayer.initialize(
							scenario,
							userSessionId,
							matchSessionId,
							ctrl.newGame,
							VideoCalling);
					}

					ISScenarioPlayerLogging.initialize(ISScenarioPlayer, matchSessionId, userSessionId);
					ISScenarioPlayer.player.stoppedEvent.subscribe(ctrl.onScenarioPlayerStopped);
				};

				ctrl.$onDestroy = function () {
					ISScenarioPlayer.player.stoppedEvent.unsubscribe(ctrl.onScenarioPlayerStopped);
					
					SentimentAnalysis.terminate()
						.then(() => {
							ISScenarioPlayerLogging.terminate();
							ISScenarioPlayer.terminate();
							
							if (ctrl.isSynchronised) {
								VideoCalling.connectedEvent.unsubscribe(PeerSignalling.sendPending);
								VideoCalling.signalReceivedEvent.unsubscribe(PeerSignalling.onSignalReceived);

								ScenarioStepper.terminate();
								PeerSignalling.terminate();
							}

							VideoCalling.terminate();
							RemoteCommands.terminate();
						});
				};
			}]
	});