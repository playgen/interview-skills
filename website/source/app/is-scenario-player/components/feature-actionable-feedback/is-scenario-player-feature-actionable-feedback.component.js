angular
    .module("app")
    .component("isScenarioPlayerFeatureActionableFeedback", {
        templateUrl: "/is-scenario-player/components/feature-actionable-feedback/is-scenario-player-feature-actionable-feedback.html",
        bindings: {
            feedback: "<",
            onSubmit: "&"
        }
    });