angular
	.module("app")
	.component("isScenarioPlayer", {
		templateUrl: "/is-scenario-player/components/player/is-scenario-player.html",
		controller: [
			"ISScenarioPlayer",
			"SentimentAnalysis",
			function (ISScenarioPlayer, 
				SentimentAnalysis) {
				const ctrl = this;
				const genericSelectedFlag = ["selected"];
				ctrl.isSynchronised = null;

				// =======================================================================================
				// Internal Controllers
				// =======================================================================================
				ctrl.promptQueue = {
					_enqueue: null,
					dequeue: null,
					empty: null,
					isVisible: false
				};

				ctrl.promptQueue.enqueue = (prompt) => {
					ctrl.promptQueue._enqueue(prompt);
					ctrl.promptQueue.isVisible = true;
				};
				ctrl.promptQueue.onEmptiedQueue = () => ctrl.promptQueue.isVisible = false;

				ctrl.job = {
					onSubmit: function (job) {
						ISScenarioPlayer.view.model.job.selection = [job];

						if (ISScenarioPlayer.player.isSynchronised) {
							ISScenarioPlayer.components.jobResultSignalling.submit(ISScenarioPlayer.view.model.job.selection);
						}
					}
				};

				// Interview Question
				ctrl.interviewQuestion = {
					onSelect: function (option) {
						ISScenarioPlayer.view.model.interviewQuestion.selection = [option];
					}
				};






				// Interview Question Respond
				ctrl.interviewQuestionRespond = {
					isVisible: false,
					state: null,
					current: null,
					total: null,
					question: null,
					length: null
				};

				// Observational Question
				ctrl.observationalQuestion = {
					submittedPrompt: null
				};

				// Observational Question Feedback
				ctrl.observationalQuestionFeedback = {
					submittedPrompt: null
				};

				// Feature Actionable Feedback
				ctrl.featureActionableFeedback = {
					submittedPrompt: null
				};

				// Waiting
				ctrl.waiting = {
					isVisible: false,
					title: null,
					body: null
				};

				// Other Left
				ctrl.otherLeft = {
					isVisible: false,
					body: null,
					onSelect: function (option) {
						ISScenarioPlayer.view.model.otherLeft.selection = [option];
					}
				};

				// Interviewer Candidate Appraisal
				ctrl.interviewerCandidateAppraisal = {
					isVisible: false,
					onSubmit: function (option, appraisalResult) {
						ISScenarioPlayer.view.model.interviewerCandidateAppraisal.selection = [option];
						ISScenarioPlayer.components.interviewerCandidateAppraisalSignalling.submit(appraisalResult);
					}
				};

				// Post Interview Swap Prompt
				ctrl.postInterviewSwapPrompt = {
					isVisible: false,
					onSelect: function (option) {
						ISScenarioPlayer.view.model.postInterviewSwapPrompt.selection = [option];
					}
				};

				// Post Interview Waiting
				ctrl.postInterviewWaiting = {
					isVisible: false
				};

				// Candidate Interview Summary
				ctrl.candidateInterviewSummary = {
					isVisible: false,
					onSelect: function (option) {
						ISScenarioPlayer.view.model.candidateInterviewSummary.selection = [option];
						ISScenarioPlayer.view.model.events.candiateInterviewSummary.publish(option);
					}
				};




				// =======================================================================================
				// View Controller Commands 
				// =======================================================================================
				ISScenarioPlayer.view.controller.showTitle = function () {
					ISScenarioPlayer.view.model.title.isShown = true;
				};

				ISScenarioPlayer.view.controller.hideTitle = function () {
					ISScenarioPlayer.view.model.title.isShown = false;
				};

				ISScenarioPlayer.view.controller.showJobSelect = function (instruction, initialSelection) {
					ISScenarioPlayer.view.model.job.selection = initialSelection;
					ISScenarioPlayer.view.model.job.instruction = instruction;
					ISScenarioPlayer.view.model.job.isShown = true;
				};

				ISScenarioPlayer.view.controller.hideJobSelect = function () {
					ISScenarioPlayer.view.model.job.isShown = false;
				};

				ISScenarioPlayer.view.controller.applyJobResult = function (result) {
					ISScenarioPlayer.view.model.job.selection = result;
				};

				ISScenarioPlayer.view.controller.showBrief = function (title) {
					ISScenarioPlayer.view.model.briefing.title = title;
					ISScenarioPlayer.view.model.briefing.body = ISScenarioPlayer.view.model.job.selection[0].id; // Should have be set in a previous job selection step
					ISScenarioPlayer.view.model.briefing.isShown = true;
				};

				ISScenarioPlayer.view.controller.hideBrief = function () {
					ISScenarioPlayer.view.model.briefing.isShown = false;
				};

				ISScenarioPlayer.view.controller.showSentimentAnalysis = function () {
					ISScenarioPlayer.view.model.sentimentAnalysis.isShown = true;
				};

				ISScenarioPlayer.view.controller.hideSentimentAnalysis = function () {
					ISScenarioPlayer.view.model.sentimentAnalysis.isShown = false;
				};

				ISScenarioPlayer.view.controller.showInterviewQuestion = function (question, state, length, options, initialSelection) {
					ISScenarioPlayer.view.model.interviewQuestion.selection = initialSelection;
					ISScenarioPlayer.view.model.interviewQuestion.state = state;
					ISScenarioPlayer.view.model.interviewQuestion.question = question;
					ISScenarioPlayer.view.model.interviewQuestion.length = length;
					ISScenarioPlayer.view.model.interviewQuestion.options = options;
					ISScenarioPlayer.view.model.interviewQuestion.isShown = true;

					if (ISScenarioPlayer.player.isSynchronised) {
						ISScenarioPlayer.components.interviewQuestionSignalling.submit(ISScenarioPlayer.view.model.interviewQuestion);
					}

					ISScenarioPlayer.view.controller.events.interviewQuestionUpdated.publish({
						question: ISScenarioPlayer.view.model.interviewQuestion.question,
						state: ISScenarioPlayer.view.model.interviewQuestion.state
					});
				};

				ISScenarioPlayer.view.controller.updateInterviewQuestion = function (state, length, options, initialSelection) {
					ISScenarioPlayer.view.model.interviewQuestion.selection = initialSelection;
					ISScenarioPlayer.view.model.interviewQuestion.state = state;
					ISScenarioPlayer.view.model.interviewQuestion.length = length;
					ISScenarioPlayer.view.model.interviewQuestion.options = options;

					if (ISScenarioPlayer.player.isSynchronised) {
						ISScenarioPlayer.components.interviewQuestionSignalling.submit(ISScenarioPlayer.view.model.interviewQuestion);
					}

					ISScenarioPlayer.view.controller.events.interviewQuestionUpdated.publish({
						question: ISScenarioPlayer.view.model.interviewQuestion.question,
						state: ISScenarioPlayer.view.model.interviewQuestion.state
					});
				};

				ISScenarioPlayer.view.controller.hideInterviewQuestion = function () {
					ISScenarioPlayer.view.model.interviewQuestion.isShown = false;
					ISScenarioPlayer.view.controller.events.interviewQuestionHidden.publish(
						ISScenarioPlayer.view.model.interviewQuestion.question);
				};












				ISScenarioPlayer.view.controller.showWaiting = function (title, body) {
					ctrl.waiting.title = title;
					ctrl.waiting.body = body;
					ctrl.waiting.isVisible = true;
				};

				ISScenarioPlayer.view.controller.hideWaiting = function () {
					ctrl.waiting.isVisible = false;
				};



				ISScenarioPlayer.view.controller.setRole = function (roleId) {
					ISScenarioPlayer.view.model.role.id = roleId;

					ISScenarioPlayer.view.model.interviewRound++;

					ISScenarioPlayer.view.model.events.role.publish(roleId);
				};

				ISScenarioPlayer.view.controller.showInterviewQuestionRespond = function (question, state, current, total, length) {
					ctrl.interviewQuestionRespond.state = state;
					ctrl.interviewQuestionRespond.question = question;
					ctrl.interviewQuestionRespond.current = current;
					ctrl.interviewQuestionRespond.total = total;
					ctrl.interviewQuestionRespond.length = length;

					ctrl.interviewQuestionRespond.isVisible = true;
				};

				ISScenarioPlayer.view.controller.hideInterviewQuestionRespond = function () {
					ctrl.interviewQuestionRespond.isVisible = false;
				};

				ISScenarioPlayer.view.controller.showObservationalQuestion = function (question, initialSelection) {
					ISScenarioPlayer.view.model.observationalQuestion.selection = initialSelection;

					const prompt = {
						type: "ObservationalQuestion",
						question: question.question,
						isBinary: question.isBinary,
						isInverse: question.isInverse,
						startLabel: question.startLabel,
						endLabel: question.endLabel
					};
					prompt.onSubmit = result => {
						const rating = result.result;
						ISScenarioPlayer.view.model.observationalQuestion.selection = [rating];
						ctrl.observationalQuestion.submittedPrompt = prompt;

						if (ISScenarioPlayer.player.isSynchronised) {
							ISScenarioPlayer.components.observationalQuestionResultSignalling.submit({
								question: question,
								rating: rating
							});
						}

						ISScenarioPlayer.view.controller.events.observationalQuestionSubmitted.publish({
							question: question,
							rating: rating
						});
					};
					prompt.onCurrent = ISScenarioPlayer.view.controller.events.observationalQuestionShown.publish(question);

					ctrl.promptQueue.enqueue(prompt);
				};

				ISScenarioPlayer.view.controller.hideObservationalQuestion = function () {
					ctrl.promptQueue.dequeue(ctrl.observationalQuestion.submittedPrompt);
					ctrl.observationalQuestion.submittedPrompt = null;
				};

				ISScenarioPlayer.view.controller.showObservationalQuestionFeedback = function (feedback, initialSelection) {
					ISScenarioPlayer.view.model.observationalQuestionFeedback.selection = initialSelection;

					const prompt = {
						type: "ObservationalQuestionFeedback",
						feedback: feedback.feedback
					};
					prompt.onSubmit = () => {
						ISScenarioPlayer.view.model.observationalQuestionFeedback.selection = genericSelectedFlag;
						ctrl.observationalQuestionFeedback.submittedPrompt = prompt;
					};

					ctrl.promptQueue.enqueue(prompt);
				};

				ISScenarioPlayer.view.controller.hideObservationalQuestionFeedback = function () {
					ctrl.promptQueue.dequeue(ctrl.observationalQuestionFeedback.submittedPrompt);
					ctrl.observationalQuestionFeedback.submittedPrompt = null;
				};

				ISScenarioPlayer.view.controller.showFeatureActionableFeedback = function (feedback, initialSelection) {
					ISScenarioPlayer.view.model.featureActionableFeedback.selection = initialSelection;

					const prompt = {
						type: "FeatureActionableFeedback",
						feedback: feedback.feedback
					};
					prompt.onSubmit = () => {
						ISScenarioPlayer.view.model.featureActionableFeedback.selection = genericSelectedFlag;
						ctrl.featureActionableFeedback.submittedPrompt = prompt;
						ISScenarioPlayer.view.controller.events.featureActionableFeedbackSubmitted.publish(feedback);
					};
					prompt.onCurrent = () => ISScenarioPlayer.view.controller.events.featureActionableFeedbackShown.publish(feedback);

					ctrl.promptQueue.enqueue(prompt);
				};

				ISScenarioPlayer.view.controller.hideFeatureActionableFeedback = function () {
					ctrl.promptQueue.dequeue(ctrl.featureActionableFeedback.submittedPrompt);
					ctrl.featureActionableFeedback.submittedPrompt = null;
				};

				ISScenarioPlayer.view.controller.showOtherLeft = function (title, body, options, initialSelection) {
					ctrl.promptQueue.empty();

					ISScenarioPlayer.view.model.otherLeft.selection = initialSelection;
					ctrl.otherLeft.options = options;
					ctrl.otherLeft.body = body;
					ctrl.otherLeft.title = title;
					ctrl.otherLeft.isVisible = true;
				};

				ISScenarioPlayer.view.controller.hideOtherLeft = function () {
					ctrl.otherLeft.isVisible = false;
				};

				ISScenarioPlayer.view.controller.showInterviewerCandidateAppraisal = function (options, initialSelection, interviewRound) {
					ctrl.promptQueue.empty();

					ISScenarioPlayer.view.model.interviewerCandidateAppraisal.selection = initialSelection;
					ctrl.interviewerCandidateAppraisal.options = options;
					ctrl.interviewerCandidateAppraisal.interviewRound = interviewRound;
					ctrl.interviewerCandidateAppraisal.isVisible = true;
				};

				ISScenarioPlayer.view.controller.hideInterviewerCandidateAppraisal = function () {
					ctrl.interviewerCandidateAppraisal.isVisible = false;
				};

				ISScenarioPlayer.view.controller.showPostInterviewSwapPrompt = function (options, initialSelection,
					interviewRound, userSessionId, matchSessionId, candidateStart, candidateEnd) {
					ctrl.promptQueue.empty();

					ISScenarioPlayer.view.model.postInterviewSwapPrompt.selection = initialSelection;

					ctrl.postInterviewSwapPrompt.options = options;
					ctrl.postInterviewSwapPrompt.interviewRound = interviewRound;
					ctrl.postInterviewSwapPrompt.userSessionId = userSessionId;
					ctrl.postInterviewSwapPrompt.matchSessionId = matchSessionId;
					ctrl.postInterviewSwapPrompt.candidateStart = candidateStart;
					ctrl.postInterviewSwapPrompt.candidateEnd = candidateEnd;

					ctrl.postInterviewSwapPrompt.isVisible = true;
				};

				ISScenarioPlayer.view.controller.hidePostInterviewSwapPrompt = function () {
					ctrl.postInterviewSwapPrompt.isVisible = false;
				};

				ISScenarioPlayer.view.controller.showPostInterviewWaiting = function (interviewRound,
					userSessionId, matchSessionId, candidateStart, candidateEnd) {
					ctrl.promptQueue.empty();

					ctrl.postInterviewWaiting.interviewRound = interviewRound;
					ctrl.postInterviewWaiting.userSessionId = userSessionId;
					ctrl.postInterviewWaiting.matchSessionId = matchSessionId;
					ctrl.postInterviewWaiting.candidateStart = candidateStart;
					ctrl.postInterviewWaiting.candidateEnd = candidateEnd;

					ctrl.postInterviewWaiting.isVisible = true;
				};

				ISScenarioPlayer.view.controller.hidePostInterviewWaiting = function () {
					ctrl.postInterviewWaiting.isVisible = false;
				};

				ISScenarioPlayer.view.controller.showCandidateInterviewSummary = function (options, initialSelection,
					job, interviewerObservationalRatings, interviewerMessage, userSessionId, matchSessionId, candidateStart, candidateEnd) {

					ISScenarioPlayer.view.model.candidateInterviewSummary.selection = initialSelection;

					ctrl.candidateInterviewSummary.options = options;
					ctrl.candidateInterviewSummary.job = job;
					ctrl.candidateInterviewSummary.interviewerObservationalRatings = interviewerObservationalRatings;
					ctrl.candidateInterviewSummary.interviewerMessage = interviewerMessage;
					ctrl.candidateInterviewSummary.userSessionId = userSessionId;
					ctrl.candidateInterviewSummary.matchSessionId = matchSessionId;
					ctrl.candidateInterviewSummary.candidateStart = candidateStart;
					ctrl.candidateInterviewSummary.candidateEnd = candidateEnd;

					ctrl.candidateInterviewSummary.isVisible = true;
				};

				ISScenarioPlayer.view.controller.hideCandidateInterviewSummary = function () {
					ctrl.candidateInterviewSummary.isVisible = false;
				};

				// ================================================================================
				// Angular Hooks 
				// ================================================================================
				ctrl.$onInit = function () {
					ctrl.model = ISScenarioPlayer.view.model;
					ctrl.isSynchronised = ISScenarioPlayer.player.isSynchronised;

					SentimentAnalysis.analysisUpdateReceivedEvent.subscribe(onSentimentAnalysisReceived);

					ISScenarioPlayer.player.start();
				};

				ctrl.$onDestroy = function () {
					if (ISScenarioPlayer.player) {
						ISScenarioPlayer.player.stop();
					}

					if (SentimentAnalysis.analysisUpdateReceivedEvent) {
						SentimentAnalysis.analysisUpdateReceivedEvent.unsubscribe(onSentimentAnalysisReceived);
					}
				};

				// Private Methods
				function onSentimentAnalysisReceived(analysisMetadata) {
					ISScenarioPlayer.view.model.sentimentAnalysis.analysisMetadata = analysisMetadata;
				}
			}
		]
	});