angular
    .module("app")
    .component("isScenarioPlayerOtherLeft", {
        templateUrl: "/is-scenario-player/components/other-left/is-scenario-player-other-left.html",
		bindings: {
			title: "<",
			body: "<",
            options: "<",
            onSelect: "&"
        }
    });