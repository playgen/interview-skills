﻿angular
	.module("app")
	.component("isScenarioPlayerPostInterviewBox", {
		templateUrl: "/is-scenario-player/components/post-interview-box/is-scenario-player-post-interview-box.html",
		bindings: {
			interviewRound: "<",
			options: "<",
			getEnabled: "<",
			onSelect: "&"
		},
		transclude: {
			icon: "postInterviewBoxIcon",
			title: "?postInterviewBoxTitle",
			content: "postInterviewBoxContent"
		},
		controller: [
			"isScenarioPlayerPostInterviewBoxConfig",
			function (isScenarioPlayerPostInterviewBoxConfig) {
				const ctrl = this;

				ctrl.title = null;

				// angular hooks
				ctrl.$onInit = function () {
					ctrl.style = isScenarioPlayerPostInterviewBoxConfig.interviewRoundStyles[ctrl.interviewRound];
					ctrl.title = isScenarioPlayerPostInterviewBoxConfig.title[ctrl.style];
				};

				// public methods
				ctrl.select = function(option) {
					ctrl.onSelect({option: option});
				};

				ctrl.getButtonInfo = function (key) {
					return isScenarioPlayerPostInterviewBoxConfig.buttonInfos[key];
				};
			}]
	});