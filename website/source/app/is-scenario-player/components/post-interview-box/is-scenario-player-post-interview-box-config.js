﻿angular
	.module("app")
	.value("isScenarioPlayerPostInterviewBoxConfig", {
		buttonInfos: {
			"quit": "Your feedback will be lost!",
			"swap": "Swap Roles! You'll be the interviewer",
			"results": "See how you did as the candidate!"
		},
		title: {
			"halftime": "Half Time!",
			"fulltime": "Full Time!"
		},
		interviewRoundStyles: {
			1: "halftime",
			2: "fulltime"
		}
	});