angular
    .module("app")
    .component("isScenarioPlayerObservationalQuestionFeedback", {
		templateUrl: "/is-scenario-player/components/observational-question-feedback/is-scenario-player-observational-question-feedback.html",
        bindings: {
            feedback: "<",
            onSubmit: "&"
        }
    });