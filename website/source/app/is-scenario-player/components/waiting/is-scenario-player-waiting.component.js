angular
    .module("app")
    .component("isScenarioPlayerWaiting", {
        templateUrl: "/is-scenario-player/components/waiting/is-scenario-player-waiting.html",
        bindings: {
            title: "<",
            body: "<"
        }
    });