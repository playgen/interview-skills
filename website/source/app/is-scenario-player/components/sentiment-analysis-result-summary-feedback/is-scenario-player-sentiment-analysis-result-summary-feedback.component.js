﻿angular
	.module("app")
	.component("isScenarioPlayerSentimentAnalysisResultSummaryFeedback", {
		templateUrl: "/is-scenario-player/components/sentiment-analysis-result-summary-feedback/is-scenario-player-sentiment-analysis-result-summary-feedback.html",
		bindings: {
			feature: "<",
			weight: "<",
			weightFromIdeal: "<"
		},
		controller: [
			"FeatureSummaryFeedbackWebClient",
			"isScenarioPlayerSentimentAnalysisResultSummaryFeedbackConfig",
			function (FeatureSummaryFeedbackWebClient, isScenarioPlayerSentimentAnalysisResultSummaryFeedbackConfig) {
				const ctrl = this;

				// private variables
				var feedback = null;

				// public variables
				ctrl.featureLabel = null;
				ctrl.feedback = null;
				ctrl.loading = isScenarioPlayerSentimentAnalysisResultSummaryFeedbackConfig.loading;

				// angular hooks
				ctrl.$onChanges = function (changes) {
					if (changes.feature || changes.weightFromIdeal) {
						const feature = changes.feature ? changes.feature.currentValue : ctrl.feature;
						const weightFromIdeal = changes.weightFromIdeal ? changes.weightFromIdeal.currentValue : ctrl.weightFromIdeal;

						if (feature && (weightFromIdeal || weightFromIdeal === 0)) {
							getFeedback(feature, weightFromIdeal);
						}
					}
				};

				// private methods
				function getFeedback(feature, weightFromIdeal) {
					ctrl.feedback = null;

					FeatureSummaryFeedbackWebClient.listFeedbacks(feature.id, weightFromIdeal)
						.then(feedbacks => {
							feedback = Array.takeRandom(feedbacks);
							ctrl.feedback = feedback.feedback;
						});
				}
			}
		]
	});