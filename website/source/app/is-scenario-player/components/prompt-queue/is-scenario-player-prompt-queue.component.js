﻿angular
    .module("app")
    .component("isScenarioPlayerPromptQueue", {
        templateUrl: "/is-scenario-player/components/prompt-queue/is-scenario-player-prompt-queue.html",
        bindings: {
            enqueue: "=",
            dequeue: "=",
            empty: "=",
            onEmptiedQueue: "&"
        },
        controller: ["isScenarioPlayerPromptQueueConfig",
            function (isScenarioPlayerPromptQueueConfig) {
                const ctrl = this;

                // Public Variables
                ctrl.current = null;

                // Private Variables
                const queue = [];

                // Angular Hooks
                ctrl.$onInit = function () {
                    ctrl.enqueue = enqueue;
                    ctrl.dequeue = dequeue;
					ctrl.empty = empty;
                    ctrl.isEmpty = () => ctrl.current === null && ctrl.queue.length === 0;
                };

                // Public Methods
                ctrl.submit = function (result) {
                    ctrl.current.onSubmit({ result: result });

                    tryMoveNext();
                };

                // Private Methods
                function empty() {
                    const didClearAny = ctrl.current || queue.length > 0;

                    ctrl.current = null;
                    queue.splice(0, queue.length);

                    if (didClearAny) {
                        ctrl.onEmptiedQueue();
                    }
                }

                function dequeue(prompt) {
                    Array.remove(queue, prompt);

                    if (ctrl.current === prompt) {
                        tryMoveNext();
                    }
                }

                function tryMoveNext() {
                    if (queue.length === 0) {
                        ctrl.current = null;
                        ctrl.onEmptiedQueue();

					} else {
						const prompt = Array.takeAt(queue, 0);
						setCurrent(prompt);
                    }
                }

				function setCurrent(prompt) {
					ctrl.current = prompt;

					if (ctrl.current.onCurrent) {
						ctrl.current.onCurrent(ctrl.current);
					}
				}

				function enqueue(prompt) {
					// Remove older
					if (queue.length >= isScenarioPlayerPromptQueueConfig.maxLength) {
                        queue.splice(0, (queue.length - isScenarioPlayerPromptQueueConfig.maxLength) + 1);
                    }

					if (isScenarioPlayerPromptQueueConfig.maxLength > 0) {
						queue.push(prompt);

						if (ctrl.current === null) {
							tryMoveNext();
						}
					} else {
						if (ctrl.current === null) {
							setCurrent(prompt);
						}
					}
				}
            }
        ]
    });
