angular
	.module("sentimentAnalysis")
	.component("isScenarioPlayerSentimentAnalysisUpdates", {
		templateUrl: "/is-scenario-player/components/sentiment-analysis-updates/is-scenario-player-sentiment-analysis-updates.html",
		controller: [
			"SentimentAnalysis",
			"$timeout",
			function (SentimentAnalysis, $timeout) {
				const ctrl = this;

				// public variables
				ctrl.analysis = null;
				ctrl.error = null;

				// private variables
				let pendingAnalyses = [];
				let takePendingResultTimer = null;
				let waitInterval = 0;//1000;
				let timerEnd = 0;
				let latestRecordingStarted = 0;

				// public methods
				ctrl.$postLink = function () {
					SentimentAnalysis.analysisUpdateReceivedEvent.subscribe(onAnalysisReceived);
					SentimentAnalysis.analysisUpdateErrorEvent.subscribe(onAnalysisError);
				};

				ctrl.$onDestroy = function () {
					if (SentimentAnalysis.analysisUpdateReceivedEvent) {
						SentimentAnalysis.analysisUpdateReceivedEvent.unsubscribe(onAnalysisReceived);
					}

					if (SentimentAnalysis.analysisUpdateErrorEvent) {
						SentimentAnalysis.analysisUpdateErrorEvent.unsubscribe(onAnalysisError);
					}
				};

				// private methods
				function onAnalysisReceived(analysisMetadata) {
					let recordingStarted = Date.parse(analysisMetadata.recordingStarted);

					if(recordingStarted < latestRecordingStarted) {
						Logger.debug("Ignoring result that is not later than the current result." + 
							`Ignored Result Recording Started: ${new Date(recordingStarted).toISOString()}. Latest Recording Started: ${new Date(latestRecordingStarted).toISOString()}.`);

					} else {
						latestRecordingStarted = recordingStarted;
						addAnalysis(analysisMetadata);
					}
				}

				function onAnalysisError(error) {
					ctrl.error = error;					
				}

				function addAnalysis(analysis) {	
					if(waitInterval <= 0) {
						ctrl.analysis = analysis;

					} else if(takePendingResultTimer) {
						pendingAnalyses.push(analysis);						
						
						let waitTime = waitInterval / pendingAnalyses.length;
						if(timerEnd > Date.now() + waitTime) {
							$timeout.cancel(takePendingResultTimer);
							startTakePendingTimer(waitInterval / pendingAnalyses.length);
						}
						
					} else {
						ctrl.analysis = analysis;
						startTakePendingTimer(waitInterval);
					}
				}

				function takePendingAnalyses() {
					ctrl.analysis = Array.takeAt(pendingAnalyses, 0);					
					
					if(pendingAnalyses.length > 0) {
						startTakePendingTimer(waitInterval / pendingAnalyses.length);
					} else {
						takePendingResultTimer = null;
					}					
				}

				function startTakePendingTimer(waitTime) {
					timerEnd = Date.now() + waitTime;
					takePendingResultTimer = $timeout(takePendingAnalyses, waitTime);
				}				
			}]
	});