angular
    .module("app")
    .component("isScenarioPlayerMessageBox", {
        templateUrl: "/is-scenario-player/components/message-box/is-scenario-player-message-box.html",
        transclude: {
            content: "messageBoxContent"
        }
    });