angular
    .module("app")
	.value("isScenarioPlayerInterviewQuestionRespondConfig", {
		stateMappings: {
			"asking": {
				title: "Answer",
				icon: "answer-question",
				instruction: "Interviewer asking question:",
				question: "Listen to the interviewer's question."
			},
			"answering": {
				title: "Answering",
				icon: "timer-filling",
				instruction: "Answer question:"
			},
			"answered": {
				title: "Time's up!",
				icon: "timer-full",
				instruction: ""
			}
		}
    });