angular
	.module("app")
	.component("isScenarioPlayerInterviewQuestionRespond", {
		templateUrl: "/is-scenario-player/components/interview-question-respond/is-scenario-player-interview-question-respond.html",
		bindings: {
			question: "<",
			state: "<",
			length: "<"
		},
		controller: ["isScenarioPlayerInterviewQuestionRespondConfig",
			function (isScenarioPlayerInterviewQuestionRespondConfig) {
				const ctrl = this;

				// public variables
				ctrl.title = null;
				ctrl.instruction = null;
				ctrl.icon = null;
				
				// angular hooks
				ctrl.$onChanges = function (changes) {
					if (changes.state && changes.state.currentValue) {
						const mappings = isScenarioPlayerInterviewQuestionRespondConfig.stateMappings[changes.state.currentValue];

						ctrl.title = mappings.title;
						ctrl.instruction = mappings.instruction;
						ctrl.icon = mappings.icon;
						ctrl.body = mappings.question ? mappings.question : ctrl.question;
					}
				};
			}]
	});