angular
    .module("app")
    .value("isScenarioPlayerSentimentAnalysisResultsConfig", {
        awaitingResponse: {
            title: "Awaiting Results..."
        },
        results: {
            title: "You are displaying:",
            instruction: "Keep it green!",
        },
        error: {
            title: "Analysis Issue:",
        },
        noSpeech: {
            title: "No speech detected."
        },
        displayCount: -1,
        sortResults: false,
        keepPreviousResults: true
	});