angular
	.module("app")
	.component("isScenarioPlayerSentimentAnalysisResults", {
		templateUrl: "/is-scenario-player/components/sentiment-analysis-results/is-scenario-player-sentiment-analysis-results.html",
		bindings: {
			analysis: "<",
			error: "<"
		},
		controller: ["isScenarioPlayerSentimentAnalysisResultsConfig", function(isScenarioPlayerSentimentAnalysisResultsConfig) {
			const ctrl = this;

			// public variables						
			ctrl.config = isScenarioPlayerSentimentAnalysisResultsConfig;
			ctrl.state = "AwaitingResponse"; // AwaitingResponse, Results, NoSpeech, Error						
			ctrl.stateConfig = isScenarioPlayerSentimentAnalysisResultsConfig.awaitingResponse;
			ctrl.displayResults = [];						

			// angular hooks
			ctrl.$onChanges = function(changes) {
				if(changes.analysis && changes.analysis.currentValue) {
					let analysis = changes.analysis.currentValue;
					let hasResults = processResults(analysis.featureResults);
					if (hasResults) {
						ctrl.state = "Results";
						ctrl.stateConfig = isScenarioPlayerSentimentAnalysisResultsConfig.results;
					}

					if(!analysis.isSpeechDetected) {
						ctrl.state = "NoSpeech";
						ctrl.stateConfig = isScenarioPlayerSentimentAnalysisResultsConfig.noSpeech;						
						Logger.debug("No Speech Detected");
					}

					if(analysis.errors && analysis.errors.length > 0) {
						ctrl.state = "Error";
						ctrl.error = analysis.errors.join('\n');
						ctrl.stateConfig = isScenarioPlayerSentimentAnalysisResultsConfig.error;
						Logger.warn(`Sentiment Analysis Result Error Received: ${ctrl.error}`);								
					}
				}

				if(changes.error && changes.error.currentValue) {
					ctrl.state = "Error";
					ctrl.stateConfig = isScenarioPlayerSentimentAnalysisResultsConfig.error;
					Logger.warn(`Sentiment Analysis Result Error Received: ${changes.error.currentValue}`);								
				}
			};

			// private methods
			function processResults(results) {			
				if (results && results.length > 0) {

					var filteredResults = isScenarioPlayerSentimentAnalysisResultsConfig.sortResults ?
						SentimentAnalysisResultsUtil.sortResults(results) :
						results;

					if (isScenarioPlayerSentimentAnalysisResultsConfig.displayCount >= 0 &&
						filteredResults.length > isScenarioPlayerSentimentAnalysisResultsConfig.displayCount) {
						filteredResults.splice(isScenarioPlayerSentimentAnalysisResultsConfig.displayCount, filteredResults.length - isScenarioPlayerSentimentAnalysisResultsConfig.displayCount);
					}

					// update current results in place to avoid the individual result html elements from being redrawn
					filteredResults.forEach(newResult => {
						var previousResult = ctrl.displayResults.find(r => r.feature.id === newResult.feature.id);

						if(previousResult) {
							previousResult.feature = newResult.feature;
							previousResult.weight = newResult.weight;
							previousResult.weightFromIdeal = newResult.weightFromIdeal;						

						} else {
							ctrl.displayResults.push({
								feature: newResult.feature,
								weight: newResult.weight,
								weightFromIdeal: newResult.weightFromIdeal
							});
						}
					});			
					
					// prune
					if(ctrl.displayResults.length > filteredResults.length) {
						for(var i = ctrl.displayResults.length - 1; i >= 0; i--) {
							/*jshint loopfunc:true */
							if(filteredResults.findIndex(fr => fr.feature.id === ctrl.displayResults[i].feature.id) < 0) {
								ctrl.displayResults.splice(i, 1);
							}
						}
					}					
				}

				return results && Object.keys(results).length > 0;
			}
		}]
	});