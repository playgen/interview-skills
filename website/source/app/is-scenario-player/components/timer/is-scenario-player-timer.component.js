﻿angular
	.module("app")
	.component("isScenarioPlayerTimer", {
		templateUrl: "/is-scenario-player/components/timer/is-scenario-player-timer.html",
		bindings: {
			length: "<"
		},
		controller: [
			"isScenarioPlayerTimerConfig",
			"$timeout",
			function (isScenarioPlayerTimerConfig, $timeout) {
				const ctrl = this;

				// public variables
				ctrl.width = 20;

				// private variables
				var timeout = null;
				var loader = null;
				var a = 0;
				var step = 0;
				
				// angular hooks
				ctrl.$postLink = function () {
					loader = angular.element(document.querySelector("#loader"));
					step = (360 * (isScenarioPlayerTimerConfig.interval / 1000)) / ctrl.length;
					draw();
				};

				ctrl.$onDestory = function() {
					$timeout.cancel(timeout);
				};

				// private methods
				function draw() {
					a += step;

					if (a > 359) {
						a = 359;
					}

					const r = (a * Math.PI / 180);
					const x = Math.sin(r) * 50;
					const y = Math.cos(r) * - 50;
					const mid = (a > 180) ? 1 : 0;

					const anim = "M 0 0 v -50" +
						" A 50 50 1 " +
						mid + " 1 " +
						x + " " +
						y + " z";

					loader.attr("d", anim);

					if (a < 359) {
						timeout = $timeout(draw, isScenarioPlayerTimerConfig.interval);
					}
				}
			}]
	});