angular
	.module("app")
	.value("isScenarioPlayerConfig", {
		observationalQuestion: {
			requireAllFeatures: false,
			tags: ["duringinterviewquestions"],
			requireAllTags: true
		},
		ratingValues: [0, 0.25, 0.5, 0.75, 1],
		roleSession: {
			candidateRoleId: "candidate"
		},
		featureActionableFeedback: {
			featureLookBackCount: 3
		}
	});