class ISScenarioPlayerCommandHandler extends SynchronisedScenarioCommandHandler {
	constructor(initParams) {
		super();

		const viewController = initParams.viewController;
		const viewModel = initParams.viewModel;

		const userSessionId = initParams.userSessionId;
		const matchSessionId = initParams.matchSessionId;
		const candidateRoleId = initParams.candidateRoleId;
		const newGame = initParams.newGame;
		const isSynchronised = initParams.isSynchronised;

		const observationalQuestionResultSignalling = initParams.observationalQuestionResultSignalling;
		const interviewerCandidateAppraisalSignalling = initParams.interviewerCandidateAppraisalSignalling;
		const interviewQuestionSignalling = initParams.interviewQuestionSignalling;
		const jobResultSignalling = initParams.jobResultSignalling;

		const videoController = initParams.videoController;
		const interviewQuestionController = initParams.interviewQuestionController;
		const observationalQuestionController = initParams.observationalQuestionController;
		const observationalQuestionFeedbackController = initParams.observationalQuestionFeedbackController;
		const featureActionableFeedbackController = initParams.featureActionableFeedbackController;
		const roleSessionController = initParams.roleSessionController;

		const sentimentAnalysis = initParams.sentimentAnalysis;

		this.register("ShowTitle", command => {
			viewController.showTitle();
		});

		this.register("HideTitle", command => {
			viewController.hideTitle();
		});

		this.register("SetRole", command => {
			viewController.setRole(command.roleId);
		});

		this.register("ShowBrief", command => {
			viewController.showBrief(command.title);
		});

		this.register("HideBrief", command => {
			viewController.hideBrief();
		});

		this.register("ShowWaiting", command => {
			viewController.showWaiting(command.title, command.body);
		});

		this.register("HideWaiting", command => {
			viewController.hideWaiting();
		});

		this.register("StartVideoCalling", command => {
			videoController.resumeStreaming();
			viewController.showSentimentAnalysis();
		});

		this.register("StopVideoCalling", command => {
			viewController.hideSentimentAnalysis();
			videoController.suspendStreaming();
		});

		this.register("SuspendVideoCallingAudio", command => {
			videoController.toggleAudio(false);
		});

		this.register("ResumeVideoCallingAudio", command => {
			videoController.toggleAudio(true);
		});

		this.register("ShowOtherLeft", command => {
			viewController.showOtherLeft(command.title, command.body, command.options, command.initialSelection);
		});

		this.register("HideOtherLeft", command => {
			viewController.hideOtherLeft();
		});

		this.register("ShowJobSelect", command => {
			viewController.showJobSelect(command.title, command.initialSelection);
		});

		this.register("HideJobSelect", command => {
			viewController.hideJobSelect();
		});

		this.register("ApplyJobResult", command => {
			var result = jobResultSignalling.takePendingResult();
			viewController.applyJobResult(result);
		});
		
		this.register("ShowInterviewQuestion", command => {
			const question = interviewQuestionController.takeAndSetActive(command.tags);

			viewModel.interviewQuestion.start = Date.now() / 1000;
			viewModel.interviewQuestion.end = viewModel.interviewQuestion.start + question.duration;

			viewController.showInterviewQuestion(question, command.state, question.duration, command.options, command.initialSelection);
		});

		this.register("UpdateInterviewQuestion", command => {
			const remaining = viewModel.interviewQuestion.end - (Date.now() / 1000);
			viewController.updateInterviewQuestion(command.state, remaining, command.options, command.initialSelection);
		});

		this.register("HideInterviewQuestion", command => {
			viewController.hideInterviewQuestion();
		});

		this.register("ShowInterviewQuestionRespond", command => {
			var question = interviewQuestionSignalling.takePendingResult();
			viewController.showInterviewQuestionRespond(question.question, question.state, question.current, question.total, question.length);
		});

		this.register("HideInterviewQuestionRespond", command => {
			viewController.hideInterviewQuestionRespond();
		});

		this.register("ShowSentimentAnalysis", command => {
			viewController.showSentimentAnalysis();
			sentimentAnalysis.startAnalyzingAndListening();
		});

		this.register("HideSentimentAnalysis", command => {
			sentimentAnalysis.stopAnalyzingAndListening();
			viewController.hideSentimentAnalysis();
		});

		this.register("GetFeatureActionableFeedback", command => {
			const analysisResults = viewModel.sentimentAnalysis.analysisMetadata.featureResults;
			featureActionableFeedbackController.getFeedbacks(analysisResults);
		});

		this.register("ShowFeatureActionableFeedback", command => {
			const feedback = featureActionableFeedbackController.takeAndSetActive();
			viewController.showFeatureActionableFeedback(feedback, command.initialSelection);
		});

		this.register("HideFeatureActionableFeedback", command => {
			viewController.hideFeatureActionableFeedback();
		});

		this.register("GetObservationalQuestion", command => {
			var features = viewModel.interviewQuestion.question.featureMappings
				.map(mapping => mapping.feature.id);

			observationalQuestionController.getQuestions(features);
		});

		this.register("ShowObservationalQuestion", command => {
			const question = observationalQuestionController.takeAndSetActive();
			viewController.showObservationalQuestion(question, command.initialSelection);
		});

		this.register("HideObservationalQuestion", command => {
			viewController.hideObservationalQuestion();
		});

		this.register("GetObservationalQuestionFeedback", command => {
			const result = observationalQuestionResultSignalling.takePendingResult();
			const featureId = result.question.featureMappings[0].featureId;
			observationalQuestionFeedbackController.getFeedbacks(result.question.id, featureId, result.rating);
		});

		this.register("ShowObservationalQuestionFeedback", command => {
			const active = observationalQuestionFeedbackController.takeAndSetActive();
			viewController.showObservationalQuestionFeedback(active.feedback, command.initialSelection);
		});

		this.register("HideObservationalQuestionFeedback", command => {
			viewController.hideObservationalQuestionFeedback();
		});

		this.register("ShowInterviewerCandidateAppraisal", command => {
			viewController.showInterviewerCandidateAppraisal(command.options, command.initialSelection, viewModel.interviewRound);
		});

		this.register("HideInterviewerCandidateAppraisal", command => {
			viewController.hideInterviewerCandidateAppraisal();
		});

		this.register("ShowPostInterviewSwapPrompt", command => {
			const candidateRoleSession = roleSessionController.getRoleSession(candidateRoleId);
			const candidateStart = candidateRoleSession.created;
			const candidateEnd = candidateRoleSession.ended;

			viewController.showPostInterviewSwapPrompt(command.options, command.initialSelection,
				viewModel.interviewRound, userSessionId, matchSessionId, candidateStart, candidateEnd);
		});

		this.register("HidePostInterviewSwapPrompt", command => {
			viewController.hidePostInterviewSwapPrompt();
		});

		this.register("ShowPostInterviewWaiting", command => {
			const candidateRoleSession = roleSessionController.getRoleSession(candidateRoleId);
			const candidateStart = candidateRoleSession.created;
			const candidateEnd = candidateRoleSession.ended;

			viewController.showPostInterviewWaiting(viewModel.interviewRound, userSessionId, matchSessionId, candidateStart, candidateEnd);
		});

		this.register("HidePostInterviewWaiting", command => {
			viewController.hidePostInterviewWaiting();
		});

		this.register("ShowCandidateInterviewSummary", command => {
			const candidateRoleSession = roleSessionController.getRoleSession(candidateRoleId);
			const job = candidateRoleSession.jobId;

			let interviewerObservationalRatings = null, interviewerMessage = null;
			if (isSynchronised) {
				const appraisalResult = interviewerCandidateAppraisalSignalling.takePendingResult();
				interviewerObservationalRatings = appraisalResult.questionRatings;
				interviewerMessage = appraisalResult.message;
			}

			const candidateStart = candidateRoleSession.created;
			const candidateEnd = candidateRoleSession.ended;

			viewController.showCandidateInterviewSummary(command.options, command.initialSelection,
				job, interviewerObservationalRatings, interviewerMessage, userSessionId, matchSessionId, candidateStart, candidateEnd);
		});

		this.register("HideCandidateInterviewSummary", command => {
			viewController.hideCandidateInterviewSummary();
		});

		this.register("NewGame", command => {
			newGame();
		});
	}
}