﻿// This is an abstract class
class ISScenarioPlayerComponentsHistoryController {
	constructor() {
		if (this.constructor === ISScenarioPlayerComponentsHistoryController) {
			throw "Cannot create an instance of " + ISScenarioPlayerComponentsHistoryController + ". It is an abstract class.";
		}

		// public variables
		this.takenEvent = new Observable();

		// private variables
		this._activeItemHistory = [];

		this._isDisposed = false;
	}

	// public methods
	dispose() {
		if (this._isDisposed) return;

		this.takenEvent.clear();
		this.takenEvent = null;

		this._isDisposed = true;
	}
	
	// private functions
	_selectItem(items) {
		const itemsSortedByHistory = this._itemsSortedByHistory(items);

		const item = itemsSortedByHistory[0];

		Array.insert(this._activeItemHistory, 0, item);

		this.takenEvent.publish(item);

		return item;
	}

	_itemsSortedByHistory(items) {
		const itemsSortedByHistory = items.map(i => {
			return {
				historyIndex: this._activeItemHistory.findIndex(aih => aih.id === i.id),
				item: i
			};
		});

		// Lower index means more recent in the history so should be placed at a higher	 index in the sorted by history array
		itemsSortedByHistory.sort((a, b) => {
			if (a.historyIndex < 0) {
				return -1;
			} else if (b.historyIndex < 0) {
				return 1;
			} else {
				return b.historyIndex - a.historyIndex;
			}
		});

		return itemsSortedByHistory.map(ish => ish.item);
	}
}

class ISScenarioPlayerComponentsInterviewQuestionController extends ISScenarioPlayerComponentsHistoryController {
	constructor(initParams) {
		super(initParams);

		// private variables
		this._cache = initParams.cache;
		this._requireAllFeatures = initParams.requireAllFeatures;
		this._tags = initParams.tags;
		this._requireAllTags = initParams.requireAllTags;
	}

	// public properties
	takeAndSetActive(tags) {
		const filtered = this._cache.listQuestions(tags);
		Array.shuffle(filtered);
		return this._selectItem(filtered);
	}
}


class ISScenarioPlayerComponentsActiveItemHistoryController extends ISScenarioPlayerComponentsHistoryController {
	constructor(initParams) {
		super(initParams);

		// private variables
		this._items = [];
		this._activeItem = [];
	}

	// public properties
	get hasItems() {
		return this._items.length > 0;
	}

	get activeItem() {
		return this._activeItem;
	}

	// public methods
	takeAndSetActive() {
		this._activeItem = this._selectItem(this._items);
		return this._activeItem;
	}
}

class ISScenarioPlayerComponentsObservationalQuestionController extends ISScenarioPlayerComponentsActiveItemHistoryController {
	constructor(initParams) {
		super(initParams);

		// private variables
		this._webClient = initParams.webClient;
		this._requireAllFeatures = initParams.requireAllFeatures;
		this._tags = initParams.tags;
		this._requireAllTags = initParams.requireAllTags;
	}

	// public functions
	getQuestions(features) {
		this._items = [];
		this._activeItem = null;

		this._webClient.listQuestions(features, this._tags, this._requireAllFeatures, this._requireAllTags, true)
			.then(questions => {
				this._items = questions;

				if (questions.length === 0) {
					Logger.warn("No Observational Questions for features: \"[" + features.join(", ") + "]\" were returned.");
				}
			});
	}
}

class ISScenarioPlayerComponentsObservationalQuestionFeedbackController extends ISScenarioPlayerComponentsActiveItemHistoryController {
	constructor(initParams) {
		super(initParams);

		// private variables
		this._webClient = initParams.webClient;
		this._weight = null;
		this._weightFromIdeal = null;
	}
	
	// public functions
	takeAndSetActive() {
		const activeItem = super.takeAndSetActive();
		return {
			feedback: activeItem,
			weight: this._weight,
			weightFromIdeal: this._weightFromIdeal
		};
	}

	getFeedbacks(questionId, featureId, weight) {
		this._items = [];
		this._activeItem = null;
		
		this._webClient.listFeedbacks(questionId, featureId, weight)
			.then(response => {
				this._items = response.feedbacks;
				this._weight = weight;
				this._weightFromIdeal = response.weightFromIdeal;

				if (response.feedbacks.length === 0) {
					Logger.warn("No Observational Question Feedbacks for QuestionId: \"" + questionId + "\"" +
						" with featureId: \"" + featureId + "\"" +
						" and weight: \"" + weight + "\" were returned.");
				}
			});
	}
}

class ISScenarioPlayerComponentsFeatureActionableFeedbackController extends ISScenarioPlayerComponentsActiveItemHistoryController {
	constructor(initParams) {
		super(initParams);
        
		// private variables
		this._webClient = initParams.webClient;
		this._featureLookBackCount = initParams.featureLookBackCount;
    }

	// public functions
	getFeedbacks(analysisResults) {
		this._items = [];
		this._activeItem = null;

		const resultForFeedback = this._selectFeatureResultForFeedback(analysisResults);

		if (!resultForFeedback) {
			Logger.warn("Couldn't select feature to get analysis for with current analysis results and feedbacks.");
			Logger.trace(analysisResults);
			return;
		}

		this._webClient.listFeedbacks(resultForFeedback.feature.id, resultForFeedback.weight)
            .then(feedbacks => {
                
                this._items = feedbacks;
                this._items.forEach(f => f.analysisResultId = resultForFeedback.id);

				if (feedbacks.length === 0) {
					Logger.warn("No Feature Actionable Feedbacks for feature: \"" + resultForFeedback.feature + "\" were returned.");
				}
			});
	}

	// private functions
	_selectFeatureResultForFeedback(analysisResults) {
		const sortedResults = SentimentAnalysisResultsUtil.sortResults(analysisResults);
		
		const recentFeaturesToExclude = this._activeItemHistory.slice(0,
			this._activeItemHistory.length > this._featureLookBackCount ? this._featureLookBackCount + 1 : this._activeItemHistory.length);

		const recentFeatureNamessToExclude = recentFeaturesToExclude.map(rfte => rfte.featureId);

		const filteredSortedResults = sortedResults.filter(
			result => !Array.contains(recentFeatureNamessToExclude, result.feature.id.toLowerCase()));

		return filteredSortedResults.length > 0 ? filteredSortedResults[0] : null;
	}
}