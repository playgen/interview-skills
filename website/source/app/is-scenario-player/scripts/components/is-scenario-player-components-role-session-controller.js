﻿class ISScenarioPlayerComponentsRoleSessionController {
	constructor(initParams) {
		// private variables
		this._webClient = initParams.webClient;
		this._userSessionId = initParams.userSessionId;
		this._matchSessionId = initParams.matchSessionId;
		this._jobId = null;
		this._roleId = null;
		this._previousRoleId = null;
		this._roleSessions = [];
	}

	// public properties
	get latestRoleSession() {
		return this._roleSessions.length > 0 ? 
			this._roleSessions[0] : 
			null;
	}

	// public methods
	getRoleSession(roleId) {
		return Array.first(this._roleSessions, rs => rs.roleId === roleId);
	}

	onJobSelection(selection) {
		this._jobId = selection.length > 0 ? selection[0].id : null;
		this._checkNewJobAndRole();
	}

	onRole(roleId) {
		this._roleId = roleId;
		this._checkNewJobAndRole();
	}

	onCandidateInterviewSummary() {
		this._webClient.end(this.latestRoleSession.id)
			.then(_ => _,
			error => Logger.error(error));
	}

	// private methods
	_checkNewJobAndRole() {
		if (this._roleId && this._jobId &&
			this._roleId !== this._previousRoleId) {

			this._previousRoleId = this._roleId;

			if (this.latestRoleSession) {
				this._webClient.end(this.latestRoleSession.id)
					.then(_ => this._newRoleSession(),
					error => Logger.error(error));
			} else {
				this._newRoleSession();
			}
		}
	}

	_newRoleSession() {
		this._webClient.add(this._userSessionId, this._matchSessionId, this._jobId, this._roleId)
			.then(roleSession => Array.insert(this._roleSessions, 0, roleSession),
			error => Logger.error(error));
	}
}