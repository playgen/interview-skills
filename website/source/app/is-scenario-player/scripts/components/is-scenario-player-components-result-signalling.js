class ISScenarioComponentsResultSignalling {
    constructor(initParams) {
        // private variables
        this._signalType = initParams.signalType;
        this._sendSignal = initParams.sendSignal;
        this._signalReceivedEvent = initParams.signalReceivedEvent;
        this._otherClientIds = initParams.otherClientIds;
        this._isDisposed = false;
		this._pendingResults = [];

        this._signalReceivedEvent.subscribe(this._onSignalReceived, this);
    }

    // public getters
    get hasPendingResults() {
        return this._pendingResults.length > 0;
    }

    // public methods
    dispose() {
        if(this._isDisposed) return;

        this._signalReceivedEvent.unsubscribe(this._onSignalReceived);
        this._pendingResults = [];

        this._isDisposed = true;
    }

	submit(result) {
		this._sendSignal(this._signalType, result, this._otherClientIds);
    }

    takePendingResult() {
        const result = this._pendingResults[0];
        this._pendingResults.splice(0, 1);
        return result;
    }

    // private methods
    _onSignalReceived(signal) {
        if(signal.type !== this._signalType) return;

        this._pendingResults.push(signal.payload);
    }
}