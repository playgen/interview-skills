class ISScenarioPlayerConditionEvaluator extends SynchronisedScenarioConditionEvaluator {
	constructor(initParams) {
		super();

		const viewModel = initParams.viewModel;
		const isMaster = initParams.isMaster;

		const jobResultSignalling = initParams.jobResultSignalling;
		const observationalQuestionResultSignalling = initParams.observationalQuestionResultSignalling;
		const interviewerCandidateAppraisalSignalling = initParams.interviewerCandidateAppraisalSignalling;
		const interviewQuestionSignalling = initParams.interviewQuestionSignalling;

		const featureActionableFeedbackController = initParams.featureActionableFeedbackController;
		const observationalQuestionController = initParams.observationalQuestionController;
		const observationalQuestionFeedbackController = initParams.observationalQuestionFeedbackController;
		
		this.register("IsMaster", function (condition, stepController, context) {
			return condition.value === isMaster;
		});

		this.register("IsConditionElapsedInterval", (condition, stepController, context) => {
			if (condition._startTime === undefined) {
				condition._startTime = Date.now();
				condition._repeatedCount = 0;
			}

			const durationMilliseconds = condition.interval * 1000;
			const isPassedInterval = Date.now() >= condition._startTime + durationMilliseconds;

			if (isPassedInterval && (condition._repeatedCount < condition.repeatCount || condition.repeatCount === -1)) {
				condition._repeatedCount++;
				condition._startTime += durationMilliseconds;
			}

			return isPassedInterval;
		});

		this.register("IsOtherLeftSelection", (condition, stepController, context) => {
			return Array.matches(viewModel.otherLeft.selection, condition.selection);
		});

		this.register("IsOtherBlocked", (condition, stepController, context) => {
			const otherClientStates = context.getClientStates();
			delete otherClientStates[context.clientId];

			return Object.areAnyValues(otherClientStates, ClientState.BLOCKED);
		});

		this.register("HasJobResult", (condition, stepController, context) => {
			return jobResultSignalling.hasPendingResults;
		});

		this.register("IsJobSelection", (condition, stepController, context) => {
			return Array.matches(viewModel.job.selection, condition.selection);
		});
		
		this.register("IsInterviewQuestionShown", (condition, stepController, context) => {
			return viewModel.interviewQuestion.isShown;
		});

		this.register("IsInterviewQuestionSelection", (condition, stepController, context) => {
			return Array.matches(viewModel.interviewQuestion.selection, condition.selection);
		});

		this.register("HasInterviewQuestionDurationElapsed", (condition, stepController, context) => {
			return Date.now() / 1000 > viewModel.interviewQuestion.end;
		});

		this.register("HasInterviewQuestionRespond", (condition, stepController, context) => {
			return interviewQuestionSignalling.hasPendingResults;
		});

		this.register("HasObservationalQuestion", (condition, stepController, context) => {
			return observationalQuestionController.hasItems;
		});

		this.register("IsObservationalQuestionSelection", (condition, stepController, context) => {
			return Array.matches(viewModel.observationalQuestion.selection, condition.selection);
		});

		this.register("HasObservationalQuestionResult", (condition, stepController, context) => {
			return observationalQuestionResultSignalling.hasPendingResults;
		});

		this.register("HasObservationalQuestionFeedback", (condition, stepController, context) => {
			return observationalQuestionFeedbackController.hasItems;
		});

		this.register("IsObservationalQuestionFeedbackSelection", (condition, stepController, context) => {
			return Array.matches(viewModel.observationalQuestionFeedback.selection, condition.selection);
		});

		this.register("HasNewSentimentAnalysisResult", (condition, stepController, context) => {
			if (condition._previousSentimentAnalysisMetadata !== viewModel.sentimentAnalysis.analysisMetadata &&
				viewModel.sentimentAnalysis.analysisMetadata !== null &&
				viewModel.sentimentAnalysis.analysisMetadata.featureResults &&
				Object.keys(viewModel.sentimentAnalysis.analysisMetadata.featureResults).length > 0) {

				condition._previousSentimentAnalysisMetadata = viewModel.sentimentAnalysis.analysisMetadata;
				return true;

			} else {
				return false;
			}
		});

		this.register("HasFeatureActionableFeedback", (condition, stepController, context) => {
			return featureActionableFeedbackController.hasItems;
		});

		this.register("IsFeatureActionableFeedbackSelection", (condition, stepController, context) => {
			return Array.matches(viewModel.featureActionableFeedback.selection, condition.selection);
		});

		this.register("IsInterviewerCandidateAppraisalSelection", (condition, stepController, context) => {
			return Array.matches(viewModel.interviewerCandidateAppraisal.selection, condition.selection);
		});

		this.register("IsPostInterviewSwapPromptSelection", (condition, stepController, context) => {
			return Array.matches(viewModel.postInterviewSwapPrompt.selection, condition.selection);
		});

		this.register("HasInterviewerCandidateAppraisal", (condition, stepController, context) => {
			return interviewerCandidateAppraisalSignalling.hasPendingResults;
		});

		this.register("IsCandidateInterviewSummarySelection", (condition, stepController, context) => {
			return Array.matches(viewModel.candidateInterviewSummary.selection, condition.selection);
		});
	}
}