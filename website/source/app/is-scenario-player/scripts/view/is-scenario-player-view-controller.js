class ISScenarioPlayerViewController {
	constructor() {
		this.showTitle = null;
		this.hideTitle = null;

		this.setRole = null;

		this.showBrief = null;
		this.hideBrief = null;

		this.showWaiting = null;
		this.hideWaiting = null;

		this.showSentimentAnalysis = null;
		this.hideSentimentAnalysis = null;
		
		this.showInterviewQuestion = null;
		this.updateInterviewQuestion = null;
		this.hideInterviewQuestion = null;

		this.showInterviewQuestionRespond = null;
		this.hideInterviewQuestionRespond = null;

		this.showObservationalQuestion = null;
		this.hideObservationalQuestion = null;

		this.showFeatureActionableFeedback = null;
		this.hideFeatureActionableFeedback = null;

		this.showObservationalQuestionFeedback = null;
		this.hideObservationalQuestionFeedback = null;

		this.showInterviewerCandidateAppraisal = null;
		this.hideInterviewerCandidateAppraisal = null;

		this.showPostInterviewSwapPrompt = null;
		this.hidePostInterviewSwapPrompt = null;

		this.showPostInterviewWaiting = null;
		this.hidePostInterviewWaiting = null;

		this.showCandidateInterviewSummary = null;
		this.hideCandidateInterviewSummary = null;

		this.showOtherLeft = null;
		this.hideOtherLeft = null;

		this.showJobSelect = null;
		this.hideJobSelect = null;
        this.applyJobResult = null;

        this.events = {
            featureActionableFeedbackShown: new Observable(),
			featureActionableFeedbackSubmitted: new Observable(),
			interviewQuestionUpdated: new Observable(),
			interviewQuestionHidden: new Observable(),
			observationalQuestionShown: new Observable(),
			observationalQuestionSubmitted: new Observable()
        };

        // private variables
        this._isDisposed = false;
    }

    // public methods
    dispose() {
        if (this._isDisposed) return;

        Object.keys(this.events).forEach(key => {
            this.events[key].clear();
            delete this.events[key];
        });

        this.events = null;

        this._isDisposed = true;
    }
}