class ISScenarioPlayerViewModel {
	constructor() {
		// public variables
		this.title = {
			isShown: false,
			title: null
		};

		this.briefing = {
			isShown: false,
			title: null,
			body: null
		};

		this.sentimentAnalysis = {
			isShown: false,
			analysisMetadata: null
		};
		
		this.job = {
			isShown: false,
			instruction: null,
			selectionEvent: new Observable(),
			set selection(selection) {
				this._selection = selection;
				this.selectionEvent.publish(selection);
			},
			get selection() {
				return this._selection;
			},
			_selection: []
		};

		this.interviewQuestion = {
			isShown: false,
			selection: null,
			state: null,
			question: null,
			length: null,
			options: null,
			start: 0,
			end: 0
		};


		this.otherLeft = { selection: null };


		this.role = {};
		
		this.interviewRound = 0;

		this.observationalQuestion = { selection: null };

		this.observationalQuestionFeedback = { selection: null };

		this.featureActionableFeedback = { selection: null };

		this.interviewerCandidateAppraisal = { selection: null };

		this.postInterviewSwapPrompt = { selection: null };

		this.swapWaiting = {};

		this.candidateInterviewSummary = { selection: null };

		// todo move these to model leval like with job
		this.events = {
			role: new Observable(),
			candiateInterviewSummary: new Observable()
		};

		// private variables
		this._isDisposed = false;
	}

	// public methods
	dispose() {
		if (this._isDisposed) return;

		Object.keys(this.events).forEach(key => {
			this.events[key].clear();
			delete this.events[key];
        });

		this.events = null;

		this._isDisposed = true;
	}
}