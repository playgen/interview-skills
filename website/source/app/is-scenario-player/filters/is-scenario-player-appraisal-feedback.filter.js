angular.module("app")
	.filter("isScenarioPlayerObservationalQuestionFeedback", function() {
		return function(appraisal, key) {
			const rating = Array.first(appraisal.ratings, r => r.key === key);
			return rating.feedback;
		};
	});