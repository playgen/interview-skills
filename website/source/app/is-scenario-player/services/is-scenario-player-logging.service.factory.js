﻿angular
	.module("app")
	.factory("ISScenarioPlayerLogging", [
		"FeatureActionableFeedbackSystemResultWebClient",
		"InterviewQuestionResultWebClient",
		"ObservationalQuestionResultWebClient",
		function (
			FeatureActionableFeedbackSystemResultWebClient,
			InterviewQuestionResultWebClient,
			ObservationalQuestionResultWebClient) {
			const service = {};

			// private variables
			let scenarioPlayer = null;
			let matchSessionId = null;
			let userSessionId = null;

			const observationalQuestionResultReceived = {};
			const observationalQuestionResultReceivedPromises = {};
			const featureActionableFeedbackSystemResultReceivedPromises = {};

			let isInitialised = false;

			// public methods
			service.initialize = function (initialisedScenarioPlayerService, inMatchSessionId, inUserSessionId) {
				if (isInitialised) {
					Logger.warn("ISScenarioPlayerLogging is already initialised. Terminating previous.");
					service.terminate();
				}

				scenarioPlayer = initialisedScenarioPlayerService;
				matchSessionId = inMatchSessionId;
				userSessionId = inUserSessionId;

				// Interview Question
				scenarioPlayer.view.controller.events.interviewQuestionUpdated.subscribe(onInterviewQuestionUpdated);
				scenarioPlayer.view.controller.events.interviewQuestionHidden.subscribe(onInterviewQuestionHidden);

				// Observational Question
				if (scenarioPlayer.isSynchronised) {
					scenarioPlayer.components.observationalQuestionController.takenEvent
						.subscribe(onObservationalQuestionReceived);
					scenarioPlayer.view.controller.events.observationalQuestionShown
						.subscribe(onObservationalQuestionShown);
					scenarioPlayer.view.controller.events.observationalQuestionSubmitted
						.subscribe(onObservationalQuestionSubmitted);
				}

				// Feature Actionable Feedback System Results
				scenarioPlayer.components.featureActionableFeedbackController.takenEvent
					.subscribe(onFeatureActionableFeedbackSystemResultReceived);
				scenarioPlayer.view.controller.events.featureActionableFeedbackShown
					.subscribe(onFeatureActionableFeedbackSystemResultShown);
				scenarioPlayer.view.controller.events.featureActionableFeedbackSubmitted
					.subscribe(onFeatureActionableFeedbackSystemResultAcknowledged);

				isInitialised = true;
			};

			service.terminate = function () {
				if (!isInitialised) {
					Logger.warn("ISScenarioPlayerLogging is not initialised.");
					return;
				}

				// Interview Question
				scenarioPlayer.view.controller.events.interviewQuestionUpdated.unsubscribe(onInterviewQuestionUpdated);
				scenarioPlayer.view.controller.events.interviewQuestionHidden.unsubscribe(onInterviewQuestionHidden);

				// Observational Question
				if (scenarioPlayer.isSynchronised) {
					scenarioPlayer.components.observationalQuestionController.takenEvent
						.unsubscribe(onObservationalQuestionReceived);
					scenarioPlayer.view.controller.events.observationalQuestionShown
						.unsubscribe(onObservationalQuestionShown);
					scenarioPlayer.view.controller.events.observationalQuestionSubmitted
						.unsubscribe(onObservationalQuestionSubmitted);
				}

				// Feature Actionable Feedback System Results
				scenarioPlayer.components.featureActionableFeedbackController.takenEvent
					.unsubscribe(onFeatureActionableFeedbackSystemResultReceived);
				scenarioPlayer.view.controller.events.featureActionableFeedbackShown
					.unsubscribe(onFeatureActionableFeedbackSystemResultShown);
				scenarioPlayer.view.controller.events.featureActionableFeedbackSubmitted
					.unsubscribe(onFeatureActionableFeedbackSystemResultAcknowledged);

				isInitialised = false;
			};

			// private methods
			// Interview Question
			function onInterviewQuestionUpdated(updatedParams) {
				InterviewQuestionResultWebClient.postUpdated(
					updatedParams.question.id,
					matchSessionId,
					updatedParams.state,
					Date.now())
					.catch(error => Logger.error(error));
			}

			function onInterviewQuestionHidden(question) {
				InterviewQuestionResultWebClient.postHidden(
					question.id,
					matchSessionId,
					Date.now())
					.catch(error => Logger.error(error));
			}

			// Feature Actionable Feedback System Results
			function onObservationalQuestionReceived(question) {
				observationalQuestionResultReceived[question.id] = Date.now();

				observationalQuestionResultReceivedPromises[question.id] = ObservationalQuestionResultWebClient.postReceived(
					question.id,
					matchSessionId,
					userSessionId,
					observationalQuestionResultReceived[question.id])
					.catch(errorRespose => Logger.warn(errorRespose));
			}

			function onObservationalQuestionShown(question) {
				observationalQuestionResultReceivedPromises[question.id].then(success => ObservationalQuestionResultWebClient.postShown(
					question.id,
					matchSessionId,
					userSessionId,
					observationalQuestionResultReceived[question.id],
					Date.now())
					.catch(errorRespose => Logger.warn(errorRespose)));
			}

			function onObservationalQuestionSubmitted(submittedParams) {
				observationalQuestionResultReceivedPromises[submittedParams.question.id].then(success => ObservationalQuestionResultWebClient.postSubmitted(
					submittedParams.question.id,
					matchSessionId,
					userSessionId,
					observationalQuestionResultReceived[submittedParams.question.id],
					submittedParams.rating,
					Date.now())
					.catch(errorRespose => Logger.warn(errorRespose)));
			}

			// Feature Actionable Feedback System Results
			function onFeatureActionableFeedbackSystemResultReceived(feedback) {
				featureActionableFeedbackSystemResultReceivedPromises[feedback.id] = FeatureActionableFeedbackSystemResultWebClient.postReceived(
					feedback.id,
					feedback.analysisResultId,
					Date.now())
					.catch(errorRespose => Logger.warn(errorRespose));
			}

			function onFeatureActionableFeedbackSystemResultShown(feedback) {
				featureActionableFeedbackSystemResultReceivedPromises[feedback.id].then(success => FeatureActionableFeedbackSystemResultWebClient.postShown(
					feedback.id,
					feedback.analysisResultId,
					Date.now())
					.catch(errorRespose => Logger.warn(errorRespose)));
			}

			function onFeatureActionableFeedbackSystemResultAcknowledged(feedback) {
				featureActionableFeedbackSystemResultReceivedPromises[feedback.id].then(success => FeatureActionableFeedbackSystemResultWebClient.postAcknowledged(
					feedback.id,
					feedback.analysisResultId,
					Date.now())
					.catch(errorRespose => Logger.warn(errorRespose)));
			}

			return service;
		}]);