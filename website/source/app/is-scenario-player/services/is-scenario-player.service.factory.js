angular
	.module("app")
	.factory("ISScenarioPlayer", [
		"scenarioPlayerConfig",
		"scenarioPlayerSynchronisedConfig",
		"$timeout",
		"InterviewQuestionCache",
		"ObservationalQuestionWebClient",
		"ObservationalQuestionFeedbackWebClient",
		"FeatureActionableFeedbackWebClient",
		"isScenarioPlayerConfig",
		"SentimentAnalysis",
		"RoleSessionWebClient",
		function (scenarioPlayerConfig,
			scenarioPlayerSynchronisedConfig,
			$timeout,
			InterviewQuestionCache,
			ObservationalQuestionWebClient,
			ObservationalQuestionFeedbackWebClient,
			FeatureActionableFeedbackWebClient,
			isScenarioPlayerConfig,
			SentimentAnalysis,
			RoleSessionWebClient) {
			const service = {};

			// public variables
			service.components = null;
			service.view = null;
			service.player = null;
			service.initialisedEvent = new Observable();
			service.terminatedEvent = new Observable();

			// private variables
			let isInitialised = false;

			// public methods
			service.initialize = function (
				scenario,
				clientId,
				matchId,
				newGame,
				videoController) {

				if (scenario.isSynchronised) {
					Logger.error("Scenario is Synchronised. Use the Synchronised scenario initialize.");
				}

				initialize(
					scenario,
					clientId,
					matchId,
					newGame,
					videoController);
			};

			service.initializeSynchronised = function (
				scenario,
				clientId,
				matchId,
				newGame,
				videoController,
				sendSignal,
				signalReceivedEvent,
				masterClientId,
				otherClientIds) {

				if (!scenario.isSynchronised) {
					Logger.error("Scenario is not Synchronised. Use the non synchronised scenario initialize.");
				}

				const components = {
					jobResultSignalling: new ISScenarioComponentsResultSignalling({
						signalType: "JobResult",
						sendSignal: sendSignal,
						signalReceivedEvent: signalReceivedEvent,
						otherClientIds: otherClientIds
					}),
					observationalQuestionResultSignalling: new ISScenarioComponentsResultSignalling({
						signalType: "ObservationalQuestionFeedback",
						sendSignal: sendSignal,
						signalReceivedEvent: signalReceivedEvent,
						otherClientIds: otherClientIds
					}),
					interviewerCandidateAppraisalSignalling: new ISScenarioComponentsResultSignalling({
						signalType: "InterviewerCandidateAppraisalSignalling",
						sendSignal: sendSignal,
						signalReceivedEvent: signalReceivedEvent,
						otherClientIds: otherClientIds
					}),
					interviewQuestionSignalling: new ISScenarioComponentsResultSignalling({
						signalType: "InterviewQuestion",
						sendSignal: sendSignal,
						signalReceivedEvent: signalReceivedEvent,
						otherClientIds: otherClientIds
					}),
					observationalQuestionController: new ISScenarioPlayerComponentsObservationalQuestionController({
						webClient: ObservationalQuestionWebClient,
						requireAllFeatures: isScenarioPlayerConfig.observationalQuestion.requireAllFeatures,
						tags: isScenarioPlayerConfig.observationalQuestion.tags,
						requireAllTags: isScenarioPlayerConfig.observationalQuestion.requireAllFeatures
					}),
					observationalQuestionFeedbackController: new ISScenarioPlayerComponentsObservationalQuestionFeedbackController({
						webClient: ObservationalQuestionFeedbackWebClient
					})
				};

				const commandHandlerParams = {
					interviewerCandidateAppraisalSignalling: components.interviewerCandidateAppraisalSignalling,
					observationalQuestionResultSignalling: components.observationalQuestionResultSignalling,
					jobResultSignalling: components.jobResultSignalling,
					interviewQuestionSignalling: components.interviewQuestionSignalling,
					observationalQuestionController: components.observationalQuestionController,
					observationalQuestionFeedbackController: components.observationalQuestionFeedbackController
				};

				const isMaster = clientId === masterClientId;

				const conditionEvaluatorParams = {
					isMaster: isMaster,
					jobResultSignalling: components.jobResultSignalling,
					interviewQuestionSignalling: components.interviewQuestionSignalling,
					observationalQuestionController: components.observationalQuestionController,
					observationalQuestionResultSignalling: components.observationalQuestionResultSignalling,
					observationalQuestionFeedbackController: components.observationalQuestionFeedbackController,
					interviewerCandidateAppraisalSignalling: components.interviewerCandidateAppraisalSignalling
				};

				const playerParams = Object.assign(
					{},
					scenarioPlayerSynchronisedConfig,
					{
						sendSignal: sendSignal,
						signalReceivedEvent: signalReceivedEvent,
						masterClientId: masterClientId,
						clientId: clientId,
						conditionContext: {
							clientId: clientId
						}
					});

				initialize(
					scenario,
					clientId,
					matchId,
					newGame,
					videoController,
					components,
					commandHandlerParams,
					conditionEvaluatorParams,
					playerParams);
			};

			function initialize(
				scenario,
				clientId,
				matchId,
				newGame,
				videoController,
				components,
				commandHandlerParams,
				conditionEvaluatorParams,
				playerParams) {

				if (isInitialised) {
					Logger.warn("ISScenarioPlayer is already initialised. Terminating previous.");
					service.terminate();
				}

				service.view = {
					controller: new ISScenarioPlayerViewController(),
					model: new ISScenarioPlayerViewModel()
				};

				service.components = Object.assign({},
					components,
					{
						interviewQuestionController: new ISScenarioPlayerComponentsInterviewQuestionController({
							cache: InterviewQuestionCache
						}),
						featureActionableFeedbackController: new ISScenarioPlayerComponentsFeatureActionableFeedbackController({
							webClient: FeatureActionableFeedbackWebClient,
							featureLookBackCount: isScenarioPlayerConfig.featureActionableFeedback.featureLookBackCount
						}),
						roleSessionController: new ISScenarioPlayerComponentsRoleSessionController({
							webClient: RoleSessionWebClient,
							userSessionId: clientId,
							matchSessionId: matchId
						})
					});

				const commandHandler = new ISScenarioPlayerCommandHandler(
					Object.assign({},
						commandHandlerParams,
						{
							isSynchronised: scenario.isSynchronised,
							viewController: service.view.controller,
							viewModel: service.view.model,
							userSessionId: clientId,
							matchSessionId: matchId,
							newGame: newGame,
							candidateRoleId: isScenarioPlayerConfig.roleSession.candidateRoleId,
							videoController: videoController,
							interviewQuestionController: service.components.interviewQuestionController,
							featureActionableFeedbackController: service.components.featureActionableFeedbackController,
							roleSessionController: service.components.roleSessionController,
							sentimentAnalysis: SentimentAnalysis
						})
				);

				const conditionEvaluator = new ISScenarioPlayerConditionEvaluator(
					Object.assign(
						{},
						conditionEvaluatorParams,
						{
							viewModel: service.view.model,
							featureActionableFeedbackController: service.components.featureActionableFeedbackController
						})
				);

				playerParams = Object.assign(
					{},
					playerParams,
					scenarioPlayerConfig,
					{
						timeout: $timeout,
						steps: scenario.steps,
						conditionEvaluator: conditionEvaluator,
						commandHandler: commandHandler
					});

				// Event binding
				service.view.model.job.selectionEvent.subscribe(service.components.roleSessionController.onJobSelection.bind(service.components.roleSessionController));
				// todo events on the model
				service.view.model.events.role.subscribe(service.components.roleSessionController.onRole.bind(service.components.roleSessionController));
				service.view.model.events.candiateInterviewSummary.subscribe(service.components.roleSessionController.onCandidateInterviewSummary.bind(service.components.roleSessionController));

				service.player = scenario.isSynchronised ? new SynchronisedScenarioPlayer(playerParams) : new ScenarioPlayer(playerParams);
				service.player.isSynchronised = scenario.isSynchronised;

				isInitialised = true;

				service.initialisedEvent.publish();
			}

			service.getIsInitialised = function() {
				return isInitialised;
			};

			service.terminate = function () {
				if (!isInitialised) {
					Logger.warn("ISScenarioPlayer is not initialised.");
					return;
				}

				service.player.dispose();

				// todo if isSynchronised
				if (service.components.observationalQuestionResultSignalling) {
					service.components.observationalQuestionResultSignalling.dispose();
				}

				if (service.components.interviewerCandidateAppraisalSignalling) {
					service.components.interviewerCandidateAppraisalSignalling.dispose();
				}

				if (service.components.jobResultSignalling) {
					service.components.jobResultSignalling.dispose();
				}

				service.view.controller.dispose();
				service.view.model.dispose();

				service.view = null;
				service.components = null;
				service.player = null;

				isInitialised = false;

				service.terminatedEvent.publish();
			};

			return service;
		}]);