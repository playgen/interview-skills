angular
    .module("app")
    .value("matchDataConfig", {
		keys: {
			session: "PlayGen.InterviewSkills.Core.Match.Session.MatchSessionData",
			scenario: "PlayGen.InterviewSkills.Core.Scenario.MatchScenarioData",
			openTokMatch: "PlayGen.InterviewSkills.Data.Contracts.Match.CustomData.OpenTok.MatchOpenTokDataResponse",
			openTokUser: "PlayGen.InterviewSkills.Core.OpenTok.MatchedParticipantOpenTokData"
        }
    });