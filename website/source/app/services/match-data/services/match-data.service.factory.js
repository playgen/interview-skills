﻿angular
	.module("app")
	.factory("MatchData", [
			"matchDataConfig",
			function(matchDataConfig) {
				const service = {};

				// public methods
				service.getScenario = function(match) {
					const scenario = match.customData[matchDataConfig.keys.scenario].scenario;
					const upperCamelCaseSteps = JSON.parse(scenario.steps);
					scenario.steps = Object.allKeysToLowerCamel(upperCamelCaseSteps);

					return scenario;
				};

				service.getSession = function (match) {
					return match.customData[matchDataConfig.keys.session];
				};

				service.getOpenTokMatch = function (match) {
					return match.customData[matchDataConfig.keys.openTokMatch];
				};

				service.getOpenTokUser = function (match) {
					return match.thisParticipant.customData[matchDataConfig.keys.openTokUser];
				};
				
				return service;
			}
		]);