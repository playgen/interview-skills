﻿angular
	.module("app")
	.value("userDataConfig", {
		keys: {
			user: "PlayGen.InterviewSkills.Data.Model.User",
			session: "PlayGen.InterviewSkills.Core.User.UserSessionData"
		}
	});