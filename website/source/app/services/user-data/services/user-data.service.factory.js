﻿angular
	.module("app")
	.factory("UserData", [
		"userDataConfig",
		function (userDataConfig) {
			const service = {};

			// public methods
			service.getSession = function (user) {
				return user.customData[userDataConfig.keys.session];
			};

			return service;
		}
	]);