﻿angular
	.module("app")
	.factory("MessageDialog", [
		function () {
			const service = {};

			// public methods
			// set by controller
			service.show = null;
			service.getIsVisible = null;
			service.hide = null;

			return service;
		}]);