﻿angular
	.module("app")
	.component("messageDialog",{
		templateUrl: "/message-dialog/message-dialog.html",
		controller: [
			"MessageDialog",
			"$q",
			"$timeout",
			function(MessageDialog, $q, $timeout) {
				const ctrl = this;

				// public variables
				ctrl.title = null;
				ctrl.body = null;
				ctrl.buttons = [];
				ctrl.timeRemaining = null;
				ctrl.isVisible = false;

				// private variables
				let buttonClickedPromise = null;
				let showTimeout = null;
				let counterTimeout = null;

				// hookup service
				MessageDialog.show = function (title, body, timeout = null, buttons = ["OK"]) {
					buttonClickedPromise = $q.defer();

					ctrl.title = title;
					ctrl.body = body;
					ctrl.buttons = buttons;
					ctrl.timeRemaining = Math.round(timeout / 1000);
					ctrl.isVisible = true;

					if (timeout) {
						counterTimeout = $timeout(updateTimeout, 1000);
						showTimeout = $timeout(ctrl.hide, timeout);
					}

					return buttonClickedPromise.promise;
				};

				MessageDialog.getIsVisible = function() {
					return ctrl.isVisible;
				};

				MessageDialog.hide = function() {
					hide();
					buttonClickedPromise.reject();
				};
				
				// public methods
				ctrl.hide = function (button) {
					hide();
					buttonClickedPromise.resolve(button);
				};

				// private methods
				function hide() {
					$timeout.cancel(counterTimeout);
					$timeout.cancel(showTimeout);
					ctrl.isVisible = false;
				}

				function updateTimeout() {
					if (ctrl.timeRemaining <= 1) {
						ctrl.timeRemaining = 0;
					} else {
						ctrl.timeRemaining -= 1;
						counterTimeout = $timeout(updateTimeout, 1000);
					}
				}
			}]
		});