class Logger {
	static error(message) {
		console.error(message);
	}

	static warn(message) {
		console.warn(message);
	}

	static info(message) {
		console.info(message);
	}

	static debug(message) {
		console.info(message);
	}

	static trace(message) {
		console.trace(message);
	}
}