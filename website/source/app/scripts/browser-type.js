﻿class BrowserType {
	static get isChrome() {
		return /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	}

	static get chromeVersion () {     
		var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);	
		return raw ? parseInt(raw[2], 10) : false;
	}
}