// Usage: String.format("This is a {0} function to {1} a {3}.", "helper", "format", "string");
if(String.format) {
	throw "String.format is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	String.format = function(format) {
		var args = Array.prototype.slice.call(arguments, 1);

		return format.replace(/{(\d+)}/g, function(match, number) { 
				return typeof args[number] !== "undefined" ? args[number] : match;
			});
	};
}

if(String.uriFormat) {
    throw "String.uriFormat is already defined somewhere else. This may lead to unexpected behaviour";
} else {
    String.uriFormat = function() {
        const formatted = String.format.apply(this, arguments);
        const uriEncoded = encodeURI(formatted);
        return uriEncoded;
    };
}

if (String.title) {
	throw "String.title is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	String.title = function (untitled) {
		const titled = untitled.split(" ")
			.map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
			.join(" ");
		return titled;
	};
}

if (String.toLowerCamel) {
	throw "String.toLowerCamel is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	String.toLowerCamel = function (input) {
		var lowerCamelCase = input;

		if (input.length > 0) {
			lowerCamelCase = input[0].toLowerCase();

			if (input.length > 1) {
				lowerCamelCase += input.substring(1);
			}
		}

		return lowerCamelCase;
	};
}