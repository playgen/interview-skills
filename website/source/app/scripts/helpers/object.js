﻿if (Object.allKeysToLowerCamel) {
	throw "Object.allKeysToLowerCamel is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Object.allKeysToLowerCamel = function (input) {
		let output = input;

		if (input instanceof Array) {
			output = [];

			input.forEach(item => {
				output.push(Object.allKeysToLowerCamel(item));
			});

		} else if (input instanceof Object) {
			output = {};

			for (let key in input) {
				output[String.toLowerCamel(key)] = Object.allKeysToLowerCamel(input[key]);
			}
		}

		return output;
	};
}

if (Object.areAllValues) {
	throw "Object.areAllValues is already defined somewhere else. This may lead to unexpected behaviour";

} else {
	Object.areAllValues = function (input, match) {
		
		for (let key in input) {
			if (input[key] !== match) {
				return false;
			}
		}

		return true;
	};
}

if (Object.areAnyValues) {
	throw "Object.areAnyValues is already defined somewhere else. This may lead to unexpected behaviour";

} else {
	Object.areAnyValues = function (input, match) {

		for (let key in input) {
			if (input[key] === match) {
				return true;
			}
		}

		return false;
	};
}