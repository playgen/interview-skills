if(Array.tryRemove) {
	throw "Array.tryRemove is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.tryRemove = function (array, item) {
		var succeeded = false;

		try {
			Array.remove(array, item);
			succeeded = true;
		} catch(error) {
			console.log(error);
		}

		return succeeded;
	};
}

if(Array.remove) {
	throw "Array.remove is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.remove = function (array, item) {
		Array.removeWhere(array, i => i === item);
	};
}

if(Array.removeWhere) {
	throw "Array.removeWhere is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.removeWhere = function(array, predicate) {
		if(array.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		let index;

		do {
			index = array.findIndex(predicate);

			if (0 <= index && index < array.length) {
				array.splice(index, 1);
			}

		} while (index > -1);
	};
}

if(Array.contains) {
	throw "Array.contains is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.contains = function (array, item) {
		return Array.containsWhere(array, i => i === item);
	};
}

if(Array.containsWhere) {
	throw "Array.containsWhere is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.containsWhere = function (array, predicate) {
		if(array.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		const index = array.findIndex(predicate);

		if(0 <= index && index < array.length) {
			return true;
		} else {
			return false;
		}
	};
}

if(Array.first) {
	throw "Array.select is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.first = function (array, predicate) {
		if(array.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		const index = array.findIndex(predicate);

		if(0 <= index && index < array.length) {
			return array[index];
		} else {
			throw "Match not found in array. \nArray: " + array + " \nPredicate: " + predicate;
		}
	};
}

if(Array.firstOrNull) {
	throw "Array.firstOrNull is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.firstOrNull = function (array, predicate) {
		if(array.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		const index = array.findIndex(predicate);

		if(0 <= index && index < array.length) {
			return array[index];
		} else {
			return null;
		}
	};
}

if(Array.insert) {
	throw "Array.insert is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.insert = function (array, index, item) {
		if(array.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		array.splice(index, 0, item);
	};
}

if(Array.move) {
	throw "Array.move is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.move = function (array, item, index) {
		if(array.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		Array.removeWhere(array, i => i === item);
		Array.insert(array, index, item);
	};
}

if(Array.tryMove) {
	throw "Array.tryMove is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.tryMove = function (array, item, index) {
		var succeeded = false;

		try {
			Array.move(array, item, index);
			succeeded = true;
		} catch(error) {
			console.log(error);
		}

		return succeeded;
	};
}

if(Array.select) {
	throw "Array.select is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.select = function (array, predicate) {
		if(array.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		return array.map(predicate);
	};
}

if(Array.where) {
	throw "Array.where is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.where = function (array, predicate) {
		if(array.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		return array.filter(predicate);
	};
}

if(Array.matches) {
	throw "Array.matches is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.matches = function (arrayA, arrayB) {
		if(arrayA.constructor !== Array || arrayB.constructor !== Array) {
			throw "Object reference does not have an array constructor.";
		}

		let areMatching = arrayA.length === arrayB.length;

		if(areMatching) {
			const different = arrayA
				.filter(i => !arrayB.includes(i));

			areMatching = different.length === 0;
		}

		return areMatching;
	};
}

if(Array.takeAt) {
	throw "Array.takeAt is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.takeAt = function (array, index) {
		const item = array[index];
		array.splice(index, 1);
		return item;
	};
}

if (Array.takeRandom) {
	throw "Array.takeRandom is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.takeRandom = function (array) {
		const randomIndex = Math.floor(Math.random() * array.length);
		return Array.takeAt(array, randomIndex);
	};
}

if (Array.shuffle) {
	throw "Array.shuffle is already defined somewhere else. This may lead to unexpected behaviour";
} else {
	Array.shuffle = function (array) {
		for (let i = array.length - 1; i > 0; i--) {
			const j = Math.floor(Math.random() * (i + 1));
			[array[i], array[j]] = [array[j], array[i]];
		}

		return array;
	};
}