﻿class Observable {
	constructor() {
		// private variables
		this._observers = [];
	}

	// public methods
	subscribe(callback, scope) {
		this._observers.push({
			callback: callback,
			scope: scope
		});
	}

	unsubscribe(callback) {
		this._observers = this._observers.filter(item => {
			if (item.callback !== callback) {
				return item;
			}
		});
	}

	publish(object) {
		this._observers.forEach(item => {
			var scope = item.scope || window;
			item.callback.call(scope, object);
		});
	}

	clear() {
		this._observers = [];
	}
}
