angular
	.module("interviewQuestion")
	.value("interviewQuestionWebClientConfig", {
		URIs: {
			// GET api/interviewQuestion/list?tags=interviewquestion1
			listQuestions: "../api/interviewquestion/list"
		}
	});