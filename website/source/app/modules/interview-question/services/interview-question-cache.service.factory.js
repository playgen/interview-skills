angular
	.module("interviewQuestion")
	.factory("InterviewQuestionCache", [
		"InterviewQuestionWebClient",
		"$q",
		function (InterviewQuestionWebClient, $q) {
			const service = {};

			// private variables
			let buildCachePromise = null;
			let isBuilt = false;
			let questions = [];

			// public methods
			service.buildCache = function () {
				buildCachePromise = $q.defer();

				isBuilt = false;
				questions = [];

				InterviewQuestionWebClient.listQuestions()
					.then(newQuestions => {
						questions = newQuestions;
						buildCachePromise.resolve(newQuestions);
						isBuilt = true;
					}, error => buildCachePromise.reject(error));

				return buildCachePromise.promise;
			};

			service.getIsBuilt = function() {
				return isBuilt;
			};

			service.listQuestions = function (tags, requireAllTags) {
				isBuiltCheck();

				const filtered = questions.slice().filter(question => {
					if (!tags) {
						return true;
					}

					if (!requireAllTags) {
						return tags.some(tag => question.tagMappings.find(tm => tm.tagId === tag));

					} else {
						return tags.every(tag => question.tagMappings.find(tm => tm.tagId === tag));
					}
				});

				return filtered;
			};

			// private methods
			function isBuiltCheck() {
				if (!isBuilt) {
					throw "Cache isn't initialised. " +
					"\nYou need to call buildCache and wait for it to complete before attempting to access the cached items.";
				}
			}

			return service;
		}]);