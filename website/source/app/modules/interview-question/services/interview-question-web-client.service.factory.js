angular
	.module("interviewQuestion")
	.factory("InterviewQuestionWebClient", ["$http", "$q", "interviewQuestionWebClientConfig", function ($http, $q, interviewQuestionWebClientConfig) {
		const service = {};

		// public methods
		service.listQuestions = function () {
			const listQuestionsPromise = $q.defer();
			
			const query = "?includes=FeatureMappings&includes=TagMappings";

			$http.get(String.uriFormat(interviewQuestionWebClientConfig.URIs.listQuestions + query))
				.then(successResponse => listQuestionsPromise.resolve(successResponse.data))
				.catch(errorResponse => listQuestionsPromise.reject(errorResponse));

			return listQuestionsPromise.promise;
		};

		return service;
	}]);