﻿angular
	.module("guestAuth")
	.value("guestAuthWebClientConfig", {
		URIs: {
			// POST api/guestAuthentication/authenticate
			authenticate: "../api/guestAuthentication/authenticate"
		}
	});