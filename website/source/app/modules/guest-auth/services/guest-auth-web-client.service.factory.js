﻿angular
	.module("guestAuth")
	.factory("GuestAuthWebClient", ["$http", "$q", "guestAuthWebClientConfig", function ($http, $q, guestAuthWebClientConfig) {
		var service = {};

		// public methods
		service.getId = function () {
			var getIdPromise = $q.defer();

			$http.post(String.uriFormat(guestAuthWebClientConfig.URIs.authenticate))
				.then(successResponse => {
					getIdPromise.resolve(successResponse.data);
				},
					errorResponse => {
						Logger.error(errorResponse);
						getIdPromise.reject(errorResponse);
					});

			return getIdPromise.promise;
		};

		return service;
	}]);