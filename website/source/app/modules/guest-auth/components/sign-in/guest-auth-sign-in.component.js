angular
	.module("guestAuth")
	.component("guestAuthSignIn", {
		templateUrl: "/modules/guest-auth/components/sign-in/guest-auth-sign-in.html",
		bindings: {
			onSignedIn: "&"
		},
		controller: [
			"GuestAuthWebClient",
			function (guestAuthWebClient) {
				const ctrl = this;

				ctrl.onGuestLoggedIn = function () {
					guestAuthWebClient.getId()
						.then(response => {
							ctrl.onSignedIn({ response: response });

							//todo clear error message
						}, error => { alert(error.statusText); });
				};
			}]
	});