angular
	.module("guestAuth")
	.component("guestAuthSignOut", {
		templateUrl: "/modules/guest-auth/components/sign-out/guest-auth-sign-out.html",
		bindings: {
			onSignedOut: "&"
		},
		controller: [function () {
			const ctrl = this;

			ctrl.signOut = function () {
				Logger.debug("Signed out of Guest.");
				ctrl.onSignedOut();
			};
		}]
	});