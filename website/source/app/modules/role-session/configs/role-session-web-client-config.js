﻿angular
	.module("roleSession")
	.value("roleSessionWebClientConfig", {
		URIs: {
			// POST api/rolesession/add
			add: "../api/rolesession/add",

			// POST api/rolesession/end/ceb58e50-2f9c-47a7-8ec0-9a8d55ca32fa/324523452
			end: "../api/rolesession/end/{0}/{1}"
		}
	});