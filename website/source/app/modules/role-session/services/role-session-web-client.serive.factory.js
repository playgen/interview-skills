﻿angular
	.module("roleSession")
	.factory("RoleSessionWebClient", [
		"$http",
		"$q",
		"roleSessionWebClientConfig",
		function ($http, $q, roleSessionWebClientConfig) {
			const service = {};

			// public methods
			service.add = function (userSessionId, matchSessionId, jobId, roleId) {
				const addPromise = $q.defer();

				$http.post(String.uriFormat(roleSessionWebClientConfig.URIs.add), {
					userSessionId: userSessionId,
					matchSessionId: matchSessionId,
					jobId: jobId,
					roleId: roleId,
					created: Date.now()

				}).then(roleSessionResponse => addPromise.resolve(roleSessionResponse.data),
					roleSessionError => addPromise.reject(roleSessionError));

				return addPromise.promise;
			};

			service.end = function (roleSessionId) {
				const endPromise = $q.defer();
				const ended = Date.now();

				$http.post(String.uriFormat(roleSessionWebClientConfig.URIs.end, roleSessionId, ended))
					.then(roleSessionResponse => endPromise.resolve(roleSessionResponse.data),
					roleSessionError => endPromise.reject(roleSessionError));

				return endPromise.promise;
			};

			return service;
		}]);