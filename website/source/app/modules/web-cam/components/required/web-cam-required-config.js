﻿angular
	.module("webCam")
	.value("webCamRequiredConfig", {
		pollInterval: 1000,
		required: {
			audio: true,
			video: true
		},
		notFoundMessage: "We cannot detect a webcam. " +
		"\n" +
		"\nPlease make sure one is connected and that you have clicked OK on the popup, allowing it to be used."
	});