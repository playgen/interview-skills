﻿angular
	.module("webCam")
	.component("webCamRequired", {
		templateUrl: "/modules/web-cam/components/required/web-cam-required.html",
		bindings: {
			onAvailable: "&",
			onUnavailable: "&"
		},
		controller: ["webCamRequiredConfig",
			"$timeout",
			"$q",
			function (webCamRequiredConfig, $timeout, $q) {
				const ctrl = this;

				// public variables
				ctrl.isAvailable = false;
				ctrl.notFoundMessage = webCamRequiredConfig.notFoundMessage;

				// private variables
				let timer = null;

				// angular hooks
				ctrl.$onInit = function () {
					detectWebCam();
				};

				ctrl.$onDestroy = function () {
					$timeout.cancel(timer);
					timer = null;
				};

				// private methods
				function detectWebCam() {
					const detectWebCamPromise = $q.defer();

					navigator.mediaDevices.getUserMedia(webCamRequiredConfig.required)
						.then(
							stream => detectWebCamPromise.resolve(stream),
							error => detectWebCamPromise.reject(error));

					detectWebCamPromise.promise
						.then(stream => {
							stream.getTracks().forEach(t => t.stop());
							
							ctrl.isAvailable = true;
							ctrl.onAvailable();

							timer = $timeout(detectWebCam, webCamRequiredConfig.pollInterval);
						}).catch(error => {
							ctrl.isAvailable = false;
							ctrl.onUnavailable({ error: error });

							timer = $timeout(detectWebCam, webCamRequiredConfig.pollInterval);
						});
				}
			}]
	});