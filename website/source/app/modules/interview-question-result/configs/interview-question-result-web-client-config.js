angular
	.module("interviewQuestionResult")
	.value("interviewQuestionResultConfig", {
		URIs: {
            // POST api/interviewQuestionResult/[questionId]/[matchSessionid]/updated/[state]/[updated]
			updated: "../api/interviewQuestionResult/{0}/{1}/updated/{2}/{3}",

			// POST api/interviewQuestionResult/[questionId]/[matchSessionid]/hidden/[hidden]
			hidden: "../api/interviewQuestionResult/{0}/{1}/hidden/{2}"
		}
	});