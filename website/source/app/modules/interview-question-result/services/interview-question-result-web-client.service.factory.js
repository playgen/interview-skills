angular
    .module("interviewQuestionResult")
    .factory("InterviewQuestionResultWebClient", [
        "$http",
        "interviewQuestionResultConfig",
		function ($http, interviewQuestionResultConfig) {
            const service = {};

            // public methods
			service.postUpdated = function (questionId, matchSessionId, state, updated) {
				return $http.post(String.uriFormat(
					interviewQuestionResultConfig.URIs.updated,
					questionId,
					matchSessionId,
					state,
					updated));
			};

			service.postHidden = function (questionId, matchSessionId, hidden) {
				return $http.post(String.uriFormat(
					interviewQuestionResultConfig.URIs.hidden,
					questionId,
					matchSessionId,
					hidden));
			};

            return service;
        }]);