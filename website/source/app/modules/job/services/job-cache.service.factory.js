angular
	.module("job")
	.factory("JobCache", [
		"JobWebClient",
		"$q",
		function (JobWebClient, $q) {
			const service = {};

			// private variables
			let buildCachePromise = null;
			let isBuilt = false;
			let jobs = [];

			// public methods
			service.buildCache = function () {
				buildCachePromise = $q.defer();
				isBuilt = false;
				jobs = [];

				JobWebClient.listJobs()
					.then(newJobs => {
						jobs = newJobs;
						buildCachePromise.resolve(newJobs);
						isBuilt = true;
					}, error => buildCachePromise.reject(error));

				return buildCachePromise.promise;
			};

			service.getIsBuilt = function() {
				return isBuilt;
			};

			service.listJobs = function (tags, requireAllTags) {
				isBuiltCheck();

				const filtered = jobs.slice().filter(job => {
					if (!tags) {
						return true;
					}

					if (!requireAllTags) {
						return tags.some(tag => job.tagMappings.find(tm => tm.tagId === tag));

					} else {
						return tags.every(tag => job.tagMappings.find(tm => tm.tagId === tag));
					}
				});

				return filtered;
			};

			// private methods
			function isBuiltCheck() {
				if (!isBuilt) {
					throw "Cache isn't initialised. " +
					"\nYou need to call buildCache and wait for it to complete before attempting to access the cached items.";
				}
			}

			return service;
		}]);