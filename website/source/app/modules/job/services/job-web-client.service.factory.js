angular
	.module("job")
	.factory("JobWebClient", [
		"$http",
		"$q",
		"jobConfig",
		function ($http, $q, jobConfig) {
			const service = {};

			// public methods
			service.listJobs = function () {
				const listJobsPromise = $q.defer();

				const query = "?includes=TagMappings";
				
				$http.get(String.uriFormat(jobConfig.URIs.listJobs + query))
					.then(successResponse => listJobsPromise.resolve(successResponse.data))
					.catch(errorResponse => listJobsPromise.reject(errorResponse));

				return listJobsPromise.promise;
			};

			return service;
		}]);