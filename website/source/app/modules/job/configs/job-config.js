angular
    .module("job")
    .value("jobConfig", {
        URIs: {
            // GET api/job/listJobs
            listJobs: "../api/job/list"
        }
    });