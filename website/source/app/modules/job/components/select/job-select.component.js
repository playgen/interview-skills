angular
	.module("job")
	.component("jobSelect", {
		templateUrl: "/modules/job/components/select/job-select.html",
		bindings: {
			tags: "<",
			onSelect: "&"
		},
		controller: [
			"JobCache",
			function (JobCache) {
				const ctrl = this;

				// public variables
				ctrl.jobs = [];
				ctrl.selected = null;

				// angluar hooks
				ctrl.$onInit = function () {
					getJobs(ctrl.tags);
				};

				ctrl.$onChanges = function (changes) {
					if (changes.tags) {
						getJobs(changes.tags.currentValue);
					}
				};

				// public methods
				ctrl.select = function (selected) {
					ctrl.selected = selected;
					ctrl.onSelect({ selected: selected });
				};

				// private methods
				function getJobs(tags) {
					ctrl.jobs = JobCache.listJobs(tags, false);
				}
			}
		]
	});