angular
	.module("emailAuth")
	.component("emailAuthSignOut", {
		templateUrl: "/modules/email-auth/components/sign-out/email-auth-sign-out.html",
		bindings: {
			onSignedOut: "&"
		},
		controller: [function () {
			const ctrl = this;

			ctrl.signOut = function () {
				Logger.debug("Signed out of Email.");
				ctrl.onSignedOut();
			};
		}]
	});