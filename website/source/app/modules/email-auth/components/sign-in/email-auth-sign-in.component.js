angular
	.module("emailAuth")
	.component("emailAuthSignIn", {
		templateUrl: "/modules/email-auth/components/sign-in/email-auth-sign-in.html",
		bindings: {
			onSignedIn: "&"
		},
		controller: [
			"EmailAuthWebClient",
			function (emailAuthWebClient) {
				const ctrl = this;

				// public variables
				ctrl.emailAddress = null;

				// public methods
				ctrl.login = function () {
					emailAuthWebClient.authenticate(ctrl.emailAddress)
						.then(response => ctrl.onSignedIn({ response: response.data }))
						.catch(error => alert(error.statusText));
				};
			}]
	});