﻿angular
	.module("emailAuth")
	.value("emailAuthWebClientConfig", {
		URIs: {
			// POST api/emailAuthentication
			// id@domain
			authenticate: "../api/emailAuthentication/authenticate"
		}
	});