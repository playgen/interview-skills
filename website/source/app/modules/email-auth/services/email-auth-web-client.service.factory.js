﻿angular
	.module("emailAuth")
	.factory("EmailAuthWebClient", [
		"$http",
		"emailAuthWebClientConfig",
		function ($http, emailAuthWebClientConfig) {
			const service = {};

			// public methods
			service.authenticate = function (emailAddress) {
				return $http.post(emailAuthWebClientConfig.URIs.authenticate, "\"" + emailAddress + "\"");
			};

			return service;
		}]);