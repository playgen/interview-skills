angular
	.module("featureSummaryFeedback")
	.value("featureSummaryFeedbackConfig", {
		URIs: {
			// GET api/featureSummaryFeedback/list/[feature]/[weightFromIdeal]
			listFeedbacks: "../api/featureSummaryfeedback/list/{0}/{1}"
		}
	});