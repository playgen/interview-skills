angular
	.module("featureSummaryFeedback")
	.factory("FeatureSummaryFeedbackWebClient", [
		"$http",
		"$q",
		"featureSummaryFeedbackConfig",
		function ($http, $q, featureSummaryFeedbackConfig) {
			const service = {};

			// public methods
			service.listFeedbacks = function (featureId, weightFromIdeal) {
				const listFeedbacksPromise = $q.defer();

				$http.get(String.uriFormat(featureSummaryFeedbackConfig.URIs.listFeedbacks, featureId, weightFromIdeal))
					.then(feedbacksResponse => {
						listFeedbacksPromise.resolve(feedbacksResponse.data);
					});

				return listFeedbacksPromise.promise;
			};

			return service;
		}]);