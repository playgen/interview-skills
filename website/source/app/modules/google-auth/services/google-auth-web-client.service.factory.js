﻿angular
	.module("googleAuth")
	.factory("GoogleAuthWebClient", ["$http", "$q", "googleAuthWebClientConfig", function ($http, $q, googleAuthWebClientConfig) {
		var service = {};

		// public methods
		service.getId = function (token) {
			var getIdPromise = $q.defer();

			$http.post(String.uriFormat(googleAuthWebClientConfig.URIs.authenticate), '"' + token + '"')
				.then(successResponse => {
					getIdPromise.resolve(successResponse.data);
				},
				errorResponse => {
					Logger.error(errorResponse);
					getIdPromise.reject(errorResponse);
				});

			return getIdPromise.promise;
		};

		return service;
	}]);