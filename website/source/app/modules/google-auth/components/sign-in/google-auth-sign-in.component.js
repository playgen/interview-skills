angular
	.module("googleAuth")
	.component("googleAuthSignIn", {
		templateUrl: "/modules/google-auth/components/sign-in/google-auth-sign-in.html",
		bindings: {
			onSignedIn: "&"
		},
		controller: ["GoogleAuthWebClient",
			function (googleAuthWebClient) {
				const ctrl = this;

				// angular hooks
				ctrl.$postLink = function () {
					gapi.signin2.render("google-sign-in-button", {
						"onsuccess": onSuccess,
						"onfailure": onFailure,
						"scope": "https://www.googleapis.com/auth/plus.login"
					});
				};

				// private methods
				function onSuccess(googleUser) {
					const idToken = googleUser.getAuthResponse().id_token;
					googleAuthWebClient.getId(idToken)
						.then(response => {
							ctrl.onSignedIn({ response: response });

							//todo clear error message
						}, error => {
							alert(error.statusText);
							Logger.error(error);
						});
				}

				function onFailure(error) {
					alert(error);
					Logger.error(error);
				}

				// setup
				window.onGoogleSignIn = ctrl.onGoogleSignIn;
			}]
	});