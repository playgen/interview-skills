angular
	.module("googleAuth")
	.component("googleAuthSignOut", {
		templateUrl: "/modules/google-auth/components/sign-out/google-auth-sign-out.html",
		bindings: {
			onSignedOut: "&"
		},
		controller: [function () {
			const ctrl = this;

			// public methods
			ctrl.signOut = function () {
				gapi.auth2.getAuthInstance().signOut()
					.then(() => {
						Logger.debug("Signed out of Google.");
						ctrl.onSignedOut();
					});
			};
		}]
	});