﻿angular
	.module("googleAuth")
	.value("googleAuthWebClientConfig", {
		URIs: {
			// POST api/googleAuthentication
			authenticate: "../api/googleAuthentication/authenticate"
		}
	});