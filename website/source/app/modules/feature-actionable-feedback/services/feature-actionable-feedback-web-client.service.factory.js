angular
	.module("featureActionableFeedback")
	.factory("FeatureActionableFeedbackWebClient", ["$http", "$q", "featureActionableFeedbackConfig", function ($http, $q, featureActionableFeedbackConfig){
		const service = {};

		// public methods
		service.listFeedbacks = function(featureId, weightFromIdeal) {
			const listFeedbacksPromise = $q.defer();

			$http.get(String.uriFormat(featureActionableFeedbackConfig.URIs.listFeedbacks, featureId, weightFromIdeal))
				.then(feedbacksResponse => {
					listFeedbacksPromise.resolve(feedbacksResponse.data);
				});

			return listFeedbacksPromise.promise;
		};

		return service;
	}]);