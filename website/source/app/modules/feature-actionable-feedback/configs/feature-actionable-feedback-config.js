angular
	.module("featureActionableFeedback")
	.value("featureActionableFeedbackConfig", {
		URIs: {
			// GET api/featureActionableFeedback/list/[feature]/[weightFromIdeal]
			listFeedbacks: "../api/featureactionablefeedback/list/{0}/{1}"
		}
	});