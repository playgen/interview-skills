angular
	.module("interpolate")
	.factory("Interpolate", [
        "$timeout",
        "interpolateConfig",
		function ($timeout, interpolateConfig) {
            const service = {};
            
            service.start = function(start, end, duration, onUpdate, easing) {
                var interpolation = {
                    start: start,
                    end: end,
                    duration: duration,
                    onUpdate: onUpdate,
                    easing: easing,
                    elapsed: 0,
                    timer: null                    
                };

                interpolation.cancel = () => {
                    if(interpolation.timer) {
                        $timeout.cancel(interpolation.timer);
                    }
                };

                interpolate(interpolation);

                return interpolation;                
            };

            function interpolate(interpolation) {
                interpolation.elapsed += interpolateConfig.updateInterval;
                var weight = interpolation.elapsed / interpolation.duration;
                weight = Math.min(Math.max(weight, 0), 1);

                if(interpolation.easing) {
                    weight = Math.sin(weight * (Math.PI / 2));
                }

                var value = (interpolation.start * (1 - weight)) + (interpolation.end * weight);

                if(weight < 1) {
                    interpolation.timer = $timeout(interpolate, interpolateConfig.updateInterval, true, interpolation);
                } else {
                    interpolation.timer = null;
                }
                
                interpolation.onUpdate(value);
            }

			return service;
		}]);