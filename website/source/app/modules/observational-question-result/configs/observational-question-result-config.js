angular
	.module("observationalQuestionResult")
	.value("observationalQuestionResultConfig", {
		URIs: {
			// POST api/observationalQuestionResult/[questionId]/[matchSessionId]/[userSessionId]/received/[received]
			received: "../api/observationalQuestionResult/{0}/{1}/{2}/received/{3}",

			// POST api/observationalQuestionResult/[questionId]/[matchSessionId]/[userSessionId]/[received]/shown/[shown]
			shown: "../api/observationalQuestionResult/{0}/{1}/{2}/{3}/shown/{4}",

			// POST api/observationalQuestionResult/[questionId]/[matchSessionId]/[userSessionId]/[received]/submitted/[weight]/[submitted]
			submitted: "../api/observationalQuestionResult/{0}/{1}/{2}/{3}/submitted/{4}/{5}"
		}
	});

