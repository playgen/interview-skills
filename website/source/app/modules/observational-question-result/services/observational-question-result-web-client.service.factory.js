angular
	.module("observationalQuestionResult")
	.factory("ObservationalQuestionResultWebClient", [
		"$http",
		"observationalQuestionResultConfig",
		function ($http, observationalQuestionResultConfig) {
			const service = {};

			// public methods
			service.postReceived = function (
				questionId,
				matchSessionId,
				userSessionId,
				received) {

				return $http.post(String.uriFormat(
					observationalQuestionResultConfig.URIs.received,
					questionId,
					matchSessionId,
					userSessionId,
					received));
			};

			service.postShown = function (
				questionId,
				matchSessionId,
				userSessionId,
				received,
				shown) {

				return $http.post(String.uriFormat(
					observationalQuestionResultConfig.URIs.shown,
					questionId,
					matchSessionId,
					userSessionId,
					received,
					shown));
			};

			service.postSubmitted = function (
				questionId,
				matchSessionId,
				userSessionId,
				received,
				weight,
				submitted) {

				return $http.post(String.uriFormat(
					observationalQuestionResultConfig.URIs.submitted,
					questionId,
					matchSessionId,
					userSessionId,
					received,
					weight,
					submitted));
			};

			return service;
		}]);