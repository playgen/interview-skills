angular
	.module("featureActionableFeedbackSystemResult")
	.value("featureActionableFeedbackSystemResultConfig", {
		URIs: {
            // POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/received/[received]
			received: "../api/featureActionableFeedbackSystemResult/{0}/{1}/received/{2}",

            // POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/shown/[shown]
            shown: "../api/featureActionableFeedbackSystemResult/{0}/{1}/shown/{2}",

            // POST api/featureActionableFeedbackSystemResult/[feedbackId]/[analysisResultId]/acknowledged/[acknowledged]
            acknowledged: "../api/featureActionableFeedbackSystemResult/{0}/{1}/acknowledged/{2}"
		}
	});