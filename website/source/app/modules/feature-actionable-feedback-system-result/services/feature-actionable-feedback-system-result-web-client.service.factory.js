angular
    .module("featureActionableFeedbackSystemResult")
    .factory("FeatureActionableFeedbackSystemResultWebClient", [
        "$http",
        "featureActionableFeedbackSystemResultConfig",
        function ($http, featureActionableFeedbackSystemResultConfig) {
            const service = {};

            // public methods
            service.postReceived = function (feedbackId, analysisResultId, received) {
                return $http.post(String.uriFormat(
                    featureActionableFeedbackSystemResultConfig.URIs.received,
                    feedbackId,
                    analysisResultId,
                    received));
            };

            service.postShown = function (feedbackId, analysisResultId, shown) {
                return $http.post(String.uriFormat(
                    featureActionableFeedbackSystemResultConfig.URIs.shown,
                    feedbackId,
                    analysisResultId,
                    shown));
            };

            service.postAcknowledged = function (feedbackId, analysisResultId, acknowledged) {
                return $http.post(String.uriFormat(
                    featureActionableFeedbackSystemResultConfig.URIs.acknowledged,
                    feedbackId,
                    analysisResultId,
                    acknowledged));
            };

            return service;
        }]);