angular
    .module('remoteCommands')
    .factory('RemoteCommands', [        
        "remoteCommandsConfig",
        "$websocket",
        "$location",
        "$q",
        function(
            remoteCommandsConfig, 
            $websocket, 
            $location, 
            $q) {
            const service = {};

            // public events
            service.updateReceivedEvent = null;

            // private variables
            let connection;
            let commandRequestCount;
            let pendingsendPromises;
            let commandsToResendOnConnect;
                       
            // public methods
            service.initialize = function() {
                if(connection) {
                    Logger.warn("RemoteCommands: Connection already exists.");

                } else {
                    service.updateReceivedEvent = new Observable();
                    commandRequestCount = 0;
                    pendingsendPromises = {};
                    commandsToResendOnConnect = [];

                    const url = String.format(
                        remoteCommandsConfig.urlTemplate, 
                        $location.$$host,
                        $location.$$port);

                    connection = $websocket(url, undefined, {
                        initialTimeout: remoteCommandsConfig.initialTimeout,
                        maxTimeout: remoteCommandsConfig.maxTimeout,
                        reconnectIfNotNormalClose: true,
                    });                    

                    connection.onOpen(event => {
                        Logger.debug("RemoteCommands: Connection Opened");
                        resendCommandsOnConnect();
                    });
                    connection.onError(event => Logger.error(`RemoteCommands: ${event.reason}`));
                    connection.onClose(event => Logger.debug(`RemoteCommands: Connection closed. Reason: ${event.reason}`));
                    connection.onMessage(onConnectionMessage);                    
                }
            };

            service.terminate = function() {
                if(connection) {                    
                    connection.close(true);            

                    service.updateReceivedEvent = null;
                    commandRequestCount = null;
                    pendingsendPromises = null;
                    commandsToResendOnConnect = null;
                    connection = null;

                } else {
                    Logger.warn("RemoteCommands: No connection to close.");
                }
            };        

            service.removeResendOnReconnectCommands = function(removeResendOnReconnectCommands) {
                removeResendOnReconnectCommands.forEach(removeCommand => Array.removeWhere(commandsToResendOnConnect, resendCommand => resendCommand.command === removeCommand));
            };

            service.sendCommand = function(command, params, resendOnReconnect, removeResendOnReconnectCommands) {
                if(!connection) {
                    const error = "RemoteCommands: No Connection. Make sure this has been initialized.";
                    Logger.warn(error);
                    return $q.reject(error);
                }

                if(removeResendOnReconnectCommands) {
                    service.removeResendOnReconnectCommands(removeResendOnReconnectCommands);
                }

                var sendPromise = $q.defer();
                commandRequestCount++;

                var commandRequest = {
                    id: commandRequestCount.toString(),
                    command: command,
                    parameters: params
                };

                pendingsendPromises[commandRequest.id] = sendPromise;

                let serializedRequest;

                if(remoteCommandsConfig.isBinary) {
                    throw "Not Implemented";       

                } else {
                    serializedRequest = JSON.stringify(commandRequest);
                }
                
                if(resendOnReconnect) {
                    commandsToResendOnConnect.push({
                        command: command,
                        params: params,
                        sendPromise : sendPromise
                    }); 
                }

                if(connection.readyState === connection._readyStateConstants.OPEN || !resendOnReconnect) {
                    connection.send(serializedRequest);
                }

                return sendPromise.promise;
            };

            // private methods
            function resendCommandsOnConnect() {                
                if(commandsToResendOnConnect) {
                    commandsToResendOnConnect.forEach(command => {
                        service.sendCommand(command.command, command.params)
                            .then(command.sendPromise.resolve);
                    });
                }
            }

            function onConnectionMessage(message) {
                let response;
               
                if(remoteCommandsConfig.isBinary) {
                    throw "Not Implemented";       

                } else {                    
                    response = JSON.parse(message.data);
                }
                 
                if(response.$type === remoteCommandsConfig.updateResponseType) {
                    service.updateReceivedEvent.publish(response);

                } else if(response.$type === remoteCommandsConfig.commandResponseType) {
                    var sendPromise = pendingsendPromises[response.requestId];
                    if(sendPromise) {
                        delete pendingsendPromises[response.requestId];
                        sendPromise.resolve(response.result);

                    } else {
                        Logger.error(`RemoteCommands: No pending promise for Command: ${response.requestId}.`);
                    }

                } else {
                    Logger.error(`RemoteCommands: Unhandled response type: ${commandResponse.$type}.`);
                }
            }

            return service;
        }
    ]);