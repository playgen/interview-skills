angular
    .module('remoteCommands')
    .value('remoteCommandsConfig', {
        isBinary: false,
        urlTemplate: "wss://{0}:{1}/remoteCommands",
        commandResponseType: "PlayGen.RemoteCommands.Command.CommandResponse, PlayGen.RemoteCommands",
        updateResponseType: "PlayGen.RemoteCommands.UpdateResponse, PlayGen.RemoteCommands",
        initialTimeout: 1 * 1000,
        maxTimeout: 10 * 1000
    });