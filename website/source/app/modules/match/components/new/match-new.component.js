angular
	.module("match")
	.component("matchNew", {
		templateUrl: "/modules/match/components/new/match-new.html",
		bindings: {
			participantId: "<",
			scenarioCategory: "<",
			scenarioName: "<",
			cantMatchPrevious: "<",
			onFoundMatch: "="
		},
		controller: [
			"MatchPolling",
			"matchConfig",
			function (MatchPolling, matchConfig) {
				const ctrl = this;

				// public methods
				ctrl.$onInit = function () {
					const scenarioConfig = {
						category: ctrl.scenarioCategory,
						name: ctrl.scenarioName,
						cantMatchPrevious: ctrl.cantMatchPrevious
					};

					if (scenarioConfig.category === undefined) {
						scenarioConfig.category = matchConfig.scenario.category;
					}

					if (scenarioConfig.name === undefined) {
						scenarioConfig.name = matchConfig.scenario.name;
					}

					if (scenarioConfig.cantMatchPrevious === undefined) {
						scenarioConfig.cantMatchPrevious = matchConfig.scenario.cantMatchPrevious;
					}

					MatchPolling.gotMatchesEvent.subscribe(onFoundMatches);
					MatchPolling.newMatch(ctrl.participantId, scenarioConfig.category, scenarioConfig.name, scenarioConfig.cantMatchPrevious);
				};

				ctrl.$onDestroy = function () {
					MatchPolling.gotMatchesEvent.unsubscribe(onFoundMatches);
				};

				// private methods
				function onFoundMatches(matches) {
					// must find a match with at least 2 participants 
					// todo move this logic to the polling and server
					const multiParticipantMatch = matches.find(m => m.participants.length > 1);

					if (multiParticipantMatch) {
						MatchPolling.gotMatchesEvent.unsubscribe(onFoundMatches);
						ctrl.onFoundMatch(multiParticipantMatch);
					} else {
						MatchPolling.existingMatches(ctrl.participantId);	
					}
				}
			}]
	});