angular
	.module("match")
	.value("matchConfig", {
		URIs: {
			// GET api/match/find/[participantId]/[scenarioCategory]/[scenarioName]?cantMatchPrevious=false
			findMatch: "../api/match/find/{0}/{1}/{2}?cantMatchPrevious={3}",
			// GET api/match/new/[participantId]/[scenarioCategory]/[scenarioName]?cantMatchPrevious=false
			newMatch: "../api/match/new/{0}/{1}/{2}?cantMatchPrevious={3}",
			// GET api/match/existing/[participantId]
			existingMatch: "../api/match/existing/{0}",
			// POST api/match/create/{scenarioCategory}/{scenarioName}?participantIds=[user 1 session Id]&participantIds=[user 2 session Id]
			createMatch: "../api/match/create/{0}/{1}?participantIds={2}",
			// Post api/match/leave/[matchId]/[participantId]
			leaveMatch: "../api/match/leave/{0}/{1}",
			// Post api/match/leaveall/[participantId]
			leaveAll: "../api/match/leaveall/{0}"
		},
		pollInterval: 1000,
		scenario: {
			category: "JobInterview",
			name: "Default",
			cantMatchPrevious: false
		}
	});