class MatchUtils {
	static getMatchCustomData(match, type){
		const matchingCustomData = Array.firstOrNull(match.customData, c => c.$type.includes(type));

		if(matchingCustomData === null) {								
			throw "Couldn't retrieve " + type;
		}

		return matchingCustomData;
	}			

	static getMatchedUser(match, userSessionId){
		const matchedUser = Array.firstOrNull(match.users, c => c.userSessionId === userSessionId);

		if(matchedUser === null) {
			throw "No matched user with session id found: " + userSessionId;
		}

		return matchedUser;
	}			

	static getMatchedUserCustomData(matchedUser, type){
		const matchingMatchedUserCustomData = Array.firstOrNull(matchedUser.customData, c => c.$type.includes(type));

		if(matchingMatchedUserCustomData === null) {								
			throw "Couldn't retrieve " + type;
		}

		return matchingMatchedUserCustomData;
	}
}