angular
	.module("match")
	.factory("MatchPolling", [
		"MatchWebClient",
		"matchConfig",
		"$timeout",
		function (MatchWebClient, matchConfig, $timeout) {
			var service = {};

			// public variables
			service.gotMatchesEvent = new Observable();

			// private variables
			var pollMatchTimeout = null;

			// public methods
			service.findMatches = function (participantId, scenarioCategory, scenarioName, cantMatchPrevious) {
				cancelPollTimeout();

				MatchWebClient
					.findMatches(participantId, scenarioCategory, scenarioName, cantMatchPrevious)
					.then(matches => onMatchResponsePollFind(matches, participantId, scenarioCategory, scenarioName, cantMatchPrevious));
			};

			service.newMatch = function (participantId, scenarioCategory, scenarioName, cantMatchPrevious) {
				cancelPollTimeout();

				MatchWebClient
					.newMatch(participantId, scenarioCategory, scenarioName, cantMatchPrevious)
					.then(match => onMatchResponsePollFind(match ? [match] : null, participantId, scenarioCategory, scenarioName, cantMatchPrevious));
			};

			service.existingMatches = function (participantId) {
				cancelPollTimeout();

				MatchWebClient
					.existingMatches(participantId)
					.then(matches => onMatchResponsePollExisting(matches, participantId));
			};

			service.leaveAll = function (participantId) {
				cancelPollTimeout();

				return MatchWebClient.leaveAll(participantId);
			};

			// private methods
			function onMatchResponsePollFind(matches, participantId, scenarioCategory, scenarioName, cantMatchPrevious) {
				if (matches && matches.length > 0) {
					service.gotMatchesEvent.publish(matches);
				} else {
					pollMatchTimeout = $timeout(() => service.findMatches(participantId, scenarioCategory, scenarioName, cantMatchPrevious), matchConfig.pollInterval);
				}
			}

			function onMatchResponsePollExisting(matches, participantId) {
				if (matches && matches.length > 0) {
					service.gotMatchesEvent.publish(matches);
				} else {
					pollMatchTimeout = $timeout(() => service.existingMatch(participantId), matchConfig.pollInterval);
				}
			}

			function cancelPollTimeout() {
				if (pollMatchTimeout !== null) {
					$timeout.cancel(pollMatchTimeout);
				}
			}

			return service;
		}]);