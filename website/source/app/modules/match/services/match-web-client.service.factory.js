angular
	.module("match")
	.factory("MatchWebClient", [
		"$http",
		"$q",
		"matchConfig",
		function ($http, $q, matchConfig) {
			const service = {};
			
			// public methods
			service.findMatches = function (participantId, scenarioCategory, scenarioName, cantMatchPrevious = false) {
				const findMatchesPromise = $q.defer();

				$http.get(String.uriFormat(matchConfig.URIs.findMatch, participantId, scenarioCategory, scenarioName, cantMatchPrevious))
					.then(matchesResponse => {
						const matches = matchesResponse.data;
						if (matches) {
							matches.forEach(match => processMatch(match, participantId));
						}
						findMatchesPromise.resolve(matches);
					})
					.catch(error => findMatchesPromise.reject(error));

				return findMatchesPromise.promise;
			};

			service.newMatch = function (participantId, scenarioCategory, scenarioName, cantMatchPrevious = false) {
				const newMatchPromise = $q.defer();

				$http.get(String.uriFormat(matchConfig.URIs.newMatch, participantId, scenarioCategory, scenarioName, cantMatchPrevious))
					.then(matchResponse => {
						const match = matchResponse.data;
						if (match) {
							processMatch(match, participantId);
						}
						newMatchPromise.resolve(match);
					})
					.catch(error => newMatchPromise.reject(error));

				return newMatchPromise.promise;
			};

			service.existingMatches = function (participantId) {
				const existingMatchesPromise = $q.defer();

				$http.get(String.uriFormat(matchConfig.URIs.existingMatch, participantId))
					.then(matchesResponse => {
						const matches = matchesResponse.data;
						if (matches) {
							matches.forEach(match => processMatch(match, participantId));
						}
						existingMatchesPromise.resolve(matches);
					})
					.catch(error => existingMatchesPromise.reject(error));

				return existingMatchesPromise.promise;
			};

			service.createMatch = function (scenarioCategory, scenarioName, participantId, participantIds) {
				const createMatchPromise = $q.defer();

				let uri = String.uriFormat(matchConfig.URIs.createMatch, scenarioCategory, scenarioName, participantIds[0]);

				for (let i = 1; i < participantIds.length; i++) {
					uri += "&participantIds=" + participantIds[i];
				}

				$http.post(uri)
					.then(matchResponse => {
						const match = matchResponse.data;
						processMatch(match, participantId);
						createMatchPromise.resolve(match);
					})
					.catch(error => createMatchPromise.reject(error));

				return createMatchPromise.promise;
			};

			service.leaveMatch = function (matchId, participantId) {
				const leaveMatchPromise = $q.defer();

				$http.post(String.uriFormat(matchConfig.URIs.leaveMatch, matchId, participantId))
					.then(matchResponse => {
						const didLeave = matchResponse.data;
						leaveMatchPromise.resolve(didLeave);
					})
					.catch(error => leaveMatchPromise.reject(error));

				return leaveMatchPromise.promise;
			};

			service.leaveAll = function (participantId) {
				const leaveAllPromise = $q.defer();

				$http.post(String.uriFormat(matchConfig.URIs.leaveAll, participantId))
					.then(matchResponse => {
						const didLeave = matchResponse.data;
						leaveAllPromise.resolve(didLeave);
					})
					.catch(error => leaveAllPromise.reject(error));

				return leaveAllPromise.promise;
			};

			// private methods
			function processMatch(match, participantId) {
				match.thisParticipant = Array.first(match.participants, p => p.matchingParticipant.id === participantId);
			}

			return service;
		}]);