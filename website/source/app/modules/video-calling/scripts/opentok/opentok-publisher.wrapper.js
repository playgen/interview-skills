class OpenTokPublisherWrapper {
	constructor(config) {
		// private variables
		this._publisher = null;
		this._config = config;
		this._isDisposed = true;
	}

	// public methods
	connect() {
		this._publisher = OT.initPublisher(this._config.elementId,
			this._config,
			error => {
				if (error) {
					if (error.name === 'OT_USER_MEDIA_ACCESS_DENIED') {
						this._onAccessDenied(error);
					} else {
						alert('Failed to get access to your camera or microphone. Please check that your webcam is connected and not being used by another application and try again.');
					}

					this.disconnect();

					throw error;
				}
			});

		this._publisher.on({
			accessDenied: this._onPublisherAccessDenied.bind(this),
			streamCreated: this._onPublisherStreamCreated.bind(this),
			streamDestroyed: this._onPublisherStreamDestroyed.bind(this)
		});
	}

	publish(session) {
		if (session.capabilities.publish === 1) {
			session.publish(this._publisher, error => {
				if (error) {
					switch (error.name) {
						case "OT_NOT_CONNECTED":
							alert("Publishing your video failed. You are not connected to the internet.");
							break;
						case "OT_CREATE_PEER_CONNECTION_FAILED":
							alert("Publishing your video failed. This could be due to a restrictive firewall.");
							break;
						default:
							alert("An unknown error occurred while trying to publish your video. Please try again later.");
					}

					this._publisher.destroy();
					this._publisher = null;
					throw error;
				} else {
					Logger.info("Publishing a stream.");
				}
			});
		} else {
			throw "You cannot publish an audio-video stream.";
		}
	}

	unpublish(session) {
		if (this._publisher !== null) {
			session.unpublish(this._publisher);
		} else {
			Logger.warn("Publisher is already null");
		}
	}

	disconnect() {
		if (this._publisher) {
			this._publisher.destroy();
			this._publisher = null;
		}
	}

	dispose() {
		if (this._isDisposed) return;

		if (this._publisher !== null) {
			this._disconnect();
		} else {
			Logger.warn("Publisher is already null");
		}

		this._isDisposed = true;
	}

	// private methods
	// callbacks
	_onAccessDenied(error) {
		Logger.debug(error);
	}

	_onPublisherAccessDenied() {
		alert('Please allow access to the Camera and Microphone and try publishing again.');
	}

	_onPublisherStreamCreated(event) {
		Logger.debug("onStreamCreated");
	}

	_onPublisherStreamDestroyed(event) {
		if (event.reason === 'networkDisconnected') {
			alert('Your publisher lost its connection. Please check your internet connection and try publishing again.');
		}

		Logger.debug("onStreamDestroyed: " + event.reason);
	}
}