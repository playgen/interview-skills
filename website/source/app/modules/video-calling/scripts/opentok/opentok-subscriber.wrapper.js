class OpenTokSubscriberWrapper {
	constructor(config) {
		// Private Variables
		this._subscribed = {};
		this._available = {};
		this._config = config;
		this._subscribeToNewAvailable = false;

		this._isDisposed = false;
	}

	// Public Methods
	subscribe (session, stream){
		this._subscribed[stream.streamId] = session.subscribe(stream,
			this._config.elementId,
			this._config,
			error => {
				if (error) {
					throw error;
				} else {
					Logger.info("Subscribing to stream: " + stream.id);
				}
			}
		);
	}

	addAvailable(session, stream) {
		this._available[stream.streamId] = stream;

		if (this._subscribeToNewAvailable) {
			this.subscribe(session, stream);
		}
	}

	removeUnavailable (streamId) {
		delete this._available[streamId];
	}

	subscribeAllAvailable(session) {
		this._subscribeToNewAvailable = true;

		Object.keys(this._available).forEach(streamId => {
			let availableStream = this._available[streamId];
			this.subscribe(session, availableStream);
		});
	}

	unsubscribe (session, streamId) {
		session.unsubscribe(this._subscribed[streamId]);
		delete this._subscribed[streamId];
	}

	unsubscribeAll(session) {
		this._subscribeToNewAvailable = false;

		Object.keys(this._subscribed).forEach(streamId => {
			session.unsubscribe(this._subscribed[streamId]);
		});

		this._subscribed = {};
	}

	toggleAudio(isOn) {
		for (let key in this._subscribed) {
			this._subscribed[key].subscribeToAudio(isOn);
		}
	}

	toggleVideo(isOn) {
		for (let key in this._subscribed) {
			this._subscribed[key].subscribeToVideo(isOn);
		}
	}

	dispose () {
		if(this._isDisposed) return;

		Object.keys(this._subscribed).forEach(streamId => {
			this._subscribed[streamId].destroy();
		});

		this._subscribed = null;
		this._available = null;

		this._isDisposed = true;
	}
}