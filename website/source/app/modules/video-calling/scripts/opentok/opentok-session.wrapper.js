class OpenTokSessionWrapper {
	constructor(config) {
		// public events
		this.signalReceivedEvent = new Observable();
		this.connectedEvent = new Observable();
		this.disconnectedEvent = new Observable();
		this.archiveStartedEvent = new Observable();
		this.archiveStoppedEvent = new Observable();
		this.streamingSuspendedEvent = new Observable();
		this.streamingResumedEvent = new Observable();

		// private variables
		this._session = null;
		this._isConnected = false;
		this._publisherWrapper = new OpenTokPublisherWrapper(config.publisher);
		this._subscriberWrapper = new OpenTokSubscriberWrapper(config.subscriber);
	}

	// public properties
	get isConnected () {
		return this._isConnected;
	}

	// public methods
	connect (apiKey, sessionId, token) {
		if (OT.checkSystemRequirements() === 1) {
			this._session = OT.initSession(apiKey, sessionId);

			this._session.on({
				connectionCreated: this._onConnectionCreated.bind(this),
				connectionDestroyed: this._onConnectionDestroyed.bind(this),
				sessionDisconnected: this._onSessionDisconnected.bind(this),
				streamCreated: this._onSessionStreamCreated.bind(this),
				streamDestroyed: this._onSessionStreamDestroyed.bind(this),
				signal: this._onSessionSignal.bind(this),
				archiveStarted: this._onArchiveStarted.bind(this),
				archiveStopped: this._onArchiveStopped.bind(this)
			});

			this._session.connect(token, error => {
				if (error) {
					if (error.name === "OT_NOT_CONNECTED") {
						alert('Failed to connect. Please check your connection and try connecting again.');
					} else {
						alert('An unknown error occurred connecting. Please try again later.');
					}

					this._session = null;
					throw error;
				} else {
					Logger.info("Connected to the session.");
				}
			});
		} else {
			throw "The client does not support WebRTC. \nCannot initialize OpenTok session.";
		}
	}

	disconnect () {
		if(this._session !== null) {
			this._session.disconnect();
			this._publisherWrapper.dispose();
			this._subscriberWrapper.dispose();

			this._session = null;
			this._publisherWrapper = null;
			this._subscriberWrapper = null;

			this._isConnected = false;
		}
	}

	signal (data) {
		this._session.signal(data, error => {
			if (error) {
				throw error;
			}
		});
	}

	suspendStreaming() {
		this._subscriberWrapper.unsubscribeAll(this._session);
		this._publisherWrapper.disconnect();
		this.streamingSuspendedEvent.publish();
	}

	resumeStreaming() {
		this._publisherWrapper.connect();
		this._publisherWrapper.publish(this._session);
		this._subscriberWrapper.subscribeAllAvailable(this._session);
		this.streamingResumedEvent.publish();
	}

	toggleAudio(isOn) {
		this._subscriberWrapper.toggleAudio(isOn);
	}

	toggleVideo(isOn) {
		this._subscriberWrapper.toggleVideo(isOn);	
	}
	
	// private methods
	// callbacks
	_onConnectionCreated(event) {
		if(event.connection.connectionId === this._session.connection.connectionId) {
			this._isConnected = true;
			this.connectedEvent.publish(event);
			Logger.debug("onConnectionCreated");

		} else {
			Logger.debug(`Another client connected: ${event.connection.connectionId}`);
		}		
	}

	_onConnectionDestroyed(event) {
		if(event.connection.connectionId === this._session.connection.connectionId) {
			this._isConnected = false;
			this.disconnectedEvent.publish(event);
			Logger.debug("onConnectionDestroyed: " + event.reason);

		} else {
			Logger.debug(`Another client disconnected: ${event.connection.connectionId}`);
		}		
	}

	_onSessionDisconnected(event) {
		this._isConnected = false;
		Logger.debug("Session Disconnected: " + event.reason);
		this.disconnectedEvent.publish(event);
		// The event is defined by the SessionDisconnectEvent class
		if (event.reason === "networkDisconnected") {
			alert("Your network connection terminated.");
		}
	}

	_onSessionStreamCreated(event) {
		this._subscriberWrapper.addAvailable(this._session, event.stream);
	}

	_onSessionStreamDestroyed(event) {
		this._subscriberWrapper.removeUnavailable(event.stream.streamId);
		Logger.debug("Stream stopped. Reason: " + event.reason);
	}

	_onSessionSignal(event) {
		this.signalReceivedEvent.publish(event.data);
	}

	_onArchiveStarted(event) {
		this.archiveStartedEvent.publish(event);
	}

	_onArchiveStopped(event) {
		this.archiveStoppedEvent.publish(event);
	}
}