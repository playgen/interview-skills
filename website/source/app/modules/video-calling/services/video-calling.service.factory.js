angular
	.module("videoCalling")
	.factory("VideoCalling", ["publisherConfig", "subscriberConfig", function(publisherConfig, subscriberConfig){
		const service = {};

		// private variables
		let sessionWrapper = null;
		let isInitialised = false; 

		// public variables
        service.signalReceivedEvent = null;
        service.archiveStartedEvent = null;
        service.archiveStoppedEvent = null;
        service.connectedEvent = null;
        service.disconnectedEvent = null;
        service.streamingSuspendedEvent = null;
		service.streamingResumedEvent = null;

		service.initialisedEvent = new Observable();
		service.terminatedEvent = new Observable();
		
		// public methods
		service.signal = null;
		service.isConnected = null;
		
        service.suspendStreaming = function() {
            sessionWrapper.suspendStreaming();
        };

		service.resumeStreaming = function() {
            sessionWrapper.resumeStreaming();
		};

		service.toggleAudio = function(isOn) {
			sessionWrapper.toggleAudio(isOn);
		};

		service.toggleVideo = function (isOn) {
			sessionWrapper.toggleVideo(isOn);
		};

		service.initialize = function (apiKey, sessionId, token) {
			if (isInitialised) {
				Logger.warn("VideoCalling is already initialised. Terminating previous.");
				service.terminate(); 
			} 

            sessionWrapper = new OpenTokSessionWrapper({publisher: publisherConfig,
                subscriber: subscriberConfig});

            service.signalReceivedEvent = sessionWrapper.signalReceivedEvent;
            service.connectedEvent = sessionWrapper.connectedEvent;
            service.disconnectedEvent = sessionWrapper.disconnectedEvent;
            service.archiveStartedEvent = sessionWrapper.archiveStartedEvent;
			service.archiveStoppedEvent = sessionWrapper.archiveStoppedEvent;
			service.streamingSuspendedEvent = sessionWrapper.streamingSuspendedEvent;
			service.streamingResumedEvent = sessionWrapper.streamingResumedEvent;

            service.signal = sessionWrapper.signal.bind(sessionWrapper);
			service.getIsConnected = () => sessionWrapper.isConnected;

			sessionWrapper.connect(apiKey, sessionId, token);
			Logger.info("VideoCalling: Connecting to session: " + sessionId + ".");

			isInitialised = true;
			service.initialisedEvent.publish();
		};

		service.getIsInitialised = function() {
			return isInitialised;
		};

		service.terminate = function () {
			if (!isInitialised) {
				Logger.warn("VideoCalling is not initialised.");
				return;
			}

			if(sessionWrapper !== null && sessionWrapper.isConnected) {
				service.disconnectedEvent.subscribe(service.terminate, service);
				sessionWrapper.disconnect();
				Logger.info("VideoCalling: Disconnected.");
				
			} else {			
				service.signalReceivedEvent = null;
				service.archiveStartedEvent = null;
				service.archiveStoppedEvent = null;
				service.connectedEvent = null;
				service.disconnectedEvent = null;
				service.streamingSuspendedEvent = null;
				service.streamingResumedEvent = null;
                sessionWrapper = null;
			}

			isInitialised = false;
			service.terminatedEvent.publish();
		};

		return service;
	}]);