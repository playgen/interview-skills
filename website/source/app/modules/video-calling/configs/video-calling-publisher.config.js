angular
	.module("videoCalling")
	.value("publisherConfig", {
		frameRate: 15, // Options: 30, 15, 7, 1
		resolution: "640x480", // Options: "1280x720", "640x480", "320x240",
		elementId: "publisher",
		insertMode: "append"
	});