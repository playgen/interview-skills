﻿class SendSignalStatus {
	static get SENT () {
		return "SENT";
	}

	static get QUEUED () {
		return "QUEUED";
	}
}