angular
	.module("peerSignalling")
	.factory("PeerSignalling", [function(){
		const service = {};

		// private variables
		let pendingSignalQueue = null;
        let clientId = null;
		let signaller = null;
		let isInitialised = false;

		// public events
		service.signalReceivedEvent = null;

		// public methods
		service.sendSignal = function(type, payload, recipients, callback = null) {
			const data = {
				serviceId: "PeerSignalling",
				sender: clientId,
				recipients: recipients,
				type: type,
				payload: payload
			};

			const signal = {
				data: JSON.stringify(data)
			};

			let status;

			if(signaller.getIsConnected()) {
				signaller.signal(signal);
				status = SendSignalStatus.SENT;

			} else {
				pendingSignalQueue.push(signal);
				status = SendSignalStatus.QUEUED;
			}

			if (callback) {
				callback(status);
			}
		};

		service.initialize = function (inSignaller, inClientId) {
			if (isInitialised) {
				Logger.warn("PeerSignalling is already initialised. Terminating previous.");
				service.terminate();
			}

			clientId = inClientId;
            signaller = inSignaller;
            pendingSignalQueue = [];
			service.signalReceivedEvent = new Observable();

			isInitialised = true;
		};

		service.terminate = function () {
			if (!isInitialised) {
				Logger.warn("PeerSignalling is not initialised.");
				return;
			}

            signaller = null;
            clientId = null;
            pendingSignalQueue = null;
			service.signalReceivedEvent = null;
			isInitialised = false;
		};

        service.sendPending = function () {
            for (let i = pendingSignalQueue.length - 1; i >= 0; i--) {
                const signal = pendingSignalQueue[i];
                pendingSignalQueue.splice(i, 1);
                signaller.signal(signal);
            }
        };

        service.onSignalReceived = function(serializedSignal) {
            const signal = JSON.parse(serializedSignal);
            if(signal.serviceId !== "PeerSignalling") return;

            if(signal.sender !== undefined && signal.recipients !== undefined && signal.type !== undefined && signal.payload !== undefined) {

                if(Array.contains(signal.recipients, clientId)) {
                    delete signal.serviceId;
                    service.signalReceivedEvent.publish(signal);
                }

            } else {
                Logger.warn("Received malformed signal");
                Logger.trace(signal);
            }
        };

		return service;
	}]);