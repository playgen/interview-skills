angular
	.module("observationalQuestionFeedback")
	.value("observationalQuestionFeedbackConfig", {
		URIs: {
			// GET api/observationalquestionfeedback/list/[questionId]/[featureId]/[weight]
			listFeedbacks: "../api/observationalquestionfeedback/list/{0}/{1}/{2}"
		}
	});