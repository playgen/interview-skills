angular
	.module("observationalQuestionFeedback")
	.factory("ObservationalQuestionFeedbackWebClient", [
		"$http", 
		"$q", 
		"observationalQuestionFeedbackConfig",
		function ($http, $q, observationalQuestionFeedbackConfig) {
			const service = {};

			// public methods
			service.listFeedbacks = function (questionId, featureId, weight) {
				const listFeedbacksPromise = $q.defer();

				$http.get(String.uriFormat(observationalQuestionFeedbackConfig.URIs.listFeedbacks, questionId, featureId, weight))
					.then(feedbacksResponse => {
						listFeedbacksPromise.resolve(feedbacksResponse.data);
					});

				return listFeedbacksPromise.promise;
			};

			return service;
		}]);