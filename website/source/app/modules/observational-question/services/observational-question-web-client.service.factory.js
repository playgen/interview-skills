angular
	.module("observationalQuestion")
	.factory("ObservationalQuestionWebClient",
	["$http", "$q", "observationalQuestionConfig",
		function ($http, $q, observationalQuestionConfig) {
			const service = {};

			// public methods
			service.listQuestions = function (features, tags, requireAllFeatures, requireAllTags, includeFeatures) {
				const listQuestionsPromise = $q.defer();

				let query = "";

				if (features !== undefined && features.length > 0) {
					query += (query ? "&" : "?") + "features=" + features.join("&features=") + "&requireAllFeatures=" + requireAllFeatures.toString();
					includeFeatures = true;
				}

				if (includeFeatures) {
					query += (query ? "&": "?") + "includes=FeatureMappings";
				}

				if (tags !== undefined && tags.length > 0) {
					query += (query ? "&" : "?") + "tags=" + tags.join("&tags=") + "&requireAllTags=" + requireAllTags.toString() + "&includes=TagMappings";
				}

				$http.get(String.uriFormat(observationalQuestionConfig.URIs.listQuestions + query)).then(
					questionsResponse => listQuestionsPromise.resolve(questionsResponse.data),
					errorResponse => listQuestionsPromise.reject(errorResponse));

				return listQuestionsPromise.promise;
			};

			return service;
		}]);