angular
	.module("observationalQuestion")
	.value("observationalQuestionConfig", {
		URIs: {
			// GET api/observationalquestionfeedback/list
			// GET api/observationalquestionfeedback/list?features=Happiness
			// GET api/observationalquestionfeedback/list?tags=interviewquestion1
			// GET api/observationalquestionfeedback/list?jobs=pilot
			listQuestions: "../api/observationalquestion/list"
		}
	});