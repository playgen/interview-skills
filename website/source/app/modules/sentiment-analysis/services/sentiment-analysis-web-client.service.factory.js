angular
	.module("sentimentAnalysis")
	.factory("SentimentAnalysisWebClient", [
		"sentimentAnalysisWebClientConfig",
		"sentimentAnalysisConfig",
		"$http",
		"$q",
		function (
			sentimentAnalysisWebClientConfig, 
			sentimentAnalysisConfig,
			$http, 
			$q) {
			const service = {};

			// public methods
			service.getLater = function (userSessionId, analysisSessionId, laterThan, params) {
				const promise = $q.defer();

				var uri = String.uriFormat(sentimentAnalysisWebClientConfig.URIs.laterAnalysis,
					userSessionId,
					analysisSessionId,
					laterThan);

				uri = appendFeatureExcludes(uri);

				$http.get(uri, params)
					.then(successResponse => {
						if (successResponse.status === StatusCodes.OK) {
							promise.resolve(successResponse.data);

						} else {
							Logger.debug("SentimentAnalysisWebClient: Non OK successful status code: \"" + successResponse.status + "\"");
							promise.resolve(null);							
						}
					},
					errorResponse => {
						Logger.error(errorResponse);
						promise.reject(errorResponse.statusText);
					});

				return promise.promise;
			};

			service.getAverages = function (userSessionId, analysisSessionId, start, end) {
				const promise = $q.defer();

				var uriTemplate = sentimentAnalysisConfig.useExclusive ? sentimentAnalysisWebClientConfig.URIs.exclusive.averages : sentimentAnalysisWebClientConfig.URIs.inclusive.averages;

				var uri = String.uriFormat(uriTemplate,
					userSessionId,
					analysisSessionId,
					start,
					end);

				uri = appendFeatureExcludes(uri);

				$http.get(uri)
					.then(successResponse => {
							if (successResponse.status === StatusCodes.OK) {
								promise.resolve(successResponse.data);

							} else {
								promise.resolve(null);
								Logger.warn("SentimentAnalysisWebClient: Non OK successful status code: \"" + successResponse.status + "\"");
							}
						},
						errorResponse => {
							Logger.error(errorResponse);
							promise.reject(errorResponse.statusText);
						});

				return promise.promise;
			};

			service.getRange = function (userSessionId, analysisSessionId, start, end) {
				const promise = $q.defer();

				var uriTemplate = sentimentAnalysisConfig.useExclusive ? sentimentAnalysisWebClientConfig.URIs.exclusive.range : sentimentAnalysisWebClientConfig.URIs.inclusive.range;

				var uri = String.uriFormat(uriTemplate,
					userSessionId,
					analysisSessionId,
					start,
					end);

				uri = appendFeatureExcludes(uri);

				$http.get(uri)
					.then(successResponse => {
							if (successResponse.status === StatusCodes.OK) {
								promise.resolve(successResponse.data);

							} else {
								promise.resolve(null);
							}
						},
						errorResponse => {
							Logger.error(errorResponse);
							promise.reject(errorResponse.statusText);
						});

				return promise.promise;
			};

			// private methods
			function appendFeatureExcludes(uri) {
				var key = sentimentAnalysisConfig.useExclusive ? sentimentAnalysisWebClientConfig.URIs.exclusive.key : sentimentAnalysisWebClientConfig.URIs.inclusive.key;
				var featureIds = sentimentAnalysisConfig.useExclusive ? sentimentAnalysisConfig.excludeFeatureIds : sentimentAnalysisConfig.includeFeatureIds;

				if (featureIds && featureIds.length > 0) {
					uri += "?" + key + "=";
					uri += featureIds.join("&" + key + "=");
				}

				return uri;
			}

			return service;
		}]);
