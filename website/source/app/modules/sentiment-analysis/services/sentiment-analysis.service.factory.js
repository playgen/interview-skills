angular
	.module("sentimentAnalysis")
	.factory("SentimentAnalysis", [
		"sentimentAnalysisConfig",
		"$timeout",
		"$q",
		function (
			sentimentAnalysisConfig, 
			$timeout, 
			$q) {
			const service = {};

			// public events
			service.analysisUpdateReceivedEvent = null;
			service.analysisUpdateErrorEvent = null;

			// private variables
			let userSessionId;
			let sessionId;
			let isInitialised;
			let isAnalyzing;
			let isListening;			
			let remoteCommands;

			// public methods
			service.initialize = function (inUserSessionId, inSessionId, RemoteCommands) {
				if (isInitialised) {
					Logger.warn("SentimentAnalysis is already initialised. Terminating previous.");
					service.terminate();
				}

				userSessionId = inUserSessionId;
				sessionId = inSessionId;
				service.analysisUpdateReceivedEvent = new Observable();
				service.analysisUpdateErrorEvent = new Observable();

				remoteCommands = RemoteCommands;

				isAnalyzing = false;
				isListening = false;
				isInitialised = true;
			};

			service.terminate = function () {
				if (!isInitialised) {
					Logger.warn("SentimentAnalysisListening is not initialised.");
					return;
				}

				let stopPromise = service.stopAnalyzingAndListening();
				
				service.analysisUpdateReceivedEvent = null;
				service.analysisUpdateErrorEvent = null;			

				isInitialised = false;

				return stopPromise;
			};


			service.startAnalyzingAndListening = function () {
				if (!isInitialised) {
					const error = "SentimentAnalysisListening is not initialised.";
					Logger.warn(error);					
					return $q.reject(error);
				}

				let analyzingProimse = service.startAnalyzing();
				let listeningPromise = service.startListening();
				return $q.all([analyzingProimse, listeningPromise]);
			};

			service.stopAnalyzingAndListening = function() {
				if (!isInitialised) {
					const error = "SentimentAnalysisListening is not initialised.";
					Logger.warn(error);					
					return $q.reject(error);
				}

				let analyzingProimse = service.stopAnalyzing();
				let listeningPromise = service.stopListening();
				return $q.all([analyzingProimse, listeningPromise]);
			};

			service.startAnalyzing = function () {
				if (!isInitialised) {
					const error = "SentimentAnalysisListening is not initialised.";
					Logger.warn(error);					
					return $q.reject(error);
				}

				if (!isAnalyzing) {
					isAnalyzing = true;
					
					let startPromise = remoteCommands.sendCommand(
						sentimentAnalysisConfig.remoteCommands.commands.startAnalyzing, 
						[sessionId], 
						true)
						.then(didStart => {
							if(didStart) {
								Logger.debug("SentimentAnalysis: Started analyzing.");
							} else {
								Logger.warn("SentimentAnalysis: Didn't start analyzing.");
							}
						}).catch(error => Logger.error(`SentimentAnalysis: ${error}`));

					return startPromise;

				} else {
					const error = "SentimentAnalysis: already analyzing.";
					Logger.warn(error);
					return $q.reject(error);
				}
			};

			service.stopAnalyzing = function () {			
				if (!isInitialised) {
					Logger.warn("SentimentAnalysisListening is not initialised.");
					return;
				}

				isAnalyzing = false;
				
				let stopPromise = remoteCommands.sendCommand(
					sentimentAnalysisConfig.remoteCommands.commands.stopAnalyzing, 
					[sessionId],
					false,
					[sentimentAnalysisConfig.remoteCommands.commands.startAnalyzing])
					.then(didStop => {
						if(didStop) {
							Logger.debug("SentimentAnalysis: Stopped analyzing.");
						} else {
							Logger.warn("SentimentAnalysis: Didn't stop analyzing.");
						}
					}).catch(error => Logger.error(`SentimentAnalysis: ${error}`));
				
				return stopPromise;
			};

			service.startListening = function () {
				if (!isInitialised) {
					Logger.warn("SentimentAnalysisListening is not initialised.");
					return;
				}

				if (!isListening) {
					isListening = true;					
					
					let startPromise = remoteCommands.sendCommand(
						sentimentAnalysisConfig.remoteCommands.commands.startListening, 
						[userSessionId], 
						true)
						.then(didStart => {
							if(didStart) {
								Logger.debug("SentimentAnalysis: Started listening.");
							} else {
								Logger.warn("SentimentAnalysis: Didn't start listening.");
							}
						}).catch(error => Logger.error(`SentimentAnalysis: ${error}`));

					remoteCommands.updateReceivedEvent.subscribe(onRemoteUpdateReceived);

					return startPromise;

				} else {
					const error = "SentimentAnalysis: already listening.";
					
					Logger.warn(error);
					return $q.reject(error);
				}
			};

			service.stopListening = function () {				
				if (!isInitialised) {
					Logger.warn("SentimentAnalysisListening is not initialised.");
					return;
				}

				isListening = false;
				
				let stopPromise = remoteCommands.sendCommand(
					sentimentAnalysisConfig.remoteCommands.commands.stopListening,
					 [userSessionId],
					false,
					[sentimentAnalysisConfig.remoteCommands.commands.startListening])
					.then(didStop => {
						if(didStop) {
							Logger.debug("SentimentAnalysis: Stopped listening.");
						} else {
							Logger.warn("SentimentAnalysis: Didn't stop listening.");
						}
					}).catch(error => Logger.error(`SentimentAnalysis: ${error}`));

				remoteCommands.updateReceivedEvent.unsubscribe(onRemoteUpdateReceived);

				return stopPromise;
			};

			// private methods
			function onRemoteUpdateReceived(remoteUpdate) {
				if(remoteUpdate.id === sentimentAnalysisConfig.remoteCommands.updates.analysisResultUpdate) {
					if(!isListening) {
						Logger.warn("SentimentAnalysis: Listening has been stopped.");
						return;
					}

					var analysisMetadata = remoteUpdate.result.analysis;
					var debugInfo = remoteUpdate.result.debugInfo;

					if(debugInfo) {
						const clientReceived = Date.now();
						debugInfo.client = [["received", clientReceived]];						

						var recordingStarted = Date.parse(analysisMetadata.recordingStarted);
						debugInfo.turnaround = clientReceived - recordingStarted;				


						Logger.debug(`SentimentAnalysis: Received Remote Update. Turnaround time: ${debugInfo.turnaround} ms. Recording Started: ${analysisMetadata.recordingStarted}.` +
						` With ${analysisMetadata.featureResults ? Object.keys(analysisMetadata.featureResults).length : 0} results` + 
						` and ${analysisMetadata.errors ? analysisMetadata.errors.length : 0} errors.`);

						debugInfo.recording.splice(0, 0, ["recordingStarted", recordingStarted]);
						debugInfo.recording.splice(1, 0, ["recordingEnded", Date.parse(analysisMetadata.recordingEnded)]);
						const phaseOrder = ["recording", "analysis", "is-server", "client"];
						const deltas = [];	
						var summedDeltas = 0;											
						for(var i = 0; i < phaseOrder.length; i++) {
							var currentPhase = phaseOrder[i];
							var currentPhaseDebugInfo = debugInfo[currentPhase];

							if(i > 0) {
								var previousPhase = phaseOrder[i - 1];
								var previousDebugInfo = debugInfo[previousPhase];
								var previousPhaseLastTask = previousDebugInfo[previousDebugInfo.length - 1];
								var currentPhaseFirstTask = currentPhaseDebugInfo[0];								
								var currentPhaseDelta = currentPhaseFirstTask[1] - previousPhaseLastTask[1];
								summedDeltas += currentPhaseDelta;
								deltas.push(`${previousPhase}.${previousPhaseLastTask[0]}->${currentPhase}.${currentPhaseFirstTask[0]} = ${currentPhaseDelta} ms.`);
							}
														
							for(var j = 1; j < currentPhaseDebugInfo.length; j++){
								var previousTask = currentPhaseDebugInfo[j - 1];
								var currentTask = currentPhaseDebugInfo[j];
								var delta = currentTask[1] - previousTask[1];
								summedDeltas += delta;
								deltas.push(`${currentPhase}.${previousTask[0]}->${currentPhase}.${currentTask[0]} = ${delta} ms.`);
							}
						}

						//Logger.debug(debugInfo);
						Logger.debug(deltas);			
						var difference = debugInfo.turnaround - summedDeltas;
						if(difference !== 0) {
							Logger.warn(`Turnaround - summed deltas = ${difference} ms. These should be the same.`);
						}						
					}			
					
					if(analysisMetadata.featureResults && analysisMetadata.featureResults.length > 0) {
						if (sentimentAnalysisConfig.useExclusive) {						
							analysisMetadata.featureResults = analysisMetadata.featureResults.filter(fr => sentimentAnalysisConfig.excludeFeatureIds.indexOf(fr.featureId) < 0);
						} else {
							analysisMetadata.featureResults = analysisMetadata.featureResults.filter(fr => sentimentAnalysisConfig.includeFeatureIds.indexOf(fr.featureId) >= 0);
						}
					}

					service.analysisUpdateReceivedEvent.publish(analysisMetadata);
				}
			}

			return service;
		}]);