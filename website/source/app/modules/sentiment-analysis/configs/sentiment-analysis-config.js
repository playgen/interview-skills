angular
	.module("sentimentAnalysis")
	.value("sentimentAnalysisConfig", {
		remoteCommands: {
			commands: {
				startAnalyzing: "SentimentAnalysisController.StartAnalyzing",
				stopAnalyzing: "SentimentAnalysisController.StopAnalyzing",
				startListening: "SentimentAnalysisController.StartListening",
				stopListening: "SentimentAnalysisController.StopListening"				
			}, updates: {
				analysisResultUpdate: "SentimentAnalysisResultUpdate"
			}
		},
		useExclusive: false,		
		excludeFeatureIds: ["deception"],		
		includeFeatureIds: ["friendliness", "interest", "positivity", "stress", "sincerity"]
	});