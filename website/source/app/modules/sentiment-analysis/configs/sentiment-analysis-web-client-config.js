angular
	.module("sentimentAnalysis")
	.value("sentimentAnalysisWebClientConfig", {
		URIs: {
			// Get api/sentimentanalysis/later/[user session id]/[open tok session id]/[later than time]
			// e.g: api/sentimentanalysis/later/82762/10/123797312
			laterAnalysis: "../api/sentimentanalysis/later/{0}/{1}/{2}",

			// Get api/sentimentanalysis/all/[user session id]/[open tok session id]
			// e.g: api/sentimentanalysis/all/82762/10/
			allAnalyses: "../api/sentimentanalysis/all",

			exclusive: {
				// GET api/sentimentanalysis/AveragesExclusive/userSessionId/matchSessionId/start/end
				// GET api/sentimentanalysis/AveragesExclusive/userSessionId/matchSessionId/start/end?excludeFeatureIds=happiness&excludeFeatureIds=boredom
				averages: "../api/sentimentanalysis/AveragesExclusive/{0}/{1}/{2}/{3}",

				// GET api/SentimentAnalysis/RangeExclusive/[userSessionId]/[matchSessionId]/[start]/[end]
				// GET api/SentimentAnalysis/RangeExclusive/[userSessionId]/[matchSessionId]/[start]/[end]?excludeFeatureIds=[happiness]&excludeFeatureIds=[boredom]
				range: "../api/sentimentanalysis/RangeExclusive/{0}/{1}/{2}/{3}",

				key: "excludeFeatureIds",
			},

			inclusive: {
				// GET api/sentimentanalysis/AveragesInclusive/userSessionId/matchSessionId/start/end
				// GET api/sentimentanalysis/AveragesInclusive/userSessionId/matchSessionId/start/end?excludeFeatureIds=happiness&excludeFeatureIds=boredom
				averages: "../api/sentimentanalysis/AveragesInclusive/{0}/{1}/{2}/{3}",

				// GET api/SentimentAnalysis/RangeInclusive/[userSessionId]/[matchSessionId]/[start]/[end]
				// GET api/SentimentAnalysis/RangeInclusive/[userSessionId]/[matchSessionId]/[start]/[end]?excludeFeatureIds=[happiness]&excludeFeatureIds=[boredom]
				range: "../api/sentimentanalysis/RangeInclusive/{0}/{1}/{2}/{3}",
				
				key: "includeFeatureIds",
			}
		}		
	});