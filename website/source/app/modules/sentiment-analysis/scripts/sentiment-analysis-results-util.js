﻿class SentimentAnalysisResultsUtil {
	static sortResults(analysisResults) {
		const sortedResults = analysisResults
			.slice(0)
			.sort((a, b) => b.weightFromIdeal - a.weightFromIdeal);
		return sortedResults;
	}
}