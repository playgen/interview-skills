angular
    .module("scenarioPlayer")
    .value("scenarioStepperConfig", {
        timeout: null,
		latencyLeeway: 1000, // In Milliseconds
		clientTimeout: 60000 // In Milliseconds
    });