angular
	.module("scenarioPlayer")
	.value("scenarioPlayerConfig", {
		tickInterval: 1000, // In milliseconds,
		commandHandler: null,
		conditionEvaluator: null,
		timeout: null
	});