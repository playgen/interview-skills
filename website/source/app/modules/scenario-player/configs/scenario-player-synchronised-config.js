angular
	.module("scenarioPlayer")
	.value("scenarioPlayerSynchronisedConfig", {
		sendSignal: null,
		signalReceivedEvent: null,
		clientId: null,
        otherClientIds: null,
		sendStartedInterval: 1 * 1000,
		clientTimeout: 10 * 1000
	});