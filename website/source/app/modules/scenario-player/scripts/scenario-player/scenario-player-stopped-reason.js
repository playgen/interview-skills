﻿class ScenarioPlayerStoppedReason {
	static get THIS_TIMED_OUT() {
		return "TIMED_OUT";
	}

	static get OTHER_TIMED_OUT() {
		return "OTHER_TIMED_OUT";
	}

	static get THIS_QUIT() {
		return "THIS_QUIT";
	}

	static get OTHER_QUIT() {
		return "OTHER_QUIT";
	}
}