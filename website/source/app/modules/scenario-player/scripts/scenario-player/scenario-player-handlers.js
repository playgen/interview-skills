class FunctionLookup {
	constructor() {
		this._functions = {};
	}

	register(key, func) {
		if (key in this._functions) {
			throw "Key: " + key + " is already registered.";
		}

		if (typeof func !== "function") {
			throw "Passed in object does not appear to be a valid function: " + func;
		}

		this._functions[key] = func;
	}

	get(key) {
		if (key in this._functions === false) {
			throw "Key: \"" + key + "\" has no registered function.";
		}

		return this._functions[key];
	}
}

class ScenarioCommandHandler extends FunctionLookup {
	constructor() {
		super();

		this.register(ScenarioCommandHandler.enqueueActionsType, (command, stepPlayer) => {
			command.actions.forEach(action => {
				stepPlayer.addAction(action);
			});

			// todo: do additional tick here
		});

		this.register(ScenarioCommandHandler.stopType, (command, stepPlayer) => {
			stepPlayer.stop();
		});
	}

	static get enqueueActionsType() {
		return "EnqueueActions";
	}

	static get stopType() {
		return "Stop";
	}
}

class SynchronisedScenarioCommandHandler extends ScenarioCommandHandler {
	constructor() {
		super();
	}
}

class ScenarioConditionEvaluator extends FunctionLookup {
	constructor() {
		super();

		this.register("Any", (condition, stepPlayer) => {
			return stepPlayer.isAnyConditionSatisfied(condition.conditions);
		});

		this.register("None", (condition, stepPlayer) => {
			return !stepPlayer.isAnyConditionSatisfied(condition.conditions);
		});
	}
}

class SynchronisedScenarioConditionEvaluator extends ScenarioConditionEvaluator {
	constructor() {
		super();

		this.register("HasOtherLeft", (condition, stepPlayer, context) => {
			return context.getIsAnyOtherClientState(ClientState.STOPPED) ||
				context.getIsAnyOtherClientState(ClientState.TIMED_OUT) ||
				context.getHasTimedOut();
		});
	}
}