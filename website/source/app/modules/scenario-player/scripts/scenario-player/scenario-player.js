class ScenarioPlayer {
	constructor(initParams) {
		// public events
		this.completedEvent = new Observable();
		this.stoppedEvent = new Observable();

		// private variables
		this._timeout = initParams.timeout;
		this._tickInterval = initParams.tickInterval;
		this._steps = initParams.steps;
		this._conditionEvaluator = initParams.conditionEvaluator;
		this._conditionContext = initParams.conditionContext !== undefined ? initParams.conditionContext : {};
		this._commandHandler = initParams.commandHandler;
		this._index = -1;
		this._stepPlayer = null;
		this._tickTimer = null;
		this._isPlaying = false;
		this._isDisposing = false;
		this._isDisposed = false;
	}

	// public methods
	start() {
		this._isPlaying = true;
		this._nextStep();
	}

	stop() {
		if (this._isPlaying) {
			this._stopPlaying();
			this.stoppedEvent.publish();
		} else {
			Logger.debug("ScenarioPlayer: Already stopped.");
		}
	}

	dispose() {
		if (this._isDisposing || this._isDisposed) return;

		this._isDisposing = true;
		this._stopPlaying();
		this._isDisposed = true;
	}

	// private methods
	_stopPlaying() {
		this._timeout.cancel(this._tickTimer);
		this._isPlaying = false;
		this._tickTimer = null;
	}

	_nextStep() {
		this._goToStep(this._index + 1);
	}

	_goToStep(index) {
		if (this._stepPlayer) {
			this._stepPlayer.exit();
		}

		this._index = index;
		if (index < this._steps.length) {
			const step = this._steps[index];
			this._startStep(step);
		} else {
			this.completedEvent.publish();
		}
	}

	_tickStep() {
		const wasBlocked = this._stepPlayer.isBlocked;

		this._stepPlayer.tick();

		if (this._isPlaying) {

			this._timeout.cancel(this._tickTimer);
			this._tickTimer = this._timeout(this._tickStep.bind(this), this._tickInterval);

			if (this._stepPlayer.processedCount === 1 || wasBlocked !== this._stepPlayer.isBlocked) {
				if (this._stepPlayer.isBlocked) {
					this._onStepBlocked();
				} else {
					this._onStepUnblocked();
				}
			}
		}
	}

	_startStep(step) {
		this._stepPlayer = new ScenarioStepPlayer({
			conditionEvaluator: this._conditionEvaluator,
			conditionContext: this._conditionContext,
			commandHandler: this._commandHandler,
			step: step,
			onStop: this.stop.bind(this)
		});

		this._tickStep();
	}

	_onStepUnblocked() {
		this._nextStep();
	}

	_onStepBlocked() {
	}
}

class SynchronisedScenarioPlayer extends ScenarioPlayer {
	constructor(initParams) {
		super(initParams);

		// private variables
		this._sendSignal = initParams.sendSignal;
		this._signalReceivedEvent = initParams.signalReceivedEvent;
		this._sendStartedInterval = initParams.sendStartedInterval;
		this._masterClientId = initParams.masterClientId;
		this._clientId = initParams.clientId;
		this._clientTimeout = initParams.clientTimeout;
		this._lastPong = null;
		this._isDisposed = false;
		this._sendStartedTimer = null;
		this._clientStates = {};

		// context
		this._conditionContext.getClientStates = () => this.clientStates;
		this._conditionContext.getIsAnyOtherClientState = state => this._isAnyOtherClientState(state);
		this._conditionContext.getHasTimedOut = () => this.hasTimedOut;
	}

	// properties
	get clientStates() {
		return this._clientStates;
	}

	get hasTimedOut() {
		return this._lastPong < Date.now() - this._clientTimeout;
	}

	// public methods
	// overrides
	start() {
		this._isPlaying = true;
		this._signalReceivedEvent.subscribe(this._onSignalReceived, this);
		this._lastPong = Date.now();
		this._goToStep(0);
		this._sendStarted();
	}

	stop() {
		if (this._isAnyOtherClientState(ClientState.STOPPED) ||
			this._isAnyOtherClientState(ClientState.TIMED_OUT) ||
			this.hasTimedOut) {
			this._stopPlaying();
			this._immediateStop();

		} else {
			this._requestStop();
		}
	}

	dispose() {
		if (this._isDisposed) return;

		this._disconnect();
		super.dispose();
	}

	// private methods
	_isAnyOtherClientState(state) {
		const otherClientStates = Object.assign({}, this._clientStates);
		delete otherClientStates[this._clientId];
		return Object.areAnyValues(otherClientStates, state);
	}

	_requestStop() {
		this._stopPlaying();
		this._sendStateSignal("RequestingStop", status => {
			if (status !== SendSignalStatus.SENT) {
				this._disconnect();
				this.stoppedEvent.publish(ScenarioPlayerStoppedReason.THIS_QUIT);
			}
		});
	}

	_immediateStop() {
		const reason = this._getStoppedReason();

		this._sendStateSignal("Stopped");
		this._disconnect();
		this.stoppedEvent.publish(reason);
	}

	_getStoppedReason() {
		let reason;

		if (this._isAnyOtherClientState(ClientState.STOPPED)) {
			reason = ScenarioPlayerStoppedReason.OTHER_QUIT;

		} else if (this._isAnyOtherClientState(ClientState.TIMED_OUT)) {
			reason = ScenarioPlayerStoppedReason.OTHER_TIMED_OUT;

		} else if (this.hasTimedOut) {
			reason = ScenarioPlayerStoppedReason.THIS_TIMED_OUT;

		} else {
			reason = ScenarioPlayerStoppedReason.THIS_QUIT;
		}

		return reason;
	}


	_disconnect() {
		this._cancelSendStartedTimer();
		this._signalReceivedEvent.unsubscribe(this._onSignalReceived);
	}

	_sendStateSignal(state, callback = null) {
		this._sendSignal("SynchronisedScenarioStepper.State", { state: state }, [this._masterClientId], callback);
	}

	_sendStarted() {
		this._sendStateSignal("Started");
		this._sendStartedTimer = this._timeout(() => {
			this._sendStarted();
		}, this._sendStartedInterval);
	}

	_cancelSendStartedTimer() {
		if (this._sendStartedTimer !== null) {
			this._timeout.cancel(this._sendStartedTimer);
			this._sendStartedTimer = null;
		}
	}

	_onSignalReceived(signal) {
		if (signal.type !== "SynchronisedScenarioPlayer.Command") return;

		this._lastPong = Date.now();

		switch (signal.payload.command) {
			case "StartedAcknowledged":
				this._cancelSendStartedTimer();
				break;

			case "GoToStep":
				this._timeout.cancel(this._tickTimer);
				this._goToStep(signal.payload.index);
				break;

			case "Stop":
				this._immediateStop();
				break;

			case "SetClientState":
				this._clientStates[signal.payload.clientId] = signal.payload.state;
				break;

			case "Pong":
				break;

			default:
				throw "Unhandled switch case: " + signal.payload;
		}
	}

	// overrides
	_startStep(step) {
		Logger.debug("SynchronisedScenarioPlayer._startStep: Step Id: \"" + step.id + "\".");

		this._stepPlayer = new SynchronisedScenarioStepPlayer({
			conditionEvaluator: this._conditionEvaluator,
			conditionContext: this._conditionContext,
			commandHandler: this._commandHandler,
			step: step,
			onStop: () => this.stop(),
			onTicked: () => this._sendStateSignal("Ping")
		});

		this._tickStep();
	}

	_onStepUnblocked() {
		this._sendStateSignal("StepUnblocked");
	}

	_onStepBlocked() {
		this._sendStateSignal("StepBlocked");
	}
}