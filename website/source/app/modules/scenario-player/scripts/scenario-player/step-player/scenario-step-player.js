class ScenarioStepPlayer {
	constructor(initParams) {
		// private variables		
		this._conditionEvaluator = initParams.conditionEvaluator;
		this._conditionContext = initParams.conditionContext;
		this._commandHandler = initParams.commandHandler;
		this._onTicked = initParams.onTicked;
		this._step = initParams.step;
		this._actions = this._step.actions === null ? [] : this._step.actions.slice(0);
		this._processedCount = 0;
		this._onStop = initParams.onStop;
	}

	// public methods	
	exit() {
		this._actions.forEach(action => {
			this._processCommands(action.onDiscardCommands, true);
		});
	}

	tick() {
		let actionCount = this._actions.length; // Action count stored here as it may increase as a result of more actions being enqueued in commands
		for (let i = 0; i < actionCount; i++) {

			const action = this._actions[i];

			const discard = this._processAction(action);

			if(discard) {
				this._actions.splice(i, 1);
				i--;
				actionCount--;

				this._processCommands(action.onDiscardCommands, true);
			}
		}
		
		this._processedCount++;

		if (this._onTicked) {
			this._onTicked();
		}
	}

	stop() {
		this._onStop();
	}

	get processedCount() {
		return this._processedCount;
	}

	get isBlocked() {
		const blockingActions = this._actions.filter(action => {
			return action.config.blockingType === ScenarioActionBlockingType.step;
		});

		return blockingActions.length > 0;
	}

	addAction(newAction) {
		const action = JSON.parse(JSON.stringify(newAction));
		const discard = this._processAction(action);
		if (discard) {
			this._processCommands(action.onDiscardCommands, true);
		} else {
			this._actions.push(action);
		}
	}

	// private methods
	static _getShortType(fullname) {
		const split1 = fullname.split(",");
		const split2 = split1[0].split(".");
		return split2[split2.length - 1];
	}

	_processAction(action) {
		let discard = action.config.discardType === ScenarioActionDiscardType.firstEvaluation;

		if (this.areAllConditionsSatisfied(action.conditions)) {
			this._processCommands(action.onConditionCommands, false);

			if (action.config.discardType === ScenarioActionDiscardType.firstTrue) {
				discard = true;
			}
		}

		return discard;
	}

	areAllConditionsSatisfied(conditions) {
		let allSatisfied = true;

		if(conditions !== null) {
			conditions.forEach(condition => {
				allSatisfied &= this._isConditionSatisfied(condition);
			});
		}

		return allSatisfied;
	}

	isAnyConditionSatisfied(conditions) {
		let anySatisfied = false;

		if(conditions !== null) {
			for(let condition of conditions) {
				if(this._isConditionSatisfied(condition)) {
					anySatisfied = true;
					break;
				}
			}
		}

		return anySatisfied;
	}

	_isConditionSatisfied(condition) {			
		const type = ScenarioStepPlayer._getShortType(condition.$type);
		const evaluate = this._conditionEvaluator.get(type);
		return evaluate(condition, this, this._conditionContext);
	}

	_processCommands(commands, isExitingStep) {
		if(commands !== null) {
			commands.forEach(command => {
				this._processCommand(command, isExitingStep);
			});
		}
	}

	_processCommand(command, isExitingStep) {
		const type = ScenarioStepPlayer._getShortType(command.$type);

		if (type === ScenarioCommandHandler.enqueueActionsType && isExitingStep) {
			Logger.error("Can't process \"" + ScenarioCommandHandler.enqueueActionsType + "\" command when exiting the step.");
			
		} else {
			const process = this._commandHandler.get(type);
			try {
				process(command, this);

			} catch (error) {
				Logger.error("Error processing command type: \"" + type + "\".");
				Logger.error(error);
			}
		}
	}
}

class SynchronisedScenarioStepPlayer extends ScenarioStepPlayer {
	constructor(initParams) {
		super(initParams);
	}
}