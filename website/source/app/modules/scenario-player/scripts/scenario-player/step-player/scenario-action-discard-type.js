class ScenarioActionDiscardType {
	static get firstEvaluation() {
		return "firstEvaluation";
	}

	static get firstTrue() {
		return "firstTrue";
	}

	static get never() {
		return "never";
	}
}