class SynchronisedScenarioStepper {
	constructor(initParams) {
		// private varaibles
		this._isDisposed = false;
		this._clientStates = {};
		this._signalReceivedEvent = initParams.signalReceivedEvent;
		this._sendSignal = initParams.sendSignal;
		this._clientIds = initParams.clientIds;
		this._timeout = initParams.timeout;
		this._clientTimeout = initParams.clientTimeout;
		this._latencyLeeway = initParams.latencyLeeway;
		this._nextStepTimer = null;

		// initialization
		this._setClients(this._clientIds);
		this._signalReceivedEvent.subscribe(this._onStateSignalReceived, this);
		this._stepIndex = 0;
	}

	// public methods 
	dispose() {
		if (this._isDisposed) return;

		this._signalReceivedEvent.unsubscribe(this._onStateSignalReceived);
		this._isDisposed = true;
	}

	// private methods
	_setClients(clientIds) {
		this._clientStates = {};

		clientIds.forEach(clientId => {
			this._clientStates[clientId] = {};
			this._updateClientState(clientId, ClientState.UNINITIALIZED);
		});
	}

	_onStateSignalReceived(signal) {
		if (signal.type !== "SynchronisedScenarioStepper.State") return;

		switch (signal.payload.state) {
			case "Started":
				if (this._clientStates[signal.sender].state === ClientState.UNINITIALIZED) {
					this._updateClientState(signal.sender, ClientState.STARTED);

					if (this._areAllClientStates(ClientState.STARTED)) {
						this._nextStep();
					}
				} else {
					Logger.debug("SynchronisedScenarioStepper: " + signal.sender + " is not in the \"Uninitialised\" state. Ignoring messages.");
				}

				this._sendSignal("SynchronisedScenarioPlayer.Command", { command: "StartedAcknowledged" }, [signal.sender]);
				break;

			case "StepUnblocked":
				this._setClientState(signal.sender, ClientState.UNBLOCKED);

				if (this._areAllClientStates(ClientState.UNBLOCKED) && this._nextStepTimer === null) {
					this._startNextStepTimeout();
				}
				break;

			case "StepBlocked":
				this._timeout.cancel(this._nextStepTimer);
				this._nextStepTimer = null;
				this._updateClientState(signal.sender, ClientState.BLOCKED);
				break;

			case "RequestingStop":
				this._updateClientState(signal.sender, ClientState.STOPPED);
				this._sendSignal("SynchronisedScenarioPlayer.Command", { command: "Stop" }, [signal.sender]);
				break;

			case "Stopped":
				this._updateClientState(signal.sender, ClientState.STOPPED);
				break;

			case "Ping":
				this._clientStates[signal.sender].lastPing = Date.now();
				this._checkClientsTimedOut(signal.sender);
				this._sendSignal("SynchronisedScenarioPlayer.Command", { command: "Pong" }, [signal.sender]);
				break;

			default:
				throw "Unhandled switch case: " + signal.payload;
		}
	}

	_areAllClientStates(state) {
		return Object.keys(this._clientStates).every(clientId => this._clientStates[clientId].state === state);
	}

	_startNextStepTimeout() {
		this._nextStepTimer = this._timeout(() => {
			this._nextStepTimer = null;
			this._nextStep();
		}, this._latencyLeeway);
	}

	_nextStep() {
		this._goToStep(this._stepIndex + 1);
	}

	_updateClientState(clientId, state) {
		this._clientStates[clientId].lastPing = Date.now();
		this._setClientState(clientId, state);
		this._checkClientsTimedOut();
	}

	_checkClientsTimedOut() {
		const timeoutPoint = Date.now() - this._clientTimeout;

		for (let clientId in this._clientStates) {
			if (this._clientStates[clientId].lastPing < timeoutPoint) {
				this._setClientState(clientId, ClientState.TIMED_OUT);
			}
		}
	}

	_setClientState(clientId, state) {
		this._clientStates[clientId].state = state;

		this._sendSignal("SynchronisedScenarioPlayer.Command", {
			command: "SetClientState",
			clientId: clientId,
			state: state
		}, this._clientIds);
	}

	_setAllClients(state) {
		for (let clientId in this._clientStates) {
			this._setClientState(clientId, state);
		}
	}

	_goToStep(index) {
		this._stepIndex = index;
		this._setAllClients(ClientState.PROCESSING_STEP);

		this._sendSignal("SynchronisedScenarioPlayer.Command", { command: "GoToStep", index: index }, this._clientIds);
	}
}