﻿class ClientState {
	static get UNINITIALIZED() {
		return "Uninitialised";
	}

	static get STARTED() {
		return "Started";
	}

	static get STOPPED() {
		return "Stopped";
	}

	static get BLOCKED() {
		return "Blocked";
	}
	
	static get UNBLOCKED() {
		return "Unblocked";
	}

	static get PROCESSING_STEP() {
		return "ProcessingStep";
	}

	static get TIMED_OUT() {
		return "TimedOut";
	}
}