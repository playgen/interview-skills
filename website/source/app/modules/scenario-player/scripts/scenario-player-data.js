﻿class ScenarioPlayerData {
	constructor(keyedValues) {
		this._keyedValues = keyedValues;
	}

	getValue(key, isSynchronised) {
		if (isSynchronised && this._keyedValues.synchronised.hasOwnProperty(key)) {
			return this._keyedValues.synchronised[key];
		} else {
			return this._keyedValues[key];
		}
	}
}