angular
	.module("scenarioPlayer")
	.factory("ScenarioStepper", [
		"scenarioStepperConfig",
		"$timeout",
		function (scenarioStepperConfig, $timeout) {
			const service = {};

			// private variables
			var scenarioStepper = null;
			let isInitialised = false;

			// public methods
			service.initialize = function (sendSignl, signalReceivedEvent, clientIds) {
				if (isInitialised) {
					Logger.warn("ScenarioStepper is already initialised. Terminating previous.");
					service.terminate();
				} 

				scenarioStepper = new SynchronisedScenarioStepper({
					clientIds: clientIds,
					sendSignal: sendSignl,
					signalReceivedEvent: signalReceivedEvent,
					timeout: $timeout,
					clientTimeout: scenarioStepperConfig.clientTimeout,
					latencyLeeway: scenarioStepperConfig.latencyLeeway
				});

				isInitialised = true;
			};

			service.terminate = function () {
				if (!isInitialised) {
					Logger.warn("ScenarioStepper is not initialised.");
					return;
				}

				if (scenarioStepper !== null) {
					scenarioStepper.dispose();
					scenarioStepper = null;
				}

				isInitialised = false;
			};

			return service;
		}]);