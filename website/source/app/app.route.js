angular
    .module("app")
    .config(["$urlRouterProvider", function($urlRouterProvider) {
        $urlRouterProvider.otherwise("/loggedin/home");
    }]);