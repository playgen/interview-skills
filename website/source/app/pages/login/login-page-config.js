﻿angular
	.module("app")
	.value("loginPageConfig", {
		description:
		"Welcome to the alpha version of interviewBetter!" +
		"<br/>" +
		"This game is designed to improve your job interview skills using human and AI feedback." +
		"<h3>How the game works:</h3>" +
		"<ul class='stars'>" +
		"<li>In a two player game, each person is assigned a role; either candidate or interviewer.</li>" +
		"<li>As candidate you pick the role, answer the questions and get feedback from the AI and other player.</li>" +
		"<li>As interviewer you ask prompted questions and rate the candidate.</li>" +
		"<li>The roles then swap around.</li>" +
		"<li>In solo play, you select the job, answer the questions and get feedback from the AI.</li>" +
		"</ul>" +
		"<br/>" +
		"<div class=\"play-flow\"></div>" +
		"<br/>" +
		"This is the first version of the game and we welcome your <a href=\"http://interviewbetter.co.uk/contact-us\" target=\"_blank\">thoughts and suggestions</a> on how to improve things.",
		authenticationTypes: [
			"email",
			"google",
			"guest"
		],
		minChromeVersion: 64
	});