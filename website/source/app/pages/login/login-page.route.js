angular
	.module("app")
	.config(["$stateProvider", function ($stateProvider) {
		$stateProvider
			.state("login", {
				url: "/login",
				component: "loginPage",
				onEnter: [
					"appContext",
					function (appContext) {
						appContext.user = undefined;
					}]
			});
	}]);