angular
	.module("app")
	.component("loginPage", {
		templateUrl: "/pages/login/login-page.html",
		controller: [
			"$state",
			"loginPageConfig",
			"appContext",
			"UserData",
			function ($state, loginPageConfig, appContext, UserData) {
				const ctrl = this;

				// public variables
				ctrl.description = loginPageConfig.description;
				ctrl.authenticationTypes = loginPageConfig.authenticationTypes;
				ctrl.chrome = {
					isChrome: BrowserType.isChrome,
					currentVersion: BrowserType.chromeVersion,
					minVersion: loginPageConfig.minChromeVersion
				};
				
				ctrl.isWebCamAvailable = true;

				// public methods
				ctrl.onAvailable = function () {
					ctrl.isWebCamAvailable = true;
				};

				ctrl.onUnavailable = function (error) {
					ctrl.isWebCamAvailable = false;
					Logger.error(error);
				};

				// public methods
				ctrl.onLoggedIn = function (user) {
					appContext.user = user;
					appContext.user.session = UserData.getSession(user);

					Logger.debug("Logged in user session: " + appContext.user.session.id);
					//$state.go("loggedinHome");
					$state.go("loggedinPlayMultiFindMatch", { cantMatchPrevious: false });
				};
			}
		]
	});