angular
	.module("app")
	.component("loggedinPlayMultiFindMatchPage", {
		templateUrl: "/pages/loggedin/play-multi/find-match/loggedin-play-multi-find-match-page.html",
		controller: [
			"appContext",
			"$state",
			"$stateParams",
			"MatchData",
			"$timeout",
			"loggedinPlayMultiFindMatchConfig",
			"MessageDialog",
			function (
				appContext,
				$state,
				$stateParams,
				MatchData,
				$timeout,
				loggedinPlayMultiFindMatchConfig,
				MessageDialog) {
				const ctrl = this;

				// public variables
				ctrl.participantId = null;
				ctrl.cantMatchPrevious = null;
				ctrl.playSolo = false;

				// private variables
				let playSoloTimeout = null;

				// angular hooks
				ctrl.$onInit = function () {
					ctrl.participantId = appContext.user.session.id;
					ctrl.cantMatchPrevious = $stateParams.cantMatchPrevious;

					playSoloTimeout = $timeout(playSolo, loggedinPlayMultiFindMatchConfig.playSoloTimeout);
				};

				ctrl.$onDestroy = function () {
					if (playSoloTimeout) {
						$timeout.cancel(playSoloTimeout);
						playSoloTimeout = null;
					}
				};

				// public methods
				ctrl.onSoloScenarioPlayerStopped = function () {
					if (ctrl.playSolo) {
						ctrl.playSolo = false;
						playSoloTimeout = $timeout(playSolo, loggedinPlayMultiFindMatchConfig.waitForMultiTimeout);
					}
				};

				ctrl.soloNewGame = function() {
					if (ctrl.playSolo) {
						ctrl.playSolo = false;
						playSolo();
					}
				};

				ctrl.onFoundMatch = function (match) {
					if (playSoloTimeout) {
						$timeout.cancel(playSoloTimeout);
						playSoloTimeout = null;
					}

					if (MessageDialog.getIsVisible()) {
						MessageDialog.hide();
					}

					if (ctrl.playSolo) {
						ctrl.playSolo = false;
						MessageDialog.show(
							loggedinPlayMultiFindMatchConfig.playMultiTitle,
							loggedinPlayMultiFindMatchConfig.playMultiBody,
							loggedinPlayMultiFindMatchConfig.playMultiTimeout)
							.then(() => playMulti(match));

					} else {
						playMulti(match);
					}
				};

				// private methods
				function playSolo() {
					MessageDialog.show(
						loggedinPlayMultiFindMatchConfig.playSoloTitle,
						loggedinPlayMultiFindMatchConfig.playSoloBody,
						loggedinPlayMultiFindMatchConfig.playSoloTimeout)
						.then(() => ctrl.playSolo = true);
				}

				function playMulti(match) {
					appContext.user.session.match = match;
					appContext.user.session.match.session = MatchData.getSession(match);

					Logger.debug("Got match id: " + appContext.user.session.match.session.id);

					$timeout(() => $state.go("loggedinPlayMultiScenario"));
				}
			}]
	});