﻿angular
	.module("app")
	.value("loggedinPlayMultiFindMatchConfig", {
		waitForMultiTimeout: 60 * 1000,
		playSoloTimeout: 10 * 1000,
		playSoloTitle: "Waiting for Match",
		playSoloBody: "You will be put into a Solo game while you wait for a match." +
			" \n\nWhen a match becomes available, we will notify you and you'll be placed in it automatically.",
		playMultiTimeout: 10 * 1000,
		playMultiTitle: "Found Match",
		playMultiBody: "Great news:" +
			" \nWe've found you a match!" +
			" \n" +
			" \nYou will now be put into the match."
	});