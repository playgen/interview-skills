angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("loggedinPlayMultiFindMatch", {
                url: "/find-match",
                component: "loggedinPlayMultiFindMatchPage",
				parent: "loggedinPlayMulti",
				params: {
					cantMatchPrevious: false
				}
            });
    }]);