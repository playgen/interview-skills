angular
	.module("app")
	.config(["$stateProvider", function ($stateProvider) {
		$stateProvider
			.state("loggedinPlayMultiScenario", {
				url: "/scenario",
				component: "loggedinPlayMultiScenarioPage",
				parent: "loggedinPlayMulti"
			});
	}]);