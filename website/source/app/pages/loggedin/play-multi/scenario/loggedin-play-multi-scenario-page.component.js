angular
	.module("app")
	.component("loggedinPlayMultiScenarioPage", {
		templateUrl: "/pages/loggedin/play-multi/scenario/loggedin-play-multi-scenario-page.html",
		controller: [
			"$state",
			"appContext",
			"MatchWebClient",
			function (
				$state,
				appContext,
				MatchWebClient) {
				const ctrl = this;

				// private variables 
				let match = null;

				// public methods
				ctrl.newGame = function (reason) {
					$state.go("loggedinPlayMultiFindMatch");
				};

				ctrl.onScenarioPlayerStopped = function (reason) {
					$state.go("loggedinHome");
				};

				// angular hooks
				ctrl.$onInit = function () {
					match = appContext.user.session.match;
				};

				ctrl.$onDestroy = function () {
					MatchWebClient.leaveMatch(match.id, appContext.user.session.id);
				};
			}]
	});