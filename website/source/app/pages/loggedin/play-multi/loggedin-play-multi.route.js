angular
	.module("app")
	.config(["$stateProvider", function ($stateProvider) {
		$stateProvider
			.state("loggedinPlayMulti", {
				abstract: true,
				template: '<ui-view/>',	// for children to populate
				parent: "loggedin",
				url: "/play-multi",
				onExit: [
					"$transition$",
					"appContext",
					function ($transition$, appContext) {
						const MatchPolling = $transition$.injector().get("MatchPolling");

						// Teardown
						MatchPolling.leaveAll(appContext.user.session.id);

						appContext.user.session.match = null;
					}]
			});
	}]);