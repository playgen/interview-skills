angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("loggedinHome", {
                url: "/home",
                component: "loggedinHomePage",
                parent: "loggedin"
            });
    }]);