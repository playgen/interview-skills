angular
	.module("app")
	.component("loggedinHomePage", {
		templateUrl: "/pages/loggedin/home/loggedin-home-page.html",
		controller: [
			"$state",
			"appContext",
			function ($state, appContext) {
				const ctrl = this;

				// public variables
				ctrl.authenticationType = appContext.user.authenticationType;
				ctrl.isWebCamAvailable = true;

				// public methods
				ctrl.onAvailable = function() {
					ctrl.isWebCamAvailable = true;
				};

				ctrl.onUnavailable = function(error) {
					ctrl.isWebCamAvailable = false;
					Logger.error(error);
				};

				ctrl.playMulti = function () {
					$state.go("loggedinPlayMultiFindMatch", {cantMatchPrevious: false});
				};

				ctrl.playMultiUnique = function () {
					if (ctrl.authenticationType === "guest") {
						alert("Guest accounts don't have their match history stored. " +
							"\n\nPlease sign in using a google account to play against someone you haven't played before.");

					} else {
						$state.go("loggedinPlayMultiFindMatch", {cantMatchPrevious: true});
					}
				};

				ctrl.playSolo = function () {
					$state.go("loggedinPlaySolo");
				};

				ctrl.onLoggedOut = function () {
					$state.go("login");
				};
			}]
	});