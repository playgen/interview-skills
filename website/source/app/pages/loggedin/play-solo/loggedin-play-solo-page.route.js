angular
	.module("app")
	.config(["$stateProvider", function ($stateProvider) {
		$stateProvider
			.state("loggedinPlaySolo", {
				url: "/play-solo",
				component: "loggedinPlaySoloPage",
				parent: "loggedin"
			});
	}]);