angular
	.module("app")
	.component("loggedinPlaySoloPage", {
		templateUrl: "/pages/loggedin/play-solo/loggedin-play-solo-page.html",
		controller: [
			"$state",
			function($state) {
				const ctrl = this;

				// public methods
				ctrl.onScenarioPlayerStopped = function(reason) {
					$state.go("loggedinHome");
				};

				ctrl.newGame = function (reason) {
					$state.reload();
				};
			}]
	});