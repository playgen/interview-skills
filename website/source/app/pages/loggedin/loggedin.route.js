angular
	.module("app")
	.config(["$stateProvider", function ($stateProvider) {
		$stateProvider
			.state("loggedin", {
				abstract: true,
				template: '<ui-view/>',	// for children to populate
				url: "/loggedin",
				resolve: {
					hasUser: [
						"appContext",
						"$state",
						"$q",
						function (appContext, $state, $q) {
							if (appContext.user === undefined || appContext.user.session === undefined) {
								$state.go("login");
								return $q.reject("No valid user data. Redirecting to login.");

							} else {
								return true;
							}
						}
					],
					hasJobCache: [
						"JobCache",
						function (JobCache) {
							if (!JobCache.getIsBuilt()) {
								return JobCache.buildCache();
							}
						}
					],
					hasInterviewQuestionCache: [
						"InterviewQuestionCache",
						function (InterviewQuestionCache) {
							if (!InterviewQuestionCache.getIsBuilt()) {
								return InterviewQuestionCache.buildCache();
							}
						}
					]
				}
			});
	}]);