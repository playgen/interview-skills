angular
    .module("app")
    .component("developmentHistoryPage", {
        templateUrl: "/pages/development/history/development-history-page.development.html",
        controller: [
            "developmentHistoryConfig",
            "SentimentAnalysisWebClient",
            function(
                developmentHistoryConfig, 
                SentimentAnalysisWebClient) {
                const ctrl = this;

                // public variables
                ctrl.featuresResults = [];

                // private variables
                const start = new Date(0).toISOString();
                const end = null;

                // angular hooks
                ctrl.$onInit = function () {
                    SentimentAnalysisWebClient.getRange(
                        developmentHistoryConfig.userSessionId, 
                        developmentHistoryConfig.matchSessionId, 
                        start, 
                        end)
                        .then(onResults);
                };

                // private methods
                function onResults(featuresResults) {                    
                    ctrl.featuresResults = [];

                    for(var i in featuresResults) {
                        var featureResults = featuresResults[i];
                        ctrl.featuresResults.push({
                            feature: featureResults.feature.id,
                            values: featureResults.results.map(result => result.weight),
                            labels: featureResults.results.map(result => result.started),                     
                            override: {
                                fill: false                                
                            },
                            options: {
                                yHighlightRange: {
                                    begin: featureResults.feature.idealWeightMin,
                                    end: featureResults.feature.idealWeightMax
                                },
                                scales: {
                                    xAxes: [{
                                        display: false
                                    }],
                                    yAxes: [{                                                                                
                                        ticks: {                                            
                                            min: 0,
                                            max: 1,
                                            display: false
                                        }
                                    }]
                                }
                            }                                
                        });                
                    }                    
                }
            }]
    });