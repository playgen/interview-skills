angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentHistory", {
                url: "/history",
                component: "developmentHistoryPage",
                parent: "development"
            });
    }]);