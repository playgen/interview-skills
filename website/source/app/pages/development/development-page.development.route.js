angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("development", {
                abstract: true,
                template: '<ui-view/>',	// for children to populate
                url: "/development"
            });
    }]);