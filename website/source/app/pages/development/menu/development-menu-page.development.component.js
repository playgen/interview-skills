angular
    .module("app")
    .component("developmentMenuPage", {
        template: "<button ng-repeat='(name, page) in $ctrl.buttonPageMappings' ng-click='$ctrl.onButtonClicked(page)' class='raised'>{{name}}</button>",
        controller: ["$state", function($state) {
            const ctrl = this;

            // public variables
            ctrl.buttonPageMappings = {
                "Feedback Result": "developmentFinalFeedback",
				"Play": "developmentPlay",
				"Scenario Player Elements": "developmentISScenarioPlayerElements",
				"Job Select": "developmentISScenarioPlayerJobSelect",
				"Interviewer Candidate Appraisal": "developmentISScenarioPlayerInterviewerCandidateAppraisal",
				"Prompt Queue": "developmentISScenarioPlayerPromptQueue",
				"Candidate Interview Summary": "developmentISScenarioPlayerCandidateInterviewSummary",
                "Post Interview Swap Prompt": "developmentISScenarioPlayerPostInterviewSwapPrompt",
				"Post Interview Waiting": "developmentISScenarioPlayerPostInterviewWaiting",
				"Interview Question": "developmentISScenarioPlayerInterviewQuestion",
				"Interview Question Respond": "developmentISScenarioPlayerInterviewQuestionRespond",
				"Web Cam Required": "developmentWebCamRequired",
                "Message Dialog": "developmentMessageDialog",
                "History": "developmentHistory"
            };

            // public methods
            ctrl.onButtonClicked = function(state) {
                console.log("Going to state: " + state);
                $state.go(state);
            };
        }]
    });