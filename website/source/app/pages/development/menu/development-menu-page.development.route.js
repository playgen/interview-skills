angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentMenu", {
                url: "/menu",
                component: "developmentMenuPage",
                parent: "development"
            });
    }]);