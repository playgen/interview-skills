angular
	.module("app")
	.component("developmentMessageDialogPage", {
		template: "<message-dialog></message-dialog>",
		controller: [
			"MessageDialog",
			function (MessageDialog) {
				const ctrl = this;

				MessageDialog.show("Some Title", "Some Body")
					.then(button => {
						alert("Message Dialog hidden with button press: " + button);

						MessageDialog.show("Some Other Title", "Some Other Body", 10 * 1000)
							.then(button => alert("Message Dialog hidden with button press or timeout: " + button));
					});
			}]
	});