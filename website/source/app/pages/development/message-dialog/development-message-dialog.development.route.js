angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentMessageDialog", {
                url: "/message-dialog",
                component: "developmentMessageDialogPage",
                parent: "development"
            });
    }]);