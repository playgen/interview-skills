angular
	.module("app")
	.component("developmentISScenarioPlayerInterviewQuestionPage",
	{
		templateUrl:
		"pages/development/is-scenario-player/interview-question/development-is-scenario-player-interview-question.development.html",
		controller: [
			function () {
				const ctrl = this;

				// public variables
				ctrl.questions = [
					{
						state: "asking",
						current: 1,
						total: 3,
						question: "What are your goals?",
						length: 40
					},
					{
						state: "answering",
						current: 1,
						total: 3,
						question: "What are your goals?",
						length: 10
					},
					{
						state: "answered",
						current: 1,
						total: 3,
						question: "What are your goals?",
						length: 40
					}
				];

				// public methods
				ctrl.onSelect = function (option) {
					alert("Clicked: " + option);
				};
			}
		]
	});