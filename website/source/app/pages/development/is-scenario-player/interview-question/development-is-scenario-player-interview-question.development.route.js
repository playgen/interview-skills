angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentISScenarioPlayerInterviewQuestion", {
                url: "/interview-question",
				component: "developmentISScenarioPlayerInterviewQuestionPage",
                parent: "developmentISScenarioPlayer"
            });
    }]);