angular
	.module("app")
	.component("developmentISScenarioPlayerInterviewerCandidateAppraisalPage", {
		templateUrl: "/pages/development/is-scenario-player/interviewer-candidate-appraisal/development-is-scenario-player-interviewer-candidate-appraisal.development.html",
		controller: function () {
			const ctrl = this;

			ctrl.appraisal1 = {
				options: ["quit", "swap"],
				interviewRound: 1
			};

			ctrl.appraisal2 = {
				options: ["results"],
				interviewRound: 2
			};

			ctrl.onSubmit = function(option, appraisalResult) {
				alert("option: \"" + option + "\"" +
					"\nquestionRatings: \n\"" + 
					Object.keys(appraisalResult.questionRatings).map(key => key + " : " + 
					appraisalResult.questionRatings[key].rating).join("\n") + "\"" +
					"\nmessage: \"" + appraisalResult.message + "\"");
			};
		}
	});