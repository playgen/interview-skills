angular
	.module("app")
	.config(["$stateProvider", function ($stateProvider) {
		$stateProvider
			.state("developmentISScenarioPlayerInterviewerCandidateAppraisal", {
				url: "/interviewer-candidate-appraisal",
				component: "developmentISScenarioPlayerInterviewerCandidateAppraisalPage",
				parent: "developmentISScenarioPlayer"
			});
	}]);