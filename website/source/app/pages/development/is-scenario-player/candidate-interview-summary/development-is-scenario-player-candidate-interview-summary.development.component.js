angular
	.module("app")
	.component("developmentISScenarioPlayerCandidateInterviewSummaryPage", {
		templateUrl: "/pages/development/is-scenario-player/candidate-interview-summary/development-is-scenario-player-candidate-interview-summary.development.html",
		controller: [
			"ObservationalQuestionWebClient",
			"isScenarioPlayerInterviewerCandidateAppraisalConfig",
			function (ObservationalQuestionWebClient, isScenarioPlayerInterviewerCandidateAppraisalConfig) {
				const ctrl = this;

				// public variables
				ctrl.options = ["home", "new game"];
				ctrl.job = "Psychic Investigator";

				ObservationalQuestionWebClient.listQuestions(undefined, isScenarioPlayerInterviewerCandidateAppraisalConfig.tags, false, true, true)
					.then(questions => {
						ctrl.interviewerObservationalRatingsList = [];

						for(var rating = 0; rating < 2; rating++) {
							var interviewerObservationalRatings = {};

							interviewerObservationalRatings[questions[0].id] = {
								question: questions[0],
								rating: rating
							};
							
							interviewerObservationalRatings[questions[1].id] = {
								question: questions[1],
								rating: rating
							};

							interviewerObservationalRatings[questions[2].id] = {
								question: questions[2],
								rating: rating
							};

							interviewerObservationalRatings[questions[3].id] = {
								question: questions[3],
								rating: rating
							};

							interviewerObservationalRatings[questions[4].id] = {
								question: questions[4],
								rating: rating
							};

							ctrl.interviewerObservationalRatingsList.push(interviewerObservationalRatings);
						}
					});

				ctrl.showBestCount = 2;
				ctrl.showWorstCount = 2;
				
				// This value needs to exist in the database from  aprevious playthrough
				ctrl.userSessionId = "08d5af7d-ff93-2bff-9c6c-888bc4bebf06";
				// This value needs to exist in the database from  aprevious playthrough
				ctrl.matchSessionId = "1_MX40NTU4Mzg1Mn5-MTUyNTE5MTAwODAxN35VNTRBU0R1dVU5WG9PSVVNajBmQWIzeFp-fg";

				ctrl.candidateStart = new Date(0).toISOString();
				ctrl.candidateEnd = null;

				ctrl.noResultStart = new Date(0).toISOString();
				ctrl.noResultEnd = new Date(0).toISOString();

				ctrl.interviewerMessage = "You tend to look away a lot, breaking eye contact. It felt a bit awkward so I'd suggest trying to maintain eye contact.";

				ctrl.onSelect = function (selected) {
					alert("Selected: " + selected);
				};
			}]
	});