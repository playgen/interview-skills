angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentISScenarioPlayerCandidateInterviewSummary", {
                url: "/candidate-interview-summary",
				component: "developmentISScenarioPlayerCandidateInterviewSummaryPage",
                parent: "developmentISScenarioPlayer"
            });
    }]);