angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentISScenarioPlayerInterviewQuestionRespond", {
                url: "/interview-question-respond",
				component: "developmentISScenarioPlayerInterviewQuestionRespondPage",
                parent: "developmentISScenarioPlayer"
            });
    }]);