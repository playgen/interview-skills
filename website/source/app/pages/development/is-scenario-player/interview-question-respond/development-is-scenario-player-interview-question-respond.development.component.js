angular
	.module("app")
	.component("developmentISScenarioPlayerInterviewQuestionRespondPage",
	{
		templateUrl:
		"pages/development/is-scenario-player/interview-question-respond/development-is-scenario-player-interview-question-respond.development.html",
		controller: [
			function () {
				const ctrl = this;

				// public variables
				ctrl.questions = [
					{
						state: "asking",
						current: 1,
						total: 3,
						question: "What are your goals?",
						length: 40
					},
					{
						state: "answering",
						current: 1,
						total: 3,
						question: "What are your goals?",
						length: 10
					},
					{
						state: "answered",
						current: 1,
						total: 3,
						question: "What are your goals?",
						length: 40
					}
				];
			}
		]
	});