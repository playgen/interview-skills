angular
	.module("app")
	.component("developmentISScenarioPlayerElementsPage", {
		templateUrl: "/pages/development/is-scenario-player/elements/development-is-scenario-player-elements.development.html",
		controller: ["developmentIsScenarioPlayerElementsConfig", "$timeout",
			function (developmentIsScenarioPlayerElementsConfig, $timeout) {
				const ctrl = this;

				ctrl.featureActionableFeedback = {
					feedback: "A feature actionable feedback",
					onSubmit: function () {
						alert("featureActionableFeedback submit clicked.");
					}
				};

				ctrl.observationalQuestionFeedback = {
					feedback: "An observational question feedback",
					onSubmit: function () {
						alert("observationalQuestion submit clicked.");
					}
				};

				ctrl.questionbox = {
					title: "Ask",
					body: "What makes you the ideal candidate for this position?",
					onSubmit: function () {
						alert("Question box submit clicked.");
					}
				};

				ctrl.observationalQuestions = [
					{
						question: "How positive does the candidate appear?",
						ratings: [1, 2, 3, 4, 5],
						startLabel: null,
						endLabel: null,
						isBinary: false,
						isInverse: false,
						onSubmit: function (rating) {
							alert("Appraisal Evaluation submitted with rating of: " + rating);
						}
					},
					{
						question: "How positive does the candidate appear?",
						ratings: [1, 2, 3, 4, 5],
						startLabel: null,
						endLabel: null,
						isBinary: false,
						isInverse: false,
						onSubmit: function (rating) {
							alert("Appraisal Evaluation submitted with rating of: " + rating);
						}
					},
					{
						question: "How calm does the candidate appear?",
						ratings: [1, 2, 3, 4, 5],
						startLabel: null,
						endLabel: null,
						isBinary: false,
						isInverse: true,
						onSubmit: function (rating) {
							alert("Appraisal Evaluation with inverse and submitted with rating of: " + rating);
						}
					}, {
						question: "Does the candidate appear happy?",
						ratings: [1, 5],
						startLabel: "yes",
						endLabel: "no",
						isBinary: true,
						isInverse: false,
						onSubmit: function (rating) {
							alert("Appraisal Evaluation submitted with rating of: " + rating);
						}
					}, {
						question: "How honest is the candidate?",
						ratings: [1, 5],
						startLabel: "dishonest",
						endLabel: "honest",
						isBinary: true,
						isInverse: false,
						onSubmit: function (rating) {
							alert("Appraisal Evaluation submitted with rating of: " + rating);
						}
					}
				];

				ctrl.briefingBox = {
					title: "Veterinary Nurse",
					body: "You are the candidate"
				};

				ctrl.analysisMetadatas = [
					{
						// An inverse result
						featureResults: [developmentIsScenarioPlayerElementsConfig.analysisMetadata.featureResults.find(f => f.featureId === "stress")],
						errors: [],
						isSpeechDetected: true
					}, 
					{
						// Results
						featureResults: developmentIsScenarioPlayerElementsConfig.analysisMetadata.featureResults,
						errors: [],
						isSpeechDetected: true
					}, 
					{
						// No Speech
						featureResults: developmentIsScenarioPlayerElementsConfig.analysisMetadata.featureResults,
						errors: null,					
						isSpeechDetected: false
					}, 
					{ 
						// Error with Results						
						featureResults: developmentIsScenarioPlayerElementsConfig.analysisMetadata.featureResults.slice(0, 3),
						errors: ["Server Error"],					
						isSpeechDetected: false
					}, 
					{
						// No Speech						
						featureResults: [],
						errors: null,
						isSpeechDetected: false
					}, 
					{
						// Error 						
						featureResults: null,
						errors: ["Couldn't connect to the server"],
						isSpeechDetected: false
					}
				];

				ctrl.featureResults = developmentIsScenarioPlayerElementsConfig.analysisMetadata.featureResults;

				var isB = false;
				const featureResult1 = {
					a: Object.assign({}, ctrl.featureResults[0]),
					b: Object.assign({}, ctrl.featureResults[0])
				};
				featureResult1.a.weight = 0;
				featureResult1.a.weightFromIdeal = 0;				
				featureResult1.b.weight = 1;
				featureResult1.b.weightFromIdeal = 1;

				const featureResult2 = {
					a: Object.assign({}, ctrl.featureResults[1]),
					b: Object.assign({}, ctrl.featureResults[1])
				};
				featureResult2.a.weight = 0.8;
				featureResult2.a.weightFromIdeal = 0.2;
				featureResult2.b.weight = 0.2;
				featureResult2.b.weightFromIdeal = 0.8;

				function changeFeatureResult() {
					if(isB) {
						isB = false;
						ctrl.featureResults[0] = featureResult1.a;
						ctrl.featureResults[1] = featureResult2.a;

					} else {
						isB = true;
						ctrl.featureResults[0] = featureResult1.b;
						ctrl.featureResults[1] = featureResult2.b;
					}

					$timeout(changeFeatureResult, 1000);
				}

				// angular hooks
				ctrl.$postLink = function() {
					changeFeatureResult();
				};
			}
		]
	});