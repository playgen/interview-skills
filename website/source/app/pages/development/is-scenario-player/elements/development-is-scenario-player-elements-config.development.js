﻿angular
	.module("app")
	.value("developmentIsScenarioPlayerElementsConfig",
	{
		analysisMetadata: {
			"$type": "PlayGen.InterviewSkills.Data.Model.Analysis.AnalysisMetadata, PlayGen.InterviewSkills.Data.Model",
			"id": 3915,
			"matchSessionId": "1_MX40NTU4Mzg1Mn5-MTUyMDAwMjE3MDU2OX4yQkEzdkRucCtmbCtxalFaWWM3S1dTeEZ-fg",
			"matchSession": null,
			"recordingStarted": "2018-03-02T14:49:37.862",
			"recordingEnded": "2018-03-02T14:49:38.862",
			"userSessionId": "08d5804c-27b0-b247-deb0-a6cd88ece646",
			"userSession": null,
			"errors": null,
			"serializedErrors": "null",
			"featureResults": [
				{
					"$type": "PlayGen.InterviewSkills.Data.Model.Analysis.AnalysisFeatureResult, PlayGen.InterviewSkills.Data.Model",
					"id": 34555,
					"analysisMetadataId": 3915,
					"featureId": "stress",
					"feature": {
						"$type": "PlayGen.InterviewSkills.Data.Model.Feature, PlayGen.InterviewSkills.Data.Model",
						"id": "stress",
						"inverse": "calmness",
						"idealWeightMin": 0,
						"idealWeightMax": 0.5
					},
					"classifier1": 0.347057879,
					"classifier2": 0.35219872,
					"classifier3": 0.300743371,
					"prediction": 2,
					"weight": 0.39203313,
					"weightFromIdeal": 0,
					"$$hashKey": "object:714"
				}, {
					"$type": "PlayGen.InterviewSkills.Data.Model.Analysis.AnalysisFeatureResult, PlayGen.InterviewSkills.Data.Model",
					"id": 34562,
					"analysisMetadataId": 3915,
					"featureId": "interest",
					"feature": {
						"$type": "PlayGen.InterviewSkills.Data.Model.Feature, PlayGen.InterviewSkills.Data.Model",
						"id": "interest",
						"inverse": null,
						"idealWeightMin": 0.5,
						"idealWeightMax": 1
					},
					"classifier1": 0.362080246,
					"classifier2": 0.3496474,
					"classifier3": 0.288272351,
					"prediction": 1,
					"weight": 0.212639928,
					"weightFromIdeal": -0.574720144,
					"$$hashKey": "object:715"
				}, {
					"$type": "PlayGen.InterviewSkills.Data.Model.Analysis.AnalysisFeatureResult, PlayGen.InterviewSkills.Data.Model",
					"id": 34564,
					"analysisMetadataId": 3915,
					"featureId": "friendliness",
					"feature": {
						"$type": "PlayGen.InterviewSkills.Data.Model.Feature, PlayGen.InterviewSkills.Data.Model",
						"id": "friendliness",
						"inverse": null,
						"idealWeightMin": 0.5,
						"idealWeightMax": 1
					},
					"classifier1": 0.3773584,
					"classifier2": 0.622641563,
					"classifier3": null,
					"prediction": 2,
					"weight": 0.622641563,
					"weightFromIdeal": 0,
					"$$hashKey": "object:716"
				}, {
					"$type": "PlayGen.InterviewSkills.Data.Model.Analysis.AnalysisFeatureResult, PlayGen.InterviewSkills.Data.Model",
					"id": 34565,
					"analysisMetadataId": 3915,
					"featureId": "positivity",
					"feature": {
						"$type": "PlayGen.InterviewSkills.Data.Model.Feature, PlayGen.InterviewSkills.Data.Model",
						"id": "positivity",
						"inverse": null,
						"idealWeightMin": 0.5,
						"idealWeightMax": 1
					},
					"classifier1": 0.00303355861,
					"classifier2": 0.881881,
					"classifier3": 0.115085416,
					"prediction": 2,
					"weight": 0.5196865,
					"weightFromIdeal": 0,
					"$$hashKey": "object:717"
				}, {
					"$type": "PlayGen.InterviewSkills.Data.Model.Analysis.AnalysisFeatureResult, PlayGen.InterviewSkills.Data.Model",
					"id": 34567,
					"analysisMetadataId": 3915,
					"featureId": "sincerity",
					"feature": {
						"$type": "PlayGen.InterviewSkills.Data.Model.Feature, PlayGen.InterviewSkills.Data.Model",
						"id": "sincerity",
						"inverse": null,
						"idealWeightMin": 0.5,
						"idealWeightMax": 1
					},
					"classifier1": 0.19389835,
					"classifier2": 0.437324077,
					"classifier3": 0.368777573,
					"prediction": 2,
					"weight": 0.5937793,
					"weightFromIdeal": 0,
					"$$hashKey": "object:718"
				}
			]
		}
	});