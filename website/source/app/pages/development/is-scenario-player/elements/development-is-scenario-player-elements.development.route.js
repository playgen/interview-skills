angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentISScenarioPlayerElements", {
                url: "/elements",
                component: "developmentISScenarioPlayerElementsPage",
                parent: "developmentISScenarioPlayer"
            });
    }]);