angular
	.module("app")
	.component("developmentISScenarioPlayerJobSelectPage", {
		templateUrl: "/pages/development/is-scenario-player/job-select/development-is-scenario-player-job-select.development.html",
		controller: [
			"JobCache",
			function (JobCache) {
				const ctrl = this;

				JobCache.buildCache();

				ctrl.onSubmit = function (selected) {
					alert("Selected Job: " + selected.id);
				};
			}]
	});