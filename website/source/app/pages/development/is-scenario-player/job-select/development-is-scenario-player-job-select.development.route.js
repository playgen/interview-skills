angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentISScenarioPlayerJobSelect", {
                url: "/job-select",
                component: "developmentISScenarioPlayerJobSelectPage",
                parent: "developmentISScenarioPlayer"
            });
    }]);