angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentISScenarioPlayerPostInterviewSwapPrompt", {
                url: "/post-interview-swap-prompt",
				component: "developmentISScenarioPlayerPostInterviewSwapPromptPage",
                parent: "developmentISScenarioPlayer"
            });
    }]);