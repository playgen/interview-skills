angular
	.module("app")
	.component("developmentISScenarioPlayerPostInterviewSwapPromptPage", {
		templateUrl: "/pages/development/is-scenario-player/post-interview-swap-prompt/development-is-scenario-player-post-interview-swap-prompt.development.html",
		controller: [function () {
			const ctrl = this;

			// public variables
			ctrl.options = ["quit", "swap"];
			ctrl.halfTime = 1;

			// This value needs to exist in the database from  aprevious playthrough
			ctrl.userSessionId = "08d582ac-0150-af5a-957a-501e8b3a70be";
			// This value needs to exist in the database from  aprevious playthrough
			ctrl.matchSessionId = "2_MX40NTU4Mzg1Mn5-MTUyMDI2Mjk4MjAyNH5nMWppaXE5aG44T1dxTmdXY1R1NFhZbHN-fg";

			ctrl.candidateStart = new Date(0).toISOString();
			ctrl.candidateEnd = null;

			ctrl.noResultStart = new Date(0).toISOString();
			ctrl.noResultEnd = new Date(0).toISOString();

			ctrl.onSelect = function (option) {
				alert("Selected: " + option);
			};
		}]
	});