﻿angular
	.module("app")
	.component("developmentISScenarioPlayerPromptQueuePage", {
		templateUrl: "/pages/development/is-scenario-player/prompt-queue/development-is-scenario-player-prompt-queue.development.html",
		controller: [
			"FeatureActionableFeedbackWebClient",
			"InterviewQuestionWebClient",
			"ObservationalQuestionWebClient",
			"$timeout",
			"isScenarioPlayerConfig",
			"developmentIsScenarioPlayerElementsConfig",
			function (FeatureActionableFeedbackWebClient, InterviewQuestionWebClient, ObservationalQuestionWebClient,
				$timeout, isScenarioPlayerConfig, developmentIsScenarioPlayerElementsConfig) {

				const ctrl = this;

				ctrl.enqueue = null;
				ctrl.dequeue = null;
				ctrl.isEmpty = null;

				const featureActionableFeedbackController = new ISScenarioPlayerComponentsFeatureActionableFeedbackController({
					webClient: FeatureActionableFeedbackWebClient,
					lookBackCount: isScenarioPlayerConfig.lookBackCount
				});

				//const interviewQuestionController = new ISScenarioPlayerComponentsInterviewQuestionController({
				//	webClient: InterviewQuestionWebClient,
				//	interviewQuestionTag: isScenarioPlayerConfig.interviewQuestion.interviewQuestionTag
				//});

				//const observationalQuestionController = new ISScenarioPlayerComponentsObservationalQuestionController({
				//	webClient: ObservationalQuestionWebClient,
				//	lookBackCount: isScenarioPlayerConfig.observationalQuestion.lookBackCount,
				//	requireAllFeatures: isScenarioPlayerConfig.observationalQuestion.requireAllFeatures,
				//	tags: isScenarioPlayerConfig.observationalQuestion.tags,
				//	requireAllTags: isScenarioPlayerConfig.observationalQuestion.requireAllFeatures
				//});

				const featureActionableFeedbackQueue = [];
				const interviewQuestionQueue = [];
				const observationalQuestionQueue = [];

				ctrl.$onInit = function () {
					// Feature Actionable Feedback
					featureActionableFeedbackQueue.push(developmentIsScenarioPlayerElementsConfig.analysisMetadata);
					//featureActionableFeedbackQueue.push(developmentIsScenarioPlayerElementsConfig.analysisMetadata);
					//featureActionableFeedbackQueue.push(developmentIsScenarioPlayerElementsConfig.analysisMetadata);
					//featureActionableFeedbackQueue.push(developmentIsScenarioPlayerElementsConfig.analysisMetadata);
					//featureActionableFeedbackQueue.push(developmentIsScenarioPlayerElementsConfig.analysisMetadata);
					processNextInFeatureActionableFeedbackQueue();

					// Interview Question
					//interviewQuestionQueue.push(1);
					//interviewQuestionQueue.push(2);
					//interviewQuestionQueue.push(3);
					//interviewQuestionQueue.push(4);
					//interviewQuestionQueue.push(5);
					//interviewQuestionQueue.push(6);
					//processNextInInterviewQuestionQueue();

					// Observational Question
					//observationalQuestionQueue.push(["happiness", "sincerity", "volume"]);
					//observationalQuestionQueue.push(["happiness", "sincerity", "volume"]);
					//observationalQuestionQueue.push(["stress"]);
					//processNextInObservationalQuestionQueue();

				};

				ctrl.onEmptiedQueue = function () {
					alert("Queue Emptied");
				};

				function processNextInFeatureActionableFeedbackQueue() {
					if (featureActionableFeedbackQueue.length === 0) {
						console.log("featureActionableFeedbackQueue emptied.");
						return;
					}

					const next = Array.takeAt(featureActionableFeedbackQueue, 0);

					featureActionableFeedbackController.getFeedbacks(next);
					$timeout(checkFeatureActionableFeedbacks, 1000);
				}

				function checkFeatureActionableFeedbacks() {
					if (featureActionableFeedbackController.hasFeedbacks === false) {
						$timeout(checkFeatureActionableFeedbacks, 1000);

					} else {
						const feedback = featureActionableFeedbackController.takeAndSetActive();

						const prompt = {
							type: "FeatureActionableFeedback",
							feedback: feedback.feedback,
							onSubmit: () => alert(feedback.feedback)
						};

						ctrl.enqueue(prompt);

						processNextInFeatureActionableFeedbackQueue();
					}
				}

				function processNextInInterviewQuestionQueue() {
					if (interviewQuestionQueue.length === 0) {
						console.log("interviewQuestionQueue emptied.");
						return;
					}

					const next = Array.takeAt(interviewQuestionQueue, 0);

					interviewQuestionController.getQuestions(next);
					$timeout(checkInterviewQuestions, 1000);
				}

				function checkInterviewQuestions() {
					if (interviewQuestionController.hasQuestions === false) {
						$timeout(checkInterviewQuestions, 1000);

					} else {
						const question = interviewQuestionController.takeAndSetRandomActive();

						const prompt = {
							type: "InterviewQuestion",
							title: "Ask", // todo reflect config driven version when implemented
							body: question.question,
							onSubmit: () => alert(question.question)
						};

						ctrl.enqueue(prompt);

						processNextInInterviewQuestionQueue();
					}
				}

				function processNextInObservationalQuestionQueue() {
					if (observationalQuestionQueue.length === 0) {
						console.log("observationalQuestionQueue emptied.");
						return;
					}

					const next = Array.takeAt(observationalQuestionQueue, 0);

					observationalQuestionController.getQuestions(next);
					$timeout(checkObservationalQuestions, 1000);
				}

				function checkObservationalQuestions() {
					if (observationalQuestionController.hasQuestions === false) {
						$timeout(checkObservationalQuestions, 1000);

					} else {
						const question = observationalQuestionController.takeAndSetActive();

						const prompt = {
							type: "ObservationalQuestion",
							title: "Diagnosis", // todo reflect config driven version when implemented
							body: question.question,
							isBinary: question.isBinary,
							startLabel: question.startLabel,
							endLabel: question.endLabel,
							onSubmit: (submission) => alert(question.question + " \nRating: " + submission.result)
						};

						ctrl.enqueue(prompt);

						processNextInObservationalQuestionQueue();
					}
				}
			}]
	});