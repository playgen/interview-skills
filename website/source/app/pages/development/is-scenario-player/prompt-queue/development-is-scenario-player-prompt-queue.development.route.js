﻿angular
	.module("app")
	.config(["$stateProvider", function ($stateProvider) {
		$stateProvider
			.state("developmentISScenarioPlayerPromptQueue", {
				url: "/prompt-queue",
				component: "developmentISScenarioPlayerPromptQueuePage",
				parent: "developmentISScenarioPlayer"
			});
	}]);