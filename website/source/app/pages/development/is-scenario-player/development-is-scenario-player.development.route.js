angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentISScenarioPlayer", {
                url: "/is-scenario-player",
                template: '<ui-view/>',	// for children to populate
                abstract: true,
                parent: "development"
            });
    }]);