angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentISScenarioPlayerPostInterviewWaiting", {
                url: "/post-interview-waiting",
				component: "developmentISScenarioPlayerPostInterviewWaitingPage",
                parent: "developmentISScenarioPlayer"
            });
    }]);