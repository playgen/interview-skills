angular
	.module("app")
	.component("developmentPlayPage", {
		template: "{{$ctrl.state}}",
		controller: [
			"$state",
			"context",
			"SUGARWebClient",
			function ($state, context, SUGARWebClient) {
				const ctrl = this;

				// public variables
				ctrl.state = "Uninitialised";

				// public methods
				ctrl.$onInit = function () {
					ctrl.state = "Logging In";
					SUGARWebClient.login("development", "development")
						.then(onLoggedIn);
				};

				// private methods
				function onLoggedIn(loginResponse) {
					ctrl.state = "Logged In";
					context.user.session = {
						id: "development_" + Date.now()
					};

					ctrl.state = "Going to loggedInPlayFindMatch";
					$state.go("loggedinPlayFindMatch");
				}
			}
		]
	});