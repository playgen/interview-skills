angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentPlay", {
                url: "/play",
                component: "developmentPlayPage",
                parent: "development"
            });
    }]);