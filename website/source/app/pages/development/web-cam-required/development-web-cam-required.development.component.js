angular
	.module("app")
	.component("developmentWebCamRequiredPage", {
		templateUrl: "/pages/development/web-cam-required/development-web-cam-required.development.html",
		controller: [
			function () {
				const ctrl = this;

				ctrl.onAvailable = function() {
					alert("web cam available");
				};

				ctrl.onUnavailable = function (error) {
					console.error(error);
				};
			}]
	});