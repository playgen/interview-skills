angular
    .module("app")
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider
            .state("developmentWebCamRequired", {
                url: "/web-cam-required",
                component: "developmentWebCamRequiredPage",
                parent: "development"
            });
    }]);