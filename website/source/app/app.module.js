angular
	.module("app", [
		"templates",
		"ui.router",
		"ngMaterial",
		"ngSanitize",	// Required to sanitize html insertion via ng-bind-html
		"match",
		"googleAuth",
		"guestAuth",
		"emailAuth",
		"webCam",
		"videoCalling",
		"peerSignalling",
		"sentimentAnalysis",
		"job",
		"roleSession",
		"scenarioPlayer",
		"featureActionableFeedback",
		"interviewQuestion",
		"interviewQuestionResult",
		"observationalQuestion",
		"observationalQuestionFeedback",
		"observationalQuestionResult",
		"featureSummaryFeedback",
		"featureActionableFeedbackSystemResult",
		"remoteCommands",
		"chart.js",
		"interpolate"])
	.value("appContext", {});