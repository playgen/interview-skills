local command = "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' " .. ngx.arg[1]
local f = assert(io.popen(command, 'r'))
local out = assert(f:read('*l'))
f:close()
return out