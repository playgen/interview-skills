﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.RemoteCommands
{
    public class UpdateResponse
    {
		public string Id { get; set; }

		public object Result { get; set; }
    }
}
