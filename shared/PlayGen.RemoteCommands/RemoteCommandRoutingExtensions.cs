﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using PlayGen.RemoteCommands.Connection;
using System.Linq;
using Playgen.Serialization;

namespace PlayGen.RemoteCommands
{
	public static class RemoteCommandRoutingExtensions
	{
		public static IServiceCollection AddRemoteCommands<TCommandRouter, TSerializer>(this IServiceCollection services)
			where TCommandRouter : CommandRouter
			where TSerializer : JsonSerializer
		{
			services.AddSingleton<TCommandRouter>();
			services.AddSingleton<TSerializer>();
			
			return services;
		}

		public static IApplicationBuilder UseRemoteCommands<TConnectionHandler>(this IApplicationBuilder app)
			where TConnectionHandler : ConnectionMiddleware
		{
			app.UseMiddleware<TConnectionHandler>();

			using (var scope = app.ApplicationServices.CreateScope())
			{
				var messageRouter = scope.ServiceProvider.GetService<CommandRouter>();
				messageRouter.MapRoutes();

				var unresolvedTypes = messageRouter.ListUnresolvedTypes();
				if (unresolvedTypes.Any())
				{
					throw new Exception("Dependency Injection cannot resolve some of the class types used by the Remote Commands." +
										$" {string.Join(", ", unresolvedTypes.Select(t => t.ToString()))}");
				}
			}

			return app;
		}
	}
}