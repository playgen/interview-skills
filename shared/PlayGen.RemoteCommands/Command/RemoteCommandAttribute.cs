﻿using System;

namespace PlayGen.RemoteCommands.Command
{
	[AttributeUsage(validOn: AttributeTargets.Method, AllowMultiple = false)]
    public class RemoteCommandAttribute : Attribute
    {
    }
}
