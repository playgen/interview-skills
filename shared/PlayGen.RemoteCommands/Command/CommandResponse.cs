﻿namespace PlayGen.RemoteCommands.Command
{
    public class CommandResponse
    {
		public string RequestId { get; set; }

		public object Result { get; set; }
    }
}
