﻿namespace PlayGen.RemoteCommands.Command
{
    public class CommandRequest
    {
		public string Id { get; set; }

		public string Command { get; set; }

		public object[] Parameters { get; set; }
    }
}
