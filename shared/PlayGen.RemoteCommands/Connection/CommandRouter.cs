﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.ExceptionServices;
using PlayGen.RemoteCommands.Command;
using PlayGen.RemoteCommands.Connection.WebSocket;

namespace PlayGen.RemoteCommands.Connection
{
	public class CommandRouter
	{
		private readonly IServiceProvider _services;

		private Dictionary<string, MethodInfo> _routes;

		public CommandRouter(IServiceProvider services)
		{
			_services = services;
		}

		public object ProcessCommand(CommandRequest command, WebSocketConnection connection)
		{
			var method = _routes[command.Command];
			var service = _services.GetService(method.DeclaringType);

			var parameters = CombineOptionalParameters(command.Parameters, method, connection);

			var result = method.Invoke(service, parameters);

			return result;
		}
		
		public void MapRoutes()
		{ 
			_routes = AppDomain.CurrentDomain
				.GetAssemblies()
				.SelectMany(a => a.GetTypes())
				.SelectMany(t => t.GetMethods())
				.Where(m => m.GetCustomAttribute<RemoteCommandAttribute>() != null)
				.ToDictionary(
					m => $"{m.DeclaringType.Name}.{m.Name}",
					m => m);
		}

		public Type[] ListUnresolvedTypes()
		{
			var unresolvedTypes = _routes.Values
				.Select(v => v.DeclaringType)
				.Where(t => _services.GetService(t) == null)
				.ToArray();

			return unresolvedTypes;
		}

		private object[] CombineOptionalParameters(object[] specifiedParameters, MethodInfo method, WebSocketConnection connection)
		{
			var methodParameters = method.GetParameters();
			var parameters = new object[methodParameters.Length];

			for (var i = 0; i < methodParameters.Length; i++)
			{
				if (specifiedParameters?.Length > i)
				{
					parameters[i] = specifiedParameters[i];
				}
				else if(methodParameters[i].IsOptional)
				{
					parameters[i] = Type.Missing;
				}
				else if(methodParameters[i].ParameterType == typeof(WebSocketConnection))
				{
					parameters[i] = connection;
				}
				else
				{
					throw new Exception($"Unhandled parameter type: {methodParameters[i].ParameterType}");
				}
			}

			return parameters;
		}
	}
}