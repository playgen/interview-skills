﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PlayGen.RemoteCommands.Connection
{
	public abstract class ConnectionMiddleware
	{
		protected readonly RequestDelegate Next;

		protected ConnectionMiddleware(RequestDelegate next)
		{
			Next = next;
		}

		public abstract Task Invoke(HttpContext context);
	}
}
