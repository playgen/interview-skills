﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PlayGen.RemoteCommands.Connection.WebSocket
{
	public class WebSocketConnectionMiddleware : ConnectionMiddleware
	{
		private readonly WebSocketConnectionRouter _connectionRouter;
		
		public WebSocketConnectionMiddleware(
			RequestDelegate next,
			WebSocketConnectionRouter connectionRouter)
			: base(next)
		{
			_connectionRouter = connectionRouter;
		}

		public override async Task Invoke(HttpContext context)
		{
			if (context.WebSockets.IsWebSocketRequest)
			{
				var webSocket = await context.WebSockets.AcceptWebSocketAsync();
				await _connectionRouter.HandleWebSocket(webSocket);
			}
			else
			{
				await Next(context);
			}
		}
	}
}
