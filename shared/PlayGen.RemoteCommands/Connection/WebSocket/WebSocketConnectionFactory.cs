﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Playgen.Serialization;

namespace PlayGen.RemoteCommands.Connection.WebSocket
{
    public class WebSocketConnectionFactory
    {
		private readonly ISerializer _serializer;
		private readonly WebSocketOptions _webSocketOptions;
		private readonly ILoggerFactory _loggerFactory;

		public WebSocketConnectionFactory(
			ISerializer serializer,
			IOptions<WebSocketOptions> webSocketOptions,
			ILoggerFactory loggerFactory)
		{
			_serializer = serializer;
			_webSocketOptions = webSocketOptions.Value;
			_loggerFactory = loggerFactory;
		}
		public WebSocketConnection Create(System.Net.WebSockets.WebSocket webSocket)
		{
			var connection = new WebSocketConnection(
				webSocket, 
				_webSocketOptions.ReceiveBufferSize, 
				_serializer, 
				_loggerFactory.CreateLogger<WebSocketConnection>());

			return connection;
		}
    }
}
