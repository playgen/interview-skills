﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Playgen.Serialization;
using PlayGen.RemoteCommands.Command;

namespace PlayGen.RemoteCommands.Connection.WebSocket
{
	public class WebSocketConnection : IConnection<WebSocketConnection>
	{
		private readonly System.Net.WebSockets.WebSocket _webSocket;
		private readonly int _bufferSize;
		private readonly ISerializer _serializer;
		private readonly ILogger<WebSocketConnection> _logger;
		private readonly bool _isBinary;

		public event Action<WebSocketConnection> ConnectionClosed;

		public WebSocketState State => _webSocket.State;

		public WebSocketConnection(
			System.Net.WebSockets.WebSocket webSocket,
			int bufferSize,
			ISerializer serializer,
			ILogger<WebSocketConnection> logger,
			bool isBinary = false)
		{
			_webSocket = webSocket;
			_bufferSize = bufferSize;
			_serializer = serializer;
			_logger = logger;
			_isBinary = isBinary;
		}

		public async Task<CommandRequest> ReceiveCommandRequestAsync()
		{
			CommandRequest commandRequest = null;
			var buffer = new byte[_bufferSize];

			var message = await _webSocket.ReceiveAsync(buffer, CancellationToken.None);

			switch (message.MessageType)
			{
				case WebSocketMessageType.Binary:
					commandRequest = _serializer.DeserializeBinary<CommandRequest>(buffer, message.Count);
					break;

				case WebSocketMessageType.Text:
					commandRequest = _serializer.DeserializeText<CommandRequest>(buffer, message.Count);
					break;

				case WebSocketMessageType.Close:
					await TrySendClose();
					break;

				default:
					throw new ArgumentOutOfRangeException();
			}

			return commandRequest;
		}

		public Task SendUpdateResponse(UpdateResponse response)
		{
			return SendAsync(response);
		}

		public Task SendCommandResponse(CommandResponse response)
		{
			return SendAsync(response);
		}

		public async Task<bool> TrySendClose()
		{
			var didCloseGracefully = false;

			try
			{
				await _webSocket.CloseAsync(
					WebSocketCloseStatus.NormalClosure,
					"Connection Closed.",
					CancellationToken.None);

				didCloseGracefully = true;
			}
			catch (Exception closeFailure)
			{
				_logger.LogError(closeFailure, "Failed to gracefully close connection.");
				_webSocket.Abort();
			}
			finally
			{
				ConnectionClosed?.Invoke(this);
			}

			return didCloseGracefully;
		}

		private async Task SendAsync(object payload)
		{
			if (_isBinary)
			{
				using (var serializedPayload = _serializer.SerializeBinary(payload))
				{
					await SendBinaryAsync(serializedPayload.ToArray());
				}
			}
			else
			{
				using (var serializedPayload = _serializer.SerializeText(payload))
				{
					await SendTextAsync(serializedPayload.ToArray());
				}
			}
		}

		private async Task SendTextAsync(byte[] textData)
		{
			await _webSocket.SendAsync(
				new ArraySegment<byte>(textData),
				WebSocketMessageType.Text,
				true,
				CancellationToken.None);
		}

		private async Task SendBinaryAsync(byte[] binaryData)
		{
			await _webSocket.SendAsync(
				new ArraySegment<byte>(binaryData),
				WebSocketMessageType.Binary,
				true,
				CancellationToken.None);
		}
	}
}
