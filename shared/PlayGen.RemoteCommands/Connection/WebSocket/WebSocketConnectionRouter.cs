﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PlayGen.RemoteCommands.Command;

namespace PlayGen.RemoteCommands.Connection.WebSocket
{
	public class WebSocketConnectionRouter
	{
		private readonly CommandRouter _commandRouter;
		private readonly WebSocketConnectionFactory _connectionFactory;
		private readonly ILogger<WebSocketConnectionRouter> _logger;

		public WebSocketConnectionRouter(
			CommandRouter commandRouter,
			WebSocketConnectionFactory connectionFactory,
			ILogger<WebSocketConnectionRouter> logger)
		{
			_commandRouter = commandRouter;
			_connectionFactory = connectionFactory;
			_logger = logger;
		}

		public async Task HandleWebSocket(System.Net.WebSockets.WebSocket webSocket)
		{
			var connection = _connectionFactory.Create(webSocket);
			await MessageLoop(connection);
		}

		private async Task MessageLoop(WebSocketConnection connection)
		{
			while (connection.State == WebSocketState.Open)
			{
				CommandRequest commandRequest = null;

				try
				{
					commandRequest = await connection.ReceiveCommandRequestAsync();

					if (commandRequest != null)
					{
						var response = ProcessRequest(commandRequest, connection);
						await connection.SendCommandResponse(response);
					}
				}
				catch (WebSocketException webSocketFailure)
				{
					_logger.LogError(webSocketFailure, "WebSocket Error.");

					await connection.TrySendClose();
				}
				catch (Exception commandProcessingFailure)
				{
					_logger.LogError(commandProcessingFailure, "Error while handling a request.");

					await connection.SendCommandResponse(new CommandResponse
					{
						RequestId = commandRequest?.Id,

						Result = commandProcessingFailure
					});
				}
			}
		}

		private CommandResponse ProcessRequest(CommandRequest request, WebSocketConnection connection)
		{
			object result;

			try
			{
				result = _commandRouter.ProcessCommand(request, connection);
			}
			catch (Exception commandFailure)
			{
				result = $"Failed to process the command: {commandFailure}";
			}

			var response = new CommandResponse
			{
				RequestId = request.Id,
				Result = result
			};

			return response;
		}
	}
}
