﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.RemoteCommands.Connection
{
    public interface IConnection<out TConnection>
	{
		event Action<TConnection> ConnectionClosed;
	}
}
