﻿using System;

namespace PlayGen.OpenTok.Server.Recording.Archive
{
    public class ArchiverFactory
    {
        private readonly Configuration _configuration = Configuration.Load();
		private readonly OpenTokManager _openTokManager;

		public event Action<global::OpenTok.Server.Archive> ArchiveStartedEvent;
		public event Action<global::OpenTok.Server.Archive> ArchiveCompletedEvent;

		public ArchiverFactory(OpenTokManager openTokManager)
		{
			_openTokManager = openTokManager;
		}

		public virtual Archiver Create(string sessionId)
        {
            var archiver = new Archiver(sessionId, _configuration.ArchiveLength, _openTokManager, TimeSpan.FromMinutes(_configuration.TimeoutMinutes));
			archiver.ArchiveStartedEvent += ArchiveStartedEvent;
			archiver.ArchiveCompletedEvent += ArchiveCompletedEvent;
			return archiver;
		}
	}
}
