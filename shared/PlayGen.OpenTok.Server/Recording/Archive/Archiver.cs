﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace PlayGen.OpenTok.Server.Recording.Archive
{
	public class Archiver
	{
		private readonly string _openTokMatchSessionId;
		private readonly int _archiveLength;
		private readonly OpenTokManager _openTokManager;
		private readonly CancellationTokenSource _disposedCancellation = new CancellationTokenSource();
		private readonly CancellationTokenSource _timeoutCancellation = new CancellationTokenSource();
		private readonly TimeSpan _timeout;

		private bool _isDisposed;

		public event Action<Archiver> TimedoutEvent;
		/// <summary>
		/// Beware that because this event is invoked as a new Task there is no guarantee that the event will be fired in the exact order in wich 
		/// archives were stopped.
		/// </summary>
		public event Action<global::OpenTok.Server.Archive> ArchiveStartedEvent;
		public event Action<global::OpenTok.Server.Archive> ArchiveCompletedEvent;

		public Archiver(string openTokMatchSessionId, int archiveLength, OpenTokManager openTokManager, TimeSpan timeout)
		{
			_openTokMatchSessionId = openTokMatchSessionId;
			_archiveLength = archiveLength;
			_openTokManager = openTokManager;
			_timeout = timeout;

			Log.Debug($"Created Archiver for Session: \"{_openTokMatchSessionId}\".");
		}

		public void Dispose()
		{
			if (_isDisposed) return;

			if (!_disposedCancellation.IsCancellationRequested)
			{
				_disposedCancellation.Cancel();
			}

			Log.Debug($"Disposed Archiver for Session: \"{_openTokMatchSessionId}\".");

			_isDisposed = true;
		}

		public async Task StartAsync()
		{
			_timeoutCancellation.CancelAfter(_timeout);
			Log.Debug($"Starting Archive for Session: {_openTokMatchSessionId} with length: {_archiveLength}.");

			while (!_timeoutCancellation.IsCancellationRequested && !_disposedCancellation.IsCancellationRequested)
			{
				try
				{
					var archive = await _openTokManager.StartArchiveAsync(_openTokMatchSessionId);
					BroadcastArchiveStartedAsync(archive);

					try
					{
						await Task.Delay(_archiveLength, _disposedCancellation.Token);
					}
					catch (TaskCanceledException) { }

					await _openTokManager.StopArchiveAsync(archive.Id.ToString());

					if (!_timeoutCancellation.IsCancellationRequested && !_disposedCancellation.IsCancellationRequested)
					{
						BroadcastArchiveCompletedAsync(archive);
					}
				}
				catch (Exception archivingFailure)
				{
					Log.Error($"Archiving for Session: \"{_openTokMatchSessionId}\". \nMessage: \"{archivingFailure.Message}\".");
				}
			}

			if (_timeoutCancellation.IsCancellationRequested)
			{
				Log.Debug($"Archiving for Session: \"{_openTokMatchSessionId}\" has timed out.");
				TimedoutEvent?.Invoke(this);
			}
		}

		private void BroadcastArchiveStartedAsync(global::OpenTok.Server.Archive archive)
		{
			Task.Run(() => ArchiveStartedEvent?.Invoke(archive));
		}

		private void BroadcastArchiveCompletedAsync(global::OpenTok.Server.Archive archive)
		{
			Task.Run(() => ArchiveCompletedEvent?.Invoke(archive));
		}
	}
}