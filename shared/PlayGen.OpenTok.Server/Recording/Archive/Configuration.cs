﻿using PlayGen.Configuration.Json;

namespace PlayGen.OpenTok.Server.Recording.Archive
{
	public class Configuration : IConfiguration
    {
        public int ArchiveLength { get; set; }

	    public int TimeoutMinutes { get; set; }

	    public static Configuration Load()
        {
            return ConfigurationLoader.Load<Configuration>();
        }
    }
}
