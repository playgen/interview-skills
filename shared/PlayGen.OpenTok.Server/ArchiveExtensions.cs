﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTok.Server;

namespace PlayGen.OpenTok.Server
{
    public static class ArchiveExtensions
    {
		public static bool IsDownloadable(this global::OpenTok.Server.Archive archive)
		{
			return archive?.Status == ArchiveStatus.AVAILABLE
					|| archive?.Status == ArchiveStatus.UPLOADED;
		}

		public static bool IsInOpenTokStorage(this global::OpenTok.Server.Archive archive)
		{
			return archive?.Status == ArchiveStatus.AVAILABLE;
		}

		public static bool IsInExternalStorage(this global::OpenTok.Server.Archive archive)
		{
			return archive?.Status == ArchiveStatus.UPLOADED;
		}
	}
}
