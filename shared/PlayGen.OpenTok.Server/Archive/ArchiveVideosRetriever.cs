﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PlayGen.OpenTok.Server.Contracts.Archive;
using PlayGen.WebUtils;

namespace PlayGen.OpenTok.Server.Archive
{
    public class ArchiveVideosRetriever
    {
		private readonly string[] _videoExtensions;
		private readonly string _metadataExtension;
		private readonly string _archiveUrl;
		private readonly string _saveDir;
		
		public ArchiveVideosRetriever(
			string[] videoExtensions, 
			string metadataExtension,
			string archiveUrl,
			string saveDir)
		{
			_videoExtensions = videoExtensions;
			_metadataExtension = metadataExtension;
			_archiveUrl = archiveUrl;
			_saveDir = saveDir;
		}

		public async Task<List<string>> RetrieveFiles()
		{
			var (unarchiver, metadata) = await GetArchive();

			var filenameByClient = metadata.Files.ToDictionary(
				f => f.GetConnetionData()[OpenTokManager.ClientIdKey],
				f => f.Filename);

			return SaveFiles(unarchiver, metadata, filenameByClient);
		}

		public async Task<string> RetrieveFile(string clientId)
		{
			var (unarchiver, metadata) = await GetArchive();

			var fileMetadata = metadata.Files
				.Single(m => m.GetConnetionData()[OpenTokManager.ClientIdKey] == clientId);

			var filenameByClient = new Dictionary<string, string>
			{
				{clientId, fileMetadata.Filename}
			};

			return SaveFiles(unarchiver, metadata, filenameByClient).Single();
		}

		private List<string> SaveFiles(
			Unarchiver unarchiver,
			ArchiveMetadata metadata,
			Dictionary<string, string> filenameByClient)
		{
			var saveFilePaths = new List<string>(filenameByClient.Count);

			foreach (var filenameAndClient in filenameByClient)
			{
				var extension = Path.GetExtension(filenameAndClient.Value);
				var clientId = filenameAndClient.Key;
				var saveFilePath = $"{_saveDir}/{metadata.SessionId}/{clientId}_{metadata.CreatedAt}_{metadata.Id}{extension}";

				unarchiver.Save(filenameAndClient.Value, saveFilePath);
				saveFilePaths.Add(saveFilePath);
			}

			return saveFilePaths;
		}

		private async Task<(Unarchiver, ArchiveMetadata)> GetArchive()
		{
			var downloader = new Downloader(_archiveUrl);
			var archive = await downloader.DownloadAsync();

			var unarchiver = new Unarchiver(archive);
			unarchiver.Extract(
				_videoExtensions,
				_metadataExtension,
				out var _,
				out var metadata);

			var metadataContent = Encoding.UTF8.GetString(metadata);
			var archiveMetadata = JsonConvert.DeserializeObject<ArchiveMetadata>(metadataContent);

			return (unarchiver, archiveMetadata);
		}
    }
}
