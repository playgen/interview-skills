﻿using PlayGen.Configuration.Json;

namespace PlayGen.OpenTok.Server.Archive
{
    public class Configuration : IConfiguration
    {
        public string[] VideoExtensions { get; set; }

        public string MetadataExtension { get; set; }

        public bool SaveArchive { get; set; }

        public string SaveArchiveDir { get; set; }

        public static Configuration Load()
        {
            return ConfigurationLoader.Load<Configuration>();
        }
    }
}
