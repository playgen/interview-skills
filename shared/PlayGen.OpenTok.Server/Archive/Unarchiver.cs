﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Serilog;
using PlayGen.StreamUtils;
using PlayGen.PathUtils;

namespace PlayGen.OpenTok.Server.Archive
{
	public class Unarchiver
    {
        private readonly byte[] _compressedBytes;

        public Unarchiver(byte[] compressedBytes)
        {
            _compressedBytes = compressedBytes;
        }

        public void Extract(string[] videoExtensions, string metadataExtension, out Dictionary<string, byte[]> videos, out byte[] metadata)
        {
            videos = new Dictionary<string, byte[]>();
            metadata = new byte[0];

            using (var compressedStream = new MemoryStream(_compressedBytes))
            {
                var archive = new ZipArchive(compressedStream, ZipArchiveMode.Read);

                foreach (var entry in archive.Entries)
                {
                    using (var stream = entry.Open())
                    {
                        var extension = Path.GetExtension(entry.Name).ToLower();

                        if (videoExtensions.Contains(extension))
                        {
                            videos[entry.Name] = stream.ToByteArray();
                        }
                        else if (metadataExtension == extension)
                        {
                            if (metadata.Length != 0)
                            {
                                Log.Warning("Metadata has already been set.");
                            }
                            else
                            {
                                metadata = stream.ToByteArray();
                            }
                        }
                        else
                        {
                            stream.Dispose();
                            Log.Warning("Unhandled file extension");
                        }
                    }
                }
            }
        }

        public void Save(string saveArchiveDir)
        {
            using (var compressedStream = new MemoryStream(_compressedBytes))
            {
                var archive = new ZipArchive(compressedStream, ZipArchiveMode.Read);
                DirectoryUtils.RemoveAddParentDir(saveArchiveDir);
                archive.ExtractToDirectory(saveArchiveDir);
            }
        }

		public void Save(string entryName, string saveArchiveFile)
		{
			using (var compressedStream = new MemoryStream(_compressedBytes))
			{
				var archive = new ZipArchive(compressedStream, ZipArchiveMode.Read);
				DirectoryUtils.RemoveAddParentDir(saveArchiveFile);
				archive.GetEntry(entryName).ExtractToFile(saveArchiveFile);
			}
		}
	}
}