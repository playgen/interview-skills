﻿using System.Threading.Tasks;
using OpenTok.Server;

namespace PlayGen.OpenTok.Server
{
	public class OpenTokManager
	{
		public const string ClientIdKey = "clientId";
		public const string ConnectionDataTemplate = ClientIdKey + ":{0}";

		private readonly global::OpenTok.Server.OpenTok _openTok;
		private readonly MediaMode _mediaMode;
		private readonly OutputMode _outputMode;

		public int ApiKey => _openTok.ApiKey;

		public OpenTokManager(
			int apiKey, 
			string apiSecret, 
			MediaMode mediaMode = MediaMode.ROUTED, 
			OutputMode outputMode = OutputMode.INDIVIDUAL)
		{
			_mediaMode = mediaMode; 
			_outputMode = outputMode; 
			 _openTok = new global::OpenTok.Server.OpenTok(apiKey, apiSecret);
	    }

		public string ExtractClientId(string connectionData)
		{
			return connectionData.Replace(ConnectionDataTemplate.Replace("{0}", null), null);
		}
        
        public string GenerateToken(string openTokSessionId, string clientId, Role role = Role.PUBLISHER)
        {
            return _openTok.GenerateToken(openTokSessionId, role, data: string.Format(ConnectionDataTemplate, clientId));
        }

        public async Task<Session> CreateSessionAsync()
	    {
		    return await _openTok.CreateSession(mediaMode: _mediaMode);
	    }

        public async Task<global::OpenTok.Server.Archive> StartArchiveAsync(string openTokSessionId)
        {
            return await _openTok.StartArchive(openTokSessionId, openTokSessionId, true, true, _outputMode);
        }

        public async Task<global::OpenTok.Server.Archive> StopArchiveAsync(string archiveId)
        {
            return await _openTok.StopArchive(archiveId);
        }

        public async Task<global::OpenTok.Server.Archive> GetArchiveAsync(string archiveId)
        {
            return await _openTok.GetArchive(archiveId);
        }

        public async Task<ArchiveList> ListArchivesAsync(int offset = 0, int count = 0)
        {
            return await _openTok.ListArchives(offset, count);
        }
		
        public async Task DeleteArchiveAsync(string archiveId)
        {
            await _openTok.DeleteArchive(archiveId);
        }
    }
}
