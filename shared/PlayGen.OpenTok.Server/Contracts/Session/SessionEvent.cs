﻿namespace PlayGen.OpenTok.Server.Contracts.Session
{
    public class SessionEvent
    {
        public string SessionId { get; set; }

        public string PartnerId { get; set; }

        public string Event { get; set; }
    }
}
