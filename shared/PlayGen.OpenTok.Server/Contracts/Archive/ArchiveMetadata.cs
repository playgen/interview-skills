﻿namespace PlayGen.OpenTok.Server.Contracts.Archive
{
    public class ArchiveMetadata
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string SessionId { get; set; }

        public long CreatedAt { get; set; }

        public FileMetadata[] Files { get; set; }
    }
}