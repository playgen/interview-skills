﻿using System.Collections.Generic;
using System.Linq;

namespace PlayGen.OpenTok.Server.Contracts.Archive
{
    public class FileMetadata
    {
        public string ConnectionData { get; set; }

        public string Filename { get; set; }

        public int Size { get; set; }

        public long StartTimeOffset { get; set; }

        public long StopTimeOffset { get; set; }

        public string StreamId { get; set; }

        public Dictionary<string, string> GetConnetionData()
        {
            return ConnectionData.Split(';')
                .Select(keyValue => keyValue.Split(':'))
                .ToDictionary(keyValueSplit => keyValueSplit[0], 
                    keyValueSplit => keyValueSplit[1]);
        }
    }
}
