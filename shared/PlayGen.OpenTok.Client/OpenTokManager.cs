﻿using OpenTok;

namespace PlayGen.OpenTok.Client
{
    public class OpenTokManager
    {
        private readonly Configuration _configuration;

        public OpenTokManager()
        {
            _configuration = Configuration.Load();
        }

        public SessionWrapper CreateSession(string sessionId)
        {
            return new SessionWrapper(_configuration.APIKey.ToString(), sessionId);
        }
    }
}
