﻿using System;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace PlayGen.OpenTok.Client
{
	public class Configuration
    {
		public int APIKey { get; set; }

        public static Configuration Load()
        {
            var type = typeof(Configuration);
		    var assembly = type.GetTypeInfo().Assembly;
            var uriBuilder = new UriBuilder(assembly.CodeBase);
            var assemblyDir = Path.GetDirectoryName(Uri.UnescapeDataString(uriBuilder.Path));
            var path = $"{assemblyDir}/{type.FullName}.json";
			var data = File.ReadAllText(path);
			return JsonConvert.DeserializeObject<Configuration>(data);
		}
    }
}
