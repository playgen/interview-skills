﻿using OpenTok;

namespace PlayGen.OpenTok.Client
{
    public class SessionWrapper
    {
        private readonly Session _session;

        public SessionWrapper(string apiKey, string sessionId)
        {
            _session = new Session(Context.Instance, apiKey, sessionId);
        }
    }
}
