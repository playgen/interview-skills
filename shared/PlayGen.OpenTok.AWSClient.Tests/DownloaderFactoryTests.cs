using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace PlayGen.OpenTok.AWSClient.Tests
{
	public class DownlaoderFactoryTests : TestsBase
	{
		private readonly DownloaderFactory _downloaderFactory;

		public DownlaoderFactoryTests()
		{
			var accessKey = Configuration.GetValue<string>("AWS:AccessKey");
			var secretKey = Configuration.GetValue<string>("AWS:SecretKey");
			var bucket = Configuration.GetValue<string>("AWS:Bucket");
			var apiKey = Configuration.GetValue<string>("OpenTok:APIKey");
			var keyTemplate = Configuration.GetValue<string>("AWS:KeyTemplate");

			_downloaderFactory = new DownloaderFactory(accessKey, secretKey, bucket, apiKey, keyTemplate);
		}

		// These archives need to exist in the S3 storage
		[Theory]
		[InlineData("20881dc5-1ce3-4d7f-ae9b-1fc4a34cb427")]
		[InlineData("68bbecf0-01a4-46a4-8999-195b9afa6e85")]
		public async void CanDownload(string archiveId)
		{
			// Arrange
			var downloader = _downloaderFactory.Create(archiveId);

			// Act
			var data = await downloader.DownloadAsync();

			// Assert
			Assert.NotEmpty(data);
		}
	}
}
