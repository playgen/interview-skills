using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace PlayGen.OpenTok.AWSClient.Tests
{
	public abstract class TestsBase
	{
		protected readonly IConfigurationRoot Configuration;

		protected TestsBase()
		{
			var builder = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json");

			Configuration = builder.Build();
		}
	}
}
