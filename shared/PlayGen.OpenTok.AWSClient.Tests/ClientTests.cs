using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace PlayGen.OpenTok.AWSClient.Tests
{
	public class ClientTests : TestsBase
	{
		private readonly AmazonS3Client _client;
		
		public ClientTests()
		{
			var options = new CredentialProfileOptions
			{
				AccessKey = Configuration.GetValue<string>("AWS:AccessKey"),
				SecretKey = Configuration.GetValue<string>("AWS:SecretKey")
			};

			var credentials = new BasicAWSCredentials(options.AccessKey, options.SecretKey);
			_client = new AmazonS3Client(credentials, RegionEndpoint.EUWest1);
		}
		
		[Fact]
		public async Task<List<S3Object>> CanList()
		{
			// Act
			var bucket = Configuration.GetValue<string>("AWS:Bucket");
			var response = await _client.ListObjectsAsync(bucket);

			// Assert
			Assert.NotEmpty(response.S3Objects);

			return response.S3Objects;
		}

		[Fact]
		public async void CanDownload()
		{
			// Arrange
			var objects = await CanList();
			var toDownloads = objects
				.Where(o => o.Key.StartsWith(Configuration.GetValue<string>("OpenTok:APIKey")))
				.Take(10);

			// Act
			foreach (var toDownload in toDownloads)
			{
				await _client.GetObjectAsync(toDownload.BucketName, toDownload.Key);
			}
		}
	}
}
