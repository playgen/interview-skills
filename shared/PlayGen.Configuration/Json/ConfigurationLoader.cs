﻿using System;
using System.IO;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace PlayGen.Configuration.Json
{
	public static class ConfigurationLoader
    {
        public static TConfiguration Load<TConfiguration>() 
            where TConfiguration : IConfiguration
		{
			var type = typeof(TConfiguration);
            var assembly = type.GetTypeInfo().Assembly;
            var uriBuilder = new UriBuilder(assembly.CodeBase);
            var assemblyDir = Path.GetDirectoryName(Uri.UnescapeDataString(uriBuilder.Path));
            var path = $"{assemblyDir}/{type.FullName.Replace(assembly.GetName().Name, "").Replace(".", "/")}.json";
            var data = File.ReadAllText(path);
            var json = JObject.Parse(data);

#if DEBUG
            var developmentPath = $"{Path.GetFileNameWithoutExtension(path)}.debug{Path.GetExtension(path)}";
            if (File.Exists(developmentPath))
            {
                var developmentData = File.ReadAllText(developmentPath);
                var developmentJson = JObject.Parse(developmentData);

                json.Merge(developmentJson);
            }
#endif

            return json.ToObject<TConfiguration>();
        }
    }
}
