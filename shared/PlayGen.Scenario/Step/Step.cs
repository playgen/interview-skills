﻿namespace PlayGen.Scenario.Step
{
	public class Step
    {
        public string Id { get; set; }

		public virtual Action.Action[] Actions { get; set; }
	}
}
