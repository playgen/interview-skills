﻿namespace PlayGen.Scenario.Step.Action.Command
{
	public class EnqueueActions : IOnConditionCommand
	{
		public Action[] Actions { get; set; }
	}
}
