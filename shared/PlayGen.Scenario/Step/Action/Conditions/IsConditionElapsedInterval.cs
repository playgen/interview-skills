﻿namespace PlayGen.Scenario.Step.Action.Conditions
{
    public class IsConditionElapsedInterval : ICondition
    {
        public int Interval { get; set; }

		public int RepeatCount { get; set; }
    }
}
