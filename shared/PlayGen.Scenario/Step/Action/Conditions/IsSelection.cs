﻿namespace PlayGen.Scenario.Step.Action.Conditions
{
    public abstract class IsSelection : ICondition
    {
        public string[] Selection { get; set; }
    }
}
