﻿namespace PlayGen.Scenario.Step.Action.Conditions
{
    public class Any : ICondition
    {
        public ICondition[] Conditions { get; set; }
    }
}
