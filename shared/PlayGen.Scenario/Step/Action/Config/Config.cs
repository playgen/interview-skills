﻿namespace PlayGen.Scenario.Step.Action.Config
{
    public class Config
    {
        public DiscardType DiscardType { get; set; }

        public BlockingType BlockingType { get; set; }
    }
}
