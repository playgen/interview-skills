﻿namespace PlayGen.Scenario.Step.Action.Config
{
    public enum DiscardType
    {
        FirstEvaluation = 0,

        FirstTrue = 1,

        Never = 2,
    }
}
