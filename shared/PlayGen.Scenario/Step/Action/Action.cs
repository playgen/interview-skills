﻿using PlayGen.Scenario.Step.Action.Command;
using PlayGen.Scenario.Step.Action.Conditions;

namespace PlayGen.Scenario.Step.Action
{
	public class Action
	{
        public Config.Config Config { get; set; }

        public ICondition[] Conditions { get; set; }

        public IOnConditionCommand[] OnConditionCommands { get; set; }

		public IOnDiscardCommand[] OnDiscardCommands { get; set; }
	}
}
