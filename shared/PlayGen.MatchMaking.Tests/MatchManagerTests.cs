using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using PlayGen.MatchMaking.Matched;
using PlayGen.MatchMaking.Matching;
using Xunit;

namespace PlayGen.MatchMaking.Tests
{
	public class MatchManagerTests
	{
		private const int DefaultMinimumParticipants = 1;
		private const int DefaultUnmatchedKeepAliveTimeout = 500;
		private const int PruneOverheadLeeway = DefaultUnmatchedKeepAliveTimeout + 100;
		private const int DefaultPruneInterval = 500;

		[Fact]
		private void DoesntMatchSelf()
		{
			// Arrange
			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => true);

			var participant1 = new MatchingParticipant("Shouldn't Match 1", participantPairMatcher);
			var participant2 = new MatchingParticipant("Shouldn't Match 1", participantPairMatcher);

			// Act
			var didParticipant1FindMatches = matchManager.TryFindMatches(participant1, out var participant1Matches);
			var didParticipant2FindMatches = matchManager.TryFindMatches(participant2, out var participant2Matches);

			// Assert
			Assert.False(didParticipant1FindMatches);
			Assert.False(didParticipant2FindMatches);

			Assert.Empty(participant1Matches);
			Assert.Empty(participant2Matches);
		}

		[Fact]
		private void DoesntMatchUnmatching()
		{
			// Arrange
			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => false);

			var participant1 = new MatchingParticipant("Shouldn't Match 1", participantPairMatcher);
			var participant2 = new MatchingParticipant("Shouldn't Match 2", participantPairMatcher);

			// Act
			var didParticipant1FindMatches = matchManager.TryFindMatches(participant1, out var participant1Matches);
			var didParticipant2FindMatches = matchManager.TryFindMatches(participant2, out var participant2Matches);

			// Assert
			Assert.False(didParticipant1FindMatches);
			Assert.False(didParticipant2FindMatches);

			Assert.Empty(participant1Matches);
			Assert.Empty(participant2Matches);
		}

		[Fact]
		private void CanRemoveFromMatch()
		{
			// Arrange
			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => true);

			var participant1 = new MatchingParticipant("1", participantPairMatcher);
			var participant2 = new MatchingParticipant("2", participantPairMatcher);

			// Act
			matchManager.TryFindMatches(participant1, out var participant1MatchesFirstAttempt);
			matchManager.TryFindMatches(participant2, out var participant2MatchesFirstAttempt);

			var didRemove = matchManager.RemoveParticipant(participant2MatchesFirstAttempt[0].Id, participant1.Id);

			var didParticipant1SecondMatchAttemptSucceed = matchManager.TryFindExistingMatches(participant1.Id, out var participant1SecondMatchesAttempt);
			var didParticipant2SecondMatchAttemptSucceed = matchManager.TryFindExistingMatches(participant2.Id, out var participant2SecondMatchesAttempt);

			// Assert
			Assert.True(didRemove);
			Assert.False(didParticipant1SecondMatchAttemptSucceed);
			Assert.Empty(participant1SecondMatchesAttempt);

			Assert.True(didParticipant2SecondMatchAttemptSucceed);
			Assert.Equal(1, participant2SecondMatchesAttempt.Count);
			Assert.Same(participant2, participant2SecondMatchesAttempt[0].Participants[0].MatchingParticipant);
		}

		[Fact]
		private void CanRemoveFromMatchAndUnmatched()
		{
			// Arrange
			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => true);

			var participant1 = new MatchingParticipant("1", participantPairMatcher);
			var participant2 = new MatchingParticipant("2", participantPairMatcher);

			// Act
			matchManager.TryFindMatches(participant1, out var participant1MatchesFirstAttempt);
			matchManager.TryFindMatches(participant2, out var participant2MatchesFirstAttempt);

			var didRemove = matchManager.RemoveParticipant(participant1.Id);

			var didParticipant1SecondMatchAttemptSucceed = matchManager.TryFindExistingMatches(participant1.Id, out var participant1SecondMatchesAttempt);
			var didParticipant2SecondMatchAttemptSucceed = matchManager.TryFindExistingMatches(participant2.Id, out var participant2SecondMatchesAttempt);

			// Assert
			Assert.True(didRemove);
			Assert.False(didParticipant1SecondMatchAttemptSucceed);
			Assert.Empty(participant1SecondMatchesAttempt);

			Assert.True(didParticipant2SecondMatchAttemptSucceed);
			Assert.Equal(1, participant2SecondMatchesAttempt.Count);
			Assert.Same(participant2, participant2SecondMatchesAttempt[0].Participants[0].MatchingParticipant);
		}

		[Fact]
		private void TransfersMasterWhenRemoved()
		{
			// Arrange
			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => true);

			var participant1 = new MatchingParticipant("Should End As Master", participantPairMatcher);
			var participant2 = new MatchingParticipant("Should Start As Master", participantPairMatcher);

			// Act
			matchManager.TryFindMatches(participant1, out var participant1FirstMatchRequest);
			matchManager.TryFindMatches(participant2, out var participant2Match);

			Assert.Equal(1, participant2Match.Count);
			Assert.NotEqual(participant1, participant2Match[0].MasterParticipant.MatchingParticipant);

			matchManager.RemoveParticipant(participant2.Id);
			matchManager.TryFindMatches(participant1, out var participant1SecondMatchesRequest);

			// Assert			
			Assert.Equal(1, participant1SecondMatchesRequest.Count);
			Assert.Equal(participant1, participant1SecondMatchesRequest[0].MasterParticipant.MatchingParticipant);
		}

		[Fact]
		private void RemovedFromPreviousMatchWhenFindNew()
		{
			// Arrange
			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => true);

			var participant1 = new MatchingParticipant("1", participantPairMatcher);
			var participant2 = new MatchingParticipant("2", participantPairMatcher);
			var participant3 = new MatchingParticipant("3", participantPairMatcher);

			// Act
			matchManager.TryFindNewMatch(participant1, out var participant1FirstMatch);
			matchManager.TryFindNewMatch(participant2, out var participant2FirstMatch);

			matchManager.TryFindNewMatch(participant1, out var participant1MatchSecondAttempt);

			var didParticipant3FindMatch = matchManager.TryFindNewMatch(participant3, out var participant3SecondMatch);
			var didParticipant1Rematch = matchManager.TryFindExistingMatches(participant1.Id, out var participant1SecondMatches);
			var isParticpant2StillMatched = matchManager.TryFindExistingMatches(participant2.Id, out var participant2FirstMatches);

			// Assert
			Assert.True(didParticipant3FindMatch);
			Assert.True(didParticipant1Rematch);

			Assert.Same(participant1SecondMatches[0], participant3SecondMatch);
			Assert.Equal(1, participant1SecondMatches.Count);
			Assert.Equal(2, participant1SecondMatches[0].Participants.Count);
			Assert.Subset(new HashSet<MatchingParticipant>(participant1SecondMatches[0].Participants
				.Select(p => p.MatchingParticipant)), new HashSet<MatchingParticipant> { participant1, participant3 });

			Assert.True(isParticpant2StillMatched);
			Assert.Equal(1, participant2FirstMatches.Count);
			Assert.Equal(1, participant2FirstMatches[0].Participants.Count);
			Assert.Same(participant2, participant2FirstMatches[0].Participants[0].MatchingParticipant);
		}

		[Fact]
		private void FindMatch_HandlesDuplicateRequests()
		{
			// Arrange
			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => true);

			var participant1 = new MatchingParticipant("Should Match 1", participantPairMatcher);
			var participant2 = new MatchingParticipant("Should Match 2", participantPairMatcher);

			var particpant3 = new MatchingParticipant("Shouldn't Match", participantPairMatcher);

			// Act
			var didParticipant1FindMatchFirstTry = matchManager.TryFindMatches(participant1, out var participant1MatchesFirstTry);
			var didParticipant2FindMatch = matchManager.TryFindMatches(participant2, out var participant2Matches);
			var didParticipant3FindMatch = matchManager.TryFindMatches(particpant3, out var participant3Matches);
			var didParticipant1FindMatchSecondTry = matchManager.TryFindMatches(participant1, out var participant1MatchesSecondTry);

			// Assert
			Assert.False(didParticipant1FindMatchFirstTry);
			Assert.Empty(participant1MatchesFirstTry);

			Assert.True(didParticipant2FindMatch);
			Assert.Equal(1, participant2Matches.Count);

			Assert.False(didParticipant3FindMatch);
			Assert.Empty(participant3Matches);

			Assert.True(didParticipant1FindMatchSecondTry);
			Assert.Equal(1, participant1MatchesSecondTry.Count);
		}

		[Theory]
		[InlineData("1")]
		[InlineData("1", "2")]
		[InlineData("1", "2", "3")]
		[InlineData("1", "2", "3", "4")]
		[InlineData("1", "2", "3", "4", "5")]
		private void FindMatch_HandlesUniqueRequests(params string[] participantIds)
		{
			// Arrange
			var participantIdPairs = GenerateParticipantPairs(participantIds);

			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => true);

			// Act
			foreach (var pair in participantIdPairs)
			{
				var firstParticipantConfig = new MatchingParticipant(pair[0], participantPairMatcher);

				var didFindFirsMatch = matchManager.TryFindMatches(firstParticipantConfig, out var firstParticipantMatches);

				// Assert
				Assert.False(didFindFirsMatch);
				Assert.Empty(firstParticipantMatches);

				if (pair.Length == 2)
				{
					var secondParticipantConfig = new MatchingParticipant(pair[1], participantPairMatcher);
					var didFindSecondMatch = matchManager.TryFindMatches(secondParticipantConfig, out var secondParticipantMatches);
					// second and first user have been matched

					// Assert
					AssertValidMatchPair(didFindSecondMatch, secondParticipantMatches, firstParticipantConfig, secondParticipantConfig);

					didFindFirsMatch =
						matchManager.TryFindMatches(firstParticipantConfig, out firstParticipantMatches);

					Assert.True(didFindFirsMatch);
					Assert.Same(secondParticipantMatches[0], firstParticipantMatches[0]);
				}
			}
		}

		[Theory]
		[InlineData("1")]
		[InlineData("1", "2")]
		[InlineData("1", "2", "3")]
		[InlineData("1", "2", "3", "4")]
		[InlineData("1", "2", "3", "4", "5")]
		[InlineData("1", "2", "1")]
		[InlineData("1", "2", "1", "3")]
		[InlineData("1", "2", "1", "3", "1")]
		private void CanFindNewAndExistingMatch(params string[] participantIds)
		{
			// Arrange
			var participantIdPairs = GenerateParticipantPairs(participantIds);

			var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval);
			var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => true);

			// Act
			foreach (var pair in participantIdPairs)
			{
				var firstParticipantConfig = new MatchingParticipant(pair[0], participantPairMatcher);

				var didFindFirsMatch = matchManager.TryFindNewMatch(firstParticipantConfig, out var firstParticipantMatch);

				// Assert
				Assert.False(didFindFirsMatch);
				Assert.Null(firstParticipantMatch);

				if (pair.Length == 2)
				{
					var secondParticipantConfig = new MatchingParticipant(pair[1], participantPairMatcher);
					var didFindSecondMatch = matchManager.TryFindNewMatch(secondParticipantConfig, out var secondParticipantMatch);
					// second and first user have been matched

					// Assert
					AssertValidMatchPair(didFindSecondMatch, new List<Match> {secondParticipantMatch}, firstParticipantConfig, secondParticipantConfig);

					didFindFirsMatch =
						matchManager.TryFindExistingMatches(firstParticipantConfig.Id, out var firstParticipantMatches);

					Assert.True(didFindFirsMatch);
					Assert.Same(secondParticipantMatch, firstParticipantMatches[0]);
				}
			}
		}

		[Fact]
		private async void PrunesUnmatched()
		{
			// Arrange
			using (var matchManager = new MatchManager(DefaultMinimumParticipants, DefaultUnmatchedKeepAliveTimeout, DefaultPruneInterval))
			{
				matchManager.Start();

				var participantPairMatcher = new MatchMakingParticipantComparer((a, b) => false);

				var participant1Id = "Should get pruned 1";
				var participant2Id = "Should get pruned 2";

				var participant1 = new MatchingParticipant(participant1Id, participantPairMatcher);
				var participant2 = new MatchingParticipant(participant2Id, participantPairMatcher);

				var didParticipant1FindMatch = matchManager.TryFindMatches(participant1, out var participant1Matches);
				var didParticipant2FindMatch = matchManager.TryFindMatches(participant2, out var participant2Matches);

				Assert.True(matchManager.IsAwaitingMatch(participant1Id));
				Assert.True(matchManager.IsAwaitingMatch(participant2Id));

				// Act
				await Task.Delay(DefaultUnmatchedKeepAliveTimeout + PruneOverheadLeeway);

				// Assert
				Assert.False(matchManager.IsAwaitingMatch(participant1Id));
				Assert.False(matchManager.IsAwaitingMatch(participant2Id));
			}
		}

		private void AssertValidMatchPair(bool didFindMatch, List<Match> matches, MatchingParticipant firstMatchingParticipant, MatchingParticipant secondMatchingParticipant)
		{
			Assert.True(didFindMatch);
			Assert.Equal(1, matches.Count);
			Assert.Equal(2, matches[0].Participants.Count);
			Assert.Contains(firstMatchingParticipant, matches[0].Participants.Select(p => p.MatchingParticipant));
			Assert.Contains(secondMatchingParticipant, matches[0].Participants.Select(p => p.MatchingParticipant));
			Assert.Equal(1, matches[0].Participants.Count(p => p.IsMaster));
			Assert.Same(secondMatchingParticipant, matches[0].Participants.Single(p => p.IsMaster).MatchingParticipant);
		}

		private List<string[]> GenerateParticipantPairs(string[] participantIds)
		{
			return participantIds.Select((x, i) => new { Index = i, Value = x })
				.GroupBy(x => x.Index / 2)
				.Select(x => x.Select(v => v.Value).ToArray())
				.ToList();
		}
	}
}
