﻿using System;
using PlayGen.CustomData;
using Xunit;

namespace PlayGen.MatchMaking.Tests
{
    public class CustomDataTests
    {
        [Fact]
        private void CanAddAndRetrieve()
        {
            // Arrange 
            var customDataContainer = new TestCustoMDataContainer();
            var customDataA = new TestCustoMDataA();
            var customDataB = new TestCustoMDataB();
            var customDataC = new TestCustoMDataC();

            // Act
            customDataContainer.AddCustomData(customDataA);
            customDataContainer.AddCustomData(customDataB);
            customDataContainer.AddCustomData(customDataC);

            var retrievedCustomDataA = customDataContainer.GetCustomData<TestCustoMDataA>();
            var retrievedCustomDataB = customDataContainer.GetCustomData<TestCustoMDataB>();
            var retrievedCustomDataC = customDataContainer.GetCustomData<TestCustoMDataC>();

            // Assert
            Assert.Same(customDataA, retrievedCustomDataA);
            Assert.Same(customDataB, retrievedCustomDataB);
            Assert.Same(customDataC, retrievedCustomDataC);
        }

        [Fact]
        private void CantAddDuplicate()
        {
            // Arrange 
            var customDataContainer = new TestCustoMDataContainer();
            var customDataA = new TestCustoMDataA();

            customDataContainer.AddCustomData(customDataA);

            // Act & Assert
            Assert.Throws<ArgumentException>(() => customDataContainer.AddCustomData(customDataA));

        }
        
        private class TestCustoMDataContainer : CustomDataContainer<ICustomData> {}

        private class TestCustoMDataA : ICustomData { }

        private class TestCustoMDataB : ICustomData { }

        private class TestCustoMDataC : ICustomData { }
    }
}
