﻿using PlayGen.MatchMaking.Matching;

namespace PlayGen.MatchMaking.Tests
{
	public class MatchMakingParticipantComparer : IMatchMakingParticipantComparer
    {
        public delegate bool ComparerFunction(MatchingParticipant a, MatchingParticipant b);

        private readonly ComparerFunction _comparerFunction;

        public MatchMakingParticipantComparer(ComparerFunction comparerFunction)
        {
            _comparerFunction = comparerFunction;
        }

        public bool AreMatch(MatchingParticipant a, MatchingParticipant b)
        {
            return _comparerFunction(a, b);
        }
    }
}
