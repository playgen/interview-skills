﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using PlayGen.WebUtils;

namespace PlayGen.OpenTok.AWSClient
{
	public class Downloader : IDownloader
	{
		private readonly RegionEndpoint _region;
		private readonly AWSCredentials _credentials;
		private readonly string _bucket;
		private readonly string _key;

		public Downloader(AWSCredentials credentials, RegionEndpoint region, string bucket, string key)
		{
			_credentials = credentials;
			_region = region;
			_bucket = bucket;
			_key = key;
		}

		public async Task<byte[]> DownloadAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			using (var client = new AmazonS3Client(_credentials, _region))
			using (var response = await client.GetObjectAsync(_bucket, _key, cancellationToken))
			using (var memoryStream = new MemoryStream())
			{
				response.ResponseStream.CopyTo(memoryStream);
				return memoryStream.ToArray();
			}
		}
	}
}
