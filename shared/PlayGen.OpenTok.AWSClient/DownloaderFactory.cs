﻿using Amazon;
using Amazon.Runtime;

namespace PlayGen.OpenTok.AWSClient
{
    public class DownloaderFactory
	{
		private readonly AWSCredentials _credentials;
		private readonly RegionEndpoint _region = RegionEndpoint.EUWest1;
		private readonly string _bucket;
		private readonly string _dir;
		private readonly string _keyTemplate;

		public DownloaderFactory(
			string accessKey,
			string secretKey,
			string bucket,
			string dir,
			string keyTemplate)
		{
			_credentials = new BasicAWSCredentials(accessKey, secretKey);
			_bucket = bucket;
			_dir = dir;
			_keyTemplate = keyTemplate;
		}

		public Downloader Create(string archiveId)
		{
			var key = string.Format(_keyTemplate, _dir, archiveId);
			var downloader = new Downloader(_credentials, _region, _bucket, key);
			return downloader;
		}
    }
}
