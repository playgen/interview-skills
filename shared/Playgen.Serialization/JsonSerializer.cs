﻿using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace Playgen.Serialization
{
    public class JsonSerializer : ISerializer
	{
		private readonly Newtonsoft.Json.JsonSerializer _jsonSerializer;

		public JsonSerializer(JsonSerializerSettings settings)
		{
			_jsonSerializer = Newtonsoft.Json.JsonSerializer.Create(settings);
		}

		public T DeserializeText<T>(byte[] buffer, int length)
		{
			using (var stream = new MemoryStream(buffer, 0, length))
			using (var streamReader = new StreamReader(stream, Encoding.ASCII))
			using (var jsonReader = new JsonTextReader(streamReader))
			{
				var deserialized = _jsonSerializer.Deserialize<T>(jsonReader);
				return deserialized;
			}
		}

		public MemoryStream SerializeText(object deserialized)
		{
			var stream = new MemoryStream();
			using (var streamWriter = new StreamWriter(stream, Encoding.ASCII))
			using (var jsonWriter = new JsonTextWriter(streamWriter))
			{
				_jsonSerializer.Serialize(jsonWriter, deserialized);
				streamWriter.Flush();
			}

			return stream;
		}

		public T DeserializeBinary<T>(byte[] buffer, int length)
		{
			using (var stream = new MemoryStream(buffer, 0, length))
			using (var reader = new BsonReader(stream))
			{
				var deserialized = _jsonSerializer.Deserialize<T>(reader);
				return deserialized;
			}
		}

		public MemoryStream SerializeBinary(object deserialized)
		{
			var stream = new MemoryStream();
			using (var writer = new BsonWriter(stream))
			{
				_jsonSerializer.Serialize(writer, deserialized);
				writer.Flush();
			}

			return stream;
		}
	}
}
