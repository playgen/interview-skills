﻿using System.IO;

namespace Playgen.Serialization
{
	public interface ISerializer
	{
		T DeserializeText<T>(byte[] buffer, int length);

		MemoryStream SerializeText(object deserialized);

		T DeserializeBinary<T>(byte[] buffer, int length);

		MemoryStream SerializeBinary(object deserialized);
	}
}