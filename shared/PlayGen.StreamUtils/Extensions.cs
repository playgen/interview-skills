﻿using System.IO;

namespace PlayGen.StreamUtils
{
	public static class Extensions
    {
        public static byte[] ToByteArray(this Stream stream)
        {
            byte[] buffer;

            if (stream.CanWrite)
            {
                stream.Position = 0;
                buffer = new byte[stream.Length];
                stream.Write(buffer, 0, buffer.Length);
            }
            else
            {
                using (var memoryStream = new MemoryStream())
                {
                    stream.CopyTo(memoryStream);                    
                    buffer = memoryStream.ToArray();
                }
            }
            return buffer;
        }
    }
}
