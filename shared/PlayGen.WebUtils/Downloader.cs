﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace PlayGen.WebUtils
{
    public class Downloader : IDownloader
    {
        private readonly string _url;

        public Downloader(string url)
        {
            _url = url;
        }
		
        public async Task<byte[]> DownloadAsync(CancellationToken cancellationToken = default (CancellationToken))
        {
            using (var client = new HttpClient())
            using (var response = await client.GetAsync(_url, cancellationToken))
            {
                return await response.Content.ReadAsByteArrayAsync();
            }
        }
    }
}
