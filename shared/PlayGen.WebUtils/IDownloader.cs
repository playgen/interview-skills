﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlayGen.WebUtils
{
	public interface IDownloader
	{
		Task<byte[]> DownloadAsync(CancellationToken cancellationToken = default(CancellationToken));
	}
}
