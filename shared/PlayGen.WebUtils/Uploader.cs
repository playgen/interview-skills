﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace PlayGen.WebUtils
{
	public class Uploader
    {
        private readonly string _url;
        private readonly byte[] _file;
        private readonly string _fileName;

        public Uploader(byte[] file, string fileName, string url)
        {
            _file = file;
            _fileName = fileName;
            _url = url;
        }
    
        public async Task<HttpResponseMessage> UploadAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using(var client = new HttpClient())
            using(var content = new ByteArrayContent(_file))
            using(var formData = new MultipartFormDataContent())
            {
                formData.Add(content, "files", _fileName);
                return await client.PostAsync(_url, formData, cancellationToken);
            }
        }
    }
}