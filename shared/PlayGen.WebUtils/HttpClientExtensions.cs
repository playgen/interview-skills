﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PlayGen.WebUtils
{
	public static class HttpClientExtensions
	{
		public static async Task<TResponse> GetAsync<TResponse>(this HttpClient httpClient, string uri)
		{
			using (var responseMessage = await httpClient.GetAsync(uri))
			{
				var content = await responseMessage.Content.ReadAsStringAsync();
				var response = JsonConvert.DeserializeObject<TResponse>(content);
				return response;
			}
		}

		public static async Task<HttpResponseMessage> PostAsync(this HttpClient httpClient, string uri, object postContent = null)
		{
			var serializedPostContent = JsonConvert.SerializeObject(postContent);
			var postStringContent = new StringContent(serializedPostContent, Encoding.UTF8, "application/json");
			var responseMessage = await httpClient.PostAsync(uri, postStringContent);
			return responseMessage;
		}

		public static async Task<TResponse> PostAsync<TResponse>(this HttpClient httpClient, string uri, object postContent = null)
		{
			using (var responseMessage = await httpClient.PostAsync(uri, postContent))
			{
				var content = await responseMessage.Content.ReadAsStringAsync();
				var response = JsonConvert.DeserializeObject<TResponse>(content);
				return response;
			}
		}
	}
}
