﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace PlayGen.WebUtils
{
    public static class HttpContentExtensions
    {
        public static ByteArrayContent ToJsonContent(this object unserializedObject)
        {
            var serializedObject = JsonConvert.SerializeObject(unserializedObject);
            var content = new StringContent(serializedObject, Encoding.UTF8, "application/json");
            return content;
        }
    }
}
