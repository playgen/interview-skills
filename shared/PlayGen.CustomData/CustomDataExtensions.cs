﻿using System.Collections.Generic;

namespace PlayGen.CustomData
{
	public static class CustomDataExtensions
    {
        public static TCustomDataSubType GetCustomData<TCustomDataType, TCustomDataSubType>(this IDictionary<string, TCustomDataType> customData)
            where TCustomDataType : ICustomData
            where TCustomDataSubType : TCustomDataType
        {
            return (TCustomDataSubType)customData[typeof(TCustomDataSubType).FullName];
        }
	}
}
