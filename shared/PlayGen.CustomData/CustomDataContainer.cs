﻿using System.Collections.Generic;

namespace PlayGen.CustomData
{
    public abstract class CustomDataContainer<TCustomDataType> where TCustomDataType : ICustomData
    {
        private readonly Dictionary<string, TCustomDataType> _customData = new Dictionary<string, TCustomDataType>();

        public IReadOnlyDictionary<string, TCustomDataType> CustomData => _customData;

        public void AddCustomData(TCustomDataType customData)
        {
            _customData.Add(customData.GetType().FullName, customData);
        }

        public TCustomDataSubType GetCustomData<TCustomDataSubType>() where TCustomDataSubType : TCustomDataType
        {
            return _customData.GetCustomData<TCustomDataType, TCustomDataSubType>();
        }
	}
}
