﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.MatchMaking.Matching
{
    public class UnmatchedEntry
    {
		public MatchingParticipant Participant { get; }

		public DateTime LastContact { get; set; } = DateTime.UtcNow;

	    public UnmatchedEntry(MatchingParticipant participant)
	    {
		    Participant = participant;
	    }
    }
}
