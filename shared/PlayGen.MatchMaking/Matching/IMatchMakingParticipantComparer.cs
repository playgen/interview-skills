﻿namespace PlayGen.MatchMaking.Matching
{
    public interface IMatchMakingParticipantComparer
    {
        bool AreMatch(MatchingParticipant a, MatchingParticipant b);
    }
}
