﻿using PlayGen.CustomData;

namespace PlayGen.MatchMaking.Matching
{
    public class MatchingParticipant : CustomDataContainer<IMatchingParticipantCustomData>
    {
        private readonly IMatchMakingParticipantComparer _participantComparer;

        public string Id { get; }

        public MatchingParticipant(string id, IMatchMakingParticipantComparer matchMakingParticipantComparer)
        {
            Id = id;
            _participantComparer = matchMakingParticipantComparer;
        }

        public bool IsMatch(MatchingParticipant other)
        {
            return _participantComparer.AreMatch(this, other);
        }
    }
}
