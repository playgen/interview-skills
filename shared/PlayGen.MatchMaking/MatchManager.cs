﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using PlayGen.MatchMaking.Matched;
using PlayGen.MatchMaking.Matching;

namespace PlayGen.MatchMaking
{
	public class MatchManager : IDisposable
	{
		private readonly Dictionary<string, UnmatchedEntry> _unmatched = new Dictionary<string, UnmatchedEntry>();
		private readonly ReaderWriterLockSlim _unmatchedLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

		private readonly List<Match> _matched = new List<Match>();
		private readonly ReaderWriterLockSlim _matchedLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

		private readonly int _minParticipants;
		private readonly int _keepAliveTimeout;

		private readonly int _pruneInterval;
		private readonly ManualResetEventSlim _unmatchedPruneSignal = new ManualResetEventSlim(false);
		private readonly Thread _unmatchedPruneThread;

		private volatile bool _isDisposed;

		public delegate void MatchMade(Match match);
		public delegate void MatchMakingParticipantCreated(MatchedParticipant matchedParticipant);

		public event MatchMade MatchCreatedEvent;
		public event MatchMakingParticipantCreated MatchedParticipantCreatedEvent;

		public MatchManager(int minParticipants, int keepAliveTimeout, int pruneInterval)
		{
			_minParticipants = minParticipants;
			_keepAliveTimeout = keepAliveTimeout;
			_pruneInterval = pruneInterval;
			_unmatchedPruneThread = new Thread(PruneUnmatchedLoop)
			{
				IsBackground = true
			};
		}

		~MatchManager()
		{
			Dispose();
		}

		public void Start()
		{
			_unmatchedPruneThread.Start();
			// todo PruneMatchedLoop start as thread as with unmatched prune
		}

		public void Dispose()
		{
			if (_isDisposed) return;

			_unmatchedPruneSignal.Dispose();

			_isDisposed = true;
		}

		public bool RemoveParticipant(string matchId, string participantId)
		{
			var didRemoveAny = RemoveMatchedParticipant(participantId, matchId);
			return didRemoveAny;
		}
		public bool RemoveParticipant(string participantId)
		{
			var didRemoveAny = RemoveUnmatchedParticipant(participantId);
			didRemoveAny |= RemoveMatchedParticipant(participantId);
			return didRemoveAny;
		}

		public bool TryFindNewMatch(MatchingParticipant matchingParticipant, out Match match)
		{
			RemoveParticipant(matchingParticipant.Id);

			AddParticipant(matchingParticipant);
			return TryFindNewMatch(matchingParticipant.Id, out match);
		}

		public bool TryFindMatches(MatchingParticipant matchingParticipant, out List<Match> matches)
		{
			var hasExistingMatches = TryFindExistingMatches(matchingParticipant.Id, out matches);
			var isAwaitingMatch = IsAwaitingMatch(matchingParticipant.Id);

			if (!hasExistingMatches && !isAwaitingMatch)
			{
				AddParticipant(matchingParticipant);
				isAwaitingMatch = true;
			}

			if (isAwaitingMatch && TryFindNewMatch(matchingParticipant.Id, out var match))
			{
				if (matches != null)
				{
					matches.Add(match);
				}
				else
				{
					matches = new List<Match> { match };
				}
			}

			return matches.Any();
		}

		public bool TryFindExistingMatches(string participantId, out List<Match> matches)
		{
			_unmatchedLock.EnterReadLock();

			try
			{
				if(_unmatched.TryGetValue(participantId, out var participantUnmatchedEntry))
				{
					participantUnmatchedEntry.LastContact = DateTime.UtcNow;
				}
			}
			finally
			{
				_unmatchedLock.ExitReadLock();
			}

			_matchedLock.EnterReadLock();

			try
			{
				matches = _matched.Where(m => m.Participants.Any(p => p.MatchingParticipant.Id == participantId)).ToList();
			}
			finally
			{
				_matchedLock.ExitReadLock();
			}

			return matches.Any();
		}

		public Match CreateMatch(List<MatchingParticipant> matchingParticipants)
		{
			Match match;

			_matchedLock.EnterWriteLock();

			try
			{
				match = MatchedWriteLocked_ComposeMatch(matchingParticipants);
			}
			finally
			{
				_matchedLock.ExitWriteLock();
			}

			return match;
		}

		public bool IsAwaitingMatch(string participantId)
		{
			_unmatchedLock.EnterReadLock();

			try
			{
				return _unmatched.ContainsKey(participantId);
			}
			finally
			{
				_unmatchedLock.ExitReadLock();
			}
		}

		private void PruneUnmatchedLoop()
		{
			while (true)
			{
				_unmatchedPruneSignal.Wait(_pruneInterval);

				if (_isDisposed)
				{
					break;
				}
				else
				{
					PruneUnmatched();
				}
			}
		}

		// todo
		/*private void PruneMatchedLoop()
		{
			while (true)
			{
				_pruneSignal.Wait(_pruneInterval);

				if (_isDisposed)
				{
					break;
				}
				else
				{
					PruneMatched();
				}
			}
		}*/

		private void PruneUnmatched()
		{
			_unmatchedLock.EnterUpgradeableReadLock();

			try
			{
				foreach (var unmatchedEntry in _unmatched.Values.ToList())
				{
					if (unmatchedEntry.LastContact < DateTime.UtcNow - TimeSpan.FromMilliseconds(_keepAliveTimeout))
					{
						_unmatchedLock.EnterWriteLock();

						try
						{
							_unmatched.Remove(unmatchedEntry.Participant.Id);
						}
						finally
						{
							_unmatchedLock.ExitWriteLock();
						}
					}
				}
			}
			finally
			{
				_unmatchedLock.ExitUpgradeableReadLock();
			}
		}

		private bool TryFindNewMatch(string participantId, out Match match)
		{
			match = null;

			_unmatchedLock.EnterWriteLock();

			try
			{
				_matchedLock.EnterWriteLock();

				try
				{
					if (UnmatchedWriteLocked_TryFindMatchParticipants(participantId, out var participants, out var participantUnmatchedEntry))
					{
						match = MatchedWriteLocked_ComposeMatch(participants);
					}
				}
				finally
				{
					_matchedLock.ExitWriteLock();
				}
			}
			finally
			{
				_unmatchedLock.ExitWriteLock();
			}

			return match != null;
		}

		// Requires _matchedLock to be write locked
		private Match MatchedWriteLocked_ComposeMatch(List<MatchingParticipant> participants)
		{
			var matchedParticipants = new List<MatchedParticipant>(participants.Count);

			for (var i = 0; i < participants.Count; i++)
			{
				var matchedParticipant = new MatchedParticipant
				{
					IsMaster = i == 0,
					MatchingParticipant = participants[i]
				};

				MatchedParticipantCreatedEvent?.Invoke(matchedParticipant);

				matchedParticipants.Add(matchedParticipant);
			}

			var match = new Match(Guid.NewGuid().ToString())
			{
				Participants = matchedParticipants
			};

			MatchCreatedEvent?.Invoke(match);

			_matched.Add(match);

			return match;
		}

		// Requires _unmatchedLock to be write locked
		private bool UnmatchedWriteLocked_TryFindMatchParticipants(
			string participantId, 
			out List<MatchingParticipant> participants, 
			out UnmatchedEntry participantUnmatchedEntry)
		{
			var didFindMatch = false;
			participants = new List<MatchingParticipant>();

			if (_unmatched.TryGetValue(participantId, out participantUnmatchedEntry))
			{
				var thisParticipant = participantUnmatchedEntry.Participant;
				var otherUnmatchedEntry = _unmatched.Values.FirstOrDefault(ue => ue.Participant.IsMatch(thisParticipant)
					&& ue.Participant.Id != participantId);

				if (otherUnmatchedEntry != null)
				{
					var otherParticipant = otherUnmatchedEntry.Participant;

					_unmatched.Remove(thisParticipant.Id);
					_unmatched.Remove(otherParticipant.Id);

					participants.Add(thisParticipant);
					participants.Add(otherParticipant);

					didFindMatch = true;
				}
				else
				{
					participantUnmatchedEntry.LastContact = DateTime.UtcNow;
				}
			}

			return didFindMatch;
		}

		private bool RemoveMatchedParticipant(string participantId, string matchId = null)
		{
			var didLeaveAny = false;
			_matchedLock.EnterWriteLock();

			try
			{
				for (var i = _matched.Count - 1; i >= 0; i--)
				{
					if (matchId == null || matchId == _matched[i].Id)
					{
						var index = _matched[i].Participants.FindIndex(p => p.MatchingParticipant.Id == participantId);
						if (index >= 0)
						{
							if (_matched[i].Participants.Count - 1 < _minParticipants)
							{
								_matched.RemoveAt(i);
							}
							else
							{
								var isMaster = _matched[i].Participants[index].IsMaster;
								_matched[i].Participants.RemoveAt(index);

								if (isMaster)
								{
									_matched[i].Participants[0].IsMaster = true;
								}

								didLeaveAny = true;
							}
						}
					}
				}
			}
			finally
			{
				_matchedLock.ExitWriteLock();
			}

			return didLeaveAny;
		}

		private bool RemoveUnmatchedParticipant(string participantId)
		{
			_unmatchedLock.EnterWriteLock();

			try
			{
				return _unmatched.Remove(participantId);
			}
			finally
			{
				_unmatchedLock.ExitWriteLock();
			}
		}

		private void AddParticipant(MatchingParticipant matchingParticipant)
		{
			_unmatchedLock.EnterWriteLock();

			try
			{
				if (!_unmatched.ContainsKey(matchingParticipant.Id))
				{
					_unmatched.Add(matchingParticipant.Id, new UnmatchedEntry(matchingParticipant)
					{
						LastContact = DateTime.UtcNow
					});
				}
			}
			finally
			{
				_unmatchedLock.ExitWriteLock();
			}
		}
	}
}
