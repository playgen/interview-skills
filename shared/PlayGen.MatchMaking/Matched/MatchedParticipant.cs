﻿using PlayGen.CustomData;
using PlayGen.MatchMaking.Matching;

namespace PlayGen.MatchMaking.Matched
{
	public class MatchedParticipant : CustomDataContainer<IMatchedParticipantCustomData>
	{		
        public MatchingParticipant MatchingParticipant { get; set; }

        public bool IsMaster { get; set; }
	}
}
