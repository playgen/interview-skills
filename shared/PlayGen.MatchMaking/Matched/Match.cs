﻿using System;
using System.Collections.Generic;

using PlayGen.CustomData;

namespace PlayGen.MatchMaking.Matched
{
	public class Match : CustomDataContainer<IMatchCustomData>
	{
		public string Id { get; }

		public List<MatchedParticipant> Participants { get; set; }

	    public MatchedParticipant MasterParticipant => Participants.Find(p => p.IsMaster);

		public Match(string id)
		{
			Id = id;
		}
	}
}
