﻿using System.IO;

namespace PlayGen.PathUtils
{
    public static class DirectoryUtils
    {
        /// <summary>
        /// Removes File but makes sure the enclosing directory exists (or creates it).
        /// </summary>
        /// <param name="dest"></param>
        public static void RemoveAddParentDir(string dest)
        {
            if (Directory.Exists(dest))
            {
                Directory.Delete(dest, true);
            }
            else if (!Directory.Exists(Path.GetDirectoryName(dest)))
            {
                var dir = Path.GetDirectoryName(dest);
                Directory.CreateDirectory(dir);
            }
        }

        public static void Clear(string dir)
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            else
            {
                var dirInfo = new DirectoryInfo(dir);

                foreach(var file in dirInfo.GetFiles())
                {
                    file.Delete();
                }

                foreach (var subDir in dirInfo.GetDirectories())
                {
                    subDir.Delete(true);
                }
            }
        }
    }
}
