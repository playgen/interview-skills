﻿using System.IO;

namespace PlayGen.PathUtils
{
    public static class FileUtils
    {
        /// <summary>
        /// Removes File but makes sure the enclosing directory exists (or creates it).
        /// </summary>
        /// <param name="dest"></param>
        public static void RemoveAddDir(string dest)
        {
            if (File.Exists(dest))
            {
                File.Delete(dest);
            }
            else if (!Directory.Exists(Path.GetDirectoryName(dest)))
            {
                var dir = Path.GetDirectoryName(dest);
                Directory.CreateDirectory(dir);
            }
        }
    }
}
