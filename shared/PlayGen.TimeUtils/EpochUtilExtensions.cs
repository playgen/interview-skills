﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayGen.TimeUtils
{
    public static class EpochUtilExtensions
    {
		public static DateTime FromUnixTimestamp(this long unixTimestamp)
		{
			return EpochUtil.FromUnixTimestamp(unixTimestamp);
		}

		public static long ToUnixTimestamp(this DateTime dotNetTimestamp)
		{
			return EpochUtil.ToUnixTimestamp(dotNetTimestamp);
		}
	}
}
