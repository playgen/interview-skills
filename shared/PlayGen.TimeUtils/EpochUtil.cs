﻿using System;

namespace PlayGen.TimeUtils
{
	public static class EpochUtil
	{
		public static DateTime UnixEpoch { get; } = new DateTime(1970, 1, 1);

		public static DateTime FromUnixTimestamp(long unixTimestamp)
		{
			return UnixEpoch + TimeSpan.FromMilliseconds(unixTimestamp);
		}

		public static long ToUnixTimestamp(DateTime dotNetTimestamp)
		{
			return (long)(dotNetTimestamp - UnixEpoch).TotalMilliseconds;
		}
	}
}
