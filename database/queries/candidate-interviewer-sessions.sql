SELECT
aAuths.AuthUsrId AS Candidate,
a.RoleId AS CandidateRoleId,

bAuths.AuthUsrId AS Interviewer,
b.RoleId AS InterviewerRoleId,

a.JobId AS Job,

a.Created AS CandidateCreated,
b.Created AS InterviewerCreated,
a.Ended AS CandidateEnded,
b.Ended AS InterviewerEnded

FROM rolssns as a
INNER JOIN rolssns as b on a.MatchSessionId=b.MatchSessionId
LEFT JOIN usrssn as aUsrssn on a.UserSessionId=aUsrssn.Id
LEFT JOIN auths as aAuths on aUsrssn.UserId=aAuths.UserId

LEFT JOIN usrssn as bUsrssn on b.UserSessionId=bUsrssn.Id
LEFT JOIN auths as bAuths on bUsrssn.UserId=bAuths.UserId

WHERE a.UserSessionId != b.UserSessionId AND a.RoleId = "candidate" AND a.RoleId != b.RoleId