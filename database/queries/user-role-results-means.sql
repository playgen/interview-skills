CREATE OR REPLACE TABLE _UserRoleSessions 
(
	SELECT 		
		auths.AuthUsrId AS UserEmail,
		auths.UserId AS UserId,
		usrssn.Id AS UserSessionId,
		mtchssnusrmpns.MatchSessionId AS MatchId,
		rolssns.Created AS RoleSessionCreated,
		rolssns.Ended AS RoleSessionEnded,
		rolssns.RoleId AS RoleId,
		rolssns.Id AS RoleSessionId
	FROM auths
	INNER JOIN usrssn ON auths.UserId = usrssn.UserId
	JOIN mtchssnusrmpns ON usrssn.Id = mtchssnusrmpns.UserSessionId 
	INNER JOIN rolssns ON mtchssnusrmpns.MatchSessionId = rolssns.MatchSessionId
		AND usrssn.Id = rolssns.UserSessionId
);

CREATE OR REPLACE TABLE _UserPreviousRoleSessionCounts
(
	SELECT 		
		UserRoleSessions.UserEmail,
		UserRoleSessions.UserId,
		UserRoleSessions.MatchId,
		UserRoleSessions.UserSessionId,
		UserRoleSessions.RoleId,
		UserRoleSessions.RoleSessionCreated,
		UserRoleSessions.RoleSessionEnded,
		UserRoleSessions.RoleSessionId,
		SUM(CASE WHEN PreviousUserRoleSessions.RoleSessionCreated < UserRoleSessions.RoleSessionCreated THEN 1 ELSE 0 END) AS PreviousSameRoleCount
	FROM _UserRoleSessions As UserRoleSessions
	
	LEFT JOIN _UserRoleSessions AS PreviousUserRoleSessions ON UserRoleSessions.UserId = PreviousUserRoleSessions.UserId
		AND UserRoleSessions.RoleId = PreviousUserRoleSessions.RoleId
	
	GROUP BY UserRoleSessions.RoleSessionId
);

CREATE OR REPLACE TABLE _UserAnalysisResults
(
	SELECT 		
		UserRoleSessions.UserEmail,
		UserRoleSessions.UserId,
		UserRoleSessions.MatchId,
		UserRoleSessions.RoleId,
		UserRoleSessions.RoleSessionCreated,
		UserRoleSessions.RoleSessionEnded,
		UserRoleSessions.RoleSessionId,
		UserRoleSessions.UserSessionId,
		archvanlysrslts.FtrId AS Feature,
		archvanlysrslts.Classifier1,
		archvanlysrslts.Classifier2,
		archvanlysrslts.Classifier3,		
		archvanlysrslts.Prediction,
		archvanlysrslts.Weight,
		archvanlysrslts.WeightFromIdeal,
		archvanlysmtadtas.ArchiveStarted,
		archvanlysmtadtas.SerializedErrors
	FROM _UserRoleSessions As UserRoleSessions
	
	LEFT JOIN archvanlysmtadtas ON UserRoleSessions.MatchId = archvanlysmtadtas.MatchSessionId
		AND UserRoleSessions.UserSessionId = archvanlysmtadtas.UserSessionId
		AND UserRoleSessions.RoleSessionCreated <= archvanlysmtadtas.ArchiveStarted
		AND archvanlysmtadtas.ArchiveStarted < UserRoleSessions.RoleSessionEnded
		
	LEFT JOIN archvanlysrslts ON archvanlysmtadtas.Id = archvanlysrslts.ArchvAnlysMtadtaId
);

CREATE OR REPLACE TABLE _RoleResultsAverage
(
	SELECT 
		UserAnalysisResults.RoleSessionId,
		UserAnalysisResults.Feature,
		AVG(UserAnalysisResults.Classifier1) AS Class1Avg,
		AVG(UserAnalysisResults.Classifier2) AS Class2Avg,
		AVG(UserAnalysisResults.Classifier3) AS Class3Avg,
		AVG(UserAnalysisResults.Prediction) AS PredictAvg
	FROM _UserAnalysisResults AS UserAnalysisResults
	
	GROUP BY UserAnalysisResults.RoleSessionId, UserAnalysisResults.Feature
);

CREATE OR REPLACE TABLE _UserRoleResultsAverage
(
	SELECT 
		_UserPreviousRoleSessionCounts.MatchId,
		_UserPreviousRoleSessionCounts.UserId,
		_UserPreviousRoleSessionCounts.RoleId,
		_UserPreviousRoleSessionCounts.RoleSessionId,
		_UserPreviousRoleSessionCounts.PreviousSameRoleCount,
		_RoleResultsAverage.Feature,
		_RoleResultsAverage.Class1Avg,
		_RoleResultsAverage.Class2Avg,
		_RoleResultsAverage.Class3Avg,
		_RoleResultsAverage.PredictAvg,
		TIMESTAMPDIFF(SECOND, _UserPreviousRoleSessionCounts.RoleSessionCreated, _UserPreviousRoleSessionCounts.RoleSessionEnded) AS RoleSessionDuration
		
	FROM _UserPreviousRoleSessionCounts
	
	LEFT JOIN _RoleResultsAverage ON _UserPreviousRoleSessionCounts.RoleSessionId = _RoleResultsAverage.RoleSessionId
);

CREATE OR REPLACE TABLE _UserRoleResultsAverageTransposed
(
	SELECT 
		_UserRoleResultsAverage.UserId,
		_UserRoleResultsAverage.RoleId,
		_UserRoleResultsAverage.Feature,
		GROUP_CONCAT(_UserRoleResultsAverage.PreviousSameRoleCount ORDER BY _UserRoleResultsAverage.PreviousSameRoleCount) AS SameRoleCounts,
		GROUP_CONCAT(_UserRoleResultsAverage.Class1Avg ORDER BY _UserRoleResultsAverage.PreviousSameRoleCount) AS Class1Avgs,
		GROUP_CONCAT(_UserRoleResultsAverage.Class2Avg ORDER BY _UserRoleResultsAverage.PreviousSameRoleCount) AS Class2Avgs,
		GROUP_CONCAT(_UserRoleResultsAverage.Class3Avg ORDER BY _UserRoleResultsAverage.PreviousSameRoleCount) AS Class3Avgs,
		GROUP_CONCAT(_UserRoleResultsAverage.PredictAvg ORDER BY _UserRoleResultsAverage.PreviousSameRoleCount) AS PredictAvgs,
		GROUP_CONCAT(_UserRoleResultsAverage.RoleSessionDuration ORDER BY _UserRoleResultsAverage.PreviousSameRoleCount) AS RoleSessionDurations
		
	FROM _UserRoleResultsAverage
	
	GROUP BY _UserRoleResultsAverage.UserId, _UserRoleResultsAverage.RoleId, _UserRoleResultsAverage.Feature
);