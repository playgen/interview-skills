SELECT
/* Match Details*/
mtchssnusrmpns.MatchSessionId AS "Match",

/* User Details*/
auths.UserId AS 'Interviewer Id',
auths.AuthUsrId AS 'Interviewer Email',

candidateAuths.UserId AS 'Candidate Id',
candidateAuths.AuthUsrId AS 'Candidate Email',

/* Role Session */
rolssns.Id AS 'Role Session Id',
rolssns.JobId AS 'Job',

/* Observational Quesitons */
obsrvqstnrslts.QuestionId AS 'Observational Question Id',
obsrvqstnrslts.Received AS 'Observational Question Received',
obsrvqstnrslts.Shown AS 'Observational Question Shown',
obsrvqstnrslts.Submitted AS 'Observational Question Submitted',
obsrvqstnrslts.Weight AS 'Observational Question Rating',
obsrvqstns.Question AS 'Observational Question'

FROM auths 
/* Every time a user logs in a user session is created 
This is so multiple people can login as the Guest User */
INNER JOIN usrssn ON auths.UserId = usrssn.UserId

/* The two users in a match are stored in the match<->user session mappings table. 
It would've been simpler to just store them on the match but at the time it didn't take much extra effort 
for a solution that we could later just tack into SUGAR as tried and tested in Interview Skills */
INNER JOIN mtchssnusrmpns ON usrssn.Id = mtchssnusrmpns.UserSessionId


INNER JOIN obsrvqstnrslts ON mtchssnusrmpns.MatchSessionId = obsrvqstnrslts.MatchSessionId
	AND usrssn.Id = obsrvqstnrslts.UserSessionId

INNER JOIN obsrvqstns ON obsrvqstnrslts.QuestionId = obsrvqstns.Id

/* Role Sessions store when a player started and ended in role */
JOIN rolssns ON mtchssnusrmpns.MatchSessionId = rolssns.MatchSessionId
	AND usrssn.Id=rolssns.UserSessionId
	AND (rolssns.Created <= obsrvqstnrslts.Received OR rolssns.Created <= obsrvqstnrslts.Shown OR rolssns.Created <= obsrvqstnrslts.Submitted)
	AND (obsrvqstnrslts.Received < rolssns.Ended OR obsrvqstnrslts.Shown < rolssns.Ended OR obsrvqstnrslts.Submitted < rolssns.Ended )

/* Candidate for Question */
JOIN rolssns AS candidateRoleSessions ON rolssns.MatchSessionId = candidateRoleSessions.MatchSessionId
	AND rolssns.JobId = candidateRoleSessions.JobId
	AND rolssns.UserSessionId != candidateRoleSessions.UserSessionId
	
INNER JOIN usrssn AS candidateSessions ON candidateRoleSessions.UserSessionId = candidateSessions.Id

INNER JOIN auths AS candidateAuths ON candidateSessions.UserId = candidateAuths.UserId