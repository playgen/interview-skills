SELECT
/* Match Details*/
mtchssnusrmpns.MatchSessionId AS "Match",

/* User Details*/
auths.UserId AS 'Interviewer Id',
auths.AuthUsrId AS 'Interviewer Email',

candidateAuths.UserId AS 'Candidate Id',
candidateAuths.AuthUsrId AS 'Candidate Email',

/* Role Session */
rolssns.Id AS 'Role Session Id',
rolssns.JobId AS 'Job',

/* Interview Quesitons */
intrvqstnrslts.QuestionId AS 'Interview Question Id',
intrvqstnrslts.Asking As 'Interview Question Asking',
intrvqstnrslts.Answering As 'Interview Question Answering',
intrvqstnrslts.Answered As 'Interview Question Answered',
intrvqstnrslts.Hidden As 'Interview Question Hidden',
intrvqstns.Question AS 'Interview Question'

FROM auths 
/* Every time a user logs in a user session is created 
This is so multiple people can login as the Guest User */
INNER JOIN usrssn ON auths.UserId = usrssn.UserId

/* The two users in a match are stored in the match<->user session mappings table. 
It would've been simpler to just store them on the match but at the time it didn't take much extra effort 
for a solution that we could later just tack into SUGAR as tried and tested in Interview Skills */
INNER JOIN mtchssnusrmpns ON usrssn.Id = mtchssnusrmpns.UserSessionId

INNER JOIN intrvqstnrslts ON mtchssnusrmpns.MatchSessionId = intrvqstnrslts.MatchSessionId

INNER JOIN intrvqstns ON intrvqstnrslts.QuestionId = intrvqstns.Id

/* Role Sessions store when a player started and ended in role */
JOIN rolssns ON mtchssnusrmpns.MatchSessionId = rolssns.MatchSessionId
	AND usrssn.Id=rolssns.UserSessionId
	AND (rolssns.Created <= intrvqstnrslts.Asking OR rolssns.Created <= intrvqstnrslts.Answering OR rolssns.Created <= intrvqstnrslts.Answered OR rolssns.Created <= intrvqstnrslts.Hidden)
	AND (intrvqstnrslts.Asking < rolssns.Ended OR intrvqstnrslts.Answering < rolssns.Ended OR intrvqstnrslts.Answered < rolssns.Ended OR intrvqstnrslts.Hidden < rolssns.Ended)
	AND rolssns.RoleId = 'interviewer'

/* Candidate for Question */
JOIN rolssns AS candidateRoleSessions ON rolssns.MatchSessionId = candidateRoleSessions.MatchSessionId
	AND rolssns.JobId = candidateRoleSessions.JobId
	AND rolssns.UserSessionId != candidateRoleSessions.UserSessionId
	
INNER JOIN usrssn AS candidateSessions ON candidateRoleSessions.UserSessionId = candidateSessions.Id

INNER JOIN auths AS candidateAuths ON candidateSessions.UserId = candidateAuths.UserId