SELECT usrssn.UserId AS UserId, 
auths.AuthUsrId As AuthUserId,
rolssns.RoleId AS RoleId,
archvanlysmtadtas.ArchiveStarted,
archvanlysrslts.FtrId AS FeatureId, 
archvanlysrslts.Weight AS ResultWeight, 
archvanlysrslts.WeightFromIdeal AS ResultWeightFromIdeal, 
ftractnfdbks.Feedback AS Feedback
FROM archvanlysrslts 

LEFT JOIN archvanlysmtadtas ON archvanlysrslts.ArchvAnlysMtadtaId=archvanlysmtadtas.Id

LEFT JOIN usrssn ON archvanlysmtadtas.UserSessionId=usrssn.Id

LEFT JOIN auths ON usrssn.UserId=auths.UserId

LEFT JOIN ftractnfdbkstmrstls ON archvanlysrslts.Id=ftractnfdbkstmrstls.ArchiveAnalysisResultId

LEFT JOIN ftractnfdbks ON ftractnfdbkstmrstls.FeedbackId=ftractnfdbks.Id

LEFT JOIN rolssns ON archvanlysmtadtas.UserSessionId=rolssns.UserSessionId 
	AND rolssns.Created <= archvanlysmtadtas.AnalysisStarted 
	AND archvanlysmtadtas.AnalysisStarted <= rolssns.Ended

LEFT JOIN intrvqstnrslts ON archvanlysmtadtas.MatchSessionId=intrvqstnrslts.MatchSessionId
	AND intrvqstnrslts.Asking <= archvanlysmtadtas.ArchiveStarted 
	AND archvanlysmtadtas.ArchiveStarted <= intrvqstnrslts.Hidden

LEFT JOIN intrvqstns ON intrvqstnrslts.QuestionId=intrvqstns.Id