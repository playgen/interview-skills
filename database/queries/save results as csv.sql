SELECT 
'UserId',
'MatchSessionId',
'RoleId',
'RoleStarted', 
'RoleEnded', 
'ArchiveStarted',
'AnalysisStarted',
'AnalysisCompleted',
'FeatureId', 
'ResultWeight', 
'ResultWeightFromIdeal', 
'SerializedClassifierResult'

UNION ALL

SELECT 
usrssn.UserId,
archvanlysmtadtas.MatchSessionId,
rolssns.RoleId,
rolssns.Created, 
rolssns.Ended, 
archvanlysmtadtas.ArchiveStarted,
archvanlysmtadtas.AnalysisStarted,
archvanlysmtadtas.AnalysisCompleted,
archvanlysrslts.FtrId, 
archvanlysrslts.Weight, 
archvanlysrslts.WeightFromIdeal,
archvanlysrslts.SerializedClassifierProbabilities
FROM archvanlysrslts 
JOIN archvanlysmtadtas ON archvanlysrslts.ArchvAnlysMtadtaId=archvanlysmtadtas.Id
JOIN usrssn ON archvanlysmtadtas.UserSessionId=usrssn.Id
JOIN rolssns ON archvanlysmtadtas.UserSessionId=rolssns.UserSessionId

INTO OUTFILE 'C:/Users/Public/results.csv'
FIELDS ENCLOSED BY '"' 
TERMINATED BY ';' 
LINES TERMINATED BY '\r\n';