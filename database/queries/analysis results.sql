SELECT
/* Match Details*/
mtchssnusrmpns.MatchSessionId AS "Match",

/* User Details*/
auths.UserId AS 'User Id',
auths.AuthUsrId AS 'User Email',

/* Role Session - As Candidate or Interviewer*/
rolssns.RoleId AS 'Role',
rolssns.Id AS 'Role Session Id',
rolssns.Created AS 'Role Session Created',
rolssns.Ended AS 'Role Session Ended',
rolssns.JobId AS 'Job',

/* Analysis Results */
archvanlysmtadtas.AnalysisCompleted AS 'Archive Analysis Completed',
archvanlysrslts.FtrId AS 'Feature',
ftrs.Inverse AS 'Feature Cosmetic Inverse',
archvanlysrslts.Weight AS 'Weight',
archvanlysrslts.WeightFromIdeal AS 'Weight From Ideal',
archvanlysrslts.SerializedClassifierProbabilities AS 'Probabilities',
archvanlysmtadtas.SerializedErrors AS 'Analysis Errors',

REGEXP_SUBSTR(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, 'Class1prob":(.*?),'), '[0-9].[0-9]+') AS 'Classifier 1',
REGEXP_SUBSTR(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, 'Class2prob":(.*?),'), '[0-9].[0-9]+') AS 'Classifier 2',
REGEXP_SUBSTR(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, 'Class3prob":(.*?),'), '[0-9].[0-9]+') AS 'Classifier 3',
REGEXP_SUBSTR(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, 'Prediction":(.*?)}'), '[0-9].[0-9]+') AS 'Prediction',

/* Feature Analysis Feedback */
ftractnfdbkstmrstls.FeedbackId AS 'System Feedback Id',
ftractnfdbkstmrstls.Shown AS 'System Feedback Shown',
ftractnfdbkstmrstls.Acknowledged AS 'System Feedback Clicked', /* User clickedtick button, acknowledging feedback */
ftractnfdbks.Feedback AS 'Feedback Message'

FROM auths 
/* Every time a user logs in a user session is created 
This is so multiple people can login as the Guest User */
INNER JOIN usrssn ON auths.UserId = usrssn.UserId

/* The two users in a match are stored in the match<->user session mappings table. 
It would've been simpler to just store them on the match but at the time it didn't take much extra effort 
for a solution that we could later just tack into SUGAR as tried and tested in Interview Skills */
INNER JOIN mtchssnusrmpns ON usrssn.Id = mtchssnusrmpns.UserSessionId

/* Analysis for all features in an archive gets done at once so 
an Archive Analysis Metadata object has the archive created time, user it's for etc. */
JOIN archvanlysmtadtas ON usrssn.Id = archvanlysmtadtas.UserSessionId
	AND mtchssnusrmpns.MatchSessionId = archvanlysmtadtas.MatchSessionId
	
/* Analysis Results are the results per feature that came as a collection 
from the Analysed Archive */
LEFT JOIN archvanlysrslts ON archvanlysmtadtas.Id = archvanlysrslts.ArchvAnlysMtadtaId

/* Inverse of feature */
LEFT JOIN ftrs ON archvanlysrslts.FtrId = ftrs.Id

/* Role Sessions store when a player started and ended in role */
JOIN rolssns ON mtchssnusrmpns.MatchSessionId=rolssns.MatchSessionId
	AND usrssn.Id=rolssns.UserSessionId
	AND rolssns.Created <= archvanlysmtadtas.ArchiveStarted
	AND archvanlysmtadtas.ArchiveStarted < rolssns.Ended

/* System selected feedback shown to the users */
LEFT JOIN ftractnfdbkstmrstls ON archvanlysrslts.Id = ftractnfdbkstmrstls.ArchiveAnalysisResultId
LEFT JOIN ftractnfdbks ON ftractnfdbkstmrstls.FeedbackId = ftractnfdbks.Id 