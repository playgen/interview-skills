CREATE OR REPLACE TABLE _UserRoleSessions 
(
	SELECT 		
		auths.AuthUsrId AS UserEmail,
		auths.UserId AS UserId,
		usrssn.Id AS UserSessionId,
		mtchssnusrmpns.MatchSessionId AS MatchId,
		rolssns.Created AS RoleSessionCreated,
		rolssns.Ended AS RoleSessionEnded,
		rolssns.RoleId AS RoleId,
		rolssns.Id AS RoleSessionId
	FROM auths
	INNER JOIN usrssn ON auths.UserId = usrssn.UserId
	JOIN mtchssnusrmpns ON usrssn.Id = mtchssnusrmpns.UserSessionId 
	INNER JOIN rolssns ON mtchssnusrmpns.MatchSessionId = rolssns.MatchSessionId
		AND usrssn.Id = rolssns.UserSessionId
);

CREATE OR REPLACE TABLE _UserPreviousRoleSessionCounts
(
	SELECT 		
		UserRoleSessions.UserEmail,
		UserRoleSessions.UserId,
		UserRoleSessions.MatchId,
		UserRoleSessions.UserSessionId,
		UserRoleSessions.RoleId,
		UserRoleSessions.RoleSessionCreated,
		UserRoleSessions.RoleSessionEnded,
		UserRoleSessions.RoleSessionId,
		SUM(CASE WHEN PreviousUserRoleSessions.RoleSessionCreated < UserRoleSessions.RoleSessionCreated THEN 1 ELSE 0 END) AS PreviousSameRoleCount
	FROM _UserRoleSessions As UserRoleSessions
	
	LEFT JOIN _UserRoleSessions AS PreviousUserRoleSessions ON UserRoleSessions.UserId = PreviousUserRoleSessions.UserId
		AND UserRoleSessions.RoleId = PreviousUserRoleSessions.RoleId
	
	GROUP BY UserRoleSessions.RoleSessionId
);

CREATE OR REPLACE TABLE _UserAnalysisResults
(
	SELECT 		
		UserRoleSessions.UserEmail,
		UserRoleSessions.UserId,
		UserRoleSessions.MatchId,
		UserRoleSessions.RoleId,
		UserRoleSessions.RoleSessionCreated,
		UserRoleSessions.RoleSessionEnded,
		UserRoleSessions.RoleSessionId,
		UserRoleSessions.UserSessionId,
		archvanlysrslts.FtrId AS Feature,
		archvanlysrslts.SerializedClassifierProbabilities,
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class1prob":)(.*?)(?=,)') AS DOUBLE) AS Classifier1,
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class2prob":)(.*?)(?=,)') AS DOUBLE) AS Classifier2,
		CAST(NULLIF(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class3prob":)(.*?)(?=,)'), '') AS DOUBLE) AS Classifier3,		
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Prediction":)(.*?)(?=})') AS DOUBLE)  AS Prediction
	FROM _UserRoleSessions As UserRoleSessions
	
	LEFT JOIN archvanlysmtadtas ON UserRoleSessions.MatchId = archvanlysmtadtas.MatchSessionId
		AND UserRoleSessions.UserSessionId = archvanlysmtadtas.UserSessionId
		AND UserRoleSessions.RoleSessionCreated <= archvanlysmtadtas.ArchiveStarted
		AND archvanlysmtadtas.ArchiveStarted < UserRoleSessions.RoleSessionEnded
		
	LEFT JOIN archvanlysrslts ON archvanlysmtadtas.Id = archvanlysrslts.ArchvAnlysMtadtaId
);

CREATE OR REPLACE TABLE _RoleResultsMax
(
	SELECT 
		UserAnalysisResults.RoleSessionId,
		UserAnalysisResults.Feature,
		MAX(UserAnalysisResults.Classifier1) AS Class1Max,
		MAX(UserAnalysisResults.Classifier2) AS Class2Max,
		MAX(UserAnalysisResults.Classifier3) AS Class3Max,
		AVG(UserAnalysisResults.Prediction) AS PredictAvg
	FROM _UserAnalysisResults AS UserAnalysisResults
	
	GROUP BY UserAnalysisResults.RoleSessionId, UserAnalysisResults.Feature
);

CREATE OR REPLACE TABLE _UserRoleResultsMax
(
	SELECT 
		_UserPreviousRoleSessionCounts.MatchId,
		_UserPreviousRoleSessionCounts.UserId,
		_UserPreviousRoleSessionCounts.RoleId,
		_UserPreviousRoleSessionCounts.RoleSessionId,
		_UserPreviousRoleSessionCounts.PreviousSameRoleCount,
		_RoleResultsMax.Feature,
		_RoleResultsMax.Class1Max,
		_RoleResultsMax.Class2Max,
		_RoleResultsMax.Class3Max,
		_RoleResultsMax.PredictAvg,
		TIMESTAMPDIFF(SECOND, _UserPreviousRoleSessionCounts.RoleSessionCreated, _UserPreviousRoleSessionCounts.RoleSessionEnded) AS RoleSessionDuration
		
	FROM _UserPreviousRoleSessionCounts
	
	LEFT JOIN _RoleResultsMax ON _UserPreviousRoleSessionCounts.RoleSessionId = _RoleResultsMax.RoleSessionId
);

CREATE OR REPLACE TABLE _UserRoleResultsMaxTransposed
(
	SELECT 
		_UserRoleResultsMax.UserId,
		_UserRoleResultsMax.RoleId,
		_UserRoleResultsMax.Feature,
		GROUP_CONCAT(_UserRoleResultsMax.PreviousSameRoleCount ORDER BY _UserRoleResultsMax.PreviousSameRoleCount) AS SameRoleCounts,
		GROUP_CONCAT(_UserRoleResultsMax.Class1Max ORDER BY _UserRoleResultsMax.PreviousSameRoleCount) AS Class1Maxs,
		GROUP_CONCAT(_UserRoleResultsMax.Class2Max ORDER BY _UserRoleResultsMax.PreviousSameRoleCount) AS Class2Maxs,
		GROUP_CONCAT(_UserRoleResultsMax.Class3Max ORDER BY _UserRoleResultsMax.PreviousSameRoleCount) AS Class3Maxs,
		GROUP_CONCAT(_UserRoleResultsMax.PredictAvg ORDER BY _UserRoleResultsMax.PreviousSameRoleCount) AS PredictAvgs,
		GROUP_CONCAT(_UserRoleResultsMax.RoleSessionDuration ORDER BY _UserRoleResultsMax.PreviousSameRoleCount) AS RoleSessionDurations
		
	FROM _UserRoleResultsMax
	
	GROUP BY _UserRoleResultsMax.UserId, _UserRoleResultsMax.RoleId, _UserRoleResultsMax.Feature
);