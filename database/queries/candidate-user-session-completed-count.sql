SELECT 
auths.AuthUsrId,
COUNT(auths.AuthUsrId)
FROM rolssns 
LEFT JOIN usrssn on rolssns.UserSessionId=usrssn.Id
LEFT JOIN auths on usrssn.UserId=auths.UserId
WHERE rolssns.RoleId = "candidate" AND rolssns.Created IS NOT NULL AND rolssns.Ended IS NOT NULL
GROUP BY auths.AuthUsrId