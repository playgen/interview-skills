CREATE OR REPLACE TABLE _UserRoleSessions 
(
	SELECT 		
		auths.AuthUsrId AS UserEmail,
		auths.UserId AS UserId,
		usrssn.Id AS UserSessionId,
		mtchssnusrmpns.MatchSessionId AS MatchId,
		rolssns.Created AS RoleSessionCreated,
		rolssns.Ended AS RoleSessionEnded,
		rolssns.RoleId AS RoleId,
		rolssns.Id AS RoleSessionId
	FROM auths
	INNER JOIN usrssn ON auths.UserId = usrssn.UserId
	JOIN mtchssnusrmpns ON usrssn.Id = mtchssnusrmpns.UserSessionId 
	INNER JOIN rolssns ON mtchssnusrmpns.MatchSessionId = rolssns.MatchSessionId
		AND usrssn.Id = rolssns.UserSessionId
);

CREATE OR REPLACE TABLE _UserPreviousRoleSessionCounts
(
	SELECT 		
		UserRoleSessions.UserEmail,
		UserRoleSessions.UserId,
		UserRoleSessions.MatchId,
		UserRoleSessions.UserSessionId,
		UserRoleSessions.RoleId,
		UserRoleSessions.RoleSessionCreated,
		UserRoleSessions.RoleSessionEnded,
		UserRoleSessions.RoleSessionId,
		SUM(CASE WHEN PreviousUserRoleSessions.RoleSessionCreated < UserRoleSessions.RoleSessionCreated THEN 1 ELSE 0 END) AS PreviousSameRoleCount
	FROM _UserRoleSessions As UserRoleSessions
	
	LEFT JOIN _UserRoleSessions AS PreviousUserRoleSessions ON UserRoleSessions.UserId = PreviousUserRoleSessions.UserId
		AND UserRoleSessions.RoleId = PreviousUserRoleSessions.RoleId
	
	GROUP BY UserRoleSessions.RoleSessionId
);

CREATE OR REPLACE TABLE _UserAnalysisResults
(
	SELECT 		
		UserRoleSessions.UserEmail,
		UserRoleSessions.UserId,
		UserRoleSessions.MatchId,
		UserRoleSessions.RoleId,
		UserRoleSessions.RoleSessionCreated,
		UserRoleSessions.RoleSessionEnded,
		UserRoleSessions.RoleSessionId,
		UserRoleSessions.UserSessionId,
		archvanlysrslts.FtrId AS Feature,
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class1prob":)(.*?)(?=,)') AS DOUBLE) AS Classifier1,
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class2prob":)(.*?)(?=,)') AS DOUBLE) AS Classifier2,
		CAST(NULLIF(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class3prob":)(.*?)(?=,)'), '') AS DOUBLE) AS Classifier3,		
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Prediction":)(.*?)(?=})') AS DOUBLE)  AS Prediction
	FROM _UserRoleSessions As UserRoleSessions
	
	LEFT JOIN archvanlysmtadtas ON UserRoleSessions.MatchId = archvanlysmtadtas.MatchSessionId
		AND UserRoleSessions.UserSessionId = archvanlysmtadtas.UserSessionId
		AND UserRoleSessions.RoleSessionCreated <= archvanlysmtadtas.ArchiveStarted
		AND archvanlysmtadtas.ArchiveStarted < UserRoleSessions.RoleSessionEnded
		
	LEFT JOIN archvanlysrslts ON archvanlysmtadtas.Id = archvanlysrslts.ArchvAnlysMtadtaId
);

CREATE OR REPLACE TABLE _RoleResultsGrouped
(
	SELECT 
		UserAnalysisResults.RoleSessionId,
		UserAnalysisResults.Feature,		
		_UserPreviousRoleSessionCounts.PreviousSameRoleCount,
		GROUP_CONCAT(UserAnalysisResults.Classifier1 ORDER BY UserAnalysisResults.Classifier1) AS Classifier1s,
		GROUP_CONCAT(UserAnalysisResults.Classifier2 ORDER BY UserAnalysisResults.Classifier2) AS Classifier2s,
		GROUP_CONCAT(UserAnalysisResults.Classifier3 ORDER BY UserAnalysisResults.Classifier3) AS Classifier3s,
		GROUP_CONCAT(UserAnalysisResults.Prediction ORDER BY UserAnalysisResults.Prediction) AS Predictions
		
	FROM _UserAnalysisResults AS UserAnalysisResults
	INNER JOIN _UserPreviousRoleSessionCounts ON UserAnalysisResults.RoleSessionId = _UserPreviousRoleSessionCounts.RoleSessionId
	
	GROUP BY UserAnalysisResults.RoleSessionId, UserAnalysisResults.Feature, _UserPreviousRoleSessionCounts.PreviousSameRoleCount
);


CREATE OR REPLACE TABLE _RoleResultsCount
(
	SELECT 
		RoleResultsGrouped.RoleSessionId,
		RoleResultsGrouped.Feature,		
		RoleResultsGrouped.PreviousSameRoleCount,
		RoleResultsGrouped.Classifier1s,
		(LENGTH(RoleResultsGrouped.Classifier1s) - LENGTH(REPLACE(RoleResultsGrouped.Classifier1s, ',', ''))) + 1 AS Classifier1Count,
		RoleResultsGrouped.Classifier2s,
		(LENGTH(RoleResultsGrouped.Classifier2s) - LENGTH(REPLACE(RoleResultsGrouped.Classifier2s, ',', ''))) + 1 AS Classifier2Count,
		RoleResultsGrouped.Classifier3s,
		(LENGTH(RoleResultsGrouped.Classifier3s) - LENGTH(REPLACE(RoleResultsGrouped.Classifier3s, ',', ''))) + 1 AS Classifier3Count,
		RoleResultsGrouped.Predictions,
		(LENGTH(RoleResultsGrouped.Predictions) - LENGTH(REPLACE(RoleResultsGrouped.Predictions, ',', ''))) + 1 AS PredictionCount
		
	FROM _RoleResultsGrouped AS RoleResultsGrouped
);

CREATE OR REPLACE TABLE _UserRoleResultsMedian
(
	SELECT 
		_UserPreviousRoleSessionCounts.MatchId,
		_UserPreviousRoleSessionCounts.UserId,
		_UserPreviousRoleSessionCounts.RoleId,
		_UserPreviousRoleSessionCounts.RoleSessionId,
		_UserPreviousRoleSessionCounts.PreviousSameRoleCount,
		RoleResultsCount.Feature,
		SUBSTRING_INDEX(SUBSTRING_INDEX(RoleResultsCount.Classifier1s, ',', RoleResultsCount.Classifier1Count/2), ',', -1) AS Class1Median,
		SUBSTRING_INDEX(SUBSTRING_INDEX(RoleResultsCount.Classifier2s, ',', RoleResultsCount.Classifier2Count/2), ',', -1) AS Class2Median,
		SUBSTRING_INDEX(SUBSTRING_INDEX(RoleResultsCount.Classifier3s, ',', RoleResultsCount.Classifier3Count/2), ',', -1) AS Class3Median,
		SUBSTRING_INDEX(SUBSTRING_INDEX(RoleResultsCount.Predictions, ',', RoleResultsCount.PredictionCount/2), ',', -1) AS PredictMedian,
		TIMESTAMPDIFF(SECOND, _UserPreviousRoleSessionCounts.RoleSessionCreated, _UserPreviousRoleSessionCounts.RoleSessionEnded) AS RoleSessionDuration
		
	FROM _UserPreviousRoleSessionCounts	
	LEFT JOIN _RoleResultsCount AS RoleResultsCount ON _UserPreviousRoleSessionCounts.RoleSessionId = RoleResultsCount.RoleSessionId
);

CREATE OR REPLACE TABLE _UserRoleResultsMedianTransposed
(
	SELECT 
		_UserRoleResultsMedian.UserId,
		_UserRoleResultsMedian.RoleId,
		_UserRoleResultsMedian.Feature,
		GROUP_CONCAT(_UserRoleResultsMedian.PreviousSameRoleCount ORDER BY _UserRoleResultsMedian.PreviousSameRoleCount) AS SameRoleCounts,
		GROUP_CONCAT(_UserRoleResultsMedian.Class1Median ORDER BY _UserRoleResultsMedian.PreviousSameRoleCount) AS Class1Median,
		GROUP_CONCAT(_UserRoleResultsMedian.Class2Median ORDER BY _UserRoleResultsMedian.PreviousSameRoleCount) AS Class2Median,
		GROUP_CONCAT(_UserRoleResultsMedian.Class3Median ORDER BY _UserRoleResultsMedian.PreviousSameRoleCount) AS Class3Median,
		GROUP_CONCAT(_UserRoleResultsMedian.PredictMedian ORDER BY _UserRoleResultsMedian.PreviousSameRoleCount) AS PredictMedian,
		GROUP_CONCAT(_UserRoleResultsMedian.RoleSessionDuration ORDER BY _UserRoleResultsMedian.PreviousSameRoleCount) AS RoleSessionDurations
		
	FROM _UserRoleResultsMedian
	
	GROUP BY _UserRoleResultsMedian.UserId, _UserRoleResultsMedian.RoleId, _UserRoleResultsMedian.Feature
);
