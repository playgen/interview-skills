CREATE OR REPLACE TABLE _UserRoleSessions 
(
	SELECT 		
		auths.AuthUsrId AS UserEmail,
		auths.UserId AS UserId,
		usrssn.Id AS UserSessionId,
		mtchssnusrmpns.MatchSessionId AS MatchId,
		rolssns.Created AS RoleSessionCreated,
		rolssns.Ended AS RoleSessionEnded,
		rolssns.RoleId AS RoleId,
		rolssns.Id AS RoleSessionId
	FROM auths
	INNER JOIN usrssn ON auths.UserId = usrssn.UserId
	JOIN mtchssnusrmpns ON usrssn.Id = mtchssnusrmpns.UserSessionId 
	INNER JOIN rolssns ON mtchssnusrmpns.MatchSessionId = rolssns.MatchSessionId
		AND usrssn.Id = rolssns.UserSessionId
);

CREATE OR REPLACE TABLE _UserPreviousRoleSessionCounts
(
	SELECT 		
		UserRoleSessions.UserEmail,
		UserRoleSessions.UserId,
		UserRoleSessions.MatchId,
		UserRoleSessions.UserSessionId,
		UserRoleSessions.RoleId,
		UserRoleSessions.RoleSessionCreated,
		UserRoleSessions.RoleSessionEnded,
		UserRoleSessions.RoleSessionId,
		SUM(CASE WHEN PreviousUserRoleSessions.RoleSessionCreated < UserRoleSessions.RoleSessionCreated THEN 1 ELSE 0 END) AS PreviousSameRoleCount
	FROM _UserRoleSessions As UserRoleSessions
	
	LEFT JOIN _UserRoleSessions AS PreviousUserRoleSessions ON UserRoleSessions.UserId = PreviousUserRoleSessions.UserId
		AND UserRoleSessions.RoleId = PreviousUserRoleSessions.RoleId
	
	GROUP BY UserRoleSessions.RoleSessionId
);

CREATE OR REPLACE TABLE _UserAnalysisResults
(
	SELECT 		
		UserRoleSessions.UserEmail,
		UserRoleSessions.UserId,
		UserRoleSessions.MatchId,
		UserRoleSessions.RoleId,
		UserRoleSessions.RoleSessionCreated,
		UserRoleSessions.RoleSessionEnded,
		UserRoleSessions.RoleSessionId,
		UserRoleSessions.UserSessionId,
		archvanlysmtadtas.ArchiveStarted,
		archvanlysrslts.FtrId AS Feature,
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class1prob":)(.*?)(?=,)') AS DOUBLE) AS Classifier1,
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class2prob":)(.*?)(?=,)') AS DOUBLE) AS Classifier2,
		CAST(NULLIF(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Class3prob":)(.*?)(?=,)'), '') AS DOUBLE) AS Classifier3,		
		CAST(REGEXP_SUBSTR(archvanlysrslts.SerializedClassifierProbabilities, '(?<=Prediction":)(.*?)(?=})') AS DOUBLE)  AS Prediction
	FROM _UserRoleSessions As UserRoleSessions
	
	LEFT JOIN archvanlysmtadtas ON UserRoleSessions.MatchId = archvanlysmtadtas.MatchSessionId
		AND UserRoleSessions.UserSessionId = archvanlysmtadtas.UserSessionId
		AND UserRoleSessions.RoleSessionCreated <= archvanlysmtadtas.ArchiveStarted
		AND archvanlysmtadtas.ArchiveStarted < UserRoleSessions.RoleSessionEnded
		
	LEFT JOIN archvanlysrslts ON archvanlysmtadtas.Id = archvanlysrslts.ArchvAnlysMtadtaId
);

CREATE OR REPLACE TABLE _UserAnalysisResultsAndPrevious
(
	SELECT 
		UserAnalysisResults.UserId,
		UserAnalysisResults.MatchId,
		_UserPreviousRoleSessionCounts.PreviousSameRoleCount,
		UserAnalysisResults.RoleId,
		UserAnalysisResults.RoleSessionCreated,
		UserAnalysisResults.RoleSessionEnded,
		UserAnalysisResults.RoleSessionId,
		UserAnalysisResults.UserSessionId,		
		UserAnalysisResults.ArchiveStarted,
		UserAnalysisResults.Feature,
		UserAnalysisResults.Classifier1,
		UserAnalysisResults.Classifier2,
		UserAnalysisResults.Classifier3,		
		UserAnalysisResults.Prediction
		
	FROM _UserAnalysisResults AS UserAnalysisResults
	LEFT JOIN _UserPreviousRoleSessionCounts ON UserAnalysisResults.RoleSessionId = _UserPreviousRoleSessionCounts.RoleSessionId
);